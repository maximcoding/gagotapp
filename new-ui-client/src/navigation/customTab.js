import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Home from '../assets/home.png';
import Profile from '../assets/profile.png';
import Category from '../assets/category.png';
import Search from '../assets/search.png';
import {colors} from '../theme/colors';

export default function CustomTab(props) {
  let currentIndex = props.state.index;
  let navigation = useNavigation();
  return (
    <View style={styles.runningRow}>
      <TouchableOpacity
        onPress={() => navigation.navigate('Home')}
        style={[currentIndex == 0 ? styles.activeItem : styles.item]}>
        <Image
          source={Home}
          style={currentIndex == 0 ? styles.activeIcon : styles.icon}
          resizeMode="contain"
        />
        <Text style={currentIndex == 0 ? styles.activeTitle : styles.title}>
          Home
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => navigation.navigate('Search')}
        style={[currentIndex == 1 ? styles.activeItem : styles.item]}>
        <Image
          source={Search}
          style={currentIndex == 1 ? styles.activeIcon : styles.icon}
          resizeMode="contain"
        />
        <Text style={currentIndex == 1 ? styles.activeTitle : styles.title}>
          Filters
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => navigation.navigate('Categories')}
        style={[currentIndex == 2 ? styles.activeItem : styles.item]}>
        <Image
          source={Category}
          style={currentIndex == 2 ? styles.activeIcon : styles.icon}
          resizeMode="contain"
        />
        <Text style={currentIndex == 2 ? styles.activeTitle : styles.title}>
          Categories
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => navigation.navigate('Settings')}
        style={[currentIndex == 3 ? styles.activeItem : styles.item]}>
        <Image
          source={Profile}
          style={currentIndex == 3 ? styles.activeIcon : styles.icon}
          resizeMode="contain"
        />
        <Text style={currentIndex == 3 ? styles.activeTitle : styles.title}>
          Profile
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  runningRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 50,
    width: '100%',
    paddingHorizontal: 20,
    backgroundColor: 'white',
    shadowColor: '#00000029',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: Platform.OS == 'ios' ? 10.25 : 0.25,
    shadowRadius: 3.84,
    elevation: 15,
  },
  title: {
    fontSize: 12,
    color: colors.textColor,
  },
  activeTitle: {
    fontSize: 12,
    color: colors.primary,
  },
  icon: {
    height: 16,
    width: 16,
    tintColor: colors.textColor,
  },
  activeIcon: {
    height: 16,
    width: 16,
    tintColor: colors.primary,
  },
  item: {
    alignItems: 'center',
  },
  activeItem: {
    alignItems: 'center',
  },
});
