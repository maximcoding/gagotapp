import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import CustomTab from './customTab';
import AddListing from '../modules/addListing';
import Home from '../modules/Feed';
import Filters from '../modules/filters';
import Details from '../modules/details';
import Calender from '../modules/calender';
import Search from '../modules/searchScreen';
import Categories from '../modules/categories';
import Settings from '../modules/settings';
import Profile from '../modules/profile';
import EditProfile from '../modules/editProfile';
import AppSettings from '../modules/appSettings';
import Messages from '../modules/messages';
import Lists from '../modules/lists';
import Visits from '../modules/visits';
import Payment from '../modules/payment';
import Contact from '../modules/contact';
import Map from '../modules/searchMap';
import Login from '../modules/auth/login';
import Signup from '../modules/auth/signup';
const Tab = createBottomTabNavigator();

const TabNavigation = () => {
  return (
    <Tab.Navigator
      tabBar={(props) => <CustomTab {...props} />}
      initialRouteName="Home">
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Search" component={Search} />
      <Tab.Screen name="Categories" component={Categories} />
      <Tab.Screen name="Settings" component={Settings} />
    </Tab.Navigator>
  );
};

const MainStack = createStackNavigator();

const App = () => {
  return (
    <MainStack.Navigator
      initialRouteName={'Signup'}
      screenOptions={{
        headerShown: false,
      }}>
      <MainStack.Screen name="Tab" component={TabNavigation} />
      <MainStack.Screen name="Details" component={Details} />
      <MainStack.Screen name="Profile" component={Profile} />
      <MainStack.Screen name="EditProfile" component={EditProfile} />
      <MainStack.Screen name="AppSettings" component={AppSettings} />
      <MainStack.Screen name="Messages" component={Messages} />
      <MainStack.Screen name="Lists" component={Lists} />
      <MainStack.Screen name="Visits" component={Visits} />
      <MainStack.Screen name="Payment" component={Payment} />
      <MainStack.Screen name="Contact" component={Contact} />
      <MainStack.Screen name="AddListing" component={AddListing} />
      <MainStack.Screen name="Filters" component={Filters} />
      <MainStack.Screen name="Login" component={Login} />
      <MainStack.Screen name="Signup" component={Signup} />
      <MainStack.Screen name="Map" component={Map} />
      <MainStack.Screen name="Calender" component={Calender} />
    </MainStack.Navigator>
  );
};

export default App;
