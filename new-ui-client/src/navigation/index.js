import React, {memo} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import Tab from './tabNavigator';

const AppStack = createStackNavigator();

const AppNavigator = () => {
  return (
    <>
      <NavigationContainer>
        <AppStack.Navigator
          initialRouteName={'Tab'}
          screenOptions={{
            headerShown: false,
          }}>
          <AppStack.Screen name="Tab" component={Tab} />
        </AppStack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default memo(AppNavigator);
