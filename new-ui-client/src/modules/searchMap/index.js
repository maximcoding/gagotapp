import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  Platform,
} from 'react-native';
import LocationView from 'react-native-location-view';
import Close from '../../assets/cancel.svg';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';

export default SelectLocationModal = ({navigation}) => {
  const onDoneFunc = (location) => {};

  const onCancelFunc = () => {
    navigation.goBack();
  };
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: colors.white}}>
      <LocationView
        apiKey={'AIzaSyA4JsBWizKjDWSrf-vxPvcn8WZEYVn_LUU'}
        initialLocation={{
          latitude: 37.0902,
          longitude: 95.7129,
        }}
        onLocationSelect={(value) => onDoneFunc(value)}
        markerColor={colors.primary}
        actionButtonStyle={{
          backgroundColor: colors.primary,
        }}
        actionTextStyle={{fontSize: fonts.normal.fontSize}}
      />
      <TouchableOpacity style={styles.icon} onPress={onCancelFunc}>
        <Close height={20} width={20} />
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  body: {
    width: '100%',
    height: '100%',
  },
  rightButton: {
    paddingRight: 10,
  },
  topbar: {
    position: 'absolute',
    backgroundColor: 'transparent',
    width: '100%',
  },
  mapView: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: colors.secondary,
  },
  icon: {
    position: 'absolute',
    top: Platform.OS == 'ios' ? 43 : 20,
    right: 20,
  },
});
