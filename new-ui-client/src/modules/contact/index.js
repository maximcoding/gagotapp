import React, {useState, memo} from 'react';
import {View, Text, KeyboardAvoidingView} from 'react-native';
import {styles} from './style';
import Input from '../../components/input';
import Button from '../../components/button';
import {fonts} from '../../theme/fonts';
import Header from '../../components/header';

export default memo(function Contact(props) {
  return (
    <KeyboardAvoidingView style={{flex: 1}}>
      <Header title="Contact Us" back />
      <View style={styles.container}>
        <Text style={styles.desc}>
          If you have any problem regarding using this application you can
          contact us here.
        </Text>
        <View style={styles.inputContainer}>
          <Text style={[fonts.title,styles.pB]}>Complain Here</Text>
          <Input
            title={'Write your message...'}
            multiline={true}
            height={140}
          />
        </View>
        <Button title="Submit" style={styles.btn} />
      </View>
    </KeyboardAvoidingView>
  );
});
