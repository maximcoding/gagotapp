import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15,
    backgroundColor: colors.secondary,
    alignItems: 'center',
    paddingTop: 20,
  },
  desc: {
    paddingTop: 10,
    flexDirection: 'column',
    textAlign: 'center',
    lineHeight: 22,
    fontSize: fonts.small.fontSize,
    opacity: 0.6,
  },
  btn: {
    marginTop: 30,
  },
  pB: {
    paddingBottom: 5,
  },
});
