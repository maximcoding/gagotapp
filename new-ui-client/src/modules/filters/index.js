import React, {useState, memo, useCallback} from 'react';
import {View, Text, ScrollView} from 'react-native';
import Input from '../../components/input';
import {fonts} from '../../theme/fonts';
import {styles} from './style';
import {widthToDp} from '../../theme/responsive';
import Tag from '../../components/tags';
import Button from '../../components/button';
import CheckBox from '../../components/checkbox';
import {categories, types, publishedTypes, includeTags, nextTo} from './data';
import RoomPicker from '../../components/roomPicker';
import ToggleText from '../../components/toggleText';
import ArrowBack from '../../assets/backward.svg';
import withLazy from '../../helpers/interactionHOC';

export default withLazy(
  memo(function Index({navigation}) {
    let [include, setInclude] = useState([]);
    let [category, setCategory] = useState([]);
    let [type, setType] = useState([]);
    let [pType, setPType] = useState([]);
    let [next, setNext] = useState([]);
    const toggleValues = useCallback((name, values, setValues) => {
      let arr = [...values];
      if (arr.includes(name)) {
        arr = arr.filter((item) => item !== name);
      } else {
        arr.push(name);
      }
      setValues(arr);
    }, []);

    const goBack = () => {
      navigation.goBack();
    };
    return (
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        <View style={styles.header}>
          <View style={{width: 50}}>
            <ArrowBack />
          </View>
          <Text style={fonts.screenTitle}>Filters</Text>
          <Text style={fonts.title} onPress={goBack}>
            Cancel
          </Text>
        </View>
        <View style={styles.subContainer}>
          <View style={styles.padd}>
            <View style={styles.padTop}>
              <View style={styles.rowJustify}>
                <Text style={fonts.title}>Category:</Text>
                <Text style={fonts.radioText}>Choose all</Text>
              </View>
              <View style={styles.wrapRow}>
                {categories.map((cat, index) => {
                  return (
                    <CheckBox
                      title={cat}
                      on={category.includes(cat)}
                      toggle={() => toggleValues(cat, category, setCategory)}
                      style={{marginLeft: index == 0 ? 0 : 10}}
                    />
                  );
                })}
              </View>
            </View>
            <View style={styles.padTop}>
              <View style={styles.rowJustify}>
                <Text style={fonts.title}>Type:</Text>
                <Text style={fonts.radioText}>Choose all</Text>
              </View>
              <View style={styles.wrapRow}>
                {types.map((cat, index) => {
                  return (
                    <CheckBox
                      title={cat}
                      on={type.includes(cat)}
                      toggle={() => toggleValues(cat, type, setType)}
                      style={{marginLeft: index == 0 ? 0 : 10}}
                    />
                  );
                })}
              </View>
            </View>
            <View>
              <Text style={[fonts.title, styles.padTop]}>Price:</Text>
              <View style={styles.rowJustify}>
                <View>
                  <Text style={{...fonts.radioText, paddingBottom: 5}}>
                    Min price
                  </Text>
                  <Input width={widthToDp(40)} title="$10 0000" />
                </View>
                <View>
                  <Text style={{...fonts.radioText, paddingBottom: 5}}>
                    Max price
                  </Text>
                  <Input width={widthToDp(40)} title="$100 0000" />
                </View>
              </View>
            </View>
            <View>
              <Text style={[fonts.title, styles.padTop]}>Square:</Text>
              <View style={styles.rowJustify}>
                <View>
                  <Text style={{...fonts.radioText, paddingBottom: 5}}>
                    Min sqm
                  </Text>
                  <Input width={widthToDp(40)} title="1" />
                </View>
                <View>
                  <Text style={{...fonts.radioText, paddingBottom: 5}}>
                    Max sqm
                  </Text>
                  <Input width={widthToDp(40)} title="50" />
                </View>
              </View>
            </View>
            <View>
              <Text style={[fonts.title, styles.padTop]}>Bathrooms:</Text>
              <RoomPicker />
            </View>
            <View>
              <Text style={[fonts.title, styles.padTop]}>Bedrooms:</Text>
              <RoomPicker />
            </View>
            <View style={[styles.wrapRow, styles.padTop]}>
              <Text style={fonts.title}>Published by:</Text>
              {publishedTypes.map((item) => {
                return (
                  <CheckBox
                    title={item}
                    on={pType.includes(item)}
                    toggle={() => toggleValues(item, pType, setPType)}
                    style={{marginLeft: 10}}
                  />
                );
              })}
            </View>
            <View style={styles.padTop}>
              <ToggleText title="Open for visits only" />
            </View>
            <View style={styles.padTop}>
              <ToggleText title="New Construction" />
            </View>
            <View style={[styles.rowJustify, styles.padTop]}>
              <Text style={fonts.title}>Published from date</Text>
              <Text style={fonts.radioText}>07.02.2020</Text>
            </View>
            <View>
              <Text style={[fonts.title, styles.padTop]}>Floor</Text>
              <View style={styles.rowJustify}>
                <Input width={widthToDp(30)} title="0" />
                <Input width={widthToDp(30)} title="3" />
              </View>
            </View>
          </View>
          <View style={[styles.wrapRow, styles.pR]}>
            <Text style={[fonts.title, styles.pL]}>Include :</Text>
            {includeTags.map((item) => {
              return (
                <Tag
                  title={item}
                  active={include.includes(item)}
                  toggle={() => toggleValues(item, include, setInclude)}
                />
              );
            })}
          </View>
          <View style={[styles.wrapRow, styles.pR]}>
            <Text style={[fonts.title, styles.pL]}>Next to :</Text>
            {nextTo.map((item) => {
              return (
                <Tag
                  title={item}
                  active={next.includes(item)}
                  toggle={() => toggleValues(item, next, setNext)}
                />
              );
            })}
          </View>
          <View style={styles.padd}>
            <View style={styles.padTop}>
              <ToggleText title="Save Filter" />
            </View>
          </View>
          <Button title="Filter" style={styles.halfBottom} />
        </View>
      </ScrollView>
    );
  }),
);
