import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../theme/colors';
import {heightToDp, widthToDp} from '../../theme/responsive';

export const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: colors.white,
  },
  subContainer: {
    backgroundColor: colors.secondary,
    flex: 0.9,
    borderRadius: 20,
    marginBottom: 100,
    paddingBottom: 70,
  },
  header: {
    width: widthToDp(100),
    height: heightToDp(6),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 10,
  },
  rowJustify: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  inputContainer: {
    paddingBottom: 20,
  },
  padLeft: {
    paddingLeft: widthToDp(4),
  },
  padTop: {
    paddingVertical: 10,
  },
  wrapRow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingTop: 10,
    alignItems: 'center',
  },
  halfBottom: {
    position: 'absolute',
    bottom: -25,
    left: 20,
  },
  padd: {
    padding: 20,
  },
  pL: {
    paddingLeft: 20,
  },
  pR: {
    paddingRight: 20,
  },
});
