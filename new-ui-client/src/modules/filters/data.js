export const categories = ['Offices', 'Clinics', 'Community Center'];

export const types = ['Buy', 'Sell'];


export const publishedTypes = ['Private', 'Agent'];



export const includeTags = [
    'Basement',
    'Air Conditioning',
    'Balcone',
    'Fast Internet',
    'Garage',
    'Firespace',
    'Parking',
    'Pool',
    'Sauna',
    'Security Cameras',
    'Smart Control',
    'Water Heating',
  ];
  
  export const nextTo = [
    'Subway',
    'Shop Mall',
    'Garden',
    'School',
    'Dog Park',
    'Sea',
    'Theater',
  ];
