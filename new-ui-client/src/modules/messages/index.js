import React, {memo} from 'react';
import {View, Text, FlatList, ActivityIndicator} from 'react-native';
import {styles} from './style';
import Item from './item';
import {colors} from '../../theme/colors';
import Header from '../../components/header';
import {data} from './data';
import withLazy from '../../helpers/interactionHOC';

export default withLazy(
  memo(function MessagesScreen({navigation}) {
    const _renderItem = ({item, index}) => {
      return <Item item={item} index={index} />;
    };
    return (
      <>
        <Header title="Messages" back={true} />
        <View style={styles.mainContainer}>
          {false ? (
            <View style={styles.loaderC}>
              <ActivityIndicator size="small" color="green" />
            </View>
          ) : (
            <FlatList
              data={data}
              renderItem={_renderItem}
              contentContainerStyle={styles.container}
              keyExtractor={(item, index) => String(index)}
              onEndReachedThreshold={0.5}
              ListEmptyComponent={() => {
                return (
                  <View style={styles.empty}>
                    <Text style={{color: colors.gray}}>No reviews yet</Text>
                  </View>
                );
              }}
            />
          )}
        </View>
      </>
    );
  }),
);
