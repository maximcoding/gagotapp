import React, {memo} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {Neomorph} from 'react-native-neomorph-shadows';
import {styles} from './style';
import moment from 'moment';

const Item = ({item, index}) => {
  return (
    <TouchableOpacity key={index}>
      <Neomorph swapShadows style={styles.row}>
        <View style={styles.subRow}>
          <Image source={{uri: item.image}} style={styles.image} />
          <View style={{alignItems: 'flex-start', paddingLeft: 10}}>
            <Text>
              {item.firstName}
              {item.lastName}
            </Text>
            <Text style={styles.content} numberOfLines={3}>
              {item.content}
            </Text>
          </View>
        </View>
        <Text style={styles.time}>
          {moment(item.createdAt).fromNow(true)} ago
        </Text>
      </Neomorph>
    </TouchableOpacity>
  );
};

export default memo(Item);
