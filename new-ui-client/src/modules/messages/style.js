import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';
import {widthToDp} from '../../theme/responsive';

export const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: colors.secondary,
    alignItems: 'center',
  },
  empty: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  mainContainer: {
    flex: 1,
    backgroundColor: colors.secondary,
  },
  loaderC: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 5,
    shadowRadius: 5,
    marginVertical: 10,
    backgroundColor: colors.secondary,
    height: 80,
    width: widthToDp(90),
  },
  image: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
  },
  subRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  content: {
    fontSize: fonts.small.fontSize,
    color: colors.gray,
  },
  time: {
    fontSize: fonts.xs.fontSize,
    color: colors.darkSecondary,
    position: 'absolute',
    bottom: 5,
    right: 10,
  },
});
