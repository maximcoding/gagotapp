import React, {memo} from 'react';
import {TouchableOpacity, Text} from 'react-native';
import {Neomorph} from 'react-native-neomorph-shadows';
import FastImage from 'react-native-fast-image';
import {fonts} from '../../theme/fonts';
import {styles} from './style';

const Item = memo(({item, index}) => {
  return (
    <Neomorph swapShadows style={styles.itemContainer} key={index}>
      <FastImage style={styles.itemPhoto} source={{uri: item.photo}} />
      <Text
        ellipsizeMode="tail"
        numberOfLines={1}
        style={[fonts.small, styles.title]}>
        {item.title}
      </Text>
      <Text
        ellipsizeMode="tail"
        numberOfLines={1}
        style={[fonts.placeholder, styles.place]}>
        {item.place}
      </Text>
    </Neomorph>
  );
});

const CategoryItem = memo(({item, index}) => {
  return (
    <TouchableOpacity key={index}>
      <Neomorph swapShadows style={styles.categoryItem}>
        <FastImage
          style={styles.categoryItemPhoto}
          source={{uri: item.photo}}
        />
        <Text style={styles.categoryItemTitle} numberOfLines={1}>
          {item.name}
        </Text>
      </Neomorph>
    </TouchableOpacity>
  );
});

export {Item, CategoryItem};
