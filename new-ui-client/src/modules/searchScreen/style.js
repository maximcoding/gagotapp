import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';
import {heightToDp, widthToDp} from '../../theme/responsive';
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
    backgroundColor: colors.secondary,
  },
  rowContainer: {
    width: widthToDp(100),
  },
  itemContainer: {
    justifyContent: 'center',
    height: 160,
    width: widthToDp(34),
    borderRadius: 10,
    backgroundColor: colors.secondary,
    shadowRadius: 5,
    marginHorizontal: 7,
    padding: 5,
    marginVertical: 20,
  },
  itemPhoto: {
    width: '100%',
    height: 100,
    borderRadius: 5,
  },
  title: {
    maxHeight: 40,
    paddingHorizontal: 5,
    paddingVertical: 1,
    color: colors.black,
  },
  place: {paddingHorizontal: 5, paddingVertical: 1},

  categoryItem: {
    height: 110,
    width: widthToDp(30),
    backgroundColor: colors.secondary,
    borderRadius: 10,
    shadowRadius: 5,
    marginHorizontal: 7,
    marginVertical: 20,
    padding: 5,
    justifyContent: 'center',
  },

  categoryItemPhoto: {
    height: 65,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    width: 110,
  },
  categoryItemTitle: {
    fontSize: fonts.small.fontSize,
    margin: 5,
  },
  searchBar: {
    height: 40,
    marginVertical: 30,
    backgroundColor: colors.secondary,
    borderRadius: 30,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 5,
    width: widthToDp(90),
    shadowRadius: 5,
  },
  search: {
    height: 40,
    alignItems: 'center',
    padding: 10,
  },
  searchLoader: {
    position: 'absolute',
    right: 10,
    top: 12,
  },
  smallIcon: {
    height: 17,
    width: 17,
    resizeMode: 'contain',
    tintColor: colors.gray,
  },
  flatList: {
    flexGrow: 1,
  },
  subContainer: {
    width: '100%',
    alignItems: 'center',
  },
});
