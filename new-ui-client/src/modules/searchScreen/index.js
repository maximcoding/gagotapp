import React, {useState, useEffect, memo} from 'react';
import {
  FlatList,
  ScrollView,
  Text,
  TextInput,
  View,
  Image,
  ActivityIndicator,
} from 'react-native';
import Header from '../../components/header';
import {Neomorph} from 'react-native-neomorph-shadows';
import {Item, CategoryItem} from './item';
import {fonts} from '../../theme/fonts';
import {styles} from './style';
import {colors} from '../../theme/colors';
import Search from '../../assets/searchIcon.png';
// data for testing
import {cat} from '../categories/data';
import {items} from './data';
import withLazy from '../../helpers/interactionHOC';

function SearchScreen() {
  let [state, setState] = useState({
    renting_data: items,
    selling_data: items,
  });
  //   let [text, setText] = useState('');
  //   let [sLoader, setSLoader] = useState(false);

  const _renderCategory = ({item, index}) => {
    return <CategoryItem item={item} index={index} />;
  };

  const _renderItem = ({item, index}) => {
    return <Item item={item} index={index} />;
  };

  return (
    <>
      <Header title="Filters" filter={true} />
      <View style={styles.container}>
        <View style={styles.subContainer}>
          <Neomorph style={styles.searchBar}>
            <TextInput placeholder={'Search here'} style={styles.search} />
            {false ? (
              <View style={styles.searchLoader}>
                <ActivityIndicator size="small" color={colors.primary} />
              </View>
            ) : (
              <Image
                source={Search}
                style={[styles.searchLoader, styles.smallIcon]}
              />
            )}
          </Neomorph>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text style={fonts.title}>Categories</Text>
          <View style={styles.categories}>
            <FlatList
              horizontal={true}
              initialNumToRender={5}
              data={cat}
              showsHorizontalScrollIndicator={false}
              renderItem={_renderCategory}
              keyExtractor={(item) => `${item.order}`}
              contentContainerStyle={styles.flatList}
            />
          </View>
          <View style={styles.rowContainer}>
            <Text style={fonts.title}>Selling</Text>
            <FlatList
              horizontal
              data={state.selling_data}
              renderItem={_renderItem}
              keyExtractor={(item) => `${item.id}`}
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={styles.flatList}
            />
          </View>
          <View style={styles.rowContainer}>
            <Text style={fonts.title}>Renting</Text>
            <View>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={state.renting_data}
                renderItem={_renderItem}
                keyExtractor={(item) => `${item.id}`}
                contentContainerStyle={styles.flatList}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

export default withLazy(memo(SearchScreen));
