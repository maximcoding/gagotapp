import React, {memo} from 'react';
import {Text, ImageBackground, FlatList, TouchableOpacity} from 'react-native';
import {Neomorph} from 'react-native-neomorph-shadows';
import {styles} from './style';
import {cat} from './data';
import Header from '../../components/header';
import withLazy from '../../helpers/interactionHOC';

export default withLazy(
  memo(function CategoriesScreen() {
    const renderListingItem = ({item, index}) => {
      return (
        <Neomorph swapShadows style={styles.imageContainer} key={index}>
          <TouchableOpacity onPress={() => {}}>
            <ImageBackground
              source={{uri: item.photo}}
              style={styles.image}
              imageStyle={styles.br}>
              <Text style={styles.title}>{item.name}</Text>
            </ImageBackground>
          </TouchableOpacity>
        </Neomorph>
      );
    };
    return (
      <>
        <Header title="Categories" />
        <FlatList
          vertical
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.container}
          data={cat}
          renderItem={renderListingItem}
          keyExtractor={(item, index) => index}
        />
      </>
    );
  }),
);
