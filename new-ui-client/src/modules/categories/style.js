import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';
import {widthToDp} from '../../theme/responsive';
export const styles = StyleSheet.create({
  imageContainer: {
    width: widthToDp(90),
    marginVertical: 10,
    height: 150,
    padding: 10,
    borderRadius: 10,
    shadowRadius: 5,
    backgroundColor: colors.secondary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: widthToDp(85),
    justifyContent: 'center',
    alignItems: 'center',
    height: 130,
  },
  br: {
    borderRadius: 5,
  },
  title: {
    fontSize: fonts.xxl.fontSize,
    color: colors.white,
    fontWeight: 'bold',
  },
  header: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    elevation: 2,
  },
  container: {
    flexGrow: 1,
    backgroundColor: colors.secondary,
    alignItems: 'center',
  },
});
