import React, {useState, memo} from 'react';
import {
  View,
  ImageBackground,
  Pressable,
  LayoutAnimation,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import Input from '../../components/input';
import Button from '../../components/button';
import {styles} from './style';
import Forward from '../../assets/forward.svg';
import Backward from '../../assets/backward.svg';
import Address from '../../assets/address.svg';
import Tag from '../../assets/tag.svg';
import Building from '../../assets/building.svg';
import House from '../../assets/house.svg';
import Toggle from '../../assets/toggle.png';
import Bed from '../../assets/bed.svg';
import Bath from '../../assets/bath.svg';
import {fonts} from '../../theme/fonts';
import Tags from '../../components/tags';
import {facilities, nextTo} from './data';
import {colors} from '../../theme/colors';
import Map from '../../components/map';
import {AgePicker} from './utils';
import {widthToDp} from '../../theme/responsive';
import withLazy from '../../helpers/interactionHOC';

let images = [
  'https://www.thebluepenguincompany.com/wp-content/uploads/2019/08/pexels-photo-106399.jpg',
  'https://i.pinimg.com/originals/1e/b6/4b/1eb64b447ed2cb60c27863097cebe62c.jpg',
  'https://kitsapmovers.com/wp-content/uploads/2015/01/house-plans-craftsman-15.jpg',
];

export default withLazy(
  memo(function PhotosDetails() {
    let [index, setIndex] = useState(0);
    const changeSliderF = () => {
      let currentIndex;
      if (index == images.length - 1) {
        currentIndex = 0;
      } else {
        currentIndex = index + 1;
      }
      LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
      setIndex(currentIndex);
    };
    const changeSliderB = () => {
      let currentIndex;
      if (index == 0) {
        currentIndex = images.length - 1;
      } else {
        currentIndex = index - 1;
      }
      LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
      setIndex(currentIndex);
    };

    return (
      <View style={styles.spaceContainer}>
        <View style={styles.imageSlider}>
          <ImageBackground
            source={{uri: images[index]}}
            style={styles.sliderImg}>
            <View style={styles.rowJustify}>
              <Pressable style={styles.wrapperIcon} onPress={changeSliderB}>
                <Backward />
              </Pressable>
              <Pressable style={styles.wrapperIcon} onPress={changeSliderF}>
                <Forward />
              </Pressable>
            </View>
          </ImageBackground>
        </View>
        <View style={[styles.rowJustify, styles.pT]}>
          <View style={styles.row}>
            <Address />
            <Text style={[fonts.addressText, styles.pL]}>
              25286 Greenholt Drive
            </Text>
          </View>
          <View style={styles.mapBtn}>
            <Text style={fonts.tabsActive}>Show on Map</Text>
          </View>
        </View>
        <View style={styles.pT} />
        <View style={styles.rowJustify}>
          <Text style={fonts.title}>Details</Text>
        </View>
        <View>
          <View style={[styles.rowJustify, styles.pT]}>
            <Text style={fonts.small}>Rent or Buy</Text>
            <Text style={fonts.small}>Buy</Text>
          </View>
          <View style={[styles.rowJustify, styles.pT]}>
            <View style={styles.row}>
              <Bed height={25} width={25} />
              <Text style={[fonts.addressText, styles.pL]}>Bedrooms</Text>
            </View>
            <Text style={fonts.small}>3</Text>
          </View>
          <View style={[styles.rowJustifyTweak, styles.pT]}>
            <View style={styles.row}>
              <Bath height={25} width={25} />
              <Text style={[fonts.addressText, styles.pL]}>Bathrooms</Text>
            </View>
            <Text style={fonts.small}>3</Text>
          </View>
          <View style={[styles.rowJustify, styles.pT]}>
            <View style={styles.row}>
              <Building height={25} width={25} />
              <Text style={[fonts.addressText, styles.pL]}>Floor</Text>
            </View>
            <Text style={fonts.small}>5</Text>
          </View>
          <View style={[styles.rowJustify, styles.pT]}>
            <View style={styles.row}>
              <House height={25} width={25} />
              <Text style={[fonts.addressText, styles.pL]}>Square</Text>
            </View>
            <Text style={fonts.small}>320 sqm</Text>
          </View>
          <View style={[styles.rowJustify, styles.pT]}>
            <View style={styles.row}>
              <Tag height={25} width={25} />
              <Text style={[fonts.addressText, styles.pL]}>Price</Text>
            </View>
            <Text style={fonts.small}>$2 000 000</Text>
          </View>
        </View>
        <View style={[styles.rowJustify, styles.pT]}>
          <Text style={fonts.title}>Facilities</Text>
        </View>
        <View style={[styles.wrapRow, styles.pR]}>
          {facilities.map((item) => {
            return (
              <Tags
                title={item}
                active={['Basement', 'Air Conditioning', 'Balcone'].includes(
                  item,
                )}
                toggle={() => {}}
              />
            );
          })}
        </View>
        <View style={[styles.rowJustify, styles.pT]}>
          <Text style={fonts.title}>Next to</Text>
        </View>
        <View style={[styles.wrapRow, styles.pR]}>
          {nextTo.map((item) => {
            return (
              <Tags
                title={item}
                active={['Subway', 'Shop Mall'].includes(item)}
                toggle={() => {}}
              />
            );
          })}
        </View>
        <View style={[styles.pH, styles.pT]}>
          <Text style={fonts.title}>Contacts</Text>
          <Text style={[fonts.small, {paddingTop: 5}]}>
            Name of the owner: Sigmund Blanda
          </Text>
          <Text style={[fonts.small, {paddingTop: 5}]}>
            Phone: 773.851.2323
          </Text>
          <Text style={[fonts.small, {paddingTop: 5}]}>
            Office: Lorem ipsum
          </Text>
          <Text style={[fonts.small, {paddingTop: 5}]}>
            Address: 1056 Willms Crest
          </Text>
        </View>
        <View style={[styles.pH, styles.pT]}>
          <Text style={fonts.title}>Current Status</Text>
        </View>
        <View style={styles.rowJustify}>
          <Text style={fonts.small}>Status:</Text>
          <View style={styles.mapBtn}>
            <Text style={fonts.tabsActive}>Open</Text>
          </View>
        </View>
        <View style={[styles.rowJustify, styles.pT]}>
          <Text style={fonts.title}>Open doors</Text>
        </View>
        <View>
          {[1, 2, 4, 5, 6].map((item, index) => {
            return (
              <View style={styles.rowJustify} key={index}>
                <Text style={fonts.small}>Monday</Text>
                <Text style={fonts.small}>20 January</Text>
                <Text style={fonts.small}>15:00</Text>
                <TouchableOpacity>
                  <Image source={Toggle} />
                </TouchableOpacity>
                <View style={[styles.mapBtn, {backgroundColor: colors.green}]}>
                  <Text style={fonts.tabsActive}>Enroll</Text>
                </View>
              </View>
            );
          })}
        </View>
        <View style={[styles.rowJustify, styles.pT]}>
          <Text style={fonts.title}>Location</Text>
        </View>
        <View style={styles.pT} />
        <View style={styles.map}>
          <Map coordinates={{latitude: 31.41667, longitude: 73.08333}} />
        </View>
        <View style={[styles.rowJustify, styles.pT]}>
          <Text style={fonts.title}>Neighborhood Details</Text>
        </View>
        <Text style={[fonts.small, styles.pH, styles.pT]}>Age:</Text>
        <View style={[styles.rowAround, styles.pT]}>
          {['0-18', '18 - 25', '25 - 45', '45 +'].map((age) => {
            return <AgePicker title={age} />;
          })}
        </View>
        <View>
          <View style={[styles.rowJustify, styles.pT]}>
            <Text style={fonts.title}>Loan Amount:</Text>
          </View>
          <View style={[styles.rowJustify, styles.pT]}>
            <Input width={widthToDp(20)} title="0" />
            <Input width={widthToDp(20)} title="10,000" />
          </View>
        </View>
        <View>
          <View style={[styles.rowJustify, styles.pT]}>
            <Text style={fonts.title}>Loan Term:</Text>
          </View>
          <View style={[styles.rowJustify, styles.pT]}>
            <Input width={widthToDp(20)} title="0" />
            <Input width={widthToDp(20)} title="30" />
          </View>
        </View>

        <View>
          <View style={[styles.rowJustify, styles.pT]}>
            <Text style={fonts.title}>Reviews</Text>
          </View>
          <View style={[styles.rowJustify, styles.pT]}>
            <Input
              height={100}
              multiline={true}
              numberOfLines={5}
              title="Enter your review"
            />
          </View>
          <View style={styles.rowJustify}>
            <Button title="Add" style={styles.halfBottom} />
          </View>
        </View>
      </View>
    );
  }),
);
