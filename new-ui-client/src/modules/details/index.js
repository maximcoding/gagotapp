import React, {useState, memo} from 'react';
import {View, Text, Pressable, ScrollView} from 'react-native';
import {Neomorph} from 'react-native-neomorph-shadows';
import {styles} from './style';
import Like from '../../assets/like.svg';
import Share from '../../assets/move.svg';
import Cancel from '../../assets/cancel.svg';
import {fonts} from '../../theme/fonts';
import {colors} from '../../theme/colors';
import PhotoDetails from './photosDetails';
import Videos from './videosDetails';


export default memo(function Index({navigation}) {
  let [tab, setTab] = useState('photos');
  return (
    <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
      <View style={styles.header}>
        <View style={styles.row}>
          <Pressable onPress={() => navigation.goBack()}>
            <Neomorph style={styles.wrapperIcon}>
              <Cancel />
            </Neomorph>
          </Pressable>
          <View style={[styles.transparentWrapper, {marginLeft: 15}]}></View>
        </View>
        <Text style={fonts.screenTitle}>Details</Text>
        <View style={styles.row}>
          <Neomorph style={styles.wrapperIcon}>
            <Like />
          </Neomorph>
          <Neomorph style={[styles.wrapperIcon, {marginLeft: 15}]}>
            <Share />
          </Neomorph>
        </View>
      </View>
      <View style={styles.tabs}>
        <Pressable
          style={{
            ...styles.tab,
            backgroundColor:
              tab == 'photos' ? colors.primary : colors.secondary,
          }}
          onPress={() => setTab('photos')}>
          <Text style={tab == 'photos' ? fonts.tabsActive : fonts.tabsText}>
            Photos
          </Text>
        </Pressable>
        <Pressable
          style={{
            ...styles.tab,
            backgroundColor:
              tab == 'videos' ? colors.primary : colors.secondary,
          }}
          onPress={() => setTab('videos')}>
          <Text style={tab == 'videos' ? fonts.tabsActive : fonts.tabsText}>
            Videos
          </Text>
        </Pressable>
      </View>
      {tab == 'photos' ? (
        <PhotoDetails />
      ) : (
        <Videos
          videos={[
            'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4',
            'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4',
          ]}
          item={{title: 'House Videos'}}
        />
      )}
    </ScrollView>
  );
});
