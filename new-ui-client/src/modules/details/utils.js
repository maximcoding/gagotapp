import React from 'react';
import {View, Text} from 'react-native';
import {styles} from './style';
import {Neomorph} from 'react-native-neomorph-shadows';
import {fonts} from '../../theme/fonts';

export const AgePicker = ({title, height}) => {
  return (
    <View style={styles.colCenter}>
      <Neomorph inner swapShadows style={styles.ageContainer}>
        <View style={styles.valuesAge}></View>
      </Neomorph>
      <Text style={[fonts.small, styles.pT]}>{title}</Text>
    </View>
  );
};
