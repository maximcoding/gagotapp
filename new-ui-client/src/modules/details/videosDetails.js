import React, {useState} from 'react';
import {View, StyleSheet, Dimensions, Text} from 'react-native';
import VideoViewComponent from '../../components/videoItem';
import HelpModal from '../../components/videoModal';
import {colors} from '../../theme/colors';
import {heightToDp, widthToDp} from '../../theme/responsive';

export default function Videos({videos = [], item}) {
  const [visible, setVisible] = useState(false);
  const [helpUrl, setHelpUrl] = useState(null);
  return (
    <View style={styles.container}>
      {videos.length > 0 ? (
        videos.map((url, index) => {
          return (
            <VideoViewComponent
              onPress={() => {
                setHelpUrl(url);
                setVisible(true);
              }}
              item={item}
              index={index}
            />
          );
        })
      ) : (
        <View style={styles.center}>
          <Text>No Video for this property</Text>
        </View>
      )}
      <HelpModal visible={visible} setVisible={setVisible} helpUrl={helpUrl} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: heightToDp(100),
    width: widthToDp(100),
    backgroundColor: colors.secondary,
    alignItems: 'center',
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
