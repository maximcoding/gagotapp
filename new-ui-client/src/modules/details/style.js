import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
import {heightToDp, widthToDp} from '../../theme/responsive';

export const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: colors.secondary,
  },
  header: {
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
  },
  wrapperIcon: {
    height: 32,
    width: 32,
    borderRadius: 32 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.secondary,
    shadowRadius: 5,
  },
  transparentWrapper: {
    height: 30,
    width: 30,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  tabs: {
    width: widthToDp(100),
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    backgroundColor: colors.secondary,
    shadowColor: 'gray',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
  },
  tab: {
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    borderRadius: 5,
  },
  imageSlider: {
    height: heightToDp(30),
    width: widthToDp(100),
    marginTop: 5,
  },

  sliderImg: {
    height: heightToDp(30),
    width: widthToDp(100),
    justifyContent: 'center',
  },
  rowJustify: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },

  rowJustifyTweak: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 18.5,
    paddingRight: 20,
  },
  rowAround: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingHorizontal: 30,
  },
  pL: {
    paddingLeft: 10,
  },
  pR: {
    paddingRight: 10,
  },

  mapBtn: {
    height: 30,
    paddingHorizontal: 20,
    backgroundColor: colors.primary,
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pT: {
    paddingTop: 10,
  },
  wrapRow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingTop: 10,
    alignItems: 'center',
  },
  pH: {
    paddingHorizontal: 20,
  },
  map: {
    height: 300,
    width: widthToDp(100),
  },
  ageContainer: {
    height: 130,
    width: 27,
    borderRadius: 30,
    backgroundColor: colors.secondary,
    shadowRadius: 5,
    padding: 3,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  valuesAge: {
    borderRadius: 30,
    width: 20,
    height: 70,
    backgroundColor: colors.primary,
  },
  colCenter: {
    alignItems: 'center',
  },
  halfBottom: {
    marginTop: 20,
  },

  spaceContainer: {
    flex: 1,
    paddingBottom: 100,
  },
});
