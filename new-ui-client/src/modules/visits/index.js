import React, {memo} from 'react';
import {View, Text, FlatList, ActivityIndicator} from 'react-native';
import {styles} from './style';
import Item from './item';
import {colors} from '../../theme/colors';
import Header from '../../components/header';
import withLazy from '../../helpers/interactionHOC';

export default withLazy(
  memo(function MessagesScreen({navigation}) {
    const _renderItem = ({item, index}) => {
      return <Item item={item} index={index} />;
    };
    return (
      <>
        <Header title="Visits" back={true} />
        <View style={styles.mainContainer}>
          {false ? (
            <View style={styles.loaderC}>
              <ActivityIndicator size="small" color="green" />
            </View>
          ) : (
            <FlatList
              data={[1, 1, 1, 1, 1]}
              renderItem={_renderItem}
              contentContainerStyle={styles.container}
              keyExtractor={(item, index) => String(index)}
              onEndReachedThreshold={0.5}
              ListEmptyComponent={() => {
                return (
                  <View style={styles.empty}>
                    <Text style={{color: colors.gray}}>No Visits yet</Text>
                  </View>
                );
              }}
            />
          )}
        </View>
      </>
    );
  }),
);
