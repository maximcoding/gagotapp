import React, {memo} from 'react';
import {View, Text, Image} from 'react-native';
import {Neomorph} from 'react-native-neomorph-shadows';
import {styles} from './style';
// import moment from 'moment';
import {fonts} from '../../theme/fonts';

const Item = ({item, index}) => {
  return (
    <Neomorph swapShadows style={styles.row} key={index}>
      <View style={styles.rowInner}>
        <Image
          source={{
            uri:
              'https://images.pexels.com/photos/2581922/pexels-photo-2581922.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          }}
          style={styles.img}
        />
        <View style={styles.col}>
          <Text style={fonts.normal}>My House Scheme</Text>
          <Text style={fonts.small}>$200,0000</Text>
          <Text style={fonts.xs}>Iqbal Nagar street 1</Text>
        </View>
      </View>
      <Text style={[fonts.xs, styles.pos]}>Monday 22:00</Text>
    </Neomorph>
  );
};

export default memo(Item);
