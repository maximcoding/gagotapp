import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
import {widthToDp} from '../../theme/responsive';

export const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: colors.secondary,
    alignItems: 'center',
  },
  empty: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  mainContainer: {
    flex: 1,
    backgroundColor: colors.secondary,
  },
  loaderC: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 5,
    shadowRadius: 5,
    marginVertical: 10,
    backgroundColor: colors.secondary,
    height: 70,
    width: widthToDp(90),
  },
  rowInner: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  img: {
    height: 70,
    width: 70,
    borderRadius: 5,
  },
  col: {
    alignItems: 'flex-start',
    paddingLeft: 10,
  },
  pos: {
    position: 'absolute',
    top: 5,
    right: 10,
  },
});
