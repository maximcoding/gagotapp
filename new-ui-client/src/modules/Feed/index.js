import React, {useState, memo} from 'react';
import {TouchableOpacity, ScrollView, View} from 'react-native';
import {styles} from './style';
import Item from '../../components/listItemImg';
import ItemFrame from '../../components/listItemFrame';
import TextItem from '../../components/textItem';
import {Neomorph} from 'react-native-neomorph-shadows';
import SortModal from '../../components/sortModal';
import GridModal from '../../components/gridModal';
import withLazy from '../../helpers/interactionHOC';

import Plus from '../../assets/plus.svg';
import Sort from '../../assets/sort.svg';
import Menu from '../../assets/menu.svg';
import Grid from '../../assets/grid.svg';

export default withLazy(
  memo(function Index({navigation}) {
    let [isOpen, setIsOpen] = useState(false);
    let [isGrid, setIsGrid] = useState(false);

    const closeSort = () => {
      setIsOpen(false);
    };
    const closeGrid = () => {
      setIsGrid(false);
    };
    const openSort = () => {
      setIsOpen(true);
    };
    const openGrid = () => {
      setIsGrid(true);
    };
    return (
      <>
        <View style={styles.header}>
          <TouchableOpacity onPress={() => navigation.navigate('AddListing')}>
            <Neomorph style={styles.itemHeader}>
              <Plus />
            </Neomorph>
          </TouchableOpacity>
          <View style={styles.row}>
            <TouchableOpacity onPress={() => navigation.navigate('Map')}>
              <Neomorph style={styles.itemHeader}>
                <Grid />
              </Neomorph>
            </TouchableOpacity>
            <TouchableOpacity onPress={openSort}>
              <Neomorph style={[styles.itemHeader, styles.ml]}>
                <Sort />
              </Neomorph>
            </TouchableOpacity>
            <TouchableOpacity onPress={openGrid}>
              <Neomorph style={[styles.itemHeader, styles.ml]}>
                <Menu />
              </Neomorph>
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView
          contentContainerStyle={styles.container}
          showsVerticalScrollIndicator={false}>
          <Item navigation={navigation} />
          <Item navigation={navigation} />
          <Item navigation={navigation} />
          <ItemFrame />
          <TextItem />
        </ScrollView>
        <SortModal
          isOpen={isOpen}
          closeSort={closeSort}
          callBack={() => {}}
          sortBy={'new'}
        />
        <GridModal
          isOpen={isGrid}
          closeSort={closeGrid}
          callBack={() => {}}
          grid={'images'}
        />
      </>
    );
  }),
);
