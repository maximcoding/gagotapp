import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';

export const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: colors.secondary,
    alignItems: 'center',
    paddingBottom: 100,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    backgroundColor: colors.secondary,
    height: 60,
  },
  itemHeader: {
    height: 35,
    width: 35,
    borderRadius: 35 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    shadowRadius: 5,
    backgroundColor: colors.secondary,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  ml: {
    marginLeft: 10,
  },
});
