import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';
import {widthToDp} from '../../theme/responsive';

export const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: colors.secondary,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabContainer: {
    width: widthToDp(100),
  },
  bottomTos: {
    width: widthToDp(100),
    alignItems: 'center',
    paddingTop: 20,
  },
  tabBar: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: colors.darkSecondary,
    marginBottom: 20,
    marginHorizontal: 20,
    alignItems: 'center',
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 12,
  },
  tabItemActive: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 12,
    borderBottomWidth: 3,
    borderBottomColor: colors.primary,
  },
  tabItemTxt: {
    fontSize: fonts.normal.fontSize,
    color: colors.darkSecondary,
  },
  tabItemTxtActive: {
    fontSize: fonts.normal.fontSize,
    color: colors.primary,
  },
  selCenter: {
    alignSelf: 'center',
  },
  mB: {
    marginBottom: 10,
  },
  mainTitle: {
    paddingBottom: 20,
    color: colors.black,
    textShadowColor: colors.gray,
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10,
  },
  pt: {
    paddingTop: 10,
  },
  link: {
    color: colors.link,
  },
  logo: {
    height: 50,
    width: 50,
    resizeMode: 'contain',
    marginBottom: 20,
    alignSelf: 'center',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 5,
  },
  cA: {
    color: colors.link,
    fontSize: fonts.small.fontSize,
    paddingLeft: 5,
  },
  extraMarkup: {
    height: 50,
    width: widthToDp(80),
    marginBottom: 10,
    alignSelf: 'center',
  },
});
