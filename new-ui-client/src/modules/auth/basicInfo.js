import React, {memo} from 'react';
import Input from '../../components/input';
import PhoneInput from '../../components/phoneInput';
import {signupData, signupPhoneData} from './data';
import {View} from 'react-native';
import {styles} from './style';
import {widthToDp} from '../../theme/responsive';

export default memo(({isPhone = false}) => {
  let data = isPhone ? signupPhoneData : signupData;
  return (
    <View style={styles.selCenter}>
      {data.map((item) => {
        if (item.value == 'phone') {
          return <PhoneInput style={styles.mB} width={widthToDp(80)} />;
        }
        return (
          <Input
            title={item.placeholder}
            keyboardType={item.keyboardType}
            secure={item.secure ? true : false}
            style={styles.mB}
            width={widthToDp(80)}
          />
        );
      })}
      {isPhone && <View style={styles.extraMarkup} />}
    </View>
  );
});
