import React, {useState, memo} from 'react';
import {Text, View, Image} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Input from '../../components/input';
import PhoneInput from '../../components/phoneInput';
import Button from '../../components/button';
import {fonts} from '../../theme/fonts';
import {styles} from './style';
import {widthToDp} from '../../theme/responsive';
import TOS from '../../components/tos';
import {loginData, phoneLoginData} from './data';
import logo from '../../assets/logo.png';
import {colors} from '../../theme/colors';

const LoginScreen = ({navigation}) => {
  const [state, setState] = useState({
    email: '',
    phone: '',
  });

  const [loading, setLoading] = useState(false);
  const [isPhone, setIsPhone] = useState(false);

  const onLogin = () => {
    setLoading(true);
  };

  const phoneLogin = () => {
    setIsPhone(!isPhone);
  };
  let data = isPhone ? phoneLoginData : loginData;
  return (
    <KeyboardAwareScrollView
      contentContainerStyle={styles.container}
      keyboardShouldPersistTaps="always">
      <View>
        <Text style={[fonts.xl, styles.selCenter, styles.mainTitle]}>
          Login to Account
        </Text>
        <View style={styles.tabContainer}>
          <Image source={logo} style={styles.logo} />
          <View style={styles.selCenter}>
            {data.map((item) => {
              if (item.value == 'phone') {
                return <PhoneInput style={styles.mB} width={widthToDp(80)} />;
              }
              return (
                <Input
                  title={item.placeholder}
                  keyboardType={item.keyboardType}
                  secure={item.secure ? true : false}
                  style={styles.mB}
                  width={widthToDp(80)}
                />
              );
            })}
            {isPhone && <View style={styles.extraMarkup} />}
            <Button
              title="Sign In"
              onPress={() => {
                navigation.navigate('Tab');
              }}
              width={widthToDp(80)}
            />
          </View>
        </View>
        <View style={styles.bottomTos}>
          <Text style={[fonts.large, styles.pt]}>OR</Text>
          {isPhone ? (
            <Text
              style={[fonts.normal, styles.pt, styles.link]}
              onPress={phoneLogin}>
              Sign In with Email
            </Text>
          ) : (
            <Text
              style={[fonts.normal, styles.pt, styles.link]}
              onPress={phoneLogin}>
              Sign In with phone number
            </Text>
          )}
          <View style={styles.row}>
            <Text style={{fontSize: fonts.small.fontSize, color: colors.gray}}>
              Don't have an Account?
            </Text>
            <Text
              style={styles.cA}
              onPress={() => navigation.navigate('Signup')}>
              Create Account
            </Text>
          </View>
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
};

export default memo(LoginScreen);
