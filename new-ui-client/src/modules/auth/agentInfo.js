import React, {memo} from 'react';
import {View} from 'react-native';
import Input from '../../components/input';
import {widthToDp} from '../../theme/responsive';
import {agentData} from './data';
import {styles} from './style';

export default memo((props) => {
  return (
    <View style={styles.selCenter}>
      {agentData.map((item, index) => {
        return (
          <Input
            title={item.placeholder}
            key={index}
            style={styles.mB}
            keyboardType={item.keyboardType}
            width={widthToDp(80)}
          />
        );
      })}
    </View>
  );
});
