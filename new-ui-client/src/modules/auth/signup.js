import React, {useState, memo} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {TabView} from 'react-native-tab-view';
import ProfileSelector from '../../components/profileSelector';
import Button from '../../components/button';
import {fonts} from '../../theme/fonts';
import AgentInfo from './agentInfo';
import BasicInfo from './basicInfo';
import {styles} from './style';
import {widthToDp} from '../../theme/responsive';
import TOS from '../../components/tos';
import {colors} from '../../theme/colors';

const SignUpScreen = ({navigation}) => {
  const [state, setState] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    companyID: '',
    address: '',
  });
  const [profilePictureURL, setProfilePictureURL] = useState(null);
  const [loading, setLoading] = useState(false);
  const [index, setIndex] = useState(0);
  const [isPhone, setIsPhone] = useState(false);
  const [imageLoader, setImageLoader] = useState(false);
  const routes = [
    {key: 'basicInfo', title: 'Basic Info'},
    {key: 'agentInfo', title: 'Agent Info'},
  ];

  const onRegister = () => {
    setLoading(true);

    const userDetails = {
      firstName,
      lastName,
      email,
      password,
      companyID,
      address,
      photoURI: profilePictureURL,
    };

    // dispatch(signUpWithEmail(userDetails, () => setLoading(false)));
  };

  const phoneSignup = () => {
    setIsPhone(!isPhone);
  };

  const uploadImageFunc = (file) => {};

  const renderScene = ({route}) => {
    const {email, password, firstName, lastName, companyID, address} = state;
    switch (route.key) {
      case 'basicInfo':
        return (
          <BasicInfo
            email={email}
            password={password}
            firstName={firstName}
            lastName={lastName}
            setState={setState}
            isPhone={isPhone}
          />
        );
      case 'agentInfo':
        return (
          <AgentInfo
            companyID={companyID}
            address={address}
            setState={setState}
          />
        );
    }
  };

  const renderTabBar = (props) => {
    const activeIndex = props.navigationState.index;
    return (
      <View style={styles.tabBar}>
        {props.navigationState.routes.map((route, i) => {
          const active = activeIndex === i;
          const tabItemStyle = active ? styles.tabItemActive : styles.tabItem;
          const tabItemTxtStyle = active
            ? styles.tabItemTxtActive
            : styles.tabItemTxt;
          return (
            <TouchableOpacity
              key={route.title}
              style={tabItemStyle}
              onPress={() => setIndex(i)}>
              <View style={{alignItems: 'center'}}>
                <Text style={tabItemTxtStyle}>{route.title}</Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  return (
    <KeyboardAwareScrollView
      contentContainerStyle={styles.container}
      keyboardShouldPersistTaps="always">
      <>
        <Text style={[fonts.xl, styles.mainTitle]}>Create An Account</Text>
        <View style={styles.tabContainer}>
          <ProfileSelector
            setProfilePictureURL={uploadImageFunc}
            imageLoader={imageLoader}
          />
          <TabView
            lazy={false}
            onIndexChange={(i) => setIndex(i)}
            renderScene={renderScene}
            renderTabBar={renderTabBar}
            navigationState={{index, routes}}
          />
        </View>
        <Button
          title="Sign Up"
          onPress={() => {
            navigation.navigate('Tab');
          }}
          width={widthToDp(80)}
        />
        <View style={styles.bottomTos}>
          <Text style={[fonts.large, styles.pt]}>OR</Text>
          {isPhone ? (
            <Text
              style={[fonts.normal, styles.pt, styles.link]}
              onPress={phoneSignup}>
              Sign up with Email
            </Text>
          ) : (
            <Text
              style={[fonts.normal, styles.pt, styles.link]}
              onPress={phoneSignup}>
              Sign up with phone number
            </Text>
          )}

          <TOS />
          <View style={styles.row}>
            <Text style={{fontSize: fonts.small.fontSize, color: colors.gray}}>
              Already have an Account?
            </Text>
            <Text
              style={styles.cA}
              onPress={() => navigation.navigate('Login')}>
              Login
            </Text>
          </View>
        </View>
      </>
    </KeyboardAwareScrollView>
  );
};

export default memo(SignUpScreen);
