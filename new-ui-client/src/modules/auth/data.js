export const signupData = [
  {
    placeholder: 'Enter First Name',
    value: 'firstName',
    keyboardType: 'default',
  },
  {
    placeholder: 'Enter Last Name',
    value: 'lastName',
    keyboardType: 'default',
  },
  {
    placeholder: 'Enter Email Address',
    value: 'email',
    keyboardType: 'default',
  },
  {
    placeholder: 'Enter Password',
    value: 'password',
    keyboardType: 'default',
    secure: true,
  },
];

export const signupPhoneData = [{
    placeholder: 'Enter First Name',
    value: 'firstName',
    keyboardType: 'default',
  },
  {
    placeholder: 'Enter Last Name',
    value: 'lastName',
    keyboardType: 'default',
  },
  {
    value: 'phone',
  },
];

export const agentData = [
  {
    placeholder: 'Enter Company Id',
    value: 'companyID',
    keyboardType: 'numeric',
  },
  {
    placeholder: 'Enter Office Address',
    value: 'address',
    keyboardType: 'default',
  },

];

export const loginData = [
  {
    placeholder: 'Enter Email Address',
    value: 'email',
    keyboardType: 'default',
  },
  {
    placeholder: 'Enter Password',
    value: 'password',
    keyboardType: 'default',
    secure: true,
  },
];

export const phoneLoginData = [
  {
    value: 'phone',
  },
];
