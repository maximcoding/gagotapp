import React, {useState, memo, useCallback} from 'react';
import {View, Text, ScrollView} from 'react-native';
import Input from '../../components/input';
import {fonts} from '../../theme/fonts';
import {styles} from './style';
import Radio from '../../components/radioButton';
import Selector from '../../components/selectComponent';
import {widthToDp} from '../../theme/responsive';
import Tag from '../../components/tags';
import {includeTags, nextTo} from './data';
import Button from '../../components/button';
import withLazy from '../../helpers/interactionHOC';
import {Neomorph} from 'react-native-neomorph-shadows';
import {colors} from '../../theme/colors';

export default withLazy(
  memo(function Index({navigation}) {
    let [type, setType] = useState(false);
    let [construct, setConstruct] = useState(false);
    let [include, setInclude] = useState([]);
    let [next, setNext] = useState([]);

    const toggleType = useCallback((name) => {
      setType(name);
    }, []);
    const toggleConstruct = useCallback((name) => {
      setConstruct(name);
    }, []);

    const toggleTags = useCallback((name, values, setValues) => {
      let arr = [...values];
      if (arr.includes(name)) {
        arr = arr.filter((item) => item !== name);
      } else {
        arr.push(name);
      }
      setValues(arr);
    }, []);

    const goBack = () => {
      navigation.goBack();
    };
    return (
      <ScrollView style={styles.container}>
        <View style={styles.header}>
          <Text style={fonts.transparent}>Cancel</Text>
          <Text style={fonts.screenTitle}>Add Listing</Text>
          <Text style={fonts.title} onPress={goBack}>
            Cancel
          </Text>
        </View>
        <View style={styles.subContainer}>
          <View style={styles.padd}>
            <View style={styles.inputContainer}>
              <View style={styles.row}>
                <Text style={fonts.title}>Title</Text>
                <Text style={fonts.hintText}> (optional field)</Text>
              </View>
              <Input title="Enter your title" />
            </View>
            <View style={styles.inputContainer}>
              <View style={styles.row}>
                <Text style={fonts.title}>Description</Text>
                <Text style={fonts.hintText}> (optional field)</Text>
              </View>
              <Input
                height={100}
                multiline={true}
                numberOfLines={5}
                title="Enter your descriptioni"
              />
            </View>
            <View style={styles.inputContainer}>
              <View style={styles.row}>
                <Text style={fonts.title}>Type</Text>
                <Radio
                  title="Rent"
                  style={styles.padLeft}
                  on={type == 'rent'}
                  toggle={() => toggleType('rent')}
                />
                <Radio
                  title="Sell"
                  style={styles.padLeft}
                  on={type == 'sell'}
                  toggle={() => toggleType('sell')}
                />
              </View>
            </View>
            <View style={styles.inputContainer}>
              <Text style={[fonts.title, {paddingBottom: 10}]}>Category</Text>
              <Selector title="Category1" />
            </View>
            <View style={styles.inputContainer}>
              <View style={styles.row}>
                <Text style={fonts.title}>Number of Floors</Text>
              </View>
              <Input title="1" />
            </View>
            <View style={styles.inputContainer}>
              <View style={styles.row}>
                <Text style={fonts.title}>Phone Number</Text>
              </View>
              <Input title="+3 8(067) 000 00 00" />
            </View>
            <View style={styles.inputContainer}>
              <View style={styles.row}>
                <Text style={fonts.title}>New Construction</Text>
                <Radio
                  title="Yes"
                  style={styles.padLeft}
                  on={construct == 'yes'}
                  toggle={() => toggleConstruct('yes')}
                />
                <Radio
                  title="No"
                  style={styles.padLeft}
                  on={construct == 'no'}
                  toggle={() => toggleConstruct('no')}
                />
              </View>
            </View>

            <View style={styles.inputContainer}>
              <View style={styles.row}>
                <Text style={fonts.title}>Price</Text>
              </View>
              <View style={styles.rowJustify}>
                <Input width={widthToDp(40)} title="1" />
                <Neomorph style={styles.currency}>
                  <Text style={fonts.title}>$ Dollars</Text>
                </Neomorph>
              </View>
            </View>
          </View>
          <View style={[styles.wrapRow, styles.pR]}>
            <Text style={[fonts.title, styles.pL]}>Include :</Text>
            {includeTags.map((item) => {
              return (
                <Tag
                  title={item}
                  active={include.includes(item)}
                  toggle={() => toggleTags(item, include, setInclude)}
                />
              );
            })}
          </View>
          <View style={[styles.wrapRow, styles.pR]}>
            <Text style={[fonts.title, styles.pL]}>Next to :</Text>
            {nextTo.map((item) => {
              return (
                <Tag
                  title={item}
                  active={next.includes(item)}
                  toggle={() => toggleTags(item, next, setNext)}
                />
              );
            })}
          </View>
          <View style={[styles.wrapRow, styles.pL]}>
            <Text
              style={{...fonts.title, color: colors.primary}}
              onPress={() => navigation.navigate('Calender')}>
              Choose Open doors
            </Text>
          </View>
          <Button title="Add Listing" style={styles.halfBottom} />
        </View>
      </ScrollView>
    );
  }),
);
