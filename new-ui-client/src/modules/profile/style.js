import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
import {widthToDp} from '../../theme/responsive';

export const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: colors.secondary,
    paddingBottom: 70,
    alignItems: 'center',
  },
  TitleContainer: {
    width: widthToDp(100),
    height: 55,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
  },
  colorSection: {
    color: colors.textColor,
  },

  settingsTypeContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    height: 60,
    width: widthToDp(95),
    backgroundColor: colors.secondary,
    paddingHorizontal: 20,
    marginVertical: 7,
    shadowRadius: 5,
    borderRadius: 10,
  },

  contentContainer: {
    width: widthToDp(100),
    backgroundColor: colors.secondary,
    alignItems: 'center',
  },
});
