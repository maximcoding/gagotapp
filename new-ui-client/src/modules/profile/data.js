export const renderData = [
  {
    title: 'PUBLIC PROFILE',
    fields: [
      {
        name: 'firstName',
        displayName: 'First Name',
        placeholder: 'Your first name',
      },
      {
        name: 'lastName',
        displayName: 'Last Name',
        placeholder: 'Your last name',
      },
      {
        name: 'registration',
        displayName: 'Registration Date',
        placeholder: '',
      },
    ],
  },
  {
    title: 'PRIVATE DETAILS',
    fields: [
      {
        name: 'email',
        displayName: 'Email',
        placeholder: 'Your email',
      },
      {
        name: 'phone',
        displayName: 'Phone Number',
        placeholder: 'Your phone Number',
      },
      {
        name: 'password',
        displayName: 'Password',
        placeholder: 'Your password',
      },
    ],
  },
];

export const renderChangePassword = {
  type: 'password',
  fields: [
    {
      name: 'password',
      displayName: 'Password',
      placeholder: 'Your password',
    },
    {
      name: 'newpassword',
      displayName: 'New Password',
      placeholder: 'Your New password',
    },
  ],
};

export const renderChangeName = {
  type: 'name',
  fields: [
    {
      name: 'cfname',
      displayName: 'New First name',
      placeholder: 'Your First name',
    },
    {
      name: 'clname',
      displayName: 'New Last name',
      placeholder: 'Your Last name',
    },
  ],
};

export const renderChangeEmailOne = {
  type: 'email',
  fields: [
    {
      name: 'cemail',
      displayName: 'New Email',
      placeholder: 'Your New Email',
    },
    {
      name: 'password',
      displayName: 'Password',
      placeholder: 'Your password',
    },
  ],
};

export const renderChangeEmailTwo = {
  type: 'email',
  fields: [
    {
      name: 'cemail',
      displayName: 'New Email',
      placeholder: 'Your New Email',
    },
  ],
};

export const renderChangePhone = {
  type: 'phone',
  fields: [
    {
      name: 'cphone',
      displayName: 'New Phone Number',
      placeholder: 'Your New Phone',
    },
  ],
};
