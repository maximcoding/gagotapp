import React, {useState, useEffect, memo} from 'react';
import {View, Text, Switch, ScrollView, TouchableOpacity} from 'react-native';
import NumberFormat from 'react-number-format';
import moment from 'moment';
import Header from '../../components/header';
import {styles} from './style';
import Forward from '../../assets/forward.svg';
import {
  renderData,
  renderChangePassword,
  renderChangeName,
  renderChangeEmailOne,
  renderChangeEmailTwo,
  renderChangePhone,
} from './data';
import {fonts} from '../../theme/fonts';
import {Neomorph} from 'react-native-neomorph-shadows';
import withLazy from '../../helpers/interactionHOC';

const Profile = ({navigation}) => {
  let user = {
    firstName: 'Sohaib',
    lastName: 'Ali',
  };
  useEffect(() => {
    setForm({
      ...form,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email ? user.email : 'Enter your Email',
      phone: user.phone ? user.phone : '+923137860725',
    });
  }, []);

  const [form, setForm] = useState({
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    password: '**********',
    registration: moment(new Date()).format('MM/DD/YYYY'),
    accountActive: false,
  });

  const navigateTo = (type) => {
    let data;
    switch (type) {
      case 'firstName':
        data = renderChangeName;
        break;
      case 'lastName':
        data = renderChangeName;
        break;
      case 'email':
        data = renderChangeEmailTwo;
        break;
      case 'phone':
        data = renderChangePhone;
        break;
      case 'password':
        data = renderChangePassword;
        break;
      default:
        data = renderChangeName;
        break;
    }
    navigation.navigate('EditProfile', {data});
  };

  const renderSwitchField = () => {
    return (
      <Neomorph swapShadows style={styles.settingsTypeContainer}>
        <Text style={fonts.normal}>Disable Your Account</Text>
        <Switch
          value={form.accountActive}
          onValueChange={(value) => setForm({...form, accountActive: value})}
          style={{transform: [{scaleX: 0.8}, {scaleY: 0.8}]}}
        />
      </Neomorph>
    );
  };

  const renderTextField = (formTextField, index) => {
    return (
      <Neomorph swapShadows style={styles.settingsTypeContainer} key={index}>
        <View>
          <Text style={fonts.normal}>{formTextField.displayName}</Text>
          {formTextField.name == 'phone' ? (
            <NumberFormat
              value={form[formTextField.name]}
              renderText={(text) => (
                <Text style={[fonts.placeholder, {paddingTop: 5}]}>{text}</Text>
              )}
              format="(###) ###-######"
              displayType={'text'}
              mask="_"
            />
          ) : (
            <Text style={[fonts.placeholder, {paddingTop: 5}]}>
              {form[formTextField.name]}
            </Text>
          )}
        </View>
        {formTextField.name !== 'registration' && (
          <TouchableOpacity onPress={() => navigateTo(formTextField.name)}>
            <Forward />
          </TouchableOpacity>
        )}
      </Neomorph>
    );
  };

  const renderSection = (section, index) => {
    return (
      <View key={index}>
        <View style={styles.TitleContainer}>
          <Text style={[fonts.large, styles.colorSection]}>
            {section.title}
          </Text>
        </View>
        <View style={styles.contentContainer}>
          {section.fields.map((field, index) => {
            return renderTextField(field, index);
          })}
        </View>
      </View>
    );
  };

  return (
    <>
      <Header title="Profile" back={true} />
      <ScrollView contentContainerStyle={styles.container}>
        {renderData.map((item, index) => {
          return renderSection(item, index);
        })}
        <View style={{padding: 5}} />
        {renderSwitchField()}
      </ScrollView>
    </>
  );
};

export default withLazy(memo(Profile))
