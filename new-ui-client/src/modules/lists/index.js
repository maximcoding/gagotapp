import React, {memo} from 'react';
import {View, Text, FlatList} from 'react-native';
import {styles} from './style';
import Item from '../../components/rentSellItem';
import {colors} from '../../theme/colors';
import Header from '../../components/header';
import withLazy from '../../helpers/interactionHOC';

export default withLazy(
  memo(function MessagesScreen({route}) {
    let {title} = route.params;
    const _renderItem = ({item, index}) => {
      return <Item item={item} index={index} />;
    };
    return (
      <>
        <Header title={title} back={true} />
        <View style={styles.mainContainer}>
          <FlatList
            data={[1, 1, 1, 1, 1, 1]}
            renderItem={_renderItem}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={styles.container}
            keyExtractor={(item, index) => String(index)}
            ListEmptyComponent={() => {
              return (
                <View style={styles.empty}>
                  <Text style={{color: colors.gray}}>No Items yet</Text>
                </View>
              );
            }}
          />
        </View>
      </>
    );
  }),
);
