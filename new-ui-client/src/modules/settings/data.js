import accountDetail from '../../assets/account-detail.png';
import settings from '../../assets/settings.png';
import communication from '../../assets/communication.png';
import wishlistFilled from '../../assets/wishlist-filled.png';
import homefilled from '../../assets/home-filled.png';
import compose from '../../assets/compose.png';
import contactUs from '../../assets/contact-us.png';

export const getMenuItems = (user, navigation) => {
  return user?.type === 'admin'
    ? [
        {
          title: 'Users',
          subTitle: 'Account',
          icon: accountDetail,
          tintColor: '#6b7be8',
          onPress: () => {
            navigation.navigate('path');
          },
        },

        {
          title: 'Messages',
          icon: contactUs,
          tintColor: '#a6a4b1',
          onPress: () => {
            navigation.navigate('path');
          },
        },
      ]
    : [
        {
          title: 'Account Details',
          subTitle: 'Account',
          icon: accountDetail,
          tintColor: '#6b7be8',
          onPress: () => {
            navigation.navigate('Profile');
          },
        },

        {
          title: 'Settings',
          icon: settings,
          tintColor: '#a6a4b1',
          onPress: () => {
            navigation.navigate('AppSettings');
          },
        },

        {
          title: 'Messages',
          icon: communication,
          tintColor: '#968cbf',
          onPress: () => {
            navigation.navigate('Messages');
          },
        },
        {
          title: 'My Favorites',
          tintColor: '#df9292',
          icon: wishlistFilled,
          onPress: () => {
            navigation.navigate('Lists', {title: 'Favorites'});
          },
        },

        {
          title: 'Selling',
          icon: homefilled,
          tintColor: '#a6a4b1',
          onPress: () => {
            navigation.navigate('Lists', {title: 'Selling'});
          },
        },
        {
          title: 'Renting',
          icon: homefilled,
          tintColor: '#a6a4b1',
          onPress: () => {
            navigation.navigate('Lists', {title: 'Renting'});
          },
        },
        {
          title: 'Recent Viewed',
          icon: homefilled,
          tintColor: '#a6a4b1',
          onPress: () => {
            navigation.navigate('Lists', {title: 'Recent'});
          },
        },
        {
          title: 'Visits',
          icon: homefilled,
          tintColor: '#a6a4b1',
          onPress: () => {
            navigation.navigate('Visits');
          },
        },
        {
          title: 'Payment Option',
          icon: compose,
          tintColor: '#a6a4b1',
          onPress: () => {
            navigation.navigate('Payment');
          },
        },

        {
          title: 'Contact Us',
          icon: contactUs,
          tintColor: '#9ee19f',
          onPress: () => {
            navigation.navigate('Contact');
          },
        },
      ];
};
