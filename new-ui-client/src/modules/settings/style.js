import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';
import {widthToDp} from '../../theme/responsive';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.secondary,
  },
  imageContainer: {
    margin: 20,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    width: widthToDp(90),
    height: 120,
    backgroundColor: colors.secondary,
    shadowRadius: 5,
    borderRadius: 10,
    padding: 10,
    marginBottom: 5,
  },
  emailCol: {flexDirection: 'column', justifyContent: 'center'},
  logout: {
    width: widthToDp(90),
    height: 50,
    fontSize: fonts.large.fontSize,
    backgroundColor: colors.secondary,
    borderRadius: 10,
    marginVertical: 30,
    shadowRadius: 5,
  },
  subLogout: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
