import React, {useState, memo} from 'react';
import {View, Text, ScrollView, Pressable} from 'react-native';
import Header from '../../components/header';
import ProfileSelector from '../../components/profileSelector';
import {styles} from './style';
import {getMenuItems} from './data';
import MenuItem from '../../components/menuItem';
import {fonts} from '../../theme/fonts';
import {colors} from '../../theme/colors';
import {Neomorph} from 'react-native-neomorph-shadows';
import withLazy from '../../helpers/interactionHOC';

export default withLazy(
  memo(function index({navigation}) {
    const [imageLoader, setImageLoader] = useState(false);
    let items = getMenuItems({type: 'user'}, navigation);
    const onLogout = () => {};
    const uploadImageFunc = () => {};
    return (
      <>
        <Header title="Settings" />
        <View style={styles.container}>
          <Neomorph swapShadows style={styles.imageContainer}>
            <ProfileSelector
              setProfilePictureURL={uploadImageFunc}
              imageLoader={imageLoader}
            />
            <View style={styles.emailCol}>
              <Text style={fonts.normal}>Sohaib Ali</Text>
              <Text style={fonts.small}>sohaia213@gmail.com</Text>
            </View>
          </Neomorph>
          <ScrollView
            style={{width: '100%', marginTop: 10}}
            contentContainerStyle={{alignItems: 'center'}}
            showsVerticalScrollIndicator={false}>
            {items.map((item, i) => {
              return (
                <MenuItem
                  title={item.title}
                  icon={item.icon}
                  iconStyle={{tintColor: item.tintColor}}
                  onPress={item.onPress}
                />
              );
            })}
            <Neomorph style={styles.logout} swapShadows>
              <Pressable
                onPress={onLogout}
                style={styles.subLogout}
                android_ripple={{
                  color: colors.darkSecondary,
                  borderless: false,
                }}
                onPress={() => navigation.navigate('Login')}>
                <Text style={fonts.normal}>Logout</Text>
              </Pressable>
            </Neomorph>
          </ScrollView>
        </View>
      </>
    );
  }),
);
