import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
import {widthToDp} from '../../theme/responsive';
export const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: colors.secondary,
    alignItems: 'center',
    paddingVertical: 30,
  },
  mapView: {
    width: '100%',
    height: '100%',
  },
  filtersButton: {
    marginRight: 10,
  },
  toggleButton: {
    marginRight: 7,
  },
  arrowIcon: {height: 20, width: 20},
  rowC: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowJustify: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    height: 50,
    marginVertical: 10,
    backgroundColor: colors.secondary,
    shadowRadius: 5,
    width: widthToDp(90),
    borderRadius: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  collap: {
    width: widthToDp(100),
    backgroundColor: colors.secondary,
    alignItems: 'center',
  },
  marginX: {
    marginVertical: 10,
  },
});
