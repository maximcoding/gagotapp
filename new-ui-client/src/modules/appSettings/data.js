export const listLanguage = [
  {value: 'ar', label: '🇸🇦 Arabic'},
  {value: 'en', label: '🇺🇸 English'},
  {value: 'fr', label: '🇫🇷 French'},
  {value: 'heb', label: '🇮🇱 Hebrew'},
  {value: 'ru', label: '🇷🇺 Russian'},
];

export const notificationsData = [
  {
    title: 'New Reviews',
    value: 'newReviews',
  },
  {
    title: 'Auction Bid Changes',
    value: 'bidChanges',
  },
  {
    title: 'Email Notifications',
    value: 'emailNotifications',
  },
  {
    title: 'New Messages',
    value: 'newMessages',
  },
  {
    title: 'News And New Features',
    value: 'newFeatures',
  },
  {
    title: 'Disable All Notifications',
    value: 'disabledNotifications',
  },
];

export const currencyUnits = [
  {value: 'dollar', label: '$ Dollar'},
  {value: 'euro', label: '€ Euro '},
  {value: 'sign', label: '₪ Sign '},
];

export const listUnits = [
  {value: 'meter', label: 'Meters'},
  {value: 'feet', label: 'Feets'},
];
