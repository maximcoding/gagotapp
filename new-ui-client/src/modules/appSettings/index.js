import React, {memo, useEffect, useState} from 'react';
import {
  TouchableOpacity,
  Switch,
  Text,
  Image,
  View,
  ScrollView,
} from 'react-native';
import Header from '../../components/header';
import {styles} from './style';
import CountryPicker from 'react-native-country-picker-modal';
import Collapsible from 'react-native-collapsible';
import {Neomorph} from 'react-native-neomorph-shadows';
import ArrowDown from '../../assets/arrowDown.svg';
import Forward from '../../assets/forward.svg';
import {fonts} from '../../theme/fonts';
import Selector from '../../components/selectComponent';
import {
  listLanguage,
  notificationsData,
  currencyUnits,
  listUnits,
} from './data';
import withLazy from '../../helpers/interactionHOC';
function SettingScreen() {
  let [state, setState] = useState({
    country: 'Select',
    language: 'en',
    squareUnits: 'meter',
    newReviews: true,
    bidChanges: true,
    emailNotifications: true,
    newMessages: true,
    newFeatures: true,
    disabledNotifications: false,
    currencyUnit: 'dollar',
  });
  let [visible, setVisible] = useState(false);
  let [show, setShow] = useState(false);
  const onChangeLanguage = async (languageSelected) => {
    setState({
      ...state,
      language: languageSelected,
    });
  };
  const onChangeUnits = (value) => {
    setState({
      ...state,
      squareUnits: value,
    });
  };
  const onChangeCurrency = (value) => {
    setState({
      ...state,
      currencyUnit: value,
    });
  };

  const changeAutoDetect = () => {};

  const onSelect = (country) => {
    setState({...state, country: country.name});
  };

  const changeNotifications = (value) => {
    if (value == 'disabledNotifications' && state[value] == false) {
      setState({
        ...state,
        newReviews: false,
        bidChanges: false,
        emailNotifications: false,
        newMessages: false,
        newFeatures: false,
        disabledNotifications: true,
      });
    } else if (value == 'disabledNotifications' && state[value] == true) {
      setState({
        ...state,
        newReviews: !false,
        bidChanges: !false,
        emailNotifications: !false,
        newMessages: !false,
        newFeatures: !false,
        disabledNotifications: !true,
      });
    } else {
      setState({...state, [value]: !state[value]});
    }
  };

  return (
    <>
      <Header title="App Settings" back={true} />
      <ScrollView contentContainerStyle={styles.container}>
        <Neomorph swapShadows style={styles.rowJustify}>
          <Text style={fonts.normal}>Country or Region</Text>
          <View style={styles.row}>
            <Text>{state.country}</Text>
            <CountryPicker
              {...{
                withCountryNameButton: true,
                withFilter: true,
                withFlag: true,
                withAlphaFilter: true,
                withEmoji: true,
                withFlagButton: true,
                renderFlagButton: () => (
                  <TouchableOpacity
                    onPress={() => setVisible(true)}
                    style={{paddingLeft: 10}}>
                    <ArrowDown />
                  </TouchableOpacity>
                ),
                onClose: () => setVisible(false),
                onSelect,
              }}
              visible={visible}
            />
          </View>
        </Neomorph>
        <Neomorph swapShadows style={styles.rowJustify}>
          <View>
            <Text style={fonts.normal}>Notifications</Text>
            <Text style={fonts.placeholder}>
              Turn on to receive order updates, price.
            </Text>
          </View>
          <TouchableOpacity onPress={() => setShow(!show)}>
            {show ? <ArrowDown /> : <ArrowDown />}
          </TouchableOpacity>
        </Neomorph>
        <Collapsible collapsed={!show}>
          <View style={styles.collap}>
            {notificationsData.map((item) => {
              return (
                <Neomorph swapShadows style={styles.rowJustify}>
                  <Text style={fonts.normal}>{item.title}</Text>
                  <Switch
                    onValueChange={() => changeNotifications(item.value)}
                    thumbColor={'#ffff'}
                    trackColor={{false: '#DDDD', true: '#0073eb'}}
                    value={state[item.value]}
                  />
                </Neomorph>
              );
            })}
          </View>
        </Collapsible>
        <Neomorph swapShadows style={styles.rowJustify}>
          <Text style={fonts.normal}>Auto Detection</Text>
          <Switch
            onValueChange={changeAutoDetect}
            thumbColor={'#ffff'}
            trackColor={{false: '#DDDD', true: '#0073eb'}}
            value={true}
          />
        </Neomorph>
        <View style={{marginTop: 10}} />
        <Selector title="Translation" inner={false} />
        <View style={styles.marginX} />
        <Selector title="Square Units" inner={false} />
        <View style={styles.marginX} />
        <Selector title="Select Currency" inner={false} />
        <View style={{marginBottom: 10}} />
        <Neomorph swapShadows style={styles.rowJustify}>
          <Text style={fonts.normal}>User Agreement</Text>
          <Forward />
        </Neomorph>
        <Neomorph swapShadows style={styles.rowJustify}>
          <Text style={fonts.normal}>Privacy</Text>
          <Forward />
        </Neomorph>
        <Neomorph swapShadows style={styles.rowJustify}>
          <Text style={fonts.normal}>About</Text>
          <Forward />
        </Neomorph>
      </ScrollView>
    </>
  );
}

export default withLazy(memo(SettingScreen));
