import React, {useState} from 'react';
import {View, Text, KeyboardAvoidingView} from 'react-native';
import Header from '../../components/header';
import Input from '../../components/input';
import Button from '../../components/button';
import PhoneInput from '../../components/phoneInput';
import {styles} from './style';
import {fonts} from '../../theme/fonts';
import withLazy from '../../helpers/interactionHOC';

export default withLazy(function IMFormEditDetails({route}) {
  let {data} = route.params;
  const [state, setState] = useState({
    newpassword: '',
    cemail: '',
    cfname: '',
    clname: '',
    cphone: '',
    password: '',
  });
  const [loader, setLoader] = useState(false);

  const stopLoader = (success) => {
    setLoader(false);
    if (success) {
      resetState();
      alert('Updated Successfully');
    }
  };

  const credLoader = (cred) => {
    setLoader(false);
    if (cred) {
      setConfirmation(cred);
      setVisible(true);
    }
  };

  const resetState = () => {
    setState({
      ...state,
      cemail: '',
      cfname: '',
      clname: '',
      cphone: '',
      password: '',
      newpassword: '',
    });
  };


  return (
    <KeyboardAvoidingView style={styles.mainContainer}>
      <Header title="Edit Details" back={true} />
      <View style={styles.container}>
        <Text style={[fonts.large, styles.title]}>Change your {data.type}</Text>
        <Text style={[fonts.placeholder, styles.desc]}>
          if you want to change the {data.type} associated with your account you
          may do so below, be sure to click the{' '}
          <Text style={styles.span}>Save Changes</Text> button when your are
          done
        </Text>
        {data.fields.map((obj) => {
          return (
            <View style={styles.inputContainer}>
              <Text style={{paddingBottom: 5}}>{obj.displayName}</Text>
              {data.type == 'phone' ? (
                <PhoneInput />
              ) : (
                <Input title={'Sohaib'} />
              )}
            </View>
          );
        })}
        <Button title="Update" style={styles.mt} />
      </View>
    </KeyboardAvoidingView>
  );
});
