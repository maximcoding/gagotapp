import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    backgroundColor: colors.secondary,
    alignItems: 'center',
    paddingTop: 20,
  },
  mainContainer: {
    flex: 1,
    backgroundColor: colors.secondary,
  },
  title: {
    fontWeight: '700',
    paddingTop: 10,
    textTransform: 'capitalize',
    color: colors.textColor,
  },
  span: {
    fontWeight: '700',
  },
  desc: {
    paddingTop: 10,
    textAlign: 'justify',
    lineHeight: 22,
    color: colors.gray,
  },
  inputContainer: {
    marginTop: 20,
    alignItems: 'flex-start',
  },
  mt: {
    marginTop: 20,
  },
});
