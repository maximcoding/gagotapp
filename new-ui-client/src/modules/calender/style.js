import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
import {heightToDp, widthToDp} from '../../theme/responsive';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  subContainer: {
    backgroundColor: colors.secondary,
    padding: 20,
    borderRadius: 20,
    paddingBottom: 50,
  },
  header: {
    width: widthToDp(100),
    height: heightToDp(6),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  date: {
    alignItems: 'center',
    marginHorizontal: 5,
    minWidth: 35,
    height: 60,
    padding: 5,
  },
  dateActive: {
    alignItems: 'center',
    padding: 5,
    backgroundColor: colors.primary,
    borderRadius: 20,
    marginHorizontal: 5,
    minWidth: 35,
    height: 60,
  },
  time: {
    alignItems: 'center',
    marginHorizontal: 5,
    minWidth: 45,
    height: 40,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  timeActive: {
    alignItems: 'center',
    backgroundColor: colors.primary,
    borderRadius: 25,
    marginHorizontal: 5,
    minWidth: 45,
    height: 40,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  datesContainer: {
    height: 100,
    flexGrow: 1,
    paddingVertical: 20,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 10,
  },
});
