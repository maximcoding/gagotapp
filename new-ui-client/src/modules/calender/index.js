import React, {useEffect, memo, useState} from 'react';
import {View, Text, Pressable, FlatList} from 'react-native';
import {fonts} from '../../theme/fonts';
import {styles} from './style';
import {getDaysArray, addDays} from '../../helpers/getDates';
import moment from 'moment';
import {colors} from '../../theme/colors';
import {times} from './data';
import CheckBox from '../../components/checkbox';
import withLazy from '../../helpers/interactionHOC';

export default withLazy(
  memo(function Index({navigation}) {
    let [dates, setDates] = useState([]);
    let [date, setDate] = useState(dates[0]);
    let [from, setFrom] = useState(times[0]);
    let [to, setTo] = useState(times[0]);
    let [close, setClose] = useState(false);
    let [open, setOpen] = useState(false);

    useEffect(() => {
      handleDates();
    }, []);

    const handleDates = () => {
      let toDate = addDays(new Date(), 29);
      let arrayOfDates = getDaysArray(new Date(), toDate);
      setDates(arrayOfDates);
      setDate(arrayOfDates[0]);
    };

    const _renderDates = ({item, index}) => {
      let cond = item == date;
      return (
        <Pressable
          key={index}
          style={cond ? styles.dateActive : styles.date}
          onPress={() => setDate(item)}>
          <Text
            style={[
              fonts.title,
              {color: cond ? colors.white : colors.textColor},
            ]}>
            {moment(item).format('dddd')[0]}
          </Text>
          <Text
            style={[
              fonts.title,
              {paddingTop: 5, color: cond ? colors.white : colors.textColor},
            ]}>
            {moment(item).format('D')}
          </Text>
        </Pressable>
      );
    };

    const _renderTimesFrom = ({item, index}) => {
      let cond = item == from;
      return (
        <Pressable
          key={index}
          style={cond ? styles.timeActive : styles.time}
          onPress={() => setFrom(item)}>
          <Text
            style={[
              fonts.title,
              {color: cond ? colors.white : colors.textColor},
            ]}>
            {item}
          </Text>
        </Pressable>
      );
    };
    const _renderTimesTo = ({item, index}) => {
      let cond = item == to;
      return (
        <Pressable
          key={index}
          style={cond ? styles.timeActive : styles.time}
          onPress={() => setTo(item)}>
          <Text
            style={[
              fonts.title,
              {color: cond ? colors.white : colors.textColor},
            ]}>
            {item}
          </Text>
        </Pressable>
      );
    };

    const goBack = () => {
      navigation.goBack();
    };

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={fonts.title} onPress={goBack}>
            Cancel
          </Text>
          <Text style={fonts.screenTitle}>Select days and time</Text>
          <Text style={fonts.title}>Next</Text>
        </View>
        <View style={styles.subContainer}>
          <View>
            <Text style={fonts.title}>Select Date:</Text>
          </View>
          <FlatList
            data={dates}
            horizontal={true}
            renderItem={_renderDates}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={styles.datesContainer}
            keyExtractor={(date) => String(date)}
          />
          <View>
            <Text style={fonts.title}>Select Time From:</Text>
          </View>
          <FlatList
            data={times}
            horizontal={true}
            renderItem={_renderTimesFrom}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={styles.datesContainer}
            keyExtractor={(date) => String(date)}
          />
          <View>
            <Text style={fonts.title}>Select Time To:</Text>
          </View>
          <FlatList
            data={times}
            horizontal={true}
            renderItem={_renderTimesTo}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={styles.datesContainer}
            keyExtractor={(date) => String(date)}
          />
          <View>
            <Text style={fonts.title}>Open/Close:</Text>
            <View style={styles.row}>
              <CheckBox
                title={'Open'}
                on={open}
                toggle={() => {
                  setOpen(!open);
                }}
              />
              <CheckBox
                title={'Close'}
                on={close}
                toggle={() => {
                  setClose(!close);
                }}
                style={{marginLeft: 10}}
              />
            </View>
          </View>
        </View>
      </View>
    );
  }),
);
