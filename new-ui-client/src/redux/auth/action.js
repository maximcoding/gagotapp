import axios from 'axios';
import {
  signUpWithEmailApi,
  loginWithEmailApi,
  sendVerificationCodeApi,
  sendVerificationCodeApiExists,
  loginWithPhoneApi,
  updateUserApi,
  addComplainApi,
} from '../../api';
import * as constants from './constants';
import Toast from 'react-native-simple-toast';
import {cloudinaryImageUrl} from '../../api/cloudinary';
import {dispatchSetUserData, dispatchLogOut, updateUserData} from '../reducers';
import {Platform} from 'react-native';
import {INSTANCE} from '../../api/instance';

export const updateUser = (user, cb) => async (dispatch) => {
  try {
    const res = await updateUserApi(user, user._id);
    dispatch({
      type: constants.UPDATE_USER,
      payload: user,
    });
    cb(true);
  } catch (error) {
    cb();
    Toast.show(error.response ? error.response.data : error);
  }
};

export const verifyPhoneNumber = (code, updatedUser, cb) => async (
  dispatch,
  getState,
) => {
  try {
    let oldCode = getState().auth.otp;
    if (+code === +oldCode) {
      const res = await updateUserApi(updatedUser, updatedUser._id);
      dispatch({
        type: constants.UPDATE_USER,
        payload: updateUser,
      });
    } else {
      Toast.show('Otp is not correct');
    }
    cb(true);
  } catch (error) {
    cb();
    Toast.show(error.response ? error.response.data : error);
  }
};

export const complainUs = (complain, user, cb) => async (dispatch) => {
  try {
    let data = {
      from: `${user.firstName} ${user.lastName}`,
      userId: user._id,
      text: complain,
    };
    const res = await addComplainApi(data);
    cb(true);
  } catch (error) {
    cb();
    Toast.show(error.response ? error.response.data : error);
  }
};

export const getCurrencyPrices = () => async (dispatch) => {
  try {
    let res = await axios.get(
      'https://api.exchangeratesapi.io/latest?base=USD',
    );
    dispatch({
      type: constants.SAVE_CURRENCY,
      payload: res.data.rates,
    });
  } catch (error) {
    Toast.show(error.response ? error.response.data : error);
  }
};

export const changeView = (value) => {
  return {
    type: constants.CHANGE_GRID_VIEW,
    payload: value,
  };
};

export const changeSort = (value) => {
  return {
    type: constants.CHANGE_SORT_VIEW,
    payload: value,
  };
};

// signup with email and password

export const signUpWithEmail = (data, cb) => async (dispatch, getState) => {
  try {
    let {notificationId} = getState().auth;
    const res = await signUpWithEmailApi({
      ...data,
      notificationId,
      device: Platform.OS,
    });
    //  let token = res.headers.authorization
    dispatch({
      type: constants.LOGIN_USER,
      payload: res,
    });
    INSTANCE.defaults.headers.common[
      'Authorization'
    ] = `Bearer ${user.headers.authorization}`;
  } catch (error) {
    cb();
    Toast.show(error.response ? error.response.data : error);
  }
};
// signin with email and password

export const signinWithEmail = (data, cb) => async (dispatch, getState) => {
  try {
    let {notificationId} = getState().auth;
    const user = await loginWithEmailApi({
      ...data,
      notificationId,
      device: Platform.OS,
    });
    if (user) {
      dispatch(
        dispatchSetUserData({
          user: user.data,
          authorization: user.headers.authorization,
        }),
      );
      INSTANCE.defaults.headers.common[
        'Authorization'
      ] = `Bearer ${user.headers.authorization}`;
    } else {
      throw Error('Something went wrong');
    }
  } catch (error) {
    Toast.show(error.response ? error.response.data : error);
  } finally {
    cb();
  }
};

// Get user action
export const getUser = () => {
  return async (dispatch, getState) => {
    let token = getState().auth.authorization;
    INSTANCE.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  };
};

// Logout user action
export const logoutUser = () => {
  return async (dispatch) => {
    try {
      dispatch(dispatchLogOut());
    } catch (error) {}
  };
};

const saveCode = (code) => ({
  type: SAVE_CODE,
  payload: code,
});

export const sendOtp = (phone, cb) => async (dispatch) => {
  try {
    let code = new Date().valueOf().toString().slice(7);
    dispatch(saveCode(code));
    let res = await sendVerificationCodeApi({phone, code});
    cb(true);
  } catch (error) {
    cb(null);
    Toast.show(error.response ? error.response.data : error);
  }
};

export const sendOptExists = (phone, cb) => async (dispatch) => {
  try {
    let code = new Date().valueOf().toString().slice(7);
    dispatch(saveCode(code));
    let res = await sendVerificationCodeApiExists({phone, code});
    cb(true);
  } catch (error) {
    cb(false);
    Toast.show(error.response ? error.response.data : error);
  }
};

export const signupWithPhone = (data, newCode, cb) => async (
  dispatch,
  getState,
) => {
  try {
    let code = getState().auth.otp;
    let {notificationId} = getState().auth;
    // console.log('otp saved', code, ':new:', newCode);
    if (+code === +newCode) {
      const user = await signUpWithEmailApi({
        ...data,
        notificationId,
        device: Platform.OS,
      });

      if (user) {
        dispatch(
          dispatchSetUserData({
            user: user.data,
            authorization: user.headers.authorization,
          }),
        );
        INSTANCE.defaults.headers.common[
          'Authorization'
        ] = `Bearer ${user.headers.authorization}`;
      } else {
        throw Error('something went wrong');
      }
    } else {
      Toast.show('Otp is not correct');
    }
  } catch (error) {
    Toast.show(error.response ? error.response.data : error);
  } finally {
    cb();
  }
};

// signin with phone

export const signinWithPhone = (data, newCode, cb) => async (
  dispatch,
  getState,
) => {
  try {
    let code = getState().auth.otp;
    let {notificationId} = getState().auth;
    // console.log('otp saved', code, ':new:', notificationId);
    if (+code === +newCode) {
      const user = await loginWithPhoneApi({
        ...data,
        notificationId,
        device: Platform.OS,
      });
      if (user) {
        dispatch(
          dispatchSetUserData({
            user: user.data,
            authorization: user.headers.authorization,
          }),
        );
        INSTANCE.defaults.headers.common[
          'Authorization'
        ] = `Bearer ${user.headers.authorization}`;
      } else {
        throw Error('something went wrong');
      }
    } else {
      Toast.show('Otp is not correct!');
    }
  } catch (error) {
    cb();
    Toast.show(error.response ? error.response.data : error);
  }
};

export const updateLocation = (position) => {
  return {
    type: USER_LOCATION,
    payload: position,
  };
};

export const uploadImage = (source) => async (dispatch, getState) => {
  try {
    let {user} = getState().auth;
    if (!source) {
      dispatch(updateUser({...user, photoURI: ''}, () => {}));
      return;
    }
    let res = await cloudinaryImageUrl(source, () => {});
    let image = res.data.secure_url;
    dispatch(updateUser({...user, photoURI: image}, () => {}));
  } catch (error) {
    Toast.show(error.response ? error.response.data : error);
  }
};

export const uploadImageSignup = (source, cb) => async (dispatch) => {
  try {
    let res = await cloudinaryImageUrl(source, () => {});
    let image = res.data.secure_url;
    cb(image);
  } catch (error) {
    cb();
    Toast.show(error.response ? error.response.data : error);
  }
};

export const getNotificationId = (id) => {
  return {
    type: 'NOTIFICATION_ID',
    payload: id,
  };
};

export const changeDetect = () => {
  return {
    type: 'AUTO_DETECT_LANG',
  };
};
