export const SAVE_CURRENCY = 'SAVE_CURRENCY';
export const CHANGE_GRID_VIEW = 'CHANGE_GRID_VIEW';
export const CHANGE_SORT_VIEW = 'CHANGE_SORT_VIEW';
export const SAVE_CODE = 'SAVE_CODE';
export const USER_LOCATION = 'USER_LOCATION';
export const NOTIFICATION_ID = 'NOTIFICATION_ID';
export const AUTO_DETECT_LANG = 'AUTO_DETECT_LANG';
export const UPDATE_USER = 'UPDATE_USER'
export const LOGIN_USER = 'LOGIN_USER'
