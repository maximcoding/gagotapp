import React, {memo} from 'react';
import {
  ImageBackground,
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import {Shadow} from 'react-native-neomorph-shadows';
import Swipeable from 'react-native-swipeable';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';
import {widthToDp} from '../../theme/responsive';
import Star from '../../assets/star.svg';
import StarFill from '../../assets/fill.svg';
import Tag from '../../assets/tag.svg';
import Address from '../../assets/address.svg';
import Building from '../../assets/building.svg';
import House from '../../assets/house.svg';
import Bed from '../../assets/bed.svg';
import Time from '../../assets/time.svg';
import Heart from '../../assets/heart.svg';
import Share from '../../assets/share.svg';
import Bath from '../../assets/bath.svg';
import Elevator from '../../assets/elevator.svg';

export default memo(function Index({height = 300, width = widthToDp(95)}) {
  const rightButtons = [
    <Shadow style={{...styles.swipeable, height}}>
      <TouchableOpacity style={{...styles.swipeBtn, borderTopRightRadius: 10}}>
        <Heart />
      </TouchableOpacity>
      <TouchableOpacity
        style={{...styles.swipeBtn, borderBottomRightRadius: 10}}>
        <Share />
      </TouchableOpacity>
    </Shadow>,
  ];
  return (
    <Swipeable rightButtons={rightButtons}>
      <Shadow style={{...styles.container, height, width}}>
        <ImageBackground
          source={{
            uri:
              'https://businessday.ng/wp-content/uploads/2019/08/real-estate.png',
          }}
          style={styles.img}
          imageStyle={styles.imgBorder}>
          <View style={styles.rowJustify}>
            <View style={styles.labelText}>
              <Text style={fonts.hintText}>Land</Text>
            </View>
            <View style={styles.labelTextIcon}>
              <Tag />
              <Text style={[fonts.hintText, {paddingLeft: 5}]}>$2,000000</Text>
            </View>
          </View>
          <View style={[styles.rowJustify]}>
            <View style={styles.labelTextIcon}>
              <StarFill />
              <StarFill />
              <StarFill />
              <StarFill />
              <Star />
              <Text style={[fonts.hintText, {paddingLeft: 5}]}>4.1</Text>
            </View>
            <View style={styles.labelTextIcon}>
              <Time />
              <Text style={[fonts.hintText, {paddingLeft: 5}]}>
                1 month ago
              </Text>
            </View>
          </View>
        </ImageBackground>
        <View style={styles.details}>
          <View style={styles.textRow}>
            <Address />
            <Text style={[fonts.addressText, {paddingLeft: 5}]}>
              25286 Greenholt Drive
            </Text>
          </View>
          <View style={styles.rowAround}>
            <View style={styles.alignColumn}>
              <View style={styles.textRow}>
                <Bed height={20} width={20}/>
                <Text style={[fonts.hintText, {paddingLeft: 5}]}>
                  3 Bedrooms
                </Text>
              </View>
              <View style={[styles.textRow, {paddingTop: 12}]}>
                <Bath height={20} width={20}/>
                <Text style={[fonts.hintText, {paddingLeft: 5}]}>
                  2 Bathrooms
                </Text>
              </View>
            </View>
            <View style={styles.alignColumn}>
              <View style={styles.textRow}>
                <Building height={20} width={20}/>
                <Text style={[fonts.hintText, {paddingLeft: 5}]}>3 Floors</Text>
              </View>
              <View style={[styles.textRow, {paddingTop: 12}]}>
                <Elevator height={20} width={20}/>
                <Text style={[fonts.hintText, {paddingLeft: 5}]}>3 Mteter</Text>
              </View>
            </View>
            <View style={styles.alignColumn}>
              <View style={styles.textRow}>
                <House height={20} width={20}/>
                <Text style={[fonts.hintText, {paddingLeft: 5}]}>230 sqm</Text>
              </View>
              <View style={[styles.textRow, {paddingTop: 12}]}>
                <Tag height={20} width={20}/>
                <Text style={[fonts.hintText, {paddingLeft: 5}]}>22%</Text>
              </View>
            </View>
          </View>
        </View>
      </Shadow>
    </Swipeable>
  );
});

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.secondary,
    marginTop: 25,
    marginLeft: widthToDp(2.5),
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 1,
    shadowColor: 'grey',
    shadowRadius: 10,
    borderRadius: 10,
  },
  swipeable: {
    width: 80,
    backgroundColor: colors.secondary,
    marginTop: 25,
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 1,
    shadowColor: 'grey',
    shadowRadius: 10,
    borderRadius: 10,
  },
  swipeBtn: {
    flex: 1,
    borderBottomColor: '#D4CFCF',
    borderBottomWidth: 0.4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    flex: 1,
    justifyContent: 'space-between',
    padding: 10,
  },
  details: {
    flex: 0.5,
    backgroundColor: colors.secondary,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    padding: 10,
    justifyContent: 'space-between',
  },
  imgBorder: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  labelText: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 30,
    backgroundColor: colors.secondary,
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
  },
  labelTextIcon: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 30,
    backgroundColor: colors.secondary,
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
  },
  rowJustify: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  textRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowAround: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 25,
  },
  alignColumn: {
    alignItems: 'flex-start',
  },
});
