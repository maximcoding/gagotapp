import React, {memo} from 'react';
import {StyleSheet, TouchableOpacity, View, Text} from 'react-native';
import {Neomorph} from 'react-native-neomorph-shadows';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';

export default memo(function Index({title, active, toggle}) {
  return (
    <TouchableOpacity
      onPress={toggle}
      style={{
        ...styles.btn,
      }}>
      <Neomorph
        style={{
          ...styles.container,
          backgroundColor: active ? colors.primary : colors.secondary,
          width: title.length >= 11 ? 130 : title.length <= 5 ? 80 : 100,
        }}>
        <Text
          style={{
            ...fonts.tagsText,
            color: active ? colors.white : colors.textColor,
          }}>
          {title}
        </Text>
      </Neomorph>
    </TouchableOpacity>
  );
});

const styles = StyleSheet.create({
  container: {
    shadowRadius: 5,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
  },
  btn: {
    marginTop: 20,
    marginLeft: 20,
  },
});
