import React, {memo} from 'react';
import {TouchableOpacity, Modal} from 'react-native';
import YoutubeViewComponent from './viewComponent';
import Cancel from '../../assets/cancel.svg';

export default memo(({visible, helpUrl, setVisible}) => {
  return (
    <Modal
      visible={visible}
      onRequestClose={() => setVisible(false)}
      transparent>
      <TouchableOpacity
        activeOpacity={1}
        style={{
          flex: 1,
          backgroundColor: 'rgba(255,255,255,.5)',
          justifyContent: 'center',
          padding: 10,
        }}>
        <TouchableOpacity
          onPress={() => setVisible(false)}
          style={{alignSelf: 'flex-end', padding: 10}}>
          <Cancel />
        </TouchableOpacity>
        <YoutubeViewComponent
          video={helpUrl}
          allowsFullscreenVideo={false}
          autoplay={1}
        />
      </TouchableOpacity>
    </Modal>
  );
});
