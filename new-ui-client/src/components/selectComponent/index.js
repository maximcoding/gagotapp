import React, {memo} from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Neomorph} from 'react-native-neomorph-shadows';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';
import {widthToDp} from '../../theme/responsive';
import ArrowDown from '../../assets/arrowDown.svg';
import RnPickerSelect from 'react-native-picker-select';

export default memo(function Index({
  title,
  height = 50,
  width = widthToDp(90),
  inner = true,
}) {
  return (
    <RnPickerSelect
      onValueChange={(value) => console.log('value')}
      items={['1', '2', '3', '4', '5', '6', '7'].map((i) => {
        return {
          label: i,
          value: i,
        };
      })}
      placeholder={{}}>
      <Neomorph
        inner={inner}
        swapShadows
        style={{...styles.container, height, width}}>
        <Text style={fonts.placeholder}>{title}</Text>
        <TouchableOpacity activeOpacity={0.6}>
          <ArrowDown />
        </TouchableOpacity>
      </Neomorph>
    </RnPickerSelect>
  );
});

const styles = StyleSheet.create({
  container: {
    shadowRadius: 5,
    borderRadius: 10,
    backgroundColor: colors.secondary,
    padding: 5,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    justifyContent: 'space-between',
  },
});
