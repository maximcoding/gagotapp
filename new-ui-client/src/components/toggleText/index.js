import React, {memo, useState} from 'react';
import {
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  LayoutAnimation,
} from 'react-native';
import {Neomorph} from 'react-native-neomorph-shadows';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';
import {widthToDp} from '../../theme/responsive';
import Toggle from '../../assets/toggle.png';

export default memo(function Index({height = 70, width = widthToDp(90),title}) {
  let [toggle, setToggle] = useState(false);

  const toggleBtn = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    setToggle(!toggle);
  };
  return (
    <Neomorph swapShadows style={{...styles.container, height, width}}>
      <Text style={fonts.title}>{title}</Text>

      {toggle ? (
        <TouchableOpacity onPress={toggleBtn} activeOpacity={0.9}>
          <Neomorph
            inner
            swapShadows
            style={{...styles.toggleContainer, alignItems: 'flex-end'}}>
            <Image source={Toggle} />
          </Neomorph>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity onPress={toggleBtn} activeOpacity={0.9}>
          <Neomorph
            inner
            swapShadows
            style={{...styles.toggleContainer, alignItems: 'flex-start'}}>
            <Image source={Toggle} />
          </Neomorph>
        </TouchableOpacity>
      )}
    </Neomorph>
  );
});

const styles = StyleSheet.create({
  container: {
    shadowRadius: 5,
    borderRadius: 10,
    backgroundColor: colors.secondary,
    padding: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  toggleContainer: {
    height: 40,
    width: 70,
    shadowRadius: 5,
    borderRadius: 20,
    backgroundColor: colors.secondary,
    justifyContent: 'center',
  },
});
