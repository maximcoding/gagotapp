import React, {memo} from 'react';
import {Text, Linking, View} from 'react-native';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';

const TermsOfUseView = (props) => {
  return (
    <View
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
      }}>
      <Text style={{fontSize: fonts.small.fontSize, color: colors.gray}}>
        By creating an account you agree with our
      </Text>
      <Text
        style={{color: 'blue', fontSize: fonts.small.fontSize}}
        onPress={() => Linking.openURL(tosLink)}>
        Terms of Use
      </Text>
    </View>
  );
};

export default memo(TermsOfUseView);
