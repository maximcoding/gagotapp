import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import {Neomorph} from 'react-native-neomorph-shadows';
import PlayVideoIcon from '../../assets/play_button.svg';
import RightArrowIcon from '../../assets/nextArrow.svg';
import {widthToDp} from '../../theme/responsive';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';

const VideoViewComponent = ({onPress, item, index}) => {
  return (
    <Neomorph style={styles.container}>
      <TouchableOpacity onPress={onPress} style={styles.subContainer}>
        <View paddingVertical={14}>
          <PlayVideoIcon width={50} height={50} />
        </View>
        <View paddingVertical={14} paddingHorizontal={8} flex={1}>
          <Text style={fonts.title}>
            {item.title} ({index + 1})
          </Text>
        </View>
        <TouchableOpacity paddingVertical={14} paddingHorizontal={24}>
          <RightArrowIcon />
        </TouchableOpacity>
      </TouchableOpacity>
    </Neomorph>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 65,
    width: widthToDp(95),
    backgroundColor: colors.secondary,
    shadowRadius: 5,
    marginTop: 20,
    borderRadius: 10,
  },
  subContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
});

export default VideoViewComponent;
