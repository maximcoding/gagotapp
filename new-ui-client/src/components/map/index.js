import React from 'react';
import MapView, {Marker} from 'react-native-maps';

export default function Map({coordinates}) {
  return (
    <MapView
      style={{width: '100%', height: 300}}
      region={{
        latitude: coordinates.latitude,
        longitude: coordinates.longitude,
        latitudeDelta: 0.02,
        longitudeDelta: 0.02,
      }}>
      <Marker
        coordinate={{
          latitude: coordinates.latitude,
          longitude: coordinates.longitude,
        }}
      />
    </MapView>
  );
}
