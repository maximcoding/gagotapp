import React, {useState, memo, useRef} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  TouchableHighlight,
  ActivityIndicator,
} from 'react-native';
import ActionSheet from 'react-native-actionsheet';
import ImageView from 'react-native-image-view';
import ImagePicker from 'react-native-image-picker';
import FastImage from 'react-native-fast-image';
import close from '../../assets/close.png';
import avatar from '../../assets/avatar.jpg';
import camera from '../../assets/camera.png';
import {styles} from './style';

const TNProfilePictureSelector = memo((props) => {
  const [profilePictureURL, setProfilePictureURL] = useState(
    props.profilePictureURL || '',
  );
  const [selectedPhotoIndex, setSelectedPhotoIndex] = useState(null);
  const [isImageViewerVisible, setIsImageViewerVisible] = useState(false);
  const [tappedImage, setTappedImage] = useState([]);
  const actionSheet = useRef(null);
  const handleProfilePictureClick = (url) => {
    if (url) {
      const isAvatar = url.search('avatar');
      const image = [
        {
          source: {
            uri: url,
          },
        },
      ];
      if (isAvatar === -1) {
        setTappedImage(image);
        setIsImageViewerVisible(true);
      } else {
        showActionSheet();
      }
    } else {
      showActionSheet();
    }
  };

  const onImageError = () => {
    Alert.alert(
      '',
      'An error occurred while trying to load Profile Picture!',
      [{text: 'OK'}],
      {
        cancelable: false,
      },
    );
    setProfilePictureURL('');
  };

  const onPressAddPhotoBtn = () => {
    const options = {
      title: 'Select photo',
      cancelButtonTitle: 'Cancel',
      takePhotoButtonTitle: 'Take Photo',
      chooseFromLibraryButtonTitle: 'Choose from Library',
      maxWidth: 2000,
      maxHeight: 2000,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    /**
     * The first arg is the options object for customization (it can also be null or omitted for default options),
     * The second arg is the callback which sends object: response (more info in the API Reference)
     */
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        // console.log('User cancelled image picker');
      } else if (response.error) {
        // console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        // console.log('User tapped custom button: ', response.customButton);
      } else {
        // console.log('i am called here what is that');
        props.setProfilePictureURL({
          uri: response.uri,
          type: response.type,
          name: response.fileName,
        });
        setProfilePictureURL(response.uri);
      }
    });
  };

  const closeButton = () => (
    <TouchableOpacity
      style={styles.closeButton}
      onPress={() => setIsImageViewerVisible(false)}>
      <Image style={styles.closeIcon} source={close} />
    </TouchableOpacity>
  );

  const showActionSheet = (index) => {
    setSelectedPhotoIndex(index);
    actionSheet.current.show();
  };

  const onActionDone = (index) => {
    if (index == 0) {
      onPressAddPhotoBtn();
    }
    if (index == 2) {
      // Remove button
      if (profilePictureURL) {
        setProfilePictureURL(null);
      }
    }
  };

  return (
    <>
      <View style={styles.imageBlock}>
        <TouchableHighlight
          style={styles.imageContainer}
          onPress={() => handleProfilePictureClick(profilePictureURL)}>
          {props.imageLoader ? (
            <ActivityIndicator color="gray" size="large" />
          ) : (
            <FastImage
              style={[styles.image, {opacity: profilePictureURL ? 1 : 0.3}]}
              source={profilePictureURL ? {uri: profilePictureURL} : avatar}
              resizeMode="cover"
              onError={onImageError}
            />
          )}
        </TouchableHighlight>
        <TouchableOpacity onPress={showActionSheet} style={styles.addButton}>
          <Image source={camera} style={styles.closeIcon} />
        </TouchableOpacity>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <ActionSheet
          ref={actionSheet}
          title={'Confirm Action'}
          options={['Change Profile Photo', 'Cancel', 'Remove Profile Photo']}
          cancelButtonIndex={1}
          destructiveButtonIndex={2}
          onPress={(index) => {
            onActionDone(index);
          }}
        />
        <ImageView
          images={tappedImage}
          isVisible={isImageViewerVisible}
          onClose={() => setIsImageViewerVisible(false)}
          controls={{close: closeButton}}
        />
      </ScrollView>
    </>
  );
});

export default TNProfilePictureSelector;
