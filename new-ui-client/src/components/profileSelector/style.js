import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../theme/colors';
const {height} = Dimensions.get('window');
const imageSize = height * 0.14;
const photoIconSize = imageSize * 0.27;

export const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: '100%',
  },
  imageBlock: {
    flex: 2,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    height: imageSize - 20,
    width: imageSize - 20,
    borderRadius: imageSize - 20,
    shadowColor: 'gray',
    shadowOffset: {
      width: 10,
      height: 10,
    },
    shadowOpacity: 0.1,
    overflow: 'hidden',
  },
  addButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.darkSecondary,
    zIndex: 2,
    marginTop: imageSize * 0.3,
    marginLeft: -imageSize * 0.2,
    width: photoIconSize,
    height: photoIconSize,
    borderRadius: photoIconSize,
  },
  closeButton: {
    alignSelf: 'flex-end',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 40,
    marginRight: 15,
    backgroundColor: colors.darkSecondary,
    width: 28,
    height: 28,
    borderRadius: 20,
    overflow: 'hidden',
  },
  closeIcon: {
    width: 27,
    height: 27,
  },
});
