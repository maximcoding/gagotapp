import React, {memo} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import Check from '../../assets/check.svg';
import Off from '../../assets/off.svg';
import {fonts} from '../../theme/fonts';

export default memo(function Index({on, title, toggle, style}) {
  return (
    <TouchableOpacity onPress={toggle} style={[styles.row, style]}>
      {on ? <Check height={30} width={30} /> : <Off height={30} width={30} />}
      <Text style={{...fonts.radioText}}>{title}</Text>
    </TouchableOpacity>
  );
});

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
