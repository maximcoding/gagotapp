import React, {memo, useCallback, useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  LayoutAnimation,
} from 'react-native';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';
import {widthToDp} from '../../theme/responsive';

let data = [
  'any',
  'line',
  '1+',
  'line',
  '2+',
  'line',
  '3+',
  'line',
  '4+',
  'line',
  '5+',
  'line',
];

export default memo(function Index({height = 50, width = widthToDp(90)}) {
  let [active, setActive] = useState('any');

  const selectValue = useCallback((item) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    setActive(item);
  }, []);
  return (
    <View style={{...styles.container, height, width}}>
      {data.map((item) => {
        return item == 'line' ? (
          <View style={styles.line} />
        ) : active == item ? (
          <TouchableOpacity
            style={styles.activeItem}
            onPress={() => selectValue(item)}>
            <Text style={[fonts.title, {color: 'white'}]}>{item}</Text>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={styles.borderItem}
            onPress={() => selectValue(item)}>
            <Text style={fonts.title}>{item}</Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.secondary,
    padding: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  },
  line: {
    height: 30,
    width: 2,
    backgroundColor: colors.textColor,
  },
  borderItem: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 30,
  },
  activeItem: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.primary,
    height: 30,
    borderRadius: 50,
    paddingHorizontal: 20,
  },
});
