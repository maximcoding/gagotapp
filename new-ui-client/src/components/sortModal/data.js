export let data = [
  {
    title: 'Date',
    secondTitle: 'Newest Posts',
    value: 'new',
  },
  {
    title: 'Date',
    secondTitle: 'Oldest Posts',
    value: 'old',
  },
  {
    title: 'Price',
    secondTitle: 'Lowest Price',
    value: 'lowest',
  },
  {
    title: 'Price',
    secondTitle: 'Highest Price',
    value: 'highest',
  },
  {
    title: 'Top Posts',
    value: 'top',
  },
  {
    title: 'Nearest',
    value: 'near',
  },
];
