import React, {useState} from 'react';
import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
import Modal from '../bottomModal';
import RadioButton from 'react-native-radio-button';
import {data} from './data';
import {fonts} from '../../theme/fonts';
import {colors} from '../../theme/colors';

export default function SortHomeModal({isOpen, closeSort, callBack, sortBy}) {
  let [active, setActive] = useState(sortBy);

  const changeRadio = (value) => {
    setActive(value);
    callBack(value);
    closeSort();
  };

  return (
    <Modal isOpen={isOpen} closeModal={closeSort}>
      <SafeAreaView style={styles.container}>
        <Text style={[fonts.title, styles.center]}>Sort</Text>
        {data.map((obj) => {
          return (
            <View style={styles.item}>
              <RadioButton
                animation={'bounceIn'}
                isSelected={active == obj.value}
                onPress={() => changeRadio(obj.value)}
                size={14}
                color={colors.primary}
              />
              <View
                style={{
                  paddingHorizontal: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={[fonts.small, styles.color]}>
                  {obj?.title}
                  {obj.secondTitle && ':'}
                </Text>
                <Text style={[fonts.small, styles.color]}>
                  {obj?.secondTitle}
                </Text>
              </View>
            </View>
          );
        })}
      </SafeAreaView>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  item: {
    width: '100%',
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 20,
    borderBottomColor: colors.darkSecondary,
    borderBottomWidth: 1,
    minHeight: 10,
  },
  color: {
    color: colors.textColor,
  },
  center: {
    alignSelf: 'center',
  },
});
