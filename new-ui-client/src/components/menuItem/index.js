import React, {memo} from 'react';
import {Image, Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import {Neomorph} from 'react-native-neomorph-shadows';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';
import {widthToDp} from '../../theme/responsive';
import Forward from '../../assets/forward.svg';

const MenuItem = (props) => {
  return (
    <Neomorph swapShadows style={styles.container}>
      <TouchableOpacity onPress={props.onPress} style={styles.subContainer}>
        <View style={styles.itemContainer}>
          <Image style={[styles.icon, props.iconStyle]} source={props.icon} />
          <View style={{paddingLeft: 7}}>
            <Text style={fonts.normal}>{props.title}</Text>
          </View>
        </View>
        <Forward />
      </TouchableOpacity>
    </Neomorph>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 55,
    width: widthToDp(90),
    paddingHorizontal: 20,
    backgroundColor: colors.secondary,
    shadowRadius: 5,
    marginVertical: 7,
    borderRadius: 10,
  },
  subContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  icon: {
    width: 24,
    height: 24,
  },
  itemContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    color: colors.gray,
    fontSize: fonts.normal.fontSize,
    marginTop: 3,
  },
});

export default memo(MenuItem);
