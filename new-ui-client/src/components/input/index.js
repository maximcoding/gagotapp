import React, {memo} from 'react';
import {TextInput, StyleSheet} from 'react-native';
import {Neomorph} from 'react-native-neomorph-shadows';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';
import {widthToDp} from '../../theme/responsive';

export default memo(function Index({
  height = 50,
  width = widthToDp(90),
  multiline = false,
  numberOfLines = 1,
  title,
  keyboardType = 'default',
  style = {},
}) {
  return (
    <Neomorph
      inner
      swapShadows
      style={{...styles.container, ...style, height, width}}>
      <TextInput
        placeholder={title}
        style={{...fonts.placeholder}}
        multiline={multiline}
        numberOfLines={numberOfLines}
        textAlignVertical={'top'}
        keyboardType={keyboardType}
      />
    </Neomorph>
  );
});

const styles = StyleSheet.create({
  container: {
    shadowRadius: 5,
    borderRadius: 10,
    backgroundColor: colors.secondary,
    padding: 5,
  },
});
