import React, {memo} from 'react';
import {TextInput, StyleSheet} from 'react-native';
import {Neomorph} from 'react-native-neomorph-shadows';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';
import {heightToDp, widthToDp} from '../../theme/responsive';
import IntlPhoneInput from 'react-native-intl-phone-input';

export default memo(function Index({
  height = 50,
  width = widthToDp(90),
  style = {},
}) {
  return (
    <Neomorph
      inner
      swapShadows
      style={{...styles.container, ...style, height, width}}>
      <IntlPhoneInput
        containerStyle={{...styles.subContainer, height: height - 10}}
        phoneInputStyle={styles.phoneInputStyle}
        flagStyle={styles.flagStyle}
        dialCodeTextStyle={styles.dialCodeTextStyle}
        defaultCountry={'IL'}
      />
    </Neomorph>
  );
});

const styles = StyleSheet.create({
  container: {
    shadowRadius: 5,
    borderRadius: 10,
    backgroundColor: colors.secondary,
    padding: 5,
  },
  phoneInputStyle: {
    fontSize: fonts.normal.fontSize,
    color: colors.gray,
    height: 55,
  },
  flagStyle: {
    fontSize: fonts.xl.fontSize,
    borderColor: colors.white,
  },
  dialCodeTextStyle: {
    fontSize: 16,
    color: colors.darkSecondary,
  },
  subContainer: {
    backgroundColor: 'transparent',
    alignItems: 'center',
  },
});
