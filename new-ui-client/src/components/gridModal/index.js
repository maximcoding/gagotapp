import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Modal from '../bottomModal';
import RadioButton from 'react-native-radio-button';
import {data} from './data';
import {fonts} from '../../theme/fonts';
import {colors} from '../../theme/colors';

export default function GridModal({isOpen, closeSort, callBack, grid}) {
  let [active, setActive] = useState(grid);
  const changeRadio = (value) => {
    setActive(value);
    callBack(value);
    closeSort();
  };

  return (
    <Modal isOpen={isOpen} closeModal={closeSort}>
      <View style={styles.container}>
        <Text style={[fonts.title, styles.center]}>Grid View</Text>
        {data.map((obj) => {
          return (
            <View style={styles.item}>
              <RadioButton
                color={colors.primary}
                animation={'bounceIn'}
                isSelected={active == obj.value}
                onPress={() => changeRadio(obj.value)}
                size={14}
              />
              <Text style={[fonts.small, styles.color]}>{obj.title}</Text>
            </View>
          );
        })}
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  item: {
    width: '100%',
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 20,
    borderBottomColor: colors.darkSecondary,
    borderBottomWidth: 1,
    minHeight: 10,
  },
  color: {
    color: colors.textColor,
    paddingHorizontal: 10,
  },
  center: {
    alignSelf: 'center',
  },
});
