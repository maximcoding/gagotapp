import React, {memo} from 'react';
import {Text, StyleSheet, Pressable} from 'react-native';
import {colors} from '../../theme/colors';
import {fonts} from '../../theme/fonts';
import {widthToDp} from '../../theme/responsive';
import LinearGradient from 'react-native-linear-gradient';

export default memo(function Index({
  height = 50,
  width = widthToDp(90),
  title,
  onPress,
  style,
}) {
  return (
    <LinearGradient
      colors={[colors.primary, '#ff5050', '#ff6600']}
      style={{...styles.container, height, width, ...style}}>
      <Pressable
        onPress={onPress}
        android_ripple={{color: 'orange', borderless: false}}
        style={{...styles.image, height: height - 2, width: width - 2}}>
        <Text style={fonts.buttonText}>{title}</Text>
      </Pressable>
    </LinearGradient>
  );
});

const styles = StyleSheet.create({
  container: {
    borderRadius: 10,
    backgroundColor: colors.secondary,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
  },
  image: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
});
