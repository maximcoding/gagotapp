import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {Neomorph} from 'react-native-neomorph-shadows';
import {colors} from '../../theme/colors';
import {widthToDp} from '../../theme/responsive';
import Arrow from '../../assets/left-arrow.png';
import Funnel from '../../assets/funnel.svg';
import {fonts} from '../../theme/fonts';
import {useNavigation} from '@react-navigation/native';

const Header = ({title, back = false, filter = false}) => {
  let navigation = useNavigation();

  const goBack = () => {
    navigation.goBack();
  };
  return (
    <View
      style={[
        styles.container,
        {justifyContent: back || filter ? 'space-between' : 'center'},
      ]}>
      {back ? (
        <TouchableOpacity onPress={goBack}>
          <Image source={Arrow} style={styles.visible} />
        </TouchableOpacity>
      ) : filter ? (
        <View style={{height: 35, width: 35}} />
      ) : (
        <></>
      )}
      <Text style={fonts.screenTitle}>{title}</Text>
      {back ? (
        <Image source={Arrow} style={styles.invisible} />
      ) : filter ? (
        <TouchableOpacity onPress={() => navigation.navigate('Filters')}>
          <Neomorph style={styles.itemHeader}>
            <Funnel height={18} width={18} />
          </Neomorph>
        </TouchableOpacity>
      ) : (
        <></>
      )}
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    height: 45,
    width: widthToDp(100),
    backgroundColor: colors.secondary,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    // shadowColor: colors.gray,
    // shadowOffset: {
    //   width: 0,
    //   height: 3,
    // },
    // shadowOpacity: 0.27,
    // shadowRadius: 4.65,
    // elevation: 6,
  },
  visible: {
    height: 30,
    width: 35,
    resizeMode: 'contain',
  },
  invisible: {
    height: 30,
    width: 35,
    resizeMode: 'contain',
    tintColor: 'transparent',
  },
  itemHeader: {
    height: 35,
    width: 35,
    borderRadius: 35 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    shadowRadius: 5,
    backgroundColor: colors.secondary,
  },
});
