import React, {memo} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import On from '../../assets/on.svg';
import Off from '../../assets/off.svg';
import {fonts} from '../../theme/fonts';

export default memo(function Index({on, title, toggle, style}) {
  return (
    <TouchableOpacity onPress={toggle} style={[styles.row, style]}>
      {on ? <On height={20} width={20} /> : <Off height={30} width={30} />}
      <Text style={{...fonts.radioText, paddingLeft: on ? 10 : 0}}>
        {title}
      </Text>
    </TouchableOpacity>
  );
});

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
