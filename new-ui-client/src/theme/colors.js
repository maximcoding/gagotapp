export const colors = {
  white: 'white',
  black: 'black',
  gray: 'gray',
  textColor: 'rgba(136, 133, 133, 1)',
  secondary: 'rgba(245, 245, 245, 1)',
  primary: 'rgba(247, 54, 10, 1)',
  darkSecondary: 'rgba(212, 212, 212, 1)',
  green: '#5EB700',
  link: 'blue',
};
