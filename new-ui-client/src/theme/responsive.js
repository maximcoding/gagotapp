import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from "react-native-responsive-screen";
  import { RFValue } from "react-native-responsive-fontsize";
  
  const widthToDp = (number) => {
    let givenWidth = typeof number == "number" ? number : parseFloat(number);
    return wp(`${givenWidth}%`);
  };
  
  const heightToDp = (number) => {
    let givenHeight = typeof number == "number" ? number : parseFloat(number);
    return hp(`${givenHeight}%`);
  };
  
  const fontToDp = (number) => {
    return RFValue(number);
  };
  
  export { widthToDp, heightToDp, fontToDp };