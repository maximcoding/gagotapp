import {colors} from './colors';
import {fontToDp} from './responsive';

export const fonts = {
  placeholder: {
    fontSize: fontToDp(12),
    color: colors.textColor,
  },
  screenTitle: {
    fontSize: fontToDp(17),
    color: colors.textColor,
    fontWeight: 'bold',
  },
  title: {
    fontSize: fontToDp(14),
    color: colors.textColor,
    fontWeight: '700',
  },
  transparent: {
    color: 'transparent',
  },
  hintText: {
    fontSize: fontToDp(9),
    color: colors.textColor,
  },
  addressText: {
    fontSize: fontToDp(12),
    color: colors.textColor,
  },
  tagsText: {
    fontSize: fontToDp(12),
    fontWeight: '700',
  },
  buttonText: {
    fontSize: fontToDp(14),
    color: colors.white,
    fontWeight: '700',
  },
  radioText: {
    fontSize: fontToDp(12),
    color: colors.textColor,
  },
  small: {
    fontSize: fontToDp(12),
    color: colors.textColor,
  },
  normal: {
    fontSize: fontToDp(14),
  },
  large: {
    fontSize: fontToDp(17),
  },
  xl: {
    fontSize: fontToDp(20),
  },
  xxl: {
    fontSize: fontToDp(22),
  },
  xs: {
    fontSize: fontToDp(10),
  },

  tabsText: {
    fontSize: fontToDp(12),
    color: colors.textColor,
  },
  tabsActive: {
    fontSize: fontToDp(12),
    color: colors.white,
  },
};
