import React, {useEffect, useState} from 'react';
import {InteractionManager, ActivityIndicator, View} from 'react-native';
import {colors} from '../theme/colors';
import {heightToDp, widthToDp} from '../theme/responsive';

function withLazy(Component) {
  return function WrappedComponent(props) {
    let [hidden, setHidden] = useState(true);

    useEffect(() => {
      InteractionManager.runAfterInteractions(() => {
        setHidden(false);
      });
    }, []);

    if (hidden) {
      return (
        <View
          style={{
            flex: 1,
            height: heightToDp(100),
            width: widthToDp(100),
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator color={colors.primary} size="small" />
        </View>
      );
    } else {
      return <Component {...props} />;
    }
  };
}

export default withLazy;
