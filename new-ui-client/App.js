import React from 'react';
import {
  SafeAreaView,
  Platform,
  UIManager,
  StatusBar,
  LogBox,
} from 'react-native';
import MainNavigator from './src/navigation/index';
import Login from './src/modules/searchScreen';
const App = () => {
  LogBox.ignoreLogs(['Warning: ...']);
  return (
    <>
      <StatusBar barStyle="light-content" />
      <SafeAreaView style={{flex: 1}}>
        <MainNavigator />
      </SafeAreaView>
    </>
  );
};

if (Platform.OS === 'android') {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

export default App;
