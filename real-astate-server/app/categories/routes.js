const express = require("express");
const router = express.Router();

const { getCategories, createCategory } = require("./controller");

//router.use(checkToken);

router.get("", getCategories);
router.post("/save", createCategory);

module.exports = router;
