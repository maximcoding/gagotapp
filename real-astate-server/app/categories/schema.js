const mongoose = require("mongoose");

const categoriesSchema = mongoose.Schema(
  {
    name: {
      type: String,
    },

    order: {
      type: Number,
    },
    photo: {
      type: String,
    },
  },
  { timestamps: true }
);

const categoriesModal = mongoose.model(
  "real_estate_categories",
  categoriesSchema
);

module.exports = categoriesModal;
