const complainSchema = require("../complain/schema");
const userSchema = require("../auth/schema");
const cons = require("consolidate");

exports.complains = async (req, res) => {
  let page = req.body.page * 20;
  let perPage = 20;

  try {
    let result = await complainSchema
      .find()
      .populate("userId")
      .limit(perPage)
      .skip(page)
      .sort({
        createdAt: -1,
      });

    res.status(200).send(result);
  } catch (error) {
    res.status(500).json(error);
  }
};

exports.getUsers = async (req, res) => {
  let page = req.body.page * 20;
  let perPage = 20;
  let { params } = req.body;

  console.log("my params are hre", params);
  let sort;
  let query;

  switch (params) {
    case "all":
      sort = {
        createdAt: -1,
      };
      query = {};
      break;

    case "new":
      sort = {
        createdAt: -1,
      };
      query = {};
      break;
    case "old":
      sort = {
        createdAt: 1,
      };
      query = {};
      break;
    case "rank":
      sort = {
        createdAt: -1,
      };
      query = {
        rank: {
          $gt: 4,
        },
      };
      break;
    case "fav":
      sort = {
        createdAt: -1,
      };
      query = {
        favorite: true,
      };
      break;
    case "users":
      sort = {
        createdAt: -1,
      };
      query = {
        type: "user",
      };
      break;
    case "agents":
      sort = {
        createdAt: -1,
      };
      query = {
        type: "agent",
      };
      break;
  }

  try {
    let result = await userSchema
      .find(query)
      .limit(perPage)
      .skip(page)
      .sort(sort);

    res.status(200).send(result);
  } catch (error) {
    res.status(500).json(error);
  }
};

exports.searchUsers = async (req, res) => {
  try {
    console.log("search is here", req.body);
    if (!req.body.search) {
      let result = await userSchema.find({}).limit(20).sort({ createdAt: -1 });
      res.status(200).send(result);
    } else {
      let result = await userSchema
        .find({
          $text: { $search: req.body.search },
        })
        .limit(100);
      res.status(200).send(result);
    }
  } catch (error) {
    res.status(500).json("Something went wrong try again!");
  }
};

exports.favUser = async (req, res) => {
  try {
    await userSchema.findOneAndUpdate(
      {
        _id: req.body.user._id,
      },
      req.body.query
    );
    res.status(200).send("Success");
  } catch (error) {
    res.status(500).json("Something went wrong try again!");
  }
};
