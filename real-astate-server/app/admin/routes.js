const express = require("express");
const router = express.Router();

const {complains, getUsers, searchUsers, favUser} = require("./controller");
const {checkToken} = require("../../middleware/checkToken");

router.use(checkToken);

router.post("/users/complains", complains);

router.post("/users", getUsers);

router.post("/users/search", searchUsers);

router.post("/users/favorites", favUser);

module.exports = router;
