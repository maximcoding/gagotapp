const express = require("express");
const router = express.Router();

const { addInRecent, getRecentListing } = require("./controller");
const { checkToken } = require("../../middleware/checkToken");

router.use(checkToken);

router.post("/addRecent", addInRecent);
router.get("/getRecent/:uid", getRecentListing);

module.exports = router;
