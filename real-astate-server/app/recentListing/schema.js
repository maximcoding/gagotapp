const mongoose = require("mongoose");

const recentListingSchema = mongoose.Schema(
  {
    listingID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "real_estate_listings",
    },

  
    userID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
    },
  },
  { timestamps: true }
);

const recentListingModal = mongoose.model(
  "recent_listings",
  recentListingSchema
);

module.exports = recentListingModal;
