const recentListingModal = require("./schema");

exports.addInRecent = async (req, res) => {
  try {
    let data = {
      ...req.body,
    };

    let newRecent = new recentListingModal(data);
    await newRecent.save();
    return res.status(200).send(newRecent);
  } catch (error) {
    console.log("error while complain", error);
    return res.status(500).json("Something went wrong try again!");
  }
};

exports.getRecentListing = async (req, res) => {
  try {
    let listings = await recentListingModal.find({ userID: req.params.uid });

    return res.status(200).send(listings);
  } catch (error) {
    console.log("error while complain", error);
    return res.status(500).json("Something went wrong try again!");
  }
};
