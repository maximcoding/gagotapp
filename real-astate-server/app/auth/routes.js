const express = require("express");
const router = express.Router();

const userSchema = require("./schema");
const {
  Signup,
  loginUser,
  ConfirmCode,
  sendOtp,
  loginWithPhone,
  updateUser,
  sendOtpExists
} = require("./controller");

const { checkToken } = require("../../middleware/checkToken");

router.post("/signup", (req, res) => {
  Signup(req, res, userSchema);
});

router.post("/login", (req, res) => {
  loginUser(req, res, userSchema);
});
router.post("/loginWithPhone", (req, res) => {
  loginWithPhone(req, res, userSchema);
});

// update user

router.put("/update/:uid", (req, res) => {
  updateUser(req, res, userSchema);
});

router.get("/authenticate", checkToken, (req, res) => {
  let user = req.user;
  delete user.password;
  res.status(200).json(user);
});

router.post("/code/confirm", (req, res) => {
  ConfirmCode(req, res, userSchema);
});

router.post("/code/verify", (req, res) => {
  sendOtp(req, res, userSchema);
});

router.post("/code/exists", (req, res) => {
  sendOtpExists(req, res, userSchema);
});

router.post("/logout", (req, res) => {
  res.clearCookie("token");
  res.status(200).json("success");
});

module.exports = router;
