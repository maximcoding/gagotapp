const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const saltRounds = 10;

const usersSchema = mongoose.Schema(
  {
    email: String,
    appIdentifier: String,
    firstName: String,
    lastName: String,
    password: String,
    photoURI: {
      type: String,
      default: "",
    },
    status: {
      type: String,
      default: "active",
    },
    favorite: {
      type: Boolean,
      default: false,
    },

    phone: {
      type: String,
      default: "",
    },
    country: {
      type: String,
      default: "",
    },
    type: {
      type: String,
      default: "user",
    },
    companyID: {
      type: String,
    },
    address: {
      type: String,
    },
    profilePictureURL: {
      type: String,
    },
    unit: {
      type: String,
      default: "",
    },
    currency: {
      type: String,
      default: "",
    },
    rank: {
      type: Number,
      default: 0,
    },
    notificationId: {
      type: String,
      default: "",
    },
    device: {
      type: String,
      default: "android",
    },
  },
  { timestamps: true }
);

usersSchema.index({ "$**": "text" });

usersSchema.pre("save", async function (next) {
  let user = this;
  if (!user.isModified("password")) {
    return next();
  }
  let hash = await bcrypt.hash(user.password, saltRounds);
  user.password = hash;
  next();
});

const usersModels = mongoose.model("users", usersSchema);

module.exports = usersModels;
