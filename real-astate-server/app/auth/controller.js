const bcrypt = require("bcrypt");
// let { EmailTemplate } = require("../template/emailTemp");
let { generateJWT } = require("../../helpers/generateJWT");
// let { signupEmail } = require("../template/signupTech");

const client = require("twilio")(
  process.env.TWILIO_SID,
  process.env.TWILIO_AUTHTOKEN
);

module.exports.Signup = async (req, res, Schema) => {
  console.log("req.body", req.body);
  try {
    let user;

    if (req.body.email) {
      user = await Schema.findOne({ email: req.body.email });
    } else {
      user = await Schema.findOne({ phone: req.body.phone });
    }

    if (user) {
      return res
        .status(409)
        .json("User with this Phone Number or Email already exists");
    } else {
      let type = req.body.companyID ? "agent" : "user";
      let data = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        ...(req.body.phone && { phone: req.body.phone }),
        appIdentifier: req.body.appIdentifier,
        photoURI: req.body.photoURI,
        ...(req.body.password && {
          password: req.body.password,
        }),
        type,
        ...(type == "agent" && {
          companyID: req.body.companyID,
        }),
        ...(type == "agent" && {
          address: req.body.address,
        }),
        country: "Israel",
        unit: "meter",
        currency: "sign",
        notificationId: req.body.notificationId,
        device: req.body.device,
      };
      let newUser = new Schema(data);
      let user = await newUser.save();
      console.log("user sa", user);
      let token = generateJWT(user.toJSON());
      res.set("Authorization", token);

      return res.status(200).send(user);
    }
  } catch (error) {
    console.log("err", error);
    res.status(500).json("Something went wrong try again!");
  }
};

// login

module.exports.loginUser = async (req, res, Schema) => {
  try {
    let { email, password } = req.body;

    let user = await Schema.findOne({ email: email });
    if (user) {
      if (user.status == "block") {
        return res.status(403).json("Blocked by admin!");
      } else {
        let result = await bcrypt.compare(password, user.password);
        if (result) {
          user.notificationId = req.body.notificationId;
          user.device = req.body.device;
          await user.save();
          let token = generateJWT(user.toJSON());

          delete user.password;
          res.set("Authorization", token);
          return res.status(200).send(user);
        } else {
          return res.status(403).json("Incorrect password");
        }
      }
    } else {
      return res.status(403).json("User not registered yet!");
    }
  } catch (error) {
    console.log("error in ", error);
    return res.status(500).json("Something went wrong try again!");
  }
};
// login

module.exports.loginWithPhone = async (req, res, Schema) => {
  try {
    let { phone } = req.body;

    let user = await Schema.findOne({ phone: phone });
    if (user) {
      user.notificationId = req.body.notificationId;
      user.device = req.body.device;
      await user.save();
      let token = generateJWT(user.toJSON());

      delete user.password;
      res.set("Authorization", token);
      return res.status(200).send(user);
    } else {
      return res.status(403).json("User not registered yet!");
    }
  } catch (error) {
    return res.status(500).json("Something went wrong try again!");
  }
};

// code Confirm

module.exports.ConfirmCode = async (req, res, Schema) => {
  try {
    let user = await Schema.findOne({
      email: req.body.email,
    });
    if (user.codeConfirm == req.body.code && user.codeStatus == "valid") {
      user.codeStatus = "invalid";
      await user.save();
      res.status(200).send("success");
    } else {
      res.status(200).send("not valid");
    }
  } catch (error) {
    res.status(500).json("Something went wrong try again!");
  }
};

module.exports.sendOtp = async (req, res, Schema) => {
  try {
    let number = req.body.phone;
    console.log("number", number);
    await client.messages.create({
      body: `${req.body.code} is Your Verification Code`,
      from: "+19495317398",
      to: number,
    });

    res.status(200).send("success");
  } catch (error) {
    console.log("error is here", error);
    res.status(500).json("Something went wrong try again!");
  }
};

module.exports.sendOtpExists = async (req, res, Schema) => {
  try {
    let number = req.body.phone;
    console.log("number", number);
    let user = await Schema.findOne({ phone: number });
    if (user) {
      return res
        .status(403)
        .json("User is already exists with this Phone Number");
    } else {
      await client.messages.create({
        body: `${req.body.code} is Your Verification Code`,
        from: "+19495317398",
        to: number,
      });
      res.status(200).send("success");
    }
  } catch (error) {
    console.log("error is here", error);
    res.status(500).json("Something went wrong try again!");
  }
};

module.exports.updateUser = async (req, res, Schema) => {
  try {
    const { type, newpassword, password, email } = req.body;

    if (type === "password") {
      let user = await Schema.findById({ _id: req.params.uid });

      let result = await bcrypt.compare(password, user.password);
      if (result) {
        user.password = newpassword;
        await user.save();
      } else {
        return res.status(403).json("Incorrect password");
      }
    } else if (type === "email") {
      let user = await Schema.findOne({ email: email });

      if (user) {
        return res.status(409).json("User with this  Email already exists");
      } else {
        await Schema.findByIdAndUpdate(
          { _id: req.params.uid },
          {
            ...req.body,
          }
        );
      }
    } else {
      await Schema.findByIdAndUpdate(
        { _id: req.params.uid },
        {
          ...req.body,
        }
      );
    }

    return res.status(200).send("successfully updated");
  } catch (err) {
    console.log("error while ", err);
    res.status(500).json("Something went wrong try again!");
  }
};
