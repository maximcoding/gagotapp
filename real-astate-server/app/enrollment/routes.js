const express = require("express");
const router = express.Router();

const { enrollUser, editTime, getEnrollers } = require("./controller");
const { checkToken } = require("../../middleware/checkToken");

router.use(checkToken);

router.post("/enrollUser", enrollUser);
router.post("/editTime", editTime);
router.post("/getEnrollers", getEnrollers);

module.exports = router;
