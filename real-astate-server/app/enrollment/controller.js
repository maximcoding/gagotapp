const enrollSchema = require("./schema");
const listSchema = require("../properties/schema");
const { scheduleTime } = require("../../helpers/scheduleTime");
const { sendRemoteNotifications } = require("../../helpers/notifications");
const {
  sendMultipleNotifications,
} = require("../../helpers/multipleNotifications");

exports.enrollUser = async (req, res) => {
  let { post, time, day } = req.body;
  try {
    let enroll = await enrollSchema.findOne({
      user: req.user._id,
      listingID: post._id,
    });
    if (enroll) {
      res
        .status(403)
        .json(`You have already enroll for this property for  day ${day}`);
    } else {
      await enrollSchema.create({
        day,
        time,
        user: req.user._id,
        listingID: post._id,
        agentId: post.authorID._id,
      });

      // set schedule notifications for alarm

      let date = scheduleTime(day, time);
      let data = {
        title: `Alert You have a visit today at ${time}`,
        description:
          "It is inform you that you have a visit today for a property",
        notificationId: req.user.notificationId,
        date,
      };
      sendRemoteNotifications(data, req.user.device);
    }
    res.status(200).send("success");
  } catch (error) {
    res.status(500).json(error);
  }
};

exports.getEnrollers = async (req, res) => {
  let page = req.body.page * 20;
  let perPage = 20;
  let { day, post } = req.body;
  try {
    let result = await enrollSchema
      .find({
        day,
        listingID: post._id,
      })
      .populate("user")
      .limit(perPage)
      .skip(page)
      .sort({
        createdAt: -1,
      });

    res.status(200).send(result);
  } catch (error) {
    console.log("error is here", error);
    res.status(500).json(error);
  }
};

exports.editTime = async (req, res) => {
  let { post, time, day } = req.body;
  try {
    let one = listSchema
      .findOneAndUpdate(
        {
          _id: post._id,
        },
        {
          ["open_doors." + day]: time,
        },
        {
          new: true,
        }
      )
      .populate("authorID")
      .lean();

    let two = enrollSchema.findOneAndUpdate(
      {
        day,
        listingID: post._id,
      },
      {
        time,
      }
    );

    let results = await Promise.all([one, two]);

    // send notifications to all users

    let enrollers = await enrollSchema
      .find({ listingID: post._id })
      .populate("user")
      .lean();

    let androidIds = enrollers
      .filter(
        (item) =>
          item.user.device == "android" && item.user.notificationId !== ""
      )
      .map((obj) => obj.user.notificationId);
    let iosIds = enrollers
      .filter(
        (item) => item.user.device == "ios" && item.user.notificationId !== ""
      )
      .map((obj) => obj.user.notificationId);
    let finalArray = [
      { device: "android", ids: androidIds },
      { device: "ios", ids: iosIds },
    ];
    let message = `Alert for property time change`;
    let description = `property name ${results[0].title} has changed its ${day} visiting time to ${time}`;
    let promises = sendMultipleNotifications(
      finalArray,
      message,
      description,
      results[0].photoURLs[0]
    );
    await Promise.all([...promises]);

    res.status(200).send(results[0]);
  } catch (error) {
    console.log("error is here", error);
    res.status(500).json(error);
  }
};
