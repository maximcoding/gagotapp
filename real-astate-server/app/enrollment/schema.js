const mongoose = require("mongoose");

const enrollmentSchema = mongoose.Schema(
  {
    time: {
      type: String,
    },
    day: {
      type: String,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
    },

    listingID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "real_estate_listings",
    },
    agentId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
    },
  },
  { timestamps: true }
);

const enrollmentModal = mongoose.model(
  "real_estate_enrollment",
  enrollmentSchema
);

module.exports = enrollmentModal;
