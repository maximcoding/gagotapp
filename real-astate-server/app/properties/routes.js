const express = require("express");
const router = express.Router();

const {checkToken} = require("../../middleware/checkToken");

const {
    createProperty,
    updatePropertyById,
    getPropertyById,
    deletePropertyById,
    filterCategories,
    getPropertiesPreviews,
    filterPropertiesPreviews,
    getPropertiesRS,
    changeStatus,
    getPropertiesForMap,
    getVisits,
    searchProperties

} = require("./controller");


router.post("/previews", getPropertiesPreviews);

router.use(checkToken);

router.post("/save", createProperty);
router.put("/update/:id", updatePropertyById);
router.get("/fetch/:id", getPropertyById);
router.delete("/delete/:id", deletePropertyById);
router.post("/previews/filter", filterPropertiesPreviews);
router.post('/previews/search', searchProperties)
router.post("/previews/rs", getPropertiesRS);
router.post('/previews/map', getPropertiesForMap)
router.post("/categories/filter", filterCategories);
router.post("/status/update", changeStatus);
router.post('/visits', getVisits)

module.exports = router;
