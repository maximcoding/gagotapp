const realStateModal = require("./schema");
const enrollSchema = require("../enrollment/schema");
const userSchema = require("../auth/schema");
const {
  sendMultipleNotifications,
} = require("../../helpers/multipleNotifications");

// const savedListingModal = require("./schema");

exports.createProperty = async (req, res) => {
  try {
    if (req.user.type == "agent" && req.user.companyID == req.body.agentID) {
      let newState = new realStateModal({ ...req.body });
      let state = await newState.save();

      return res.status(200).send(state);
    } else if (req.user.type == "user") {
      let agUser = await userSchema.findOne({
        companyID: req.body.agentID,
      });
      if (agUser) {
        let newState = new realStateModal({
          ...req.body,
          authorID: agUser._id,
        });
        let state = await newState.save();
        return res.status(200).send(state);
      } else {
        res.status(403).json("agentID is not valid");
      }
    } else {
      res.status(403).json("agentID is not valid");
    }
  } catch (err) {
    res.status(500).json("Something went wrong try again!");
  }
};
exports.getPropertyById = async (req, res) => {
  try {
    let state = await realStateModal
        .findById({ _id: req.params.id })
        .populate("authorID");
    return res.status(200).send(state);
  } catch (err) {
    return res.status(500).json("Something went wrong try again!");
  }
};
exports.updatePropertyById = async (req, res) => {
  try {
    let listId = req.params.id;
    let state = await realStateModal
        .findOneAndUpdate({ _id: listId }, { ...req.body }, { new: true })
        .lean();

    // send notifications to subscribed users
    let enrollers = await enrollSchema
        .find({ listingID: listId })
        .populate("user")
        .lean();

    let androidIds = enrollers
        .filter(
            (item) =>
                item.user.device == "android" && item.user.notificationId !== ""
        )
        .map((obj) => obj.user.notificationId);

    let iosIds = enrollers
        .filter(
            (item) => item.user.device == "ios" && item.user.notificationId !== ""
        )
        .map((obj) => obj.user.notificationId);

    let finalArray = [
      { device: "android", ids: androidIds },
      { device: "ios", ids: iosIds },
    ];
    let message = "Alert Property details changes";
    let description = `property ${state.title} has changed its details`;
    let promises = sendMultipleNotifications(
        finalArray,
        message,
        description,
        state.photoURLs[0]
    );
    await Promise.all([...promises]);

    res.status(200).send(state);
  } catch (err) {
    return res.status(500).json("Something went wrong try again!");
  }
};
exports.deletePropertyById = async (req, res) => {
  try {
    await realStateModal.findByIdAndUpdate(
        { _id: req.params.id },
        {
          delete: true,
        },
        { new: true }
    );

    return res.status(200).json("success");
  } catch (err) {
    return res.status(500).json("Something went wrong try again!");
  }
};
exports.getPropertiesPreviews = async (req, res) => {
  try {
    const { range, sort } = req.body;
    let page = req.body.page * 20;
    let perPage = 20;
    let realStates;
    if (sort == "near") {
      realStates = await realStateModal
          .find({
            geoHash: { $gt: range.lower, $lt: range.upper },
            delete: false,
          })
          .populate("authorID")
          .limit(perPage)
          .skip(page)
          .sort({
            updatedAt: -1,
          });
    } else {
      let type;
      switch (sort) {
        case "new":
          type = "-createdAt";
          break;
        case "old":
          type = "+createdAt";
          break;
        case "lowest":
          type = "+price";
          break;
        case "highest":
          type = "-price";
          break;
        case "top":
          type = "-starCount";
          break;
      }
      realStates = await realStateModal
          .find({ delete: false })
          .populate("authorID")
          .limit(perPage)
          .skip(page)
          .sort(type);
    }
    return res.status(200).send(realStates);
  } catch (err) {
    res.status(500).json("Something went wrong try again!");
  }
};
exports.filterPropertiesPreviews = async (req, res) => {
  const {
    rent,
    category,
    newConstruction,
    built,
    useExact,
    bedrooms,
    bathrooms,
    price,
    square,
    deposit,
  } = req.body;
  try {
    let page = req.body.page * 20;
    let perPage = 20;

    let query = {
      ...(rent && {
        type: rent,
      }),
      ...(category && {
        categoryID: category,
      }),
      ...(newConstruction && {
        newConstruction: newConstruction,
      }),

      ...(built && {
        built: built,
      }),
      ...(bedrooms !== 0 &&
          (useExact
              ? {
                bedroom: bedrooms,
              }
              : {
                bedroom: { $gte: bedrooms },
              })),
      ...(bathrooms !== 0 &&
          (useExact
              ? {
                bathroom: bathrooms,
              }
              : {
                bathroom: { $gte: bathrooms },
              })),

      ...(price[0] !== 0 && {
        price: { $gte: price[0], $lte: price[1] },
      }),
      ...(square[0] !== 0 && {
        square: { $gte: square[0], $lte: square[1] },
      }),
      ...(deposit[0] !== 0 && {
        deposit: { $gte: deposit[0], $lte: deposit[1] },
      }),
      delete: false,
    };
    let one = realStateModal
        .find(query)
        .populate("authorID")
        .limit(perPage)
        .skip(page)
        .sort({
          createdAt: -1,
        });

    let two = realStateModal.countDocuments(query);

    let results = await Promise.all([one, two]);

    return res.status(200).send(results);
  } catch (err) {
    console.log("err while filter", err);
    return res.status(500).json("Something went wrong try again!");
  }
};
exports.filterCategories = async (req, res) => {
  try {
    let page = req.body.page * 20;
    let perPage = 20;
    let one = realStateModal
      .find({
        categoryID: req.body.categoryId,
      })
      .skip(page)
      .limit(perPage)
      .populate("authorID")
      .sort({
        createdAt: -1,
      });
    let two = realStateModal.countDocuments({
      categoryID: req.body.categoryId,
    });
    let results = await Promise.all([one, two]);
    return res.status(200).send(results);
  } catch (err) {
    res.status(500).json("Something went wrong try again!");
  }
};
exports.getPropertiesForMap = async (req, res) => {
  try {
    const { range, filters } = req.body;
    console.log("my posts are hre", range);

    if (filters) {
      const {
        rent,
        category,
        newConstruction,
        built,
        useExact,
        bedrooms,
        bathrooms,
        price,
        square,
        deposit,
      } = filters;

      let query = {
        ...(rent && {
          type: rent,
        }),
        ...(category && {
          categoryID: category,
        }),
        ...(newConstruction && {
          newConstruction: newConstruction,
        }),

        ...(built && {
          built: built,
        }),
        ...(bedrooms !== 0 &&
          (useExact
            ? {
                bedroom: bedrooms,
              }
            : {
                bedroom: { $gte: bedrooms },
              })),
        ...(bathrooms !== 0 &&
          (useExact
            ? {
                bathroom: bathrooms,
              }
            : {
                bathroom: { $gte: bathrooms },
              })),

        ...(price[0] !== 0 && {
          price: { $gte: price[0], $lte: price[1] },
        }),
        ...(square[0] !== 0 && {
          square: { $gte: square[0], $lte: square[1] },
        }),
        ...(deposit[0] !== 0 && {
          deposit: { $gte: deposit[0], $lte: deposit[1] },
        }),
        delete: false,
        geoHash: { $gt: range.lower, $lt: range.upper },
      };

      let realStates = await realStateModal.find(query).populate("authorID");
      res.status(200).send(realStates);
    } else {
      let realStates = await realStateModal
        .find({
          geoHash: { $gt: range.lower, $lt: range.upper },
          delete: false,
        })
        .populate("authorID");

      res.status(200).send(realStates);
    }
  } catch (error) {
    console.log("error is here", error);
    res.status(500).json("Something went wrong try again!");
  }
};
exports.getPropertiesRS = async (req, res) => {
  try {
    let page = req.body.page * 20;
    let perPage = 20;
    let { type } = req.body;
    let state = await realStateModal
      .find({
        authorID: req.user._id,
        type: type,
        delete: false,
      })
      .populate("authorID")
      .limit(perPage)
      .skip(page)
      .sort({
        createdAt: -1,
      });

    return res.status(200).send(state);
  } catch (err) {
    return res.status(500).json("Something went wrong try again!");
  }
};
exports.getVisits = async (req, res) => {
  try {
    let page = req.body.page * 20;
    let perPage = 20;
    let data = await enrollSchema
      .find({
        user: req.user._id,
      })
      .populate("listingID")
      .limit(perPage)
      .skip(page)
      .sort({
        createdAt: -1,
      });

    return res.status(200).send(data);
  } catch (err) {
    return res.status(500).json("Something went wrong try again!");
  }
};
exports.changeStatus = async (req, res) => {
  try {
    let { status, id } = req.body;
    let property = await realStateModal
      .findOneAndUpdate(
        { _id: id },
        {
          status,
        },
        { new: true }
      )
      .lean();

    // send notifications to subscribed users
    let enrollers = await enrollSchema
      .find({ listingID: id })
      .populate("user")
      .lean();
    let androidIds = enrollers
      .filter(
        (item) =>
          item.user.device == "android" && item.user.notificationId !== ""
      )
      .map((obj) => obj.user.notificationId);

    let iosIds = enrollers
      .filter(
        (item) => item.user.device == "ios" && item.user.notificationId !== ""
      )
      .map((obj) => obj.user.notificationId);

    let finalArray = [
      { device: "android", ids: androidIds },
      { device: "ios", ids: iosIds },
    ];
    let message = "Alert Property Status changes";
    let description = `property ${property.title} has changed its status to ${status}`;
    let promises = sendMultipleNotifications(
      finalArray,
      message,
      description,
      property.photoURLs[0]
    );
    await Promise.all([...promises]);

    res.status(200).json("success");
  } catch (err) {
    console.log("error is here", err);
    return res.status(500).json("Something went wrong try again!");
  }
};
exports.searchProperties = async (req, res) => {
  try {
    if (!req.body.text) {
      res.status(200).send([]);
    } else {
      let result = await realStateModal
        .find({
          $text: { $search: req.body.text },
        })
        .limit(20);
      res.status(200).send(result);
    }
  } catch (error) {
    res.status(500).json("Something went wrong try again!");
  }
};
