const mongoose = require("mongoose");

const realStateSchema = mongoose.Schema(
    {
        title: {
            type: String,
        },
        description: String,
        type: {
            type: [String],
            enum: ["buy", "rent"],
        },
        categoryID: Number,
        floors: Number,
        authorID: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "users",
        },
        authorType: {
            type: String,
            enum: ["agent", "user"],
        },
        agentID: String,
        newConstruction: String,
        built: String,
        price: {
            type: String,
            default: "",
        },
        bathroom: String,
        bedroom: String,
        coordinate: {
            type: [Number],
            default: [0, 0],
        },
        delete: {
            type: Boolean,
            default: false,
        },
        deposit: String,
        isApproved: {
            type: Boolean,
            default: true,
        },
        latitude: Number,
        longitude: Number,
        photo: String,
        photoURLs: [String],
        videoURLs: [String],
        place: {
            type: String,
            default: "",
        },
        square: {
            type: String,
            default: "",
        },
        starCount: {
            type: String,
            default: "",
        },
        unit: {
            type: String,
            default: "",
        },
        status: {
            type: String,
            default: "open",
        },
        agentPhone: String,
        open_doors: {
            mon: String,
            tue: String,
            wed: String,
            thurs: String,
            fri: String,
            sat: String,
            sun: String,
        },
        geoHash: {
            type: String,
        },
        facilities: {
            type: [String],
            enum: ["basement",
                "airConditioning",
                "balcony",
                "wifi",
                "fireplace",
                "garage",
                "parkingLot",
                "pool",
                "sauna",
                "cameras",
                "fitnessCenter",
                "smartControl",
                "boiler",
                "lawn",
                "centralHeating",
                "tennisCourt",
                "dualSinks"
            ],
        },
        nextTo: {
            type: [String],
            enum: ['subway', 'bus', 'train', 'airport', 'school', 'garden', 'shop', 'mall', 'theater', 'sea']
        }
    },
    {timestamps: true}
);

realStateSchema.index({"$**": "text"});

const realStateModal = mongoose.model("real_estate_listings", realStateSchema);

module.exports = realStateModal;
