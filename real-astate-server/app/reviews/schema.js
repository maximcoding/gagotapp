const mongoose = require("mongoose");

const reviewsSchema = mongoose.Schema(
  {
    authorID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
    },
    postUser: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
    },
    listingID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "real_estate_listings",
    },
    content: {
      type: String,
    },

    firstName: {
      type: String,
    },
    image: {
      type: String,
    },
    lastName: {
      type: String,
    },
    starCount: {
      type: Number,
    },
    replies: [{ createdAt: { type: Date, default: new Date() }, text: String }],
  },
  { timestamps: true }
);

const reviewsModal = mongoose.model("real_estate_reviews", reviewsSchema);

module.exports = reviewsModal;
