const reviewModal = require("./schema");

exports.createReview = async (req, res) => {
  try {
    let data = {
      ...req.body,
    };

    let newReview = new reviewModal(data);
    await newReview.save();

    let reviews = await reviewModal.find({ listingID: req.body.listingID });

    return res.status(200).send(reviews);
  } catch (error) {
    return res.status(500).json("Something went wrong try again!");
  }
};

exports.getReviewByPropertyId = async (req, res) => {
  try {
    let reviews = await reviewModal.find({ listingID: req.params.listingID });
    return res.status(200).send(reviews);
  } catch (err) {
    return res.status(500).json("Something went wrong try again!");
  }
};

exports.getReviews = async (req, res) => {
  try {
    let page = req.body.page * 20;
    let perPage = 20;
    let reviews = await reviewModal
      .find({ postUser: req.user._id })
      .limit(perPage)
      .skip(page)
      .sort({
        createdAt: -1,
      });

    return res.status(200).send(reviews);
  } catch (err) {
    return res.status(500).json("Something went wrong try again!");
  }
};

exports.replyReview = async (req, res) => {
  try {
    await Schema.findByIdAndUpdate(
      { _id: req.params.reviewID },
      {
        ...req.body,
      }
    );
    return res.status(200).json("success");
  } catch (err) {
    console.log("error while review", error);
    return res.status(500).json("Something went wrong try again!");
  }
};
