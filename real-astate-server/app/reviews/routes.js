const express = require("express");
const router = express.Router();

const {
    createReview,
    getReviewByPropertyId,
    getReviews,
    replyReview,
} = require("./controller");
const {checkToken} = require("../../middleware/checkToken");

router.use(checkToken);

router.post("", getReviews);
router.post("/save", createReview);
router.get("/property/fetch/:id", getReviewByPropertyId);
router.put("/reply/:reviewID", replyReview);

module.exports = router;
