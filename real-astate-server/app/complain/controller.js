const complainModal = require("./schema");

module.exports.addComplain = async (req, res) => {
  try {
    let data = {
      ...req.body,
    };

    let newComplain = new complainModal(data);
    await newComplain.save();
    return res.status(200).json("successfully posted");
  } catch (error) {
    console.log("error while complain", error);
    return res.status(500).json("Something went wrong try again!");
  }
};
