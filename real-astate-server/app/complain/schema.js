const mongoose = require("mongoose");

const complainSchema = mongoose.Schema(
  {
    from: {
      type: String,
    },

    text: {
      type: String,
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
    },
  },
  { timestamps: true }
);

const complainModal = mongoose.model("complains", complainSchema);

module.exports = complainModal;
