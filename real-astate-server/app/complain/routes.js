const express = require("express");
const router = express.Router();

const { addComplain } = require("./controller");
const { checkToken } = require("../../middleware/checkToken");

router.use(checkToken);

router.post("/save", addComplain);

module.exports = router;
