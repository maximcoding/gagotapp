const addNumbersToDay = (arr, schDay, date) => {
  let result;
  switch (schDay) {
    case "sun":
      result = date.setDate(date.getDate() + arr[0]);
      return result;
    case "mon":
      result = date.setDate(date.getDate() + arr[1]);
      return result;
    case "tue":
      result = date.setDate(date.getDate() + arr[2]);
      return result;
    case "wed":
      result = date.setDate(date.getDate() + arr[3]);
      return result;
    case "thur":
      result = date.setDate(date.getDate() + arr[4]);
      return result;
    case "fri":
      result = date.setDate(date.getDate() + arr[5]);
      return result;
    case "sat":
      result = date.setDate(date.getDate() + arr[6]);
      return result;
  }
};
module.exports.scheduleTime = (day, time) => {
  let timeAdd = time == "open" ? "13:00" : time;
  let calTime = timeAdd.split(":");
  var days = ["sun", "mon", "tue", "wed", "thur", "fri", "sat"];
  var d = new Date();
  var dayName = days[d.getDay()];

  let resultDate;

  switch (dayName) {
    case "sun":
      resultDate = addNumbersToDay([0, 1, 2, 3, 4, 5, 6], day, d);

      break;
    case "mon":
      resultDate = addNumbersToDay([6, 0, 1, 2, 3, 4, 5], day, d);
      console.log("date of calaulation is here", resultDate);
      break;
    case "tue":
      resultDate = addNumbersToDay([5, 6, 0, 1, 2, 3, 4], day, d);
      break;
    case "wed":
      resultDate = addNumbersToDay([4, 5, 6, 0, 1, 2, 3], day, d);
      break;
    case "thur":
      resultDate = addNumbersToDay([3, 4, 5, 6, 0, 1, 2], day, d);
      break;
    case "fri":
      resultDate = addNumbersToDay([2, 3, 4, 5, 6, 0, 1], day, d);
      break;
    case "sat":
      resultDate = addNumbersToDay([1, 2, 3, 4, 5, 6, 0], day, d);
      break;
  }
  let result = new Date(resultDate).setHours(
    Number(calTime[0]) - 1,
    Number(calTime[1])
  );
  return result;
};
