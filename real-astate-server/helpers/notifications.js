var axios = require("axios");

const sendRemoteNotifications = (data, device = "android") => {
  let appId =
    device == "android"
      ? "98174422-fea1-42f9-9eda-68ecb3206f0b"
      : "ff8478cd-2296-4ca8-948f-f8b7297a7777";

  let token =
    device == "android"
      ? "Njc5NzFmYzktZWUwYS00YTcxLWFmMTktYTQ3MGIwMmYwZDlk"
      : "MjViNzFmNTctYjk2YS00N2Y2LTk2OWQtNmM1ZDc0ZjZmMGUx";

  let template =
    device == "android"
      ? "8d9f0467-190f-4c76-9a97-05cccaf758cc"
      : "6189a567-7178-4ee8-8f6a-5e4b5694434d";

  var message = {
    app_id: appId,
    contents: { en: data.description },
    headings: { en: data.title },
    template_id: template,
    include_player_ids: [data.notificationId],
    ...(device == "android" && {
      android_channel_id: "a1d8c489-4c27-40cb-b2e2-a7b2dabd7102",
    }),
    ...(device == "ios" && {
      ios_badgeType: "Increase",
      ios_badgeCount: 1,
    }),
    send_after: new Date(data.date),
  };

  axios
    .post("https://onesignal.com/api/v1/notifications", message, {
      headers: {
        Authorization: `Basic ${token}`,
      },
    })
    .then((res) => {
      console.log("Success occured for notification", res);
    })
    .catch((err) => {
      console.log("error is here", err);
    });
};

module.exports = { sendRemoteNotifications };
