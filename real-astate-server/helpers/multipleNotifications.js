const axios = require("axios");
module.exports.sendMultipleNotifications = (
  finalArray,
  title,
  description,
  image
) => {
  let promises = finalArray.map((ob) => {
    if (ob.ids.length == 0) {
      return;
    }
    console.log("user ids are here", ob.ids);
    let appId =
      ob.device == "android"
        ? "98174422-fea1-42f9-9eda-68ecb3206f0b"
        : "ff8478cd-2296-4ca8-948f-f8b7297a7777";

    let token =
      ob.device == "android"
        ? "Njc5NzFmYzktZWUwYS00YTcxLWFmMTktYTQ3MGIwMmYwZDlk"
        : "MjViNzFmNTctYjk2YS00N2Y2LTk2OWQtNmM1ZDc0ZjZmMGUx";

    let template =
      ob.device == "android"
        ? "8d9f0467-190f-4c76-9a97-05cccaf758cc"
        : "6189a567-7178-4ee8-8f6a-5e4b5694434d";

    var message = {
      app_id: appId,
      contents: { en: description },
      headings: { en: title },
      template_id: template,
      include_player_ids: ob.ids,

      ...(ob.device == "android" &&
        image && {
          big_picture: image,
        }),

      ...(ob.device == "ios" &&
        image && {
          ios_attachments: { id: image },
        }),

      ...(ob.device == "android" && {
        android_channel_id: "a1d8c489-4c27-40cb-b2e2-a7b2dabd7102",
      }),
      ...(ob.device == "ios" && {
        ios_badgeType: "Increase",
        ios_badgeCount: 1,
      }),
    };
    let res = axios.post(
      "https://onesignal.com/api/v1/notifications",
      message,
      {
        headers: {
          Authorization: `Basic ${token}`,
        },
      }
    );

    return res;
  });

  return promises;
};
