const jwt = require("jsonwebtoken");
module.exports.generateJWT = (user) => {
  const token = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: "30d",
  });

  return token;
};
