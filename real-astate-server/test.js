var faker = require("faker");
const bcrypt = require("bcrypt");
var geohash = require("ngeohash");

const { createApi } = require("unsplash-js");
const nodeFetch = require("node-fetch");

const unsplash = createApi({
  accessKey: "pEnz6WAwQIj2z_UjihVPnREdEdl7o41JeLpr3_6TzKo",
  fetch: nodeFetch,
});



const userSchema = require("./app/auth/schema");
const categorySchema = require("./app/categories/schema");
const listSchema = require("./app/properties/schema");
const { dbConnection } = require("./config/dbConnection");
const cons = require("consolidate");
require("dotenv").config();

dbConnection();

const addEmailUsersAgents = async () => {
  try {
    let hash = await bcrypt.hash("max12345", 10);
    let users = [];
    for (let i = 0; i < 250; i++) {
      users.push({
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        password: hash,
        phone: faker.phone.phoneNumber(),
        photoURI: faker.internet.avatar(),
        type: "agent",
        country: "Israel",
        unit: "meter",
        currency: "sign",
        device: "android",
        appIdentifier: "rn-real-estate-android",
        companyID: faker.random.number(),
        address: faker.address.streetAddress(),
      });
    }
    console.log("fake agents are here", users);
    await userSchema.insertMany(users);
  } catch (error) {
    console.log("error is here", error);
  }
};

const addEmailUsers = async () => {
  try {
    let hash = await bcrypt.hash("max12345", 10);
    let users = [];
    for (let i = 0; i < 250; i++) {
      users.push({
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        password: hash,
        phone: faker.phone.phoneNumber(),
        photoURI: faker.internet.avatar(),
        appIdentifier: "rn-real-estate-android",
        type: "user",
        country: "Israel",
        unit: "meter",
        currency: "sign",
        device: "android",
      });
    }
    console.log("fake agents are here", users);
    await userSchema.insertMany(users);
  } catch (error) {
    console.log("error is here", error);
  }
};

const addProperties = async () => {
  let photos = await unsplash.search.getPhotos({
    query: "house",
    page: 1,
    perPage: 500,
  });

  let images = photos.response.results.map((item) => {
    return item.urls.regular;
  });

  let list = [];
  let users = await userSchema.find({ type: "agent" }).lean();
  let categories = await categorySchema.find({}).lean();

  let types = ["buy", "rent"];
  try {
    for (let i = 0; i < 250; i++) {
      let latitude = faker.address.latitude();
      let longitude = faker.address.longitude();
      let type = types[Math.floor(Math.random() * 1)];
      list.push({
        authorID: users[i]._id,
        authorType: "agent",
        bathroom: faker.random.number({ min: 1, max: 4 }),
        bedroom: faker.random.number({ min: 1, max: 4 }),
        built: ">2000",
        categoryID: categories[Math.floor(Math.random() * 5)]._id,
        ...(type == "buy" && {
          deposit: faker.random.number({ min: 10, max: 30 }),
        }),
        description: faker.lorem.text(),
        floors: faker.random.number({ min: 1, max: 4 }),
        latitude: latitude,
        longitude: longitude,
        coordinate: [latitude, longitude],
        newConstruction: "Yes",
        photo: images[Math.floor(Math.random() * 30)],
        photoURLs: [
          images[Math.floor(Math.random() * 30)],
          images[Math.floor(Math.random() * 30)],
          images[Math.floor(Math.random() * 30)],
          images[Math.floor(Math.random() * 30)],
        ],
        videoURLs: [
          "https://vimeo.com/487796860/f53ad6d25b",
          "https://vimeo.com/487796860/f53ad6d25b",
        ],
        place: faker.address.streetAddress(),
        price: faker.random.number({ min: 1000, max: 100000 }),
        square: faker.random.number({ min: 1, max: 150 }),
        title: faker.name.title(),
        type: type,
        agentID: users[i].companyID,
        agentPhone: users[i].phone,
        geoHash: geohash.encode(latitude, longitude),
        open_doors: {
          mon: "open",
          tue: "open",
          wed: "open",
          thurs: "open",
          fri: "open",
          sat: "closed",
          sun: "closed",
        },
      });
    }
    console.log("list to be added is here", list);
    await listSchema.insertMany(list);
  } catch (error) {
    console.log("error is here", error);
  }
};

//addEmailUsers();
// addEmailUsersAgents();
addProperties();


