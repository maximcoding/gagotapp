const jwt = require("jsonwebtoken");
const userSchema = require("../app/auth/schema");

module.exports.checkToken = async (req, res, next) => {
    
    let token = req.headers["x-access-token"] || req.headers["authorization"];
    let message = req.url == "/authenticate" ? {} : "Unauthorized";
    if (token == null) return res.status(401).send(message);
    if (token.startsWith("Bearer ")) {
        token = token.slice(7, token.length);
    }
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, async (err, user) => {
        if (err) {
            return res.status(403).send(message);
        }
        let newUser = await userSchema.findOne({_id: user._id});
        req.user = newUser;
        next();
    });
};
