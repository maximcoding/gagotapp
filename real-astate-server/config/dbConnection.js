const mongoose = require("mongoose");
const chalk = require('chalk');

function dbConnection() {

    console.log(chalk.yellowBright('mongoose connecting %s'), process.env.DB_URI);

    mongoose.connect(
        `${process.env.DB_URI}`,

        {useNewUrlParser: true, useFindAndModify: false},
        function (err) {
            if (err) {
                console.log(chalk.redBright('mongoose error %s'), err);
            } else {
                console.log(chalk.greenBright('mongoose connected %s'), process.env.DB_URI);
            }
        }
    );
}

module.exports = {dbConnection};
