const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const cors = require("cors");
const chalk = require('chalk');
const listEndpoints = require('express-list-endpoints')

const cookieparser = require("cookie-parser");

require("dotenv").config();

//import from custom files
const userRoute = require("./app/auth/routes");
const stateRoute = require("./app/properties/routes");
const complainRout = require("./app/complain/routes");
const categoriesRoute = require("./app/categories/routes");
const recentListingRoute = require("./app/recentListing/routes");
const reviewRoute = require("./app/reviews/routes");
const enrollRoute = require("./app/enrollment/routes");
const adminRoute = require("./app/admin/routes");

// db connection
const {dbConnection} = require("./config/dbConnection");

//app create for server
const app = express();

const PORT = process.env.PORT || 5000;

//add middleware
app.use(cors());
// logger
app.use(morgan("tiny"));
app.use(cookieparser());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.get("/ping", (req, res) => {
    const time = new Date().toUTCString();
    console.log(chalk.yellow('server ping check #s', time));
    res.status(200).send("Server Time: " + time);
});

//db connection
dbConnection();

// user routes

app.use("/user", userRoute);
app.use("/properties", stateRoute);
app.use("/complain", complainRout);
app.use("/categories", categoriesRoute);
app.use("/recent", recentListingRoute);
app.use("/reviews", reviewRoute);
app.use("/enroll", enrollRoute);
app.use("/admin", adminRoute);

let route, routes = [];

app.listen(PORT, function () {
    console.log(listEndpoints(app));
    console.log(chalk.greenBright('express server running on port %s'), PORT);
});

// in case of an error
app.on("error", (appErr, appCtx) => {
    console.log(chalk.redBright('app error %s'), appErr.stack);
    console.log(chalk.redBright('on url %s'), appCtx.req.url);
    console.log(chalk.redBright('with headers %s'), appCtx.req.headers);
});
