//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Sohaib Ali on 09/12/2020.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
