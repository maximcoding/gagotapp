import React, {useEffect} from 'react';
import {Text} from 'react-native';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {AppRegistry, PermissionsAndroid, Platform} from 'react-native';
import configureStore from './redux/store';
import {RootNavigator} from './navigation/AppNavigator';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import AsyncStorage from '@react-native-community/async-storage';
import {
  setI18nConfig,
  setI18nConfig2,
  translationGetters,
} from './core/i18n/IMLocalization';
import OneSignal from 'react-native-onesignal';
import {enabledConfig} from '../src/utils/notificationHelper';
import {getNotificationId} from '../src/redux/actions/user';
import * as RNLocalize from 'react-native-localize';

let {store, persistor} = configureStore();

const App = () => {

  
  const getStarter = async () => {
    const {autoDetectLanguage} = store.getState().auth;
    if (autoDetectLanguage) {
      // get current mobile language and use it
      const {languageTag, isRTL} = RNLocalize.findBestAvailableLanguage(
        Object.keys(translationGetters),
      );
      await AsyncStorage.setItem('language', languageTag);
      setI18nConfig(languageTag);
    } else {
      let code = await AsyncStorage.getItem('language');
      let languageCode = code ? code : 'en';
      setI18nConfig(languageCode);
    }
  };

  const onIds = (device, dispatch, Func) => {
    if (device.userId) {
      dispatch(Func(device.userId));
    }
  };

  const onReceived = () => {};

  const onOpened = () => {};

  const getPermissions = async () => {
    try {
      if (Platform.OS == 'ios') {
        return true;
      } else {
        const permission = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );
        if (permission === PermissionsAndroid.RESULTS.GRANTED) {
          return true;
        } else {
          await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          );
        }
      }
    } catch (error) {
      getPermissions();
    }
  };

  const getImagePermission = async () => {
    try {
      if (Platform.OS == 'ios') {
      } else {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          return true;
        } else {
          await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
          );
        }
      }
    } catch (err) {
      getImagePermission();
    }
  };

  const handleLocalizationChange = () => {
    setI18nConfig2();
  };

  useEffect(() => {
    enabledConfig(
      OneSignal,
      Platform,
      onIds,
      onReceived,
      onOpened,
      store.dispatch,
      getNotificationId,
    );
    getStarter();
    getPermissions();
    RNLocalize.addEventListener('change', handleLocalizationChange);
    return () =>
      RNLocalize.removeEventListener('change', handleLocalizationChange);
  }, []);

  console.disableYellowBox = true;
  return (
    <Provider store={store}>
      <PersistGate loading={<Text></Text>} persistor={persistor}>
        <SafeAreaProvider>
          <RootNavigator />
        </SafeAreaProvider>
      </PersistGate>
    </Provider>
  );
};

App.propTypes = {};
App.defaultProps = {};
AppRegistry.registerComponent('App', () => App);

export default App;
