import {Platform, StyleSheet, Dimensions, I18nManager} from "react-native";

const {width, height} = Dimensions.get("window");
const SCREEN_WIDTH = width < height ? width : height;

const numColumns = 2;
import {Configuration} from "./Configuration";
import {IS_ANDROID} from "./utils/statics";
import {Colors, Fonts} from "./themes";
import {widthToDp, heightToDp} from './themes/responsive'

export const AppStyles = {
    navThemeConstants: {
        light: {
            backgroundColor: "#fff",
            fontColor: "#000",
            activeTintColor: "#3875e8",
            inactiveTintColor: "#ccc",
            hairlineColor: "#e0e0e0",
        },
        dark: {
            backgroundColor: "#000",
            fontColor: "#fff",
            activeTintColor: "#3875e8",
            inactiveTintColor: "#888",
            hairlineColor: "#222222",
        },
        main: "#3875e8",
    },
    headerOptionsContainer: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: 50,
        paddingVertical: 2,
        paddingHorizontal: 10,
        width: width
    },
    screenContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        width: "100%",
        height: "100%",
        backgroundColor: Colors.backgroundColor,
    },
    screenTransparent: {
        width: "100%",
        height: "100%",
    },
    orTextStyle: {
        color: Colors.black,
        fontSize: Fonts.size.large,
        marginVertical: 30,
        alignSelf: "center",
    },
    tos: {
        alignItems: "center",
        justifyContent: "center",
        height: 40,
    },
    facebookContainer: {
        width: "70%",
        backgroundColor: Colors.facebook,
        borderRadius: 25,
        marginTop: 20,
        alignSelf: "center",
        // padding: 10,
    },
    facebookText: {
        color: Colors.white,
    },
    inputContainer: {
        textAlign: I18nManager.isRTL ? "right" : "left",
        borderBottomWidth: 1,
        borderColor: Colors.border,
        color: Colors.primary,
        // padding: 10,
        width: "70%",
        backgroundColor: Colors.inputColor,
        borderRadius: 4,
        height: 56,
        fontSize: Fonts.size.large,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
    },
    inputBorderContainer: {
        textAlign: I18nManager.isRTL ? "right" : "left",
        borderWidth: 1,
        borderColor: Colors.border,
        color: Colors.primary,
        // padding: 10,
        width: "70%",
        backgroundColor: Colors.inputColor,
        borderRadius: 4,
        height: 56,
        paddingLeft: 10,
        fontSize: Fonts.size.large,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
    },
    buttonInverseContainer: {
        width: "70%",
        borderRadius: 8,
        marginTop: 10,
        height: 56,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        borderColor: Colors.primary,
        borderWidth: 1,
    },
    buttonContainer: {
        backgroundColor: Colors.primary,
        width: "70%",
        borderRadius: 8,
        marginTop: 10,
        height: 56,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
    },
    textInputWidth: "80%",
    pickerSelectStyles: StyleSheet.create({
        inputIOS: {
            height: 33,
            fontSize: Fonts.size.regular,
            borderRadius: 4,
            color: Colors.black
        },
        inputAndroid: {
            fontSize: Fonts.size.regular,
            paddingHorizontal: 10,
            paddingVertical: 8,
            color: Colors.black
        },


    }),
    pickerSelectStyles2: StyleSheet.create({
        inputIOS: {
            height: 60,
            fontSize: Fonts.size.regular,
            borderRadius: 4,
            color: Colors.black
        },
        inputAndroid: {
            fontSize: Fonts.size.regular,
            paddingHorizontal: 10,
            paddingVertical: 8,
            color: Colors.black
        },


    }),
    menuBtn: {
        container: {
            backgroundColor: Colors.backgroundColor,
            borderRadius: 22.5,
            // padding: 10,
            marginLeft: 10,
            marginRight: 10,
        },
        icon: {
            tintColor: Colors.black,
            width: 15,
            height: 15,
        },
    },
    searchBar: {
        container: {
            marginLeft: Platform.OS === "ios" ? 30 : 0,
            backgroundColor: "transparent",
            borderBottomColor: "transparent",
            borderTopColor: "transparent",
            flex: 1,
        },
        input: {
            backgroundColor: Colors.backgroundColor,
            borderRadius: 10,
            color: "black",
        },
    },
    rightNavButton: {
        marginRight: 10,
    },
    borderRadius: {
        main: 25,
        small: 5,
    },
    navigationIcon: {
        height: 15,
        width: 15,
        tintColor: Colors.grey9,
        transform: [{scaleX: I18nManager.isRTL ? -1 : 1}],
    },
    backArrowStyle: {
        resizeMode: "contain",
        tintColor: "rgba(90, 100, 200, 1.0)",
        width: 20,
        height: 20,
        marginTop: Platform.OS === "ios" ? 50 : 20,
        marginLeft: 10,
        transform: [{scaleX: I18nManager.isRTL ? -1 : 1}],
    },
    placeholderTextColor: Colors.placeholder,
    text: {
        headerTitleStyle: {
            fontWeight: "bold",
            fontSize: Fonts.style.title,
            alignSelf: "center"
        },
        headerTitle: {
            fontWeight: "bold",
            fontSize: Fonts.style.title,
            alignSelf: "center",

        },
        homeHeaderTitle: {
            position: 'absolute',
            fontWeight: "bold",
            fontSize: Fonts.style.title,
            alignSelf: "center",

        },
        headerButton: {
            backgroundColor: Colors.transparent,
            color: Colors.green,
            fontWeight: "300",
            fontSize: Fonts.size.normal,
            fontFamily: "Arial",
        },
        modalTitle: {
            fontWeight: "bold",
            fontSize: Fonts.style.title,
        },
        listTitle: {
            fontFamily: "Arial",
            fontSize: Fonts.size.regular,
            color: Colors.title,
        },
        listValue: {
            fontFamily: "Arial",
            textTransform: "capitalize",
            fontSize: Fonts.size.regular - 1,
            opacity: 0.6,
        },
        title: {
            fontFamily: "Arial",
            fontWeight: "bold",
            color: Colors.title,
            fontSize: Fonts.size.xxxlarge,
        },
        sectionTitle: {
            fontFamily: "Arial",
            paddingVertical: 10,
            fontWeight: "bold",
            textTransform: "capitalize",
            color: Colors.title,
            fontSize: Fonts.style.title,
            paddingHorizontal: 10,


        },
        avatar: {
            width: 50,
            height: 50,
            borderRadius: 50,

        },
        agentCont: {
            flexDirection: "row",
            paddingHorizontal: 25
        },

        agentDetail: {
            paddingLeft: 10,
            paddingTop: 5
        },

        subTitle: {
            fontFamily: "Arial",
            paddingTop: 10,
            paddingBottom: 20,
            color: Colors.subTitle,
            fontSize: Fonts.style.subTitle,
        },
        description: {
            fontFamily: "Arial",
            fontSize: Fonts.size.normal,
            opacity: 0.6,
        },
        buttonText: {
            color: Colors.white,
            fontSize: Fonts.size.large
        },
        buttonInverseText: {
            color: Colors.primary,
            fontSize: Fonts.size.large,
        },
        buttonInverse: {
            fontSize: Fonts.size.large - 2,
        },
        placeholder: {
            fontSize: Fonts.size.regular,
            fontFamily: "Arial",
            color: Colors.black,
        },
        input: {
            fontSize: Fonts.size.regular,
            color: Colors.title,
        },
    },
    logo: {
        marginTop: 80,
        marginBottom: '15%',
        width: widthToDp(30),
        height: heightToDp(15),
        alignSelf: "center",
    },
    logoImage: {
        width: "100%",
        height: "100%",
        resizeMode: "contain",
    },
    tabStyles: {
        tabBar: {
            flexDirection: 'row',
            borderBottomWidth: 1,
            borderBottomColor: 'rgb(197, 198, 202)',
            marginBottom: 20,
            marginHorizontal: 20
        },
        tabItem: {
            flex: 1,
            alignItems: 'center',
            paddingVertical: 12,
        },
        tabItemActive: {
            flex: 1,
            alignItems: 'center',
            paddingVertical: 12,
            borderBottomWidth: 2,
            borderBottomColor: Colors.primary,
        },
        tabItemTxt: {
            fontSize: Fonts.size.normal,
            color: 'rgb(197, 198, 202)',
        },
        tabItemTxtActive: {
            fontSize: Fonts.size.normal,
            color: Colors.primary,
        },
    }
};

export const AppIcon = {
    container: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 8,
        marginRight: 10,
    },
    containerMapBtn: {
        backgroundColor: "white",
        borderRadius: 20,
        paddingLeft: 8,
        paddingRight: 8,
        marginRight: 10,
    },
    containerAddListingBtn: {
        backgroundColor: "white",
        borderRadius: 20,
        paddingLeft: 8,
        paddingRight: 8,
    },
    style: {
        tintColor: Colors.green,
        width: 25,
        height: 25,
    },
};

export const HeaderButtonStyle = StyleSheet.create({
    headerButtonContainer: {
        justifyContent: "flex-end",
        flexDirection: "row",
        width: "100%",
        paddingHorizontal: 20,
    },
    container: {
        // padding: 10,
    },
    image: {
        justifyContent: "center",
        width: 35,
        height: 35,
        margin: 6,
    },
    rightButton: {
        color: Colors.tint,
        marginRight: 10,
        fontWeight: "normal",
        fontFamily: Fonts.family.main,
    },
    main: {
        flexDirection: "row",
        alignItems: "baseline",
        width: '100%',
        justifyContent: "space-between",
        paddingHorizontal: 20,
        backgroundColor: Colors.backgroundColor,
        paddingTop: 10,
        height: 50,
    },
    title: {
        fontSize: Fonts.style.title,
    },
});
export const ListStyle = StyleSheet.create({
    title: {
        marginLeft: 10
    },
    subtitleView: {
        minHeight: 55,
        flexDirection: "row",
        paddingTop: 2,
        marginLeft: 10,
    },
    leftSubtitle: {
        flex: 2,
    },
    address: {
        marginLeft: '1%',
        padding: 2,
        paddingLeft: '1%',
        color: Colors.primary
    },
    price: {},
    time: {
        marginLeft: 10
    },
    avatarStyle: {
        height: 120,
        width: 120,
        marginLeft: 5,
    },
    itemContainer: {
        flex: 1,
        width: "100%",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
    },
});
export const TwoColumnListStyle = {
    listings: {
        marginTop: 15,
        width: "100%",
        flex: 1,
    },
    showAllButtonContainer: {
        borderWidth: 1,
        borderRadius: 5,
        borderColor: Colors.border,
        height: 50,
        width: "100%",
        marginBottom: 30,
    },
    showAllButtonText: {
        // padding: 10,
        textAlign: "center",
        color: Colors.border,
        fontFamily: Fonts.family.main,
        justifyContent: "center",
    },
    listingItemContainer: {
        justifyContent: "center",
        marginBottom: 20,
        marginRight: Configuration.home.listing_item.offset,
        width: (SCREEN_WIDTH - Configuration.home.listing_item.offset * 3) / numColumns,
        borderRadius: 6,
        paddingBottom: 10,
        backgroundColor: "white",
        shadowColor: Colors.shadow,
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
        borderWidth: 1,
        borderColor: Colors.border,
    },
    photo: {
        // position: "absolute",
    },
    listingPhoto: {
        width: "100%",
        height: Configuration.home.listing_item.height,
        borderRadius: 5,
    },
    savedIcon: {
        position: "absolute",
        top: Configuration.home.listing_item.saved.position_top,
        left:
            (SCREEN_WIDTH - Configuration.home.listing_item.offset * 3) / numColumns -
            Configuration.home.listing_item.offset -
            Configuration.home.listing_item.saved.size,
        width: Configuration.home.listing_item.saved.size,
        height: Configuration.home.listing_item.saved.size,
    },
    listingView: {
        height: 60,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingHorizontal: 10,
        borderBottomWidth: 1,
        borderBottomColor: Colors.border,
        backgroundColor: Colors.backgroundColor,
    },
    listingDescription: {
        fontSize: Fonts.size.normal,
        fontWeight: "500",
        color: "rgba(0,0,0,0.5)",
    },
    listingPlace: {
        paddingHorizontal: 10,
        fontFamily: Fonts.family.main,
        fontSize: Fonts.size.normal,
        marginTop: 6,
    },
};
export const ModalSelectorStyle = {
    optionTextStyle: {
        fontSize: Fonts.size.normal,
        fontFamily: Fonts.family.main,
    },
    selectedItemTextStyle: {
        fontSize: Fonts.size.normal,
        fontFamily: Fonts.family.main,
        fontWeight: "bold",
    },
    optionContainerStyle: {
        backgroundColor: Colors.white,
    },
    cancelContainerStyle: {
        backgroundColor: Colors.white,
        borderRadius: 10,
    },
    sectionTextStyle: {
        fontSize: Fonts.size.normal,
        color: Colors.title,
        fontFamily: Fonts.family.main,
        fontWeight: "bold",
    },

    cancelTextStyle: {
        fontSize: Fonts.size.normal,
        fontFamily: Fonts.family.main,
    },
};
export const ModalHeaderStyle = {
    bar: {},
    text: {
        title: {
            fontSize: Fonts.size.regular,
            alignSelf: "center",
            fontWeight: "bold",
        },
    },
};
