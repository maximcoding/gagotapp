export const onRefresh = (params, setPage, setFetching, dispatch, Func) => {
  const stopRefresher = () => {
    setFetching(false);
  };
  setPage(0);
  setFetching(true);
  dispatch(Func(stopRefresher, 0, params));
};

export const getFunctionHelper = (params, page, setLoader, dispatch, Func) => {
  const stopLoader = () => {
    setLoader(false);
  };
  setLoader(true);
  dispatch(Func(stopLoader, page, params));
};

export const loadMore = (
  params,
  moreLoader,
  page,
  setMoreLoader,
  setPage,
  dispatch,
  Func,
) => {
  const stopLoaderMore = () => {
    setMoreLoader(false);
  };
  if (moreLoader) {
    return;
  }
  let newPage = page + 1;
  setMoreLoader(true);
  setPage(newPage);
  dispatch(Func(stopLoaderMore, newPage, params, true));
};
