import { DynamicValue } from "react-native-dark-mode";
import invert from "invert-color";

export const DynamicColor = (hexStringColor) => {
  return new DynamicValue(hexStringColor, invert(hexStringColor));
};
