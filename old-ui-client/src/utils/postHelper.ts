import geohash from 'ngeohash';
import {postListing, uploadImage} from '../redux/actions/postListing';
import {convertIntoDollar, convertintoMeter} from '../utils/currencyConverter';
import {Translate} from '../core/i18n/IMLocalization';
import {IProperty, PropertyType} from '../types/property';

export const getInitialState = (categories, selectedItem) => {
    let categoryData = categories.map((category) => ({
        key: category._id,
        label: category.name,
    }));

    if (selectedItem) {
        let {
            title,
            latitude,
            longitude,
            photoURLs,
            videoURLs,
            description,
            place,
            floors,
            price,
            phone,
            agentID,
            built,
            newConstruction,
            square,
            bathroom,
            bedroom,
            deposit,
            open_doors,
            type,
        } = selectedItem;

        let category = categories.find(
            (category) => selectedItem.categoryID === category._id,
        );
        return {
            categories: categoryData,
            category: category,
            title,
            noFloor: floors,
            description,
            location: {
                latitude,
                longitude,
            },
            photoURLs: photoURLs,
            videoURLs: videoURLs ? videoURLs : [],
            price,
            formattedPrice: price,
            agentID,
            phone,
            open_doors,
            bedroom: bedroom,
            bathroom: bathroom,
            square,
            newConstruction,
            built,
            address: place,
            locationModalVisible: false,
            loading: false,
            deposit,
            type,
            selectedPhotoIndex: 0,
        };
    } else {
        return {
            categories: categoryData,
            category: {name: 'Select...'},
            title: '',
            noFloor: '0',
            description: '',
            location: {
                latitude: 31.5204,
                longitude: 74.3587,
            },
            open_doors: {},
            photoURLs: [],
            videoURLs: [],
            agentID: '',
            price: '0',
            formattedPrice: '0',
            phone: '',
            bedroom: [0],
            bathroom: [0],
            square: '0',
            newConstruction: 'No',
            built: '>2000',
            address: '',
            locationModalVisible: false,
            loading: false,
            deposit: '0',
            type: 'rent',
            selectedPhotoIndex: 0,
        };
    }
};

export const typesData = (Translate) => {
    return [
        {
            label: Translate('Buy'),
            value: 'buy',
        },
        {
            label: Translate('Rent'),
            value: 'rent',
        },
    ];
};

export const newConTypes = [
    {label: 'Yes', value: 'Yes'},
    {label: 'No', value: 'No'},
];

export const conDateTypes = [
    {label: '<2000', value: '<2000'},
    {label: '>2000', value: '>2000'},
];

export const onPost = async (
    state,
    setState,
    ServerConfig,
    user,
    onCancel,
    selectedItem,
    currencyRates,
) => {

    if (!state.title) {
        alert(Translate('Title was not provided.'));
        return;
    } else if (!state.description) {
        alert(Translate('Description was not set.'));
        return;
    } else if (!state.phone) {
        alert('Agent Phone is Required');
        return;
    } else if (!state.agentID) {
        alert('Agent ID is Required');
        return;
    } else if (!state.type) {
        alert(Translate('Type was not set.'));
        return;
    } else if (!state.formattedPrice) {
        alert(Translate('Price is empty.'));
        return;
    } else if (!state.square) {
        alert(Translate('square in feets was not set.'));
        return;
    } else if (state.category.name == 'select...') {
        alert(Translate('Category was not set.'));
        return;
    } else if (state.type == PropertyType.Buy && !state.deposit) {
        alert(Translate('Deposit was not set.'));
        return;
    } else if (!state.bathroom) {
        alert(Translate('Bathrooms was not set.'));
        return;
    } else if (!state.bedroom) {
        alert(Translate('Bedrooms was not set.'));
        return;
    } else if (state.photoURLs.length == 0) {
        alert(Translate('Please choose at least one photo.'));
        return;
    } else if (Object.keys(state.open_doors).length == 0) {
        alert('Must select Open doors events timing');
        return;
    } else {
        setState({...state, loading: true});
        const hash = geohash.encode(
            state.location.latitude,
            state.location.longitude,
        );

        try {
            const uploadObject: IProperty = {
                isApproved: !ServerConfig.isApprovalProcessEnabled,
                authorID: user._id,
                categoryID: state.category.id,
                description: state.description,
                latitude: state.location.latitude,
                longitude: state.location.longitude,
                geoHash: hash,
                title: state.title,
                floors: state.noFloor,
                bedroom: state.bedroom,
                bathroom: state.bathroom,
                square: convertintoMeter(Number(state.square), user),
                type: state.type,
                ...(state.type == PropertyType.Buy && {deposit: state.deposit}),
                newConstruction: state.newConstruction,
                built: state.built,
                price: convertIntoDollar(Number(state.price), user, currencyRates),
                place: state.address,
                phone: state.phone,
                agentID: state.agentID,
                photo: state.photoURLs.length > 0 ? state.photoURLs[0] : null,
                photoURLs: state.photoURLs,
                videoURLs: state.videoURLs,
                open_doors: state.open_doors,
                delete: false,
            };
            const callBack = (success) => {
                setState({...state, loading: false});
                if (success) {
                    onCancel();
                }
            };
            postListing(selectedItem, uploadObject, callBack);
        } catch (error) {
            // console.log('error hiwl upload', error);
            setState({...state, loading: false});
            alert(error);
        }
    }
};

export const getDay = (val) => {
    switch (val) {
        case 'mon':
            return 'Monday';
        case 'tue':
            return 'Tuesday';
        case 'wed':
            return 'Wednesday';
        case 'thurs':
            return 'Thursday';
        case 'fri':
            return 'Friday';
        case 'sat':
            return 'Saturday';
        case 'sun':
            return 'Sunday';
    }
};
