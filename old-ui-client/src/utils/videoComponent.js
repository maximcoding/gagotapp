import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import PlayVideoIcon from '../../assets/icons/play_button.svg';
import RightArrowIcon from '../../assets/icons/nextArrow.svg';

const VideoViewComponent = ({onPress, item, index}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View paddingVertical={14}>
        <PlayVideoIcon width={50} height={50} />
      </View>

      <View paddingVertical={14} paddingHorizontal={8} flex={1}>
        <Text
          fontSize={16}
          color="#000"
          fontFamily="CircularStd-Medium"
          textTransform="capitalize">
          {item.title} ({index + 1})
        </Text>
      </View>

      <TouchableOpacity paddingVertical={14} paddingHorizontal={24}>
        <RightArrowIcon />
      </TouchableOpacity>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 73,
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(197, 198, 202)',
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
});

export default VideoViewComponent;
