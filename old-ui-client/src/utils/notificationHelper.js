export const enabledConfig = (
  OneSignal,
  Platform,
  onIds,
  onReceived,
  onOpened,
  dispatch,
  getNotificationId,
) => {
  OneSignal.init(
    Platform.select({
      android: '98174422-fea1-42f9-9eda-68ecb3206f0b',
      ios: 'ff8478cd-2296-4ca8-948f-f8b7297a7777',
    }),
    {
      kOSSettingsKeyAutoPrompt: true,
    },
  );
  OneSignal.inFocusDisplaying(5);
  OneSignal.addEventListener('ids', (device) =>
    onIds(device, dispatch, getNotificationId),
  );
  OneSignal.addEventListener('received', onReceived);
  OneSignal.addEventListener('opened', onOpened);
};

// com.dhdglobal.NhaSpa
