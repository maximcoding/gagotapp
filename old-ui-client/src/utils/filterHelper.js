export const selectorData2 = (categories) => {
    return [
        {
            title: "Categories",
            name: "category",
            data: categories ? categories.map((c) => {
                return {label: c.name, value: c._id}
            }) : [],
            defaultValue: ''
        },
        {
            title: "New Construction",
            name: "newConstruction",
            data: [
                {label: "Yes", value: "yes"},
                {label: "No", value: "no"},
            ],
            defaultValue: "yes"
        }
        // {
        //     title: "Year Built",
        //     name: "built",
        //     data: [
        //         {label: "<2000", value: "<2000"},
        //         {label: ">2000", value: ">2000"},
        //     ],
        // },
    ];
};
