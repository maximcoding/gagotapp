import moment from "moment";
import { Translate } from "../translations";

export const dateFormat = (timeStamp) => {
  if (timeStamp) {
    if (moment(timeStamp).isValid()) {
      return "";
    }
    if (moment().diff(moment.unix(timeStamp.seconds), "days") == 0) {
      return moment.unix(timeStamp.seconds).format("H:mm");
    }
    return moment.unix(timeStamp.seconds).fromNow();
  }
  return "";
};

const monthNames = [
  Translate("Jan"),
  Translate("Feb"),
  Translate("Mar"),
  Translate("Apr"),
  Translate("May"),
  Translate("Jun"),
  Translate("Jul"),
  Translate("Aug"),
  Translate("Sep"),
  Translate("Oct"),
  Translate("Nov"),
  Translate("Dec"),
];

export const dateTimestamp = (timestamp) => {
  if (timestamp) {
    let time = moment(timestamp.toDate());
    if (moment().diff(time, "days") == 0) {
      return time.format("H:mm");
    } else if (moment().diff(time, "week") == 0) {
      return time.fromNow();
    } else {
      return `${monthNames[timestamp.toDate().getMonth()]} ${time.format(
        "D, Y"
      )}`;
    }
  }
  return "";
};

export default dateTimestamp;
