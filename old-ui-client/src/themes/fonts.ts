import {fontToDp} from './responsive'
const type = {
  base: "Avenir-Book",
  bold: "Avenir-Black",
  emphasis: "HelveticaNeue-Italic",
};

const family = {
  main: "Avenir-Book", // iCielVAGRoundedNext-Regular
  bold: "bold", // iCielVAGRoundedNext-Bold
  emphasis: "HelveticaNeue-Italic",
  medium: "medium", // iCielVAGRoundedNext-Medium
  light: "light", // iCielVAGRoundedNext-Light
  demiBold: "demiBold", // iCielVAGRoundedNext-DemiBold
  lightItalic: "lightItalic", // iCielVAGRoundedNext-LightItalic
};

const size = {
  xxxlarge: fontToDp(22),
  xxlarge: fontToDp(20),
  xlarge: fontToDp(17),
  large: fontToDp(15),
  normal: fontToDp(14),
  medium: fontToDp(13),
  regular: fontToDp(12),
  small: fontToDp(10),
  tiny: fontToDp(8.5),
  
};

const style = {
  header: size.xlarge,
  title: size.xlarge,
  input: size.large,
  placeholder: size.normal,
  label: size.normal,
  button: size.normal,
  bigButton: size.large,
  smallButton: size.small,
  subTitle: size.normal,
  description: size.normal,
  key: size.regular,
  value: size.small,
};
const Fonts = { family, size, type, style };
export default Fonts;
