import moment from "moment";

export const Configuration = {
  homeConfig: {
    mainCategoryID: "aaPdF5Dskd3DrHMWV3SwLs",
    mainCategoryName: "Houses",
  },
  home: {
    tab_bar_height: 50,
    initial_show_count: 4,
    listing_item: {
      height: 100,
      offset: 10,
      saved: {
        position_top: 5,
        size: 25,
      },
    },
  },
  map: {
    origin: {
      latitude: 37.78825,
      longitude: -122.4324,
    },
    delta: {
      latitude: 0.0422,
      longitude: 0.0221,
    },
  },
};
