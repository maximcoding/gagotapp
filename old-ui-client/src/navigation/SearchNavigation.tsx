import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import SearchScreen from '../screens/SearchScreen';
import SelectFilters from '../screens/SelectFilters';
import {HeaderMode, StackStyles} from './AppNavigatorConfig';
import DetailsScreen from '../screens/DetailsScreen/DetailsScreen';
import DetailScreen from '../screens/DetailsScreen/DetailsScreen';

const Stack = createStackNavigator();

export const SearchNavigation = () => {
  return (
    <Stack.Navigator
      screenOptions={StackStyles}
      initialRouteName="Search"
      headerMode={HeaderMode.Float}>
      <Stack.Screen
        name="Search"
        component={SearchScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="FiltersScreen"
        component={SelectFilters}
        options={{headerShown: false}}
      />
      <Stack.Screen
        options={(params) => DetailsScreenOptions(params)}
        name="SearchDetail"
        component={DetailsScreen}
      />

      <Stack.Screen
        options={(params) => DetailsScreenOptions(params)}
        name="ListingProfileModalDetailsScreen"
        component={DetailScreen}
      />

    </Stack.Navigator>
  );
};
