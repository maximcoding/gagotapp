import React from 'react';
import DrawerContainer from '../components/DrawerContainer';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {HomeNavigation} from './HomeNavigation';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {SearchNavigation} from './SearchNavigation';
import {MyProfileNavigation} from './MyProfileNavigation';
import {InitialRouteParams} from './AppNavigatorConfig';
import {useSelector} from 'react-redux';
import Categories from '../screens/CategoryScreen/CategoriesScreen';

const BottomTab = createBottomTabNavigator();
const DrawerNav = createDrawerNavigator<any>();

export const Drawer = () => {
  let {updator} = useSelector((state) => state.languageReducer);
  return (
    <BottomTab.Navigator
      initialRouteName="Home"
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName = ``;
          switch (route.name.toString()) {
            case 'Messages':
            case 'Home':
              iconName = focused ? 'home' : 'home';
              break;
            case 'Search':
              iconName = focused ? 'search' : 'search';
              break;
            case 'Filter':
              iconName = focused ? 'search' : 'search';
              break;
            case 'Categories':
              iconName = focused ? 'list' : 'list';
              break;
            case 'Profile':
              return <Ionicons name={'md-person'} size={size} color={color} />;
            case 'Favorites':
              iconName = focused ? 'heart' : 'heart';
          }
          return <FontAwesome5 name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }}>
      <BottomTab.Screen name="Home" component={HomeNavigation} />
      <BottomTab.Screen name="Filter" component={SearchNavigation} />
      <BottomTab.Screen name="Categories" component={Categories} />
      <BottomTab.Screen name="Profile" component={MyProfileNavigation} />
    </BottomTab.Navigator>
  );
};
