import React, {useEffect, useRef, useState} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {LoadScreen} from '../core/onboarding/index';
import {Drawer} from './Drawer';
import {LoginNavigation} from './LoginNavigation';
import {NavigationContainer} from '@react-navigation/native';
import {
  HeaderMode,
  InitialRouteParams,
  StackStyles,
} from './AppNavigatorConfig';
import {getCurrencyPrices} from '../redux/actions/user';
import {useDispatch, useSelector} from 'react-redux';
import * as RootNavigation from './rootNavigation';
import {navigationRef} from './rootNavigation';
import {getUser} from '../redux/actions/user';
import linking from './linking'

const Stack = createStackNavigator();

const AppNavigator = () => {
  let dispatch = useDispatch();
  const isInitialMount = useRef(true);
  let [loader, setLoader] = useState(true);

  const {user} = useSelector((state) => state.auth);

  useEffect(() => {
    checkingCurrentUser();
    dispatch(getCurrencyPrices());
    setTimeout(() => {
      setLoader(false);
    }, 1500);
  }, []);

  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      if (user) {
        setLoader(false);
        RootNavigation.navigate('DrawerStack');
      } else {
        setLoader(false);
        RootNavigation.navigate('LoginStack', {
          appStyles: InitialRouteParams.appStyles,
          appConfig: InitialRouteParams.appConfig,
        });
      }
    }
  }, [user, loader]);

  const checkingCurrentUser = async () => {
    dispatch(getUser());
  };

  if (loader) {
    return <LoadScreen />;
  } else {
    return (
      <NavigationContainer ref={navigationRef} linking={linking}>
        <Stack.Navigator
          screenOptions={StackStyles}
          initialRouteName="DrawerStack"
          headerMode={HeaderMode.None}>
          {user ? (
            <Stack.Screen
              name="DrawerStack"
              component={Drawer}
              initialParams={InitialRouteParams}
            />
          ) : (
            <Stack.Screen
              name="LoginStack"
              component={LoginNavigation}
              initialParams={InitialRouteParams}
            />
          )}
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
};

export const RootNavigator = AppNavigator;
