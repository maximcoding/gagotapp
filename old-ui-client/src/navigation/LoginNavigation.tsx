import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
  LoginScreen,
  SignUpScreen,
  SmsAuthenticationScreen,
  WelcomeScreen,
} from '../core/onboarding';
import HomeScreen from '../screens/HomeScreen/HomeScreen';
import {
  HeaderMode,
  InitialRouteParams,
  ScreenMode,
  StackScreenOptions,
} from './AppNavigatorConfig';

const Stack = createStackNavigator();

export const LoginNavigation = () => {
  return (
    <Stack.Navigator
      screenOptions={({route, navigation}) => ({
        cardStyle: {},
        gestureEnabled: false,
        cardOverlayEnabled: false,
        headerTitleAlign: 'center'
      })}
      initialRouteName="Home"
      mode={ScreenMode.Card}
      headerMode={HeaderMode.Screen}>
      <Stack.Screen
        name="Welcome"
        component={WelcomeScreen}
        initialParams={InitialRouteParams}
        options={{headerShown: false}}
      />
      <Stack.Screen name="Home" component={HomeScreen} />

      <Stack.Screen
        name="Login"
        options={{...StackScreenOptions, headerShown: false}}
        component={LoginScreen}
        initialParams={InitialRouteParams}
      />
      <Stack.Screen
        name="SignUp"
        options={{...StackScreenOptions, headerShown: false}}
        component={SignUpScreen}
        initialParams={InitialRouteParams}
      />
      <Stack.Screen
        name="Sms"
        options={{...StackScreenOptions, headerShown: false}}
        component={SmsAuthenticationScreen}
        initialParams={InitialRouteParams}
      />
    </Stack.Navigator>
  );
};
