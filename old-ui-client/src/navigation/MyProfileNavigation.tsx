// @ts-ignore
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import SettingScreen from '../components/SettingScreen';
import AboutScreen from '../screens/AboutScreen';
import LegalScreen from '../components/LegalScreen';
import PrivacyScreen from '../screens/PrivacyScreen';
import AgreementScreen from '../screens/AgreementScreen';
import {HeaderMode, StackStyles} from './AppNavigatorConfig';
import MyProfileScreen, {
  MyProfileScreenOptions,
} from '../screens/ProfileScreen/MyProfileScreen';
import IMFormEditDetails from '../core/profile/ui/components/IMFormEditDetails';
import IMEditProfileScreen from '../core/profile/ui/components/IMEditProfileScreen';
import ListingScreen from '../screens/ListingScreen';
import SaveRecentListing from '../components/saveRecent';
import DetailScreen from '../screens/DetailsMain';
import MessagesScreen from '../screens/MessagesScreen';
import Contact from '../screens/ContactScreen';
import Payment from '../screens/paymentScreen';
import Complains from '../screens/Complains';
import Users from '../screens/Users';
import Visits from '../screens/Visits';
import {Translate} from '../core/i18n/IMLocalization';
import {useSelector} from 'react-redux';

const Stack = createStackNavigator();

export const MyProfileNavigation = () => {
  let {user} = useSelector((state) => state.auth);
  return (
    <Stack.Navigator
      screenOptions={{...StackStyles, headerTitleAlign: 'center'}}
      initialRouteName="MyProfile"
      headerMode={HeaderMode.Float}>
      <Stack.Screen
        options={() => MyProfileScreenOptions()}
        name="MyProfile"
        component={MyProfileScreen}
      />
      <Stack.Screen name="Settings" component={SettingScreen} />
      <Stack.Screen name="About" component={AboutScreen} />
      <Stack.Screen name="Legal" component={LegalScreen} />
      <Stack.Screen name="Privacy" component={PrivacyScreen} />
      <Stack.Screen name="User Agreement" component={AgreementScreen} />
      <Stack.Screen
        name="AccountDetail"
        component={IMEditProfileScreen}
        options={{title: Translate('Account Details')}}
      />
      <Stack.Screen
        name="EditDetails"
        component={IMFormEditDetails}
        options={{title: Translate('Edit Details')}}
      />
      <Stack.Screen name="Detail" component={DetailScreen} />
      <Stack.Screen name="Messages" component={MessagesScreen} />
      <Stack.Screen name="Contact" component={Contact} />
      <Stack.Screen name="Payment" component={Payment} />
      <Stack.Screen name="Visits" component={Visits} />

      {user?.type == 'admin' && (
        <>
          <Stack.Screen name="Complains" component={Complains} />
          <Stack.Screen
            name="Users"
            component={Users}
            options={{headerShown: false}}
          />
        </>
      )}

      <Stack.Screen
        name="ListingScreen"
        component={ListingScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="SaveRecentListing"
        component={SaveRecentListing}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};
