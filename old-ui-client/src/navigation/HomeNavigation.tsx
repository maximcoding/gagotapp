import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen/HomeScreen';
import ListingScreen from '../screens/ListingScreen';
import DetailScreen from '../screens/DetailsMain';
import MapScreen, {MapScreenNavigationOptions} from '../screens/MapScreen';
import Enrollers from '../screens/Enrollers';

import IMUserSettingsScreen, {
  IMUserSettingsOptions,
} from '../core/profile/ui/components/IMUserSettingsScreen';
import {HeaderMode, ScreenMode, StackStyles} from './AppNavigatorConfig';
import MyProfileScreen, {
  MyProfileScreenOptions,
} from '../screens/ProfileScreen/MyProfileScreen';
const Stack = createStackNavigator();
import {useSelector} from 'react-redux';

const MainStack = () => {
  let {updator} = useSelector((state) => state.languageReducer);

  return (
    <Stack.Navigator
      screenOptions={{...StackStyles, headerTitleAlign: 'center'}}
      initialRouteName="Home"
      mode={ScreenMode.Card}
      headerMode={HeaderMode.Float}>
      <Stack.Screen
        options={{headerShown: false}}
        name="Home"
        component={HomeScreen}
      />
      <Stack.Screen
        options={({navigation, route}) =>
          ListingScreenOptions(navigation, route)
        }
        name="Listing"
        component={ListingScreen}
      />
      <Stack.Screen
        options={DetailScreen.navigationOptions}
        name="Detail"
        component={DetailScreen}
      />
      <Stack.Screen name="Enrollments" component={Enrollers} />
      <Stack.Screen
        options={() => MapScreenNavigationOptions()}
        name="Map"
        component={MapScreen}
      />
      <Stack.Screen
        options={() => MyProfileScreenOptions()}
        name="MyProfile"
        component={MyProfileScreen}
      />
    </Stack.Navigator>
  );
};

export const HomeNavigation = () => {
  let {updator} = useSelector((state) => state.languageReducer);
  return (
    <Stack.Navigator screenOptions={StackStyles} headerMode={HeaderMode.None}>
      <Stack.Screen name="Home" component={MainStack} />
      <Stack.Screen
        options={() => MyProfileScreenOptions()}
        name="MyProfile"
        component={MyProfileScreen}
      />
      <Stack.Screen
        options={DetailScreen.navigationOptions}
        name="MyListingDetailModal"
        component={DetailScreen}
      />
      <Stack.Screen
        options={({navigation, route}) =>
          IMUserSettingsOptions(navigation, route)
        }
        name="Settings"
        component={IMUserSettingsScreen}
      />
    </Stack.Navigator>
  );
};
