import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import ConversationsScreen, {
  ConversationsScreenOptions,
} from "../screens/ConversationsScreen";
import IMChatScreen, {
  IMChatScreenOptions,
} from "../core/chat/IMChatScreen/IMChatScreen";
import { HeaderMode, StackStyles } from "./AppNavigatorConfig";
const Stack = createStackNavigator();

export const MessageNavigation = () => {
  return (
    <Stack.Navigator
      screenOptions={StackStyles}
      initialRouteName="Message"
      headerMode={HeaderMode.Float}
    >
      <Stack.Screen
        name="Message"
        component={ConversationsScreen}
        options={() => ConversationsScreenOptions()}
      />
      <Stack.Screen
        //@ts-ignore
        options={(params) => IMChatScreenOptions(params)}
        name="PersonalChat"
        component={IMChatScreen}
      />
    </Stack.Navigator>
  );
};
