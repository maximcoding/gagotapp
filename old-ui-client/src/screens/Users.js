import React, {useState, useEffect, memo} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  TextInput,
  ActivityIndicator as AC,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FIcon from 'react-native-vector-icons/Feather';
import {getFunctionHelper, onRefresh, loadMore} from '../utils/lazyLoading';
import {getUsers, searchUsers, changeSort} from '../redux/actions/admin';
import ActivityIndicator from '../components/activityIndicator';
import UserItem from '../components/userItem';
import SortModal from '../components/userSort';
import {Colors, Fonts} from '../themes';

let {width} = Dimensions.get('window');

export default memo(function Users({route, navigation}) {
  const dispatch = useDispatch();
  let {allUsers, isSort} = useSelector((state) => state.adminReducer);
  let [isFetching, setFetching] = useState(false);
  let [loader, setLoader] = useState(false);
  let [moreLoader, setMoreLoader] = useState(false);
  let [page, setPage] = useState(0);
  const [searchLoader, setSearchLoader] = useState(false);
  const [open, setOpen] = useState(false);

  const renderItem = ({item, index}) => {
    return <UserItem item={item} index={index} />;
  };

  useEffect(() => {
    getFunctionHelper(isSort, 0, setLoader, dispatch, getUsers);
  }, []);

  const stopSearch = () => {
    setSearchLoader(false);
  };

  const closeModal = () => {
    setOpen(false);
  };

  const sortUsers = (value) => {
    dispatch(changeSort(value));
    // console.log('values are here', value);
    getFunctionHelper(value, 0, setLoader, dispatch, getUsers);
  };

  if (loader) {
    return <ActivityIndicator />;
  } else
    return (
      <>
        <View style={styles.header}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <FIcon name="arrow-left" color="black" size={24} />
          </TouchableOpacity>
          <Text style={styles.title}>Users</Text>
          <TouchableOpacity onPress={() => setOpen(true)}>
            <Icon name="filter-list" color="black" size={24} />
          </TouchableOpacity>
        </View>

        <View style={styles.searchBar}>
          <View style={styles.search}>
            <FIcon
              name="search"
              color="gray"
              size={19}
              style={styles.iSearch}
            />
            <TextInput
              placeholder="Search User"
              style={styles.searchF}
              onChangeText={(text) => {
                setSearchLoader(true);
                dispatch(searchUsers(text, stopSearch));
              }}
            />
            {searchLoader && <AC size={'small'} color="gray" />}
          </View>
        </View>
        <FlatList
          vertical
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{flexGrow: 1}}
          data={allUsers}
          renderItem={renderItem}
          keyExtractor={(item, index) => item._id}
          onEndReachedThreshold={0.5}
          onRefresh={() =>
            onRefresh(isSort, setPage, setFetching, dispatch, getUsers)
          }
          refreshing={isFetching}
          onEndReached={() =>
            loadMore(
              isSort,
              moreLoader,
              page,
              setMoreLoader,
              setPage,
              dispatch,
              getUsers,
            )
          }
          ListEmptyComponent={() => {
            return (
              <View style={styles.blankView}>
                <Text style={styles.backImage}>No Users !</Text>
              </View>
            );
          }}
        />
        <SortModal
          isOpen={open}
          closeSort={closeModal}
          callBack={sortUsers}
          isSort={isSort}
        />
      </>
    );
});

const styles = StyleSheet.create({
  backImage: {
    color: Colors.grey,
  },
  img: {
    height: 55,
    width: 55,
    borderRadius: 55 / 2,
  },
  blankView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.white,
  },
  header: {
    width: '100%',
    height: 55,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    backgroundColor: Colors.white,
    elevation: 2,
  },
  searchBar: {
    flexDirection: 'row',
    width: width,
    padding: 10,
    backgroundColor: Colors.white,
    alignItems: 'center',
  },
  title: {
    fontSize: Fonts.size.normal,
    fontWeight: '700',
  },
  search: {
    width: '100%',
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: Colors.white,
    paddingRight: 5,
  },
  searchF: {
    padding: 5,
    fontSize: Fonts.size.small,
  },
  searchText: {
    color: Colors.white,
    paddingLeft: 7,
  },
  iSearch: {
    paddingLeft: 10,
  },
});
