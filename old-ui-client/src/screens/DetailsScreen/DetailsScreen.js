import React, {useState, useEffect, memo, useRef} from 'react';
import {
  Dimensions,
  StyleSheet,
  ScrollView,
  Text,
  View,
  TextInput,
  SafeAreaView,
  ActivityIndicator,
  Image,
  Modal,
} from 'react-native';
import {AppStyles} from '../../AppStyles';
import ReviewModal from '../../components/ReviewModal';
import {Translate} from '../../core/i18n/IMLocalization';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Fonts from '../../themes/fonts';
import {useSelector, useDispatch} from 'react-redux';
import ImageViewer from 'react-native-image-zoom-viewer';
import Swiper from 'react-native-swiper';
import FastImage from 'react-native-fast-image';
import Map from '../../components/map';
import RadioButton from 'react-native-radio-button';
import {getDay} from '../../utils/postHelper';
import EditTime from '../TimeSelection/screen/edit';
const {width, height} = Dimensions.get('window');
import {
  saveRecentListing,
  getDocById,
  updateStatus,
  enrollUser,
  editEnrollTime,
} from '../../redux/actions/postListing';
import {subscribeReviews} from '../../redux/actions/review';
import Review from '../../components/review';
import {TouchableOpacity} from 'react-native-gesture-handler';
import VerifyModal from '../../components/verificationModal';
import {unitConverter} from '../../utils/currencyConverter';
import Colors from '../../themes/colors';
let defaultImg =
  'https://assets.stickpng.com/images/585e4bf3cb11b227491c339a.png';

export default memo(function DetailsScreen({item, reviewId, navigation}) {
  let {user} = useSelector((state) => state.auth);
  let {updator} = useSelector((state) => state.languageReducer);
  let {recentPosts} = useSelector((state) => state.postReducer);
  const scrollRef = useRef();
  const dispatch = useDispatch();
  let [state, setState] = useState({
    data: '',
    reviewModalVisible: false,
  });
  let [stateImg, setStateImg] = useState({
    viewVisible: false,
    imageIndex: 0,
  });
  const [reviews, setReviews] = useState([]);
  const [revLoader, setRevLoader] = useState(false);
  const [loader, setLoader] = useState(false);
  const [index, setIndex] = useState(false);
  const [day, setDay] = useState('mon');
  const [showWarning, setShowWarning] = useState(user?.phone == '');
  const [statusLoader, setStatusLoader] = useState(false);
  const [enroller, setEnroller] = useState(false);
  const [edit, setEdit] = useState(false);
  let {data} = state;

  const reviewLoader = (reviews) => {
    setRevLoader(false);
    if (reviews) {
      setReviews(reviews);
    }
    let index = reviews.findIndex((item) => item.id == reviewId);
    setIndex(index);
  };

  const stopLoader = (result) => {
    setState({...state, data: result});
    setLoader(false);
    if (reviewId) {
      scrollView();
    }
  };

  const scrollView = () => {
    let index = reviews.findIndex((item) => item.id == reviewId);
    setTimeout(() => {
      scrollRef.current.scrollTo({
        y: height + index * 300,
        animated: true,
      });
    }, 200);
  };

  useEffect(() => {
    setLoader(true);
    dispatch(getDocById(item._id, stopLoader));
    dispatch(saveRecentListing(item, recentPosts));
    dispatch(subscribeReviews(item._id, reviewLoader));
    const unsubscribe = navigation.addListener('focus', () => {
      if (user?.phone == '') {
        setShowWarning(true);
      }
    });
    return unsubscribe;
  }, [navigation]);

  const onReviewCancel = () => {
    setState({...state, reviewModalVisible: false});
  };

  const onReviewDone = () => {
    setState({...state, reviewModalVisible: false});
    dispatch(subscribeReviews(item._id, reviewLoader));

    // save review
  };

  const renderReviewItem = (item, i) => (
    <Review
      item={item}
      index={index}
      i={i}
      showReply={user?._id == data.authorID?._id}
    />
  );

  const changeStatusHandler = () => {
    setStatusLoader(true);
    let statusObj = {
      status: data.status === 'open' ? 'close' : 'open',
      id: data._id,
      type: data.type,
    };
    dispatch(updateStatus(statusObj, changeStatusFinally));
  };

  const changeStatusFinally = (type, status) => {
    if (type === 'success') {
      setState({...state, data: {...state.data, status}});
    }
    setStatusLoader(false);
  };

  function openModal(index) {
    setStateImg({...stateImg, viewVisible: true, imageIndex: index});
  }

  const closeModal = () => {
    setStateImg({
      ...stateImg,
      viewVisible: false,
    });
  };

  const stopEnroller = () => {
    setEnroller(false);
  };

  const getEnrollUser = () => {
    setEnroller(true);
    let time = data.open_doors[day];
    dispatch(enrollUser(day, time, data, stopEnroller));
  };

  const close = () => {
    setEdit(false);
  };

  const stopEdit = (data) => {
    setEnroller(false);
    if (data) {
      setState({...state, data});
    }
  };

  const editTime = (time) => {
    setEnroller(true);
    dispatch(editEnrollTime(day, time, data, stopEdit));
    // edit then
  };

  const editNow = (day) => {
    setDay(day);
    setEdit(true);
  };

  return (
    <>
      {loader ? (
        <SafeAreaView style={styles.mContainer}>
          <ActivityIndicator size="small" color="green" />
        </SafeAreaView>
      ) : (
        <ScrollView
          contentContainerStyle={styles.container}
          showsVerticalScrollIndicator={false}
          ref={scrollRef}>
          <Swiper width={width} height={250} autoplay={true}>
            {item.photoURLs.length > 0 &&
              item.photoURLs.map((img, index) => {
                return (
                  <TouchableOpacity
                    style={styles.imgView}
                    onPress={() => openModal(index)}>
                    <FastImage
                      source={{
                        uri: img,
                      }}
                      style={styles.img}
                      resizeMode={FastImage.resizeMode.center}
                    />
                  </TouchableOpacity>
                );
              })}
          </Swiper>

          <Text style={AppStyles.text.sectionTitle}>
            {Translate('Address')}
          </Text>
          <Text numberOfLines={3} style={styles.longTextValue}>
            {data.place}
          </Text>
          <View style={styles.horizontalLine} />
          <Text style={AppStyles.text.sectionTitle}>
            {Translate('Details')}
          </Text>
          <View style={styles.details}>
            <Text style={AppStyles.text.listTitle}>
              {Translate('Rent or Buy')}
            </Text>
            <Text style={AppStyles.text.listValue}>{data.type}</Text>
          </View>
          <View style={styles.details}>
            <Text style={AppStyles.text.listTitle}>
              {Translate('Bedrooms')}
            </Text>
            <Text style={AppStyles.text.listValue}>{data.bedroom}</Text>
          </View>
          <View style={styles.details}>
            <Text style={AppStyles.text.listTitle}>
              {Translate('Bathrooms')}
            </Text>
            <Text style={AppStyles.text.listValue}>{data.bathroom}</Text>
          </View>
          <View style={styles.details}>
            <Text style={AppStyles.text.listTitle}>{Translate('Floors')}</Text>
            <Text style={AppStyles.text.listValue}>{data.floors}</Text>
          </View>
          <View style={styles.details}>
            <Text style={AppStyles.text.listTitle}>{Translate('Square')}</Text>
            <Text style={AppStyles.text.listValue}>
              {unitConverter(Number(data.square), user)} {user?.unit}
            </Text>
          </View>
          <View style={styles.horizontalLine} />
          <View style={styles.statusCont}>
            <Text style={AppStyles.text.listTitle}>
              Current Status:{' '}
              <Text
                style={[
                  AppStyles.text.listValue,
                  {
                    textTransform: 'uppercase',
                    color: data.status === 'open' ? Colors.green : Colors.fire,
                  },
                ]}>
                {data.status}
              </Text>
            </Text>
            {user?._id === data?.authorID?._id && (
              <TouchableOpacity
                onPress={changeStatusHandler}
                style={[
                  styles.statusAction,
                  data.status === 'open'
                    ? {backgroundColor: Colors.fire}
                    : {backgroundColor: Colors.green},
                ]}>
                {statusLoader ? (
                  <ActivityIndicator size="small" color="#fff" />
                ) : (
                  <Text style={{color: '#fff'}}>
                    {data.status === 'open' ? 'close' : 'open'}
                  </Text>
                )}
              </TouchableOpacity>
            )}
          </View>
          <View style={styles.horizontalLine} />
          <Text style={[AppStyles.text.sectionTitle]}>
            {Translate('Additional Details')}
          </Text>
          <Text numberOfLines={3} style={styles.longTextValue}>
            {data.description}
          </Text>
          <View style={styles.horizontalLine} />

          {/*agent detials  */}
          <View>
            <Text style={[AppStyles.text.sectionTitle]}>Agent Details</Text>
            <View style={AppStyles.text.agentCont}>
              <Image
                style={AppStyles.text.avatar}
                source={{
                  uri: data?.authorID?.photoURI
                    ? data?.authorID?.photoURI
                    : defaultImg,
                }}
              />
              <View style={AppStyles.text.agentDetail}>
                <Text>
                  Name:{' '}
                  {`${data?.authorID?.firstName} ${data?.authorID?.lastName}`}
                </Text>
                <Text>Phone: {data?.authorID?.phone}</Text>
                <Text>Address: {data?.authorID?.address}</Text>
              </View>
            </View>
          </View>
          <View style={styles.horizontalLine} />

          <View>
            <Text style={[AppStyles.text.sectionTitle]}>Open doors</Text>

            {user?._id == data?.authorID?._id ? (
              <View>
                {Object.keys(data ? data.open_doors : {}).map((key) => {
                  return (
                    <View style={styles.row}>
                      <Text style={styles.key}>{getDay(key)}</Text>

                      <Text style={styles.time}>
                        {data.open_doors[key] == 'open'
                          ? '24 hours'
                          : data.open_doors[key]}
                      </Text>

                      <TouchableOpacity
                        onPress={() => editNow(key)}
                        style={styles.enroll}>
                        {enroller && day == key ? (
                          <ActivityIndicator size={12} color="green" />
                        ) : (
                          <Icon name="mode-edit" size={15} color="green" />
                        )}
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() =>
                          navigation.navigate('Enrollments', {
                            day: key,
                            post: data,
                          })
                        }
                        style={styles.enroll}>
                        <Icon name="people" size={15} color="green" />
                      </TouchableOpacity>
                    </View>
                  );
                })}
                <EditTime open={edit} close={close} getOpendoors={editTime} />
              </View>
            ) : (
              <View>
                {user?.type == 'user' && (
                  <>
                    {Object.keys(data ? data.open_doors : {}).map((key) => {
                      return (
                        <View style={styles.row}>
                          <Text style={styles.key}>{getDay(key)}</Text>

                          <Text style={styles.time}>
                            {data.open_doors[key] == 'open'
                              ? '24 hours'
                              : data.open_doors[key]}
                          </Text>

                          <RadioButton
                            color={'green'}
                            animation={'bounceIn'}
                            isSelected={day == key}
                            onPress={() => {
                              if (data.open_doors[key] == 'closed') {
                                return;
                              }
                              setDay(key);
                            }}
                            size={10}
                          />
                          {
                            <TouchableOpacity
                              onPress={getEnrollUser}
                              disabled={day !== key}
                              style={
                                day == key ? styles.enroll : styles.enrollD
                              }>
                              {enroller && day == key ? (
                                <ActivityIndicator size={12} color="green" />
                              ) : (
                                <Text
                                  style={
                                    day == key
                                      ? styles.enrollText
                                      : styles.enrollTextD
                                  }>
                                  Enroll
                                </Text>
                              )}
                            </TouchableOpacity>
                          }
                        </View>
                      );
                    })}
                  </>
                )}
              </View>
            )}
          </View>

          <View style={styles.horizontalLine} />
          <Text style={AppStyles.text.sectionTitle}>
            {Translate('Location')}
          </Text>
          <Map
            coordinates={{
              latitude: data.latitude,
              longitude: data.longitude,
            }}
          />
          <Text style={[AppStyles.text.sectionTitle, {paddingTop: 10}]}>
            {Translate('Reviews')}
          </Text>
          {user?._id !== data?.authorID?._id && (
            <View style={[styles.details, {paddingBottom: 20}]}>
              <TextInput
                placeholder={Translate('Write...')}
                style={styles.placeholder}
                onFocus={() => setState({...state, reviewModalVisible: true})}
              />
              <TouchableOpacity
                style={{display: 'flex'}}
                onPress={() => setState({...state, reviewModalVisible: true})}>
                <Icon name="rate-review" size={20} color="#00b33c" />
              </TouchableOpacity>
            </View>
          )}
          {reviews.length > 0 && (
            <>
              {reviews.map((item, index) => {
                return renderReviewItem(item, index);
              })}
            </>
          )}

          {state.reviewModalVisible && (
            <ReviewModal
              listing={data}
              onCancel={onReviewCancel}
              onDone={onReviewDone}
              dispatch={dispatch}
              user={user}
              visible={state.reviewModalVisible}
            />
          )}
        </ScrollView>
      )}
      <VerifyModal
        navigation={navigation}
        showWarning={showWarning}
        setShowWarning={setShowWarning}
      />

      <Modal
        visible={stateImg.viewVisible}
        transparent={true}
        onBackdropPress={closeModal}>
        <ImageViewer
          imageUrls={
            item.photoURLs.length &&
            item.photoURLs.map((item) => {
              return {
                url: item,
              };
            })
          }
          index={stateImg.imageIndex}
          enableSwipeDown={true}
          onSwipeDown={closeModal}
          useNativeDriver={true}
          enableImageZoom={true}
        />
        <TouchableOpacity
          onPress={closeModal}
          style={{position: 'absolute', top: 20, right: 20}}>
          <Icon name="rate-review" size={25} color="white" />
        </TouchableOpacity>
      </Modal>
    </>
  );
});

const styles = StyleSheet.create({
  headerIconContainer: {
    marginRight: 10,
  },
  details: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '85%',
    alignSelf: 'center',
    paddingVertical: 1,
  },
  horizontalLine: {
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    minHeight: 10,
    alignSelf: 'center',
    width: width / 1.1,
  },
  statusCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15,
    width: '85%',
    alignSelf: 'center',
    alignItems: 'center',
  },
  statusAction: {
    paddingVertical: 7,
    paddingHorizontal: 20,
    borderRadius: 20,
  },
  longTextValue: {
    ...AppStyles.text.listValue,
    paddingLeft: 10,
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'space-between',
    width: '90%',
  },
  headerIcon: {
    tintColor: '#21c064',
    height: 20,
    width: 20,
  },
  container: {
    backgroundColor: 'white',
    flexGrow: 1,
  },
  mContainer: {
    height: height,
    width: width,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  reviewTitle: {
    paddingTop: 0,
    paddingBottom: 10,
  },
  description: {
    fontFamily: Fonts.family.main,
    padding: 10,
    color: Colors.description,
  },
  photoItem: {
    backgroundColor: Colors.grey,
    height: 250,
    width: '100%',
  },
  paginationContainer: {
    flex: 1,
    position: 'absolute',
    alignSelf: 'center',
    paddingVertical: 8,
    marginTop: 220,
  },
  paginationDot: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginHorizontal: 0,
  },
  mapView: {
    width: '100%',
    height: 200,
    // backgroundColor: Colors.grey
  },
  loadingMap: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  extra: {
    padding: 30,
    paddingTop: 10,
    paddingBottom: 0,
    marginBottom: 30,
  },
  extraRow: {
    flexDirection: 'row',
    paddingBottom: 10,
  },
  extraKey: {
    flex: 2,
    color: Colors.title,
    fontWeight: 'bold',
  },
  extraValue: {
    flex: 1,
    color: Colors.title,
  },

  reviewInput: {
    marginHorizontal: 10,
    padding: 5,
  },
  img: {
    width: '100%',
    height: '100%',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 25,
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  enroll: {
    height: 30,
    width: 70,
    backgroundColor: '#e6fff5',
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  enrollD: {
    height: 30,
    width: 70,
    backgroundColor: '#f0f5f5',
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  enrollTextD: {
    color: 'gray',
    fontSize: Fonts.size.small,
  },
  enrollText: {
    color: 'green',
    fontSize: Fonts.size.small,
  },
  time: {
    color: 'gray',
    fontSize: Fonts.size.small,
    width: '15%',
  },
  key: {
    width: '22%',
  },
});
