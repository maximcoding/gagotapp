import React, {memo, useState} from 'react';
import {View, Text} from 'react-native';
import stripe from 'tipsi-stripe';
import Button from '../components/bottomButton';
import {CreditCardInput} from 'react-native-credit-card-input';

stripe.setOptions({
    publishableKey: 'pk_test_h75LctrembPtHHmij1pFDGXz0039y7CUuG',
    androidPayMode: 'test',
});

var payData = {
    status: {cvc: 'valid', expiry: 'incomplete', number: 'incomplete'},
    valid: false,
    values: {
        cvc: '',
        expiry: '',
        number: '',
        type: 'visa',
    },
};
export default memo((props) => {
    const [loader, setLoader] = useState(false);
    const _onChange = (form) => {
        payData = form;
    };
    const add = () => {
        let {number, expiry, cvc} = payData.values;
        const [mm, yy] = expiry.split('/');
        if (number !== '' && mm !== '' && yy !== '') {
            submitToken({
                number,
                expMonth: parseInt(mm),
                expYear: parseInt(yy),
                cvc,
                // name,
            });
        } else {
            alert('All fields are required');
        }
    };

    submitToken = async (params) => {
        setLoader(true);
        try {
            const token = await stripe.createTokenWithCard(params);
            setLoader(false);
        } catch (error) {
            setLoader(false);
            alert(error.toString());
        }
    };

    return (
        <View style={[{flex: 1, backgroundColor: 'white', padding: 10}]}>
            <CreditCardInput onChange={_onChange}/>
            <Button loading={loader} title="Submit" onPress={add} disabled={loader}/>
        </View>
    );
});
