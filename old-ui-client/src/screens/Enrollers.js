import React, {useState, useEffect, memo} from 'react';
import {View, Text, FlatList, StyleSheet, Image} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {getFunctionHelper, onRefresh, loadMore} from '../utils/lazyLoading';
import {getEnrollers} from '../redux/actions/postListing';
import ActivityIndicator from '../components/activityIndicator';
import moment from 'moment';
import { Fonts } from '../themes';
let defaultImg =
  'https://assets.stickpng.com/images/585e4bf3cb11b227491c339a.png';

export default memo(function Enrollers({route}) {
  let {day, post} = route.params;
  let params = {day, post};
  const dispatch = useDispatch();
  let {enrollUsers} = useSelector((state) => state.postReducer);
  let [isFetching, setFetching] = useState(false);
  let [loader, setLoader] = useState(false);
  let [moreLoader, setMoreLoader] = useState(false);
  let [page, setPage] = useState(0);

  const renderItem = ({item, index}) => {
    return (
      <View style={styles.row}>
        <View style={styles.rowInner}>
          <Image
            source={{uri: item.user.photoURI ? item.user.photoURI : defaultImg}}
            style={styles.img}
          />
          <View style={styles.col}>
            <Text>
              {item.user.firstName}
              {item.user.lastName}
            </Text>
            <Text>{item.user.email}</Text>
            <Text>{item.user.phone}</Text>
          </View>
        </View>
        <Text style={styles.time}>
          {' '}
          {moment(item.createdAt).fromNow(true)} ago
        </Text>
      </View>
    );
  };

  useEffect(() => {
    getFunctionHelper(params, 0, setLoader, dispatch, getEnrollers);
  }, [day, post]);

  if (loader) {
    return <ActivityIndicator />;
  } else
    return (
      <FlatList
        vertical
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{flexGrow: 1, backgroundColor: 'white'}}
        data={enrollUsers}
        renderItem={renderItem}
        keyExtractor={(item, index) => item._id}
        onEndReachedThreshold={0.5}
        onRefresh={() =>
          onRefresh(params, setPage, setFetching, dispatch, getEnrollers)
        }
        refreshing={isFetching}
        onEndReached={() =>
          loadMore(
            params,
            moreLoader,
            page,
            setMoreLoader,
            setPage,
            dispatch,
            getEnrollers,
          )
        }
        ListEmptyComponent={() => {
          return (
            <View style={styles.blankView}>
              <Text style={styles.backImage}>
                No Enrollments for this day found !
              </Text>
            </View>
          );
        }}
      />
    );
});

const styles = StyleSheet.create({
  backImage: {
    color: 'gray',
  },
  img: {
    height: 55,
    width: 55,
    borderRadius: 55 / 2,
  },
  blankView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  col: {
    alignItems: 'flex-start',
    paddingLeft: 10,
  },
  row: {
    width: '95%',
    alignSelf: 'center',
    backgroundColor: '#f0f5f5',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10,
    marginTop: 10,
  },
  rowInner: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  time: {
    fontSize: Fonts.size.tiny,
    color: 'gray',
  },
});
