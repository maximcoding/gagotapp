import React, {memo, useEffect} from 'react';
import {
    Switch,
    StyleSheet,
    Text,
    View,
    ActivityIndicator,
    ScrollView,
    TextInput,
    LayoutAnimation,
    Platform,
    UIManager,
    TouchableOpacity,
    SafeAreaView, Dimensions,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import QuantityPicker from '../components/quantityPicker';
import TextButton from 'react-native-button';
import {useSelector, useDispatch} from 'react-redux';
import {setSearchFilter} from '../redux/actions/filters';
import Icon from 'react-native-vector-icons/Ionicons';
import {Translate} from '../core/i18n/IMLocalization';
import SliderCustomLabel from '../components/SliderCustomLabel';
import {Colors, Fonts} from '../themes';
import {AppStyles} from '../AppStyles';
import {getSymbol} from '../utils/currencyConverter';
import {onClearFilter, onChangeFilter} from '../redux/actions/filters';
import LazyHOC from '../components/lazyHOC';
import {selectorData2} from "../utils/filterHelper";

export default memo(function SelectFilters({navigation}) {
    let {screenWidth} = Dimensions.get('window');
    const dispatch = useDispatch();
    const {categories} = useSelector((state) => state.categories);
    const {user} = useSelector((state) => state.auth);
    const {filterValues = {}} = useSelector((state) => state.filterReducer);
    const selectedCategoryName = categories?.find(obj => obj._id === filterValues.category)?.name || 'all';
    const clearFilter = () => {
        dispatch(onClearFilter());
    };

    const callBack = () => {
        navigation.navigate('Home');
    };

    const onChangeFilterHandler = (name, value) => {
        dispatch(onChangeFilter({name, value}));
    };

    const filterPostsAction = () => {
        callBack();
        dispatch(setSearchFilter());
    };

    const onSquareFromChange = (text) => {
        if (/^\d+$/.test(text.toString())) {
            const value = [Number(text), filterValues.square[1]];
            onChangeFilterHandler('square', value);
        } else {
            onChangeFilterHandler('square', [0, filterValues.square[1]])
        }
    }

    const onSquareToChange = (text) => {
        if (/^\d+$/.test(text.toString())) {
            filterValues.square[1] = Number(text);
        } else {
            filterValues.square[1] = 0;
        }
    }

    const getPriceSteps = () => {
        switch (filterValues.rent) {
            case 'buy':
                return 5000;
            default:
                return 10;
        }
    }

    const getMaxPrice = () => { //TODO max from db !!
        switch (selectedCategoryName.toString()) {
            case 'Apartments':
                return filterValues.rent === 'buy' ? 25000000 : 999999999
            case 'Houses':
            case 'Buildings':
            case 'Offices':
            case 'Clinics':
            case 'Land':
            case 'Condos':
            case 'Town Houses':
            default:
                return 99000; // // max from db !!
        }
    }

    const getDefaultMaxPrice = () => {  //TODO should be same logic as max price!
        switch (filterValues.rent) {
            case 'buy':
                return 1500000;
            default:
                return 25000;
        }
    }

    const squareFromValue = () => {
        return filterValues.square[0];
    }

    const squareToValue = () => {
        return filterValues.square[1];
    }

    useEffect(() => {
        if (!filterValues.persistState) {
            clearFilter();
        }
    }, []);
    return (
        <LazyHOC>
            <SafeAreaView style={styles.safearew}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Icon name="arrow-back" size={25}/>
                    </TouchableOpacity>
                    <Text
                        style={[
                            AppStyles.text.headerTitle,
                            {alignItems: 'center', textAlign: 'center'},
                        ]}>
                        {Translate('Filters')}
                    </Text>
                    <TouchableOpacity onPress={clearFilter}>
                        <Text style={AppStyles.text.headerButton}>
                            {Translate('Clear')}
                        </Text>
                    </TouchableOpacity>
                </View>


                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                        flexGrow: 1,
                        backgroundColor: Colors.backgroundColor,
                        paddingHorizontal: 10,
                    }}>
                    <>
                        <View style={styles.row}>
                            <Text style={styles.title}>{Translate('RentOrBuy')}</Text>
                            <RNPickerSelect
                                placeholder={{
                                    label: Translate('RentOrBuy'),
                                    value: {label: "Rent", value: "rent"},
                                    color: 'black',
                                }}
                                onValueChange={(value) => onChangeFilterHandler('rent', value)}
                                value={filterValues['rent'] ? filterValues['rent'] : 'rent'}
                                items={[
                                    {label: Translate('Rent'), value: "rent"},
                                    {label: Translate('Buy'), value: "buy"},
                                ]}
                                style={AppStyles.pickerSelectStyles2}
                                useNativeAndroidPickerStyle={false}
                            />
                        </View>
                        <View style={styles.divider}/>
                        <View style={styles.row}>
                            <Text style={styles.title}>{Translate('Categories')}</Text>
                            <RNPickerSelect
                                placeholder={{
                                    label: Translate('All'),
                                    value: 'all',
                                    color: 'black',
                                }}
                                onValueChange={(value) => onChangeFilterHandler('category', value)}
                                value={filterValues['category']}
                                items={categories.map((c) => {
                                    return {label: c.name, value: c._id};
                                })}
                                style={AppStyles.pickerSelectStyles2}
                                useNativeAndroidPickerStyle={false}
                            />
                        </View>
                        <View style={styles.divider}/>
                    </>
                    <View style={{paddingVertical: 10}}>
                        <Text style={[styles.title, {paddingVertical: 20}]}>
                            {Translate('PriceIn')} {getSymbol(user)}
                        </Text>
                        <View style={{paddingRight: 0, alignItems: 'center'}}>
                            <MultiSlider
                                selectedStyle={{backgroundColor: '#0073eb'}}
                                trackStyle={{height: 4}}
                                sliderLength={320}
                                onValuesChange={(v) => onChangeFilterHandler('price', v)}
                                customLabel={SliderCustomLabel}
                                isMarkersSeparated={true}
                                enableLabel
                                step={getPriceSteps()}
                                min={10}
                                max={getMaxPrice()}
                                snapped
                                allowOverlap={false}
                                values={[
                                    filterValues.price[0] ? filterValues.price[0] : 10,
                                    filterValues.price[1] ? filterValues.price[1] : getDefaultMaxPrice()]
                                }
                            />
                        </View>
                    </View>
                    <View style={styles.divider}/>
                    {filterValues.rent == 'buy' && (
                        <View style={{paddingVertical: 10}}>
                            <Text style={[styles.title, {paddingVertical: 20}]}>{Translate('Deposit')} %</Text>
                            <View style={{paddingRight: 0, alignItems: 'center'}}>
                                <MultiSlider
                                    sliderLength={320}
                                    trackStyle={{height: 4}}
                                    customLabel={SliderCustomLabel}
                                    selectedStyle={{backgroundColor: '#0073eb'}}
                                    onValuesChange={(v) => onChangeFilterHandler('deposit', v)}
                                    isMarkersSeparated={true}
                                    enableLabel
                                    step={1}
                                    min={0}
                                    max={100}
                                    snapped
                                    allowOverlap={false}
                                    values={[filterValues.deposit[0], filterValues.deposit[1] ? filterValues.deposit[1] : 30]}
                                />
                            </View>
                        </View>
                    )}
                    <View style={{paddingVertical: 0}}>
                        <Text style={[styles.title, {paddingVertical: 20}]}>
                            {Translate(`Square in ` + (user.unit || 'feet'))}
                        </Text>
                        {/*<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>*/}
                        {/*    <TextInput*/}
                        {/*        style={[AppStyles.inputBorderContainer, {width: '48%'}]}*/}
                        {/*        placeholder={Translate('from')}*/}
                        {/*        maxLength={10}*/}
                        {/*        keyboardType='numeric'*/}
                        {/*        placeholderTextColor={Colors.placeholder}*/}
                        {/*        onChangeText={(text) => onSquareFromChange(text)}*/}
                        {/*        value={squareFromValue()}*/}
                        {/*        underlineColorAndroid="transparent"*/}
                        {/*        autoCapitalize="none"*/}
                        {/*    />*/}
                        {/*    <TextInput*/}
                        {/*        style={[AppStyles.inputBorderContainer, {width: '48%'}]}*/}
                        {/*        placeholderTextColor={Colors.placeholder}*/}
                        {/*        maxLength={10}*/}
                        {/*        keyboardType='numeric'*/}
                        {/*        placeholder={Translate('to')}*/}
                        {/*        onChangeText={(text) => onSquareToChange(text)}*/}
                        {/*        value={filterValues.square[1]}*/}
                        {/*        underlineColorAndroid="transparent"*/}
                        {/*        autoCapitalize="none"*/}
                        {/*    />*/}
                        {/*</View>*/}
                        <View style={{paddingRight: 0, alignItems: 'center'}}>
                            <MultiSlider
                                selectedStyle={{backgroundColor: '#0073eb'}}
                                customLabel={SliderCustomLabel}
                                trackStyle={{height: 4}}
                                sliderLength={320}
                                onValuesChange={(v) => onChangeFilterHandler('square', v)}
                                isMarkersSeparated={true}
                                enableLabel
                                step={1}
                                min={5}
                                max={9999}
                                snapped
                                allowOverlap={false}
                                values={[filterValues.square[0] ? filterValues.square[0] : 1, filterValues.square[1] ? filterValues.square[1] : 9999]}
                            />
                        </View>
                    </View>
                    <View style={styles.divider}/>

                    {filterValues.useExact ? (
                        <>{['bathrooms', 'bedrooms'].map((o) => (
                            <>
                                <View style={[styles.appSettingsTypeContainer]}>
                                    <Text style={styles.title}>{o}</Text>
                                    <TextInput
                                        underlineAndroid="transparent"
                                        style={[styles.text, {textAlign: 'right'}]}
                                        onChangeText={(text) => {
                                            onChangeFilterHandler(o, text);
                                        }}
                                        placeholderTextColor={Colors.greyPlaceholder}
                                        keyboardType={'phone-pad'}
                                        placeholder={Translate('EnterNumberOf')`${o}`}
                                        value={filterValues[o]}
                                        maxLength={3}
                                    />
                                </View>
                            </>
                        ))}
                        </>
                    ) : (
                        <>
                            <QuantityPicker
                                title={Translate('Bathrooms')}
                                name="bathrooms"
                                getValue={(value) => onChangeFilterHandler('bathrooms', value)}
                                value={filterValues.bathrooms}
                            />
                            <QuantityPicker
                                title={Translate('Bedrooms')}
                                name="bedrooms"
                                getValue={(value) => onChangeFilterHandler('bedrooms', value)}
                                value={filterValues.bedrooms}
                            />
                        </>
                    )}
                    {<>
                        <View style={styles.row}>
                            <Text style={styles.title}>{Translate('New Construction')}</Text>
                            <RNPickerSelect
                                placeholder={{
                                    label: Translate('Any'),
                                    value: 'any',
                                    color: 'black',
                                }}
                                onValueChange={(value) => onChangeFilterHandler('newConstruction', value)}
                                value={filterValues['newConstruction']}
                                items={[{label: "Yes", value: "yes"}, {label: "No", value: "no"}]}
                                style={AppStyles.pickerSelectStyles2}
                                useNativeAndroidPickerStyle={false}
                            />
                        </View>
                        <View style={styles.divider}/>
                    </>}
                    {/*<View*/}
                    {/*  style={{*/}
                    {/*    flexDirection: 'row',*/}
                    {/*    alignItems: 'center',*/}
                    {/*    justifyContent: 'space-between',*/}
                    {/*    marginVertical: 15,*/}
                    {/*  }}>*/}
                    {/*  <Text style={styles.title}>{Translate('UseExactMatch')}</Text>*/}
                    {/*  <Switch*/}
                    {/*    onValueChange={() => {*/}
                    {/*      LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);*/}
                    {/*      onChangeFilterHandler('useExact', !filterValues.useExact);*/}
                    {/*    }}*/}
                    {/*    thumbColor={'#ffff'}*/}
                    {/*    trackColor={{false: '#DDDD', true: '#0073eb'}}*/}
                    {/*    value={filterValues.useExact}*/}
                    {/*  />*/}
                    {/*</View>*/}
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            marginVertical: 15,
                        }}>
                        <Text style={styles.title}>{Translate('SaveFilter')}</Text>
                        <Switch
                            onValueChange={() => {
                                onChangeFilterHandler(
                                    'persistState',
                                    !filterValues.persistState,
                                );
                            }}
                            thumbColor={Colors.white}
                            trackColor={{false: '#DDDD', true: '#0073eb'}}
                            value={filterValues.persistState}
                        />
                    </View>
                    <TextButton
                        containerStyle={[
                            styles.addButtonContainer,
                            {backgroundColor: Colors.green},
                        ]}
                        onPress={filterPostsAction}
                        style={[styles.filterButton]}>
                        Filter
                    </TextButton>
                </ScrollView>
            </SafeAreaView>
        </LazyHOC>
    );
});

const styles = StyleSheet.create(
    {
        mapView: {
            width: '100%',
            height: '100%',
            backgroundColor: Colors.grey,
        },
        filtersButton: {
            marginRight: 10,
        },
        toggleButton: {
            marginRight: 7,
        },
        safearew: {
            flex: 1,
            backgroundColor: Colors.white
        },
        header: {
            height: 50,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingHorizontal: 15,
            elevation: 2,
            backgroundColor: 'white',
        },
        headerTitle: {
            fontSize: Fonts.size.normal,
        },
        clearBtn: {
            fontSize: Fonts.size.normal,
            color: '#00cc66'
        },
        addButtonContainer: {
            backgroundColor: Colors.foregroundColor,
            borderRadius: 5, padding: 15, margin: 10,
            marginTop: 27, marginBottom: 55
        },
        filterButton: {
            color: Colors.white,
            fontWeight: 'bold', fontSize: Fonts.size.medium
        },
        selectList: {
            flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', borderBottomWidth: 1,
            borderBottomColor: Colors.border
        },
        title: {
            fontSize: Fonts.size.normal,
            fontWeight: '700', color: 'rgba(0,0,0,0.5)', paddingHorizontal: 10
        },
        appSettingsTypeContainer: {
            flexDirection: 'row',
            alignItems: 'center', justifyContent: 'space-between', height: 60,
        },
        text: {
            fontSize: Fonts.size.normal,
            color: 'gray', paddingRight: Platform.OS == 'ios' ? 5 : 15
        },
        divider: {height: 0.5, width: '100%', alignSelf: 'flex-end', backgroundColor: '#d3d3d3'},
        row: {
            paddingRight: 20,
            flexDirection: 'row', alignItems: 'center',
            justifyContent: 'space-between',
            height: 60
        },
    });

if (Platform.OS === 'android') {
    if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
}
