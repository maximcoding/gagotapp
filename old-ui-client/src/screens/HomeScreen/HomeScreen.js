import React, {useState, useEffect, memo, useRef} from 'react';
import {View, Alert, Text, SafeAreaView, Linking} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import ActionSheet from 'react-native-actionsheet';
import AddListingScreen from '../AddListingScreen';
import Geolocation from '@react-native-community/geolocation';
import {Translate} from '../../core/i18n/IMLocalization';
import {styles} from './HomeScreenStyle';
import SortHomeModal from '../../components/sortHomeModal';
import GridModal from '../../components/girdViews';
import PostsComponent from '../../components/postsComponent';
import {
  subscribeListingCategories,
  getPropertyPreviews,
  removeProperty,
  filterCategories,
} from '../../redux/actions/postListing';
import {resetFilter, filterPosts} from '../../redux/actions/filters';
import {changeView, changeSort, updateLocation, getCurrencyPrices} from '../../redux/actions/user';
import {HomeScreenOptions, FilterHeader} from './HomeScreenHeaders';
import {AppStyles} from '../../AppStyles';

function HomeScreen(props) {
  let {user, grid, sortBy} = useSelector((state) => state.auth);
  let {posts} = useSelector((state) => state.postReducer);
  let {categories} = useSelector((state) => state.categories);
  let {showFilter, filterValues, filterCount, category, filters} = useSelector(
    (state) => state.filterReducer,
  );
  let {updator} = useSelector((state) => state.languageReducer);
  const dispatch = useDispatch();
  const refAction = useRef();
  let [state, setState] = useState({
    selectedItem: null,
    postModalVisible: false,
  });
  let [isOpen, setIsOpen] = useState(false);
  let [isGrid, setIsGrid] = useState(false);

  useEffect(() => {
    dispatch(getCurrencyPrices());
    if (!categories.length) {
      dispatch(subscribeListingCategories(onCategoriesCollectionUpdate));
    }
    getCurrentPosition();
    Linking.getInitialURL().then((url) => {
      // console.log('initial url is here');
    });
  }, []);

  const getCurrentPosition = () => {
    Geolocation.getCurrentPosition(
      (position) => {
        dispatch(updateLocation(position.coords));
      },
      (error) => console.log(error.message),
      {enableHighAccuracy: false, timeout: 6000},
    );
  };

  const closeSort = () => {
    setIsOpen(false);
  };

  const closeGrid = () => {
    setIsGrid(false);
  };

  const resetFilterHelper = () => {
    dispatch(resetFilter());
  };

  const openSort = () => {
    setIsOpen(true);
  };

  const openGrid = () => {
    setIsGrid(true);
  };

  const onCategoriesCollectionUpdate = () => {
    setState({...state, loading: false});
  };

  const onPressPost = () => {
    setState({
      ...state,
      selectedItem: null,
      postModalVisible: true,
    });
  };

  const onPostCancel = () => {
    setState({...state, postModalVisible: false});
  };

  const onLongPressListingItem = (item) => {
    if (item.authorID._id == user._id || user.type == 'admin') {
      setState({...state, selectedItem: item});
      refAction.current.show();
    }
  };

  const sortPosts = (value) => {
    dispatch(changeSort(value));
    dispatch(getPropertyPreviews(() => {}, 0, value));
  };

  const setGrid = (value) => {
    dispatch(changeView(value));
  };

  const onListingItemActionDone = (index) => {
    if (index == 0) {
      setState({
        ...state,
        postModalVisible: true,
      });
    }
    if (index == 1) {
      Alert.alert(
        Translate('Delete Listing'),
        Translate('Are you sure you want to remove this listing?'),
        [
          {
            text: Translate('Yes'),
            onPress: removeListingFunc,
            style: 'destructive',
          },
          {text: Translate('No')},
        ],
        {cancelable: false},
      );
    }
  };
  const deleteCallback = (success) => {
    if (!success) {
      alert(
        Translate('There was an error deleting the listing. Please try again'),
      );
    }
  };
  const removeListingFunc = () => {
    dispatch(removeProperty(state.selectedItem, deleteCallback));
  };

  return (
    <SafeAreaView style={AppStyles.screenContainer}>
      {user && (
        <>
          {showFilter ? (
            <FilterHeader
              resetFilter={resetFilterHelper}
              length={filterCount}
            />
          ) : (
            <HomeScreenOptions
              navigation={props.navigation}
              user={user}
              onPressPost={onPressPost}
              openSort={openSort}
              openGrid={openGrid}
              isOpen={isOpen}
              isGrid={isGrid}
            />
          )}
        </>
      )}
      <View style={styles.container}>
        <PostsComponent
          data={showFilter ? filters : posts}
          Func={
            showFilter == 'category'
              ? filterCategories
              : showFilter == 'filter'
              ? filterPosts
              : getPropertyPreviews
          }
          params={
            showFilter == 'category'
              ? category
              : showFilter == 'filter'
              ? filterValues
              : sortBy
          }
          grid={grid}
          navigation={props.navigation}
          onLongPressListingItem={onLongPressListingItem}
        />
        {state.postModalVisible && (
          <AddListingScreen
            categories={categories}
            onCancel={onPostCancel}
            selectedItem={state.selectedItem}
          />
        )}
        <ActionSheet
          ref={refAction}
          title={Translate('Confirm')}
          options={[
            Translate('Edit Listing'),
            Translate('Remove Listing'),
            Translate('Cancel'),
          ]}
          cancelButtonIndex={2}
          destructiveButtonIndex={1}
          onPress={(index) => {
            onListingItemActionDone(index);
          }}
        />
        <SortHomeModal
          isOpen={isOpen}
          closeSort={closeSort}
          callBack={sortPosts}
          sortBy={sortBy}
        />
        <GridModal
          isOpen={isGrid}
          closeSort={closeGrid}
          callBack={setGrid}
          grid={grid}
        />
      </View>
    </SafeAreaView>
  );
}

export default memo(HomeScreen);
