import React from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet} from 'react-native';
import moment from 'moment';
import {ListStyle} from '../../AppStyles';
import {getSymbol, getPrice} from '../../utils/currencyConverter';
import {Colors} from '../../themes';
import NumberFormat from 'react-number-format';
import {Translate} from '../../core/i18n/IMLocalization';

export default function HomeList({
                                     item,
                                     onPressListingItem,
                                     onLongPressListingItem,
                                     currencyRates,
                                     user,
                                 }) {
    return (
        <TouchableOpacity
            onPress={() => onPressListingItem(item)}
            onLongPress={() => {
                onLongPressListingItem(item);
            }}>
            <View style={styles.rowM}>
                <Image source={{uri: item.photo}} style={ListStyle.avatarStyle}/>
                <View style={{alignItems: 'flex-start', justifyContent: 'space-between'}}>
                    <Text style={ListStyle.address}>{item.place.slice(0, 20)}</Text>
                    <Text style={ListStyle.time}>
                        {moment(item.createdAt).fromNow(true)} {Translate('ago')}
                    </Text>
                </View>
                <NumberFormat
                    renderText={(text) => (
                        <Text style={[ListStyle.price, styles.pri]}>{text}</Text>
                    )}
                    value={getPrice(Number(item.price), user, currencyRates)}
                    displayType={'text'}
                    thousandSeparator={true}
                    prefix={getSymbol(user)}
                />
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    pri: {position: 'absolute', bottom: 0, right: 4},

    rowM: {
        flexDirection: 'row',
        marginTop: 2,
        height: 120,
        width: '100%',
        backgroundColor: Colors.backgroundColor,
        shadowColor: Colors.primary,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
        borderWidth: 1,
        borderColor: Colors.border,
    },
});
