import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import moment from 'moment';
import EIcon from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {getSymbol, getPrice} from '../../utils/currencyConverter';
import {Colors, Fonts} from '../../themes';
import NumberFormat from 'react-number-format';

export default function HomeTextList({
  item,
  onPressSavedIcon,
  savedPosts,
  onPressListingItem,
  onLongPressListingItem,
  currencyRates,
  user,
}) {
  return (
    <TouchableOpacity
      onPress={() => onPressListingItem(item)}
      onLongPress={() => {
        onLongPressListingItem(item);
      }}
      style={styles.container}>
      <Text style={styles.address} numberOfLines={1} ellipsizeMode="tail">
        <EIcon name="address" color="gray" /> {item.place}
      </Text>
      <View style={styles.row}>
        <TouchableOpacity onPress={() => onPressSavedIcon(item)}>
          <FontAwesome
            name={'heart'}
            size={15}
            style={{
              color: savedPosts.some((e) => e._id === item._id)
                ? 'red'
                : '#d3d3d3',
            }}
          />
        </TouchableOpacity>

        <NumberFormat
          renderText={(text) => <Text style={styles.price}>{text}</Text>}
          value={getPrice(Number(item.price), user, currencyRates)}
          displayType={'text'}
          thousandSeparator={true}
          prefix={getSymbol(user)}
        />

        <Text style={styles.date}>
          <EIcon name="clock" color="gray" />
          {moment(item.createdAt).fromNow(true)} ago
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginVertical: 2,
    padding: 10,
    backgroundColor: Colors.backgroundColor,
    shadowColor: Colors.primary,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    borderWidth: 1,
    borderColor: Colors.border,
  },
  address: {
    color: Colors.primary
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  price: {
  },
  date: {
  },
});
