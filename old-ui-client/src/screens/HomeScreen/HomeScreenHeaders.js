import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, SafeAreaView} from 'react-native';
import {styles} from './HomeScreenStyle';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {AppStyles, HeaderButtonStyle} from '../../AppStyles';
import {Translate} from '../../core/i18n/IMLocalization';
import Colors from '../../themes/colors';
import {Fonts, Images} from '../../themes';

export const HomeScreenOptions = ({
                                      navigation,
                                      user,
                                      onPressPost,
                                      openGrid,
                                      isOpen,
                                      openSort,
                                      isGrid,
                                  }) => {
    let [show, setShow] = useState(true);
    useEffect(() => {
        if (isOpen || isGrid) {
            setShow(false);
        } else {
            setShow(true);
        }
    }, [isOpen, isGrid]);
    return (
        <View style={AppStyles.headerOptionsContainer}>
            <Text style={[AppStyles.text.homeHeaderTitle]}>{Translate('Home')}</Text>
            {show && (
                <View style={{display: 'flex', flexDirection: 'row', flex: 1}}>
                    <View
                        style={{
                            display: 'flex',
                            flexDirection: 'row',
                            flex: 1,
                            alignItems: 'center',
                        }}>
                        <TouchableOpacity
                            onPress={() => onPressPost()}
                            style={{elevation: 3, opacity: 0.8}}
                            disabled={user?.phone == null}>
                            <Ionicons
                                name={'add-circle-outline'}
                                size={25}
                                color={Colors.green}
                            />
                        </TouchableOpacity>
                    </View>
                    <View
                        style={{
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}>
                        <>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('Map')}
                                style={{elevation: 3, opacity: 0.8, paddingRight: 5}}
                                disabled={user?.phone == null}>
                                <FontAwesome5
                                    style={styles.headerButton}
                                    name={'map-marked-alt'}
                                    size={21}
                                    color={Colors.green}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => openGrid()}
                                style={{elevation: 3, opacity: 0.8, paddingRight: 5}}
                                disabled={user?.phone == null}>
                                <Ionicons
                                    name={
                                        Platform.OS === 'ios' ? 'ios-grid-outline' : 'grid-outline'
                                    }
                                    size={21}
                                    color={Colors.green}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => openSort()}
                                style={{elevation: 3, opacity: 0.8}}
                                disabled={user?.phone == null}>
                                <FontAwesome5
                                    style={[styles.headerButton, {marginRight: 0}]}
                                    name={'sort-amount-up'}
                                    size={21}
                                    color={Colors.green}
                                />
                            </TouchableOpacity>
                        </>
                    </View>
                </View>
            )}
        </View>
    );
};

export const FilterHeader = ({resetFilter, length}) => {
    return (
        <View style={HeaderButtonStyle.main}>
            <View>
                <FontAwesome5 name="filter" size={17} color={Colors.green}/>
                {length > 0 && (
                    <View style={styles.badge}>
                        <Text style={{
                            color: Colors.white,
                            fontWeight: 'bold',
                            fontSize: Fonts.size.small
                        }}>{length}</Text>
                    </View>
                )}
            </View>
            <Text
                style={{
                    fontSize: Fonts.style.title,
                    fontWeight: 'bold',
                    paddingLeft: '5%'
                }}>
                {Translate('Filter Result')}
            </Text>
            <View>
                <TouchableOpacity onPress={() => resetFilter()}>
                    <Text style={{color: Colors.green, fontSize: 16}}>
                        {Translate('Reset')}
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};
