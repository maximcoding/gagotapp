import React, {useState, memo} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {AppStyles} from '../AppStyles';
import {TabView} from 'react-native-tab-view';
import DetailsScreen from './DetailsScreen/DetailsScreen';
import Videos from './Videos';

const Details = ({route, navigation}) => {
  let item = route.params.item;
  let reviewId = route.params?.reviewId ?? false;

  const [index, setIndex] = useState(0);
  const routes = [
    {key: 'photos', title: 'Photos'},
    {key: 'videos', title: 'Videos'},
  ];

  const renderScene = ({route}) => {
    switch (route.key) {
      case 'photos':
        return (
          <DetailsScreen
            item={item}
            reviewId={reviewId}
            navigation={navigation}
          />
        );
      case 'videos':
        return <Videos videos={item.videoURLs} item={item} />;
    }
  };

  const renderTabBar = (props) => {
    const activeIndex = props.navigationState.index;

    return (
      <View
        style={[
          AppStyles.tabStyles.tabBar,
          {marginHorizontal: 0, marginBottom: 0, backgroundColor: 'white'},
        ]}>
        {props.navigationState.routes.map((route, i) => {
          const active = activeIndex === i;

          const tabItemStyle = active
            ? AppStyles.tabStyles.tabItemActive
            : AppStyles.tabStyles.tabItem;

          const tabItemTxtStyle = active
            ? AppStyles.tabStyles.tabItemTxtActive
            : AppStyles.tabStyles.tabItemTxt;

          return (
            <TouchableOpacity
              key={route.title}
              style={tabItemStyle}
              onPress={() => setIndex(i)}>
              <View style={{alignItems: 'center'}}>
                <Text style={tabItemTxtStyle}>{route.title}</Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <TabView
        lazy={false}
        onIndexChange={(i) => setIndex(i)}
        renderScene={renderScene}
        renderTabBar={renderTabBar}
        navigationState={{index, routes}}
      />
    </View>
  );
};

export default memo(Details);
