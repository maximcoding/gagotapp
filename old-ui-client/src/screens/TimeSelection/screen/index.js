import React, {memo, useState, useRef} from 'react';
import {Modal, View, Text, TouchableOpacity} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import styles from '../component/styles';
import Day from '../component/day';
import TimePicker from 'react-native-24h-timepicker';

const commonColor = '#00cc44';

export default memo(({open, close, getOpendoors}) => {
  const [is24Hr, set24Hr] = useState({
    mon: false,
    tue: false,
    wed: false,
    fri: false,
    thurs: false,
    sat: false,
    sun: false,
  });
  const [isClosed, setClosed] = useState({
    mon: false,
    tue: false,
    wed: false,
    fri: false,
    thurs: false,
    sat: false,
    sun: false,
  });
  const [day, setDay] = useState('mon');
  const [from, setFrom] = useState({
    mon: '',
    tue: '',
    wed: '',
    fri: '',
    thurs: '',
    sat: '',
    sun: '',
  });
  const [to, setTo] = useState({
    mon: '',
    tue: '',
    wed: '',
    fri: '',
    thurs: '',
    sat: '',
    sun: '',
  });

  const changeFrom = (text) => {
    setFrom({...from, [day]: text});
    cancelFrom();
  };
  const changeTo = (text) => {
    setTo({...to, [day]: text});
    cancelTo();
  };

  const pickerOne = () => {
    ref1.open();
  };

  const cancelFrom = () => {
    ref1.close();
  };

  const pickerTwo = () => {
    ref2.open();
  };

  const cancelTo = () => {
    ref2.close();
  };

  const getValueDay = (value) => {
    if (isClosed[value]) {
      return 'closed';
    } else if (is24Hr[value]) {
      return 'open';
    } else {
      return `${from[value]} to ${to[value]}`;
    }
  };

  const getValues = () => {
    if (
      (from['mon'] == '' || to['mon'] == '') &&
      !is24Hr['mon'] &&
      !isClosed['mon']
    ) {
      alert('Must select Day Monday value');
      return;
    } else if (
      (from['tue'] == '' || to['tue'] == '') &&
      !is24Hr['tue'] &&
      !isClosed['tue']
    ) {
      alert('Must select Day Tuesday value');
      return;
    } else if (
      (from['wed'] == '' || to['wed'] == '') &&
      !is24Hr['wed'] &&
      !isClosed['wed']
    ) {
      alert('Must select Day Wednesday value');
      return;
    } else if (
      (from['thurs'] == '' || to['thurs'] == '') &&
      !is24Hr['thurs'] &&
      !isClosed['thurs']
    ) {
      alert('Must select Day Thursday value');
      return;
    } else if (
      (from['fri'] == '' || to['fri'] == '') &&
      !is24Hr['fri'] &&
      !isClosed['fri']
    ) {
      alert('Must select Day Friday value');
      return;
    } else if (
      (from['sat'] == '' || to['sat'] == '') &&
      !is24Hr['sat'] &&
      !isClosed['sat']
    ) {
      alert('Must select Day Saturday value');
      return;
    } else if (
      (from['sun'] == '' || to['sun'] == '') &&
      !is24Hr['sun'] &&
      !isClosed['sun']
    ) {
      alert('Must select Day Sunday value');
      return;
    } else {
      // values here
      let open_doors = {
        mon: getValueDay('mon'),
        tue: getValueDay('tue'),
        wed: getValueDay('wed'),
        thurs: getValueDay('thurs'),
        fri: getValueDay('fri'),
        sat: getValueDay('sat'),
        sun: getValueDay('sun'),
      };
      getOpendoors(open_doors);
      close();
    }
  };

  let ref1 = useRef();
  let ref2 = useRef();

  return (
    <Modal
      visible={open}
      animationType="slide"
      transparent={true}
      onRequestClose={close}>
      <View style={styles.modalCont}>
        <View style={styles.card}>
          <Text>Select days & time</Text>
          <View style={styles.daysCont}>
            <Day day="M" active={day === 'mon'} onPress={() => setDay('mon')} />
            <Day day="T" active={day === 'tue'} onPress={() => setDay('tue')} />
            <Day day="W" active={day === 'wed'} onPress={() => setDay('wed')} />
            <Day
              day="T"
              active={day === 'thurs'}
              onPress={() => setDay('thurs')}
            />
            <Day day="F" active={day === 'fri'} onPress={() => setDay('fri')} />
            <Day day="S" active={day === 'sat'} onPress={() => setDay('sat')} />
            <Day day="S" active={day === 'sun'} onPress={() => setDay('sun')} />
          </View>
          <View style={styles.checkCont}>
            <TouchableOpacity
              onPress={() => set24Hr({...is24Hr, [day]: !is24Hr[day]})}
              style={styles.checkItem}
              disabled={isClosed[day]}>
              <CheckBox
                tintColors={{true: commonColor}}
                disabled={isClosed[day]}
                value={is24Hr[day]}
                onValueChange={(newValue) =>
                  set24Hr({...is24Hr, [day]: newValue})
                }
              />
              <Text style={styles.label}>Open 24 Hours</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setClosed({...isClosed, [day]: !isClosed[day]})}
              style={styles.checkItem}
              disabled={is24Hr[day]}>
              <CheckBox
                tintColors={{true: commonColor}}
                disabled={is24Hr[day]}
                value={isClosed[day]}
                onValueChange={(newValue) =>
                  setClosed({...isClosed, [day]: newValue})
                }
              />
              <Text style={styles.label}>Closed</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.fieldCont}>
            <View style={styles.input}>
              <Text style={styles.label}>Open Time</Text>
              <TouchableOpacity
                style={styles.hoursBtn}
                onPress={pickerOne}
                disabled={is24Hr[day] || isClosed[day] ? true : false}>
                <Text style={styles.hourText}>
                  {from[day] ? from[day] : '00:00'}
                </Text>
              </TouchableOpacity>
              <TimePicker
                ref={(ref) => {
                  ref1 = ref;
                }}
                onCancel={() => cancelFrom()}
                onConfirm={(hour, minute) => changeFrom(`${hour}:${minute}`)}
              />
            </View>
            <View style={[styles.input, styles.inputLast]}>
              <Text style={styles.label}>Close Time</Text>
              <TouchableOpacity
                style={styles.hoursBtn}
                onPress={pickerTwo}
                disabled={is24Hr[day] || isClosed[day] ? true : false}>
                <Text style={styles.hourText}>
                  {to[day] ? to[day] : '00:00'}
                </Text>
              </TouchableOpacity>
              <TimePicker
                ref={(ref) => {
                  ref2 = ref;
                }}
                onCancel={() => cancelTo()}
                onConfirm={(hour, minute) => changeTo(`${hour}:${minute}`)}
              />
            </View>
          </View>
          <View style={styles.actionCont}>
            <TouchableOpacity style={styles.cancelBtn} onPress={close}>
              <Text style={styles.cancelTxt}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.saveBtn} onPress={getValues}>
              <Text style={styles.saveTxt}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
});
