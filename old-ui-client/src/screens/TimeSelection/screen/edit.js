import React, {memo, useState, useRef} from 'react';
import {Modal, View, Text, TouchableOpacity} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import styles from '../component/styles';
import TimePicker from 'react-native-24h-timepicker';

const commonColor = '#00cc44';

export default memo(({open, close, getOpendoors}) => {
  const [is24Hr, set24Hr] = useState(false);
  const [isClosed, setClosed] = useState(false);

  const [from, setFrom] = useState('');
  const [to, setTo] = useState('');

  const changeFrom = (text) => {
    setFrom(text);
    cancelFrom();
  };
  const changeTo = (text) => {
    setTo(text);
    cancelTo();
  };

  const pickerOne = () => {
    ref1.open();
  };

  const cancelFrom = () => {
    ref1.close();
  };

  const pickerTwo = () => {
    ref2.open();
  };

  const cancelTo = () => {
    ref2.close();
  };

  const getValues = () => {
    let value = is24Hr ? 'open' : isClosed ? 'closed' : `${from} to ${to}`;
    getOpendoors(value);
    close();
  };

  let ref1 = useRef();
  let ref2 = useRef();

  return (
    <Modal
      visible={open}
      animationType="slide"
      transparent={true}
      onRequestClose={close}>
      <View style={styles.modalCont}>
        <View style={styles.card}>
          <Text>Edit Time</Text>

          <View style={styles.checkCont}>
            <TouchableOpacity
              onPress={() => set24Hr(!is24Hr)}
              style={styles.checkItem}
              disabled={isClosed}>
              <CheckBox
                tintColors={{true: commonColor}}
                disabled={isClosed}
                value={is24Hr}
                onValueChange={(newValue) => set24Hr(newValue)}
              />
              <Text style={styles.label}>Open 24 Hours</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setClosed(!isClosed)}
              style={styles.checkItem}
              disabled={is24Hr}>
              <CheckBox
                tintColors={{true: commonColor}}
                disabled={is24Hr}
                value={isClosed}
                onValueChange={(newValue) => setClosed(newValue)}
              />
              <Text style={styles.label}>Closed</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.fieldCont}>
            <View style={styles.input}>
              <Text style={styles.label}>Open Time</Text>

              <TouchableOpacity
                style={styles.hoursBtn}
                onPress={pickerOne}
                disabled={is24Hr || isClosed ? true : false}>
                <Text style={styles.hourText}>{from ? from : '00:00'}</Text>
              </TouchableOpacity>

              <TimePicker
                ref={(ref) => {
                  ref1 = ref;
                }}
                onCancel={() => cancelFrom()}
                onConfirm={(hour, minute) => changeFrom(`${hour}:${minute}`)}
              />
            </View>
            <View style={[styles.input, styles.inputLast]}>
              <Text style={styles.label}>Close Time</Text>

              <TouchableOpacity
                style={styles.hoursBtn}
                onPress={pickerTwo}
                disabled={is24Hr || isClosed ? true : false}>
                <Text style={styles.hourText}>{to ? to : '00:00'}</Text>
              </TouchableOpacity>

              <TimePicker
                ref={(ref) => {
                  ref2 = ref;
                }}
                onCancel={() => cancelTo()}
                onConfirm={(hour, minute) => changeTo(`${hour}:${minute}`)}
              />
            </View>
          </View>
          <View style={styles.actionCont}>
            <TouchableOpacity style={styles.cancelBtn} onPress={close}>
              <Text style={styles.cancelTxt}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.saveBtn} onPress={getValues}>
              <Text style={styles.saveTxt}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
});
