import React, {memo} from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {Colors} from '../../../themes';
import styles from './styles';

export default memo(({day, active, onPress}) => {
  return (
    <TouchableOpacity
      style={[styles.day, active && {backgroundColor: Colors.grey9}]}
      onPress={onPress}>
      <Text style={active && styles.activeDayText}>{day}</Text>
    </TouchableOpacity>
  );
});
