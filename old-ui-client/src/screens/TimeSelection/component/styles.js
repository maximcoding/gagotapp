import {StyleSheet, Dimensions} from 'react-native';
import {Colors} from '../../../themes';

let {width} = Dimensions.get('window');

const commonColor = Colors.green;

export default StyleSheet.create({
  modalCont: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.3)',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 25,
  },
  card: {
    backgroundColor: Colors.white,
    padding: 20,
    width: '100%',
    borderRadius: 8,
  },
  daysCont: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop: 15,
  },
  day: {
    borderWidth: 1,
    borderColor: Colors.grey6,
    width: 30,
    height: 30,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  checkCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 20,
  },
  checkItem: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    color: Colors.grey9,
  },

  activeDayText: {
    color: Colors.white,
  },
  fieldCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 20,
  },
  input: {
    width: '50%',
  },
  inputLast: {
    marginLeft: '5%',
  },
  field: {
    borderWidth: 1,
    borderColor: Colors.grey6,
    width: '95%',
    borderRadius: 5,
    height: 40,
    marginTop: 5,
  },
  actionCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15,
  },

  cancelBtn: {
    borderWidth: 1,
    borderColor: commonColor,
    borderRadius: 8,
    paddingVertical: 3,
    width: '32%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  saveBtn: {
    backgroundColor: commonColor,
    borderRadius: 8,
    width: '32%',
    paddingVertical: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  saveTxt: {
    color: Colors.white,
    fontWeight: 'bold',
  },
  cancelTxt: {
    fontWeight: 'bold',
    color: Colors.grey9,
  },
  hoursBtn: {
    width: width * 0.3,
    height: 35,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#d3d3d3',
    borderWidth: 0.5,
    marginTop: 10,
  },
  hourText: {
    color: Colors.grey,
  },
});
