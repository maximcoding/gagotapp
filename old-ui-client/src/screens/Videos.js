import React, {useState} from 'react';
import {View, StyleSheet, Dimensions, Text} from 'react-native';
import VideoViewComponent from '../utils/videoComponent';
import HelpModal from '../components/videoModal';
let {width} = Dimensions.get('window');

export default function Videos({videos = [], item}) {
  const [visible, setVisible] = useState(false);
  const [helpUrl, setHelpUrl] = useState(null);
  return (
    <View style={styles.container}>
      {videos.length > 0 ? (
        videos.map((url, index) => {
          return (
            <VideoViewComponent
              onPress={() => {
                setHelpUrl(url);
                setVisible(true);
              }}
              item={item}
              index={index}
            />
          );
        })
      ) : (
        <View style={styles.center}>
          <Text>No Video for this property</Text>
        </View>
      )}
      <HelpModal visible={visible} setVisible={setVisible} helpUrl={helpUrl} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
