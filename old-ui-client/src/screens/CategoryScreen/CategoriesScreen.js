import React, {memo} from 'react';
import {
  View,
  Text,
  ImageBackground,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {showCategoryFilter} from '../../redux/actions/postListing';
import {Colors, Fonts} from '../../themes';

export default memo(function CategoriesScreen({navigation}) {
  const dispatch = useDispatch();
  let {categories} = useSelector((state) => state.categories);

  const callBack = () => {
    navigation.navigate('Home');
  };

  const onPressCategoryItem = (item) => {
    dispatch(showCategoryFilter(item._id));
    callBack();
  };

  const renderListingItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => onPressCategoryItem(item)}
        style={{paddingHorizontal: 10}}>
        <ImageBackground source={{uri: item.photo}} style={styles.image}>
          <Text style={styles.title}>{item.name}</Text>
        </ImageBackground>
      </TouchableOpacity>
    );
  };
  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: Colors.white,
      }}>
      <View style={styles.header}>
        <Text style={styles.headTitle}>Categories</Text>
      </View>
      <FlatList
        vertical
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{flexGrow: 1, paddingBottom: 100}}
        data={categories}
        renderItem={renderListingItem}
        keyExtractor={(item, index) => index}
      />
    </SafeAreaView>
  );
});

const styles = StyleSheet.create({
  image: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    height: 150,
  },
  title: {
    fontSize: Fonts.size.xxxlarge,
    color: 'white',
    fontWeight: 'bold',
  },
  header: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    elevation: 2,
  },
  headTitle: {
    fontSize: Fonts.style.title,
    fontWeight: 'bold',
  },
});
