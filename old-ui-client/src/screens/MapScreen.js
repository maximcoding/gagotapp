import React, {memo, useState, useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import {AppStyles} from '../AppStyles';
import {Translate} from '../core/i18n/IMLocalization';
import Colors from '../themes/colors';
import CustomMarker from '../components/customMarker';
import {useSelector, useDispatch} from 'react-redux';
import MapStyle from '../components/mapStyle';
import {getMapsPosts} from '../redux/actions/postListing';
import Search from '../components/mapSearch';
export const MapScreenNavigationOptions = () => {
  return {
    title: Translate('Map View'),
    headerTitleStyle: AppStyles.text.headerTitleStyle,
  };
};

function MapScreen(props) {
  const {navigation} = props;
  const dispatch = useDispatch();
  let {coordinates} = useSelector((state) => state.auth);
  let {mapPosts} = useSelector((state) => state.postReducer);

  useEffect(() => {
    dispatch(getMapsPosts(coordinates));
  }, [coordinates]);

  const onPress = (item) => {
    navigation.navigate('Detail', {
      item: item,
      customLeft: true,
      headerLeft: null,
      routeName: 'Map',
    });
  };

  return (
    <View>
      <MapView
        style={styles.mapView}
        customMapStyle={MapStyle}
        showsUserLocation={true}
        showsMyLocationButton={true}
        showsBuildings={true}
        region={{
          latitude: coordinates.latitude,
          longitude: coordinates.longitude,
          latitudeDelta: 0.25,
          longitudeDelta: 0.28,
        }}>
        {mapPosts.length > 0 &&
          mapPosts.map((item, i) => {
            return (
              <Marker
                key={i}
                coordinate={{
                  latitude: item.latitude,
                  longitude: item.longitude,
                }}
                onPress={() => onPress(item)}>
                <CustomMarker item={item} />
              </Marker>
            );
          })}
      </MapView>
      <View style={styles.search}>
        <Search />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mapView: {
    width: '100%',
    height: '100%',
    backgroundColor: Colors.backgroundColor,
  },
  search: {
    position: 'absolute',
    top: 50,
    left: 0,
    right: 0,
  },
});

export default memo(MapScreen);
