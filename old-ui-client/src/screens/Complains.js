import React, {useState, useEffect, memo} from 'react';
import {View, Text, FlatList, StyleSheet, Image} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {getFunctionHelper, onRefresh, loadMore} from '../utils/lazyLoading';
import {getComplains} from '../redux/actions/admin';
import ActivityIndicator from '../components/activityIndicator';
import ComplainItem from '../components/complainItem';
import {Fonts} from '../themes';

export default memo(function Complains({route}) {
  const dispatch = useDispatch();
  let {complains} = useSelector((state) => state.adminReducer);
  let [isFetching, setFetching] = useState(false);
  let [loader, setLoader] = useState(false);
  let [moreLoader, setMoreLoader] = useState(false);
  let [page, setPage] = useState(0);

  const renderItem = ({item, index}) => {
    return <ComplainItem item={item} index={index} />;
  };

  useEffect(() => {
    getFunctionHelper('complain', 0, setLoader, dispatch, getComplains);
  }, []);

  if (loader) {
    return <ActivityIndicator />;
  } else
    return (
      <FlatList
        vertical
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{flexGrow: 1, backgroundColor: 'white'}}
        data={complains}
        renderItem={renderItem}
        keyExtractor={(item, index) => item._id}
        onEndReachedThreshold={0.5}
        onRefresh={() =>
          onRefresh('complain', setPage, setFetching, dispatch, getComplains)
        }
        refreshing={isFetching}
        onEndReached={() =>
          loadMore(
            'complain',
            moreLoader,
            page,
            setMoreLoader,
            setPage,
            dispatch,
            getComplains,
          )
        }
        ListEmptyComponent={() => {
          return (
            <View style={styles.blankView}>
              <Text style={styles.backImage}>No Complains yet !</Text>
            </View>
          );
        }}
      />
    );
});

const styles = StyleSheet.create({
  backImage: {
    color: 'gray',
  },
  img: {
    height: 55,
    width: 55,
    borderRadius: 55 / 2,
  },
  blankView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  col: {
    alignItems: 'flex-start',
    paddingLeft: 10,
  },
  row: {
    width: '95%',
    alignSelf: 'center',
    backgroundColor: '#f0f5f5',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10,
    marginTop: 10,
  },
  rowInner: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  time: {
    fontSize: Fonts.size.tiny,
    color: 'gray',
  },
});
