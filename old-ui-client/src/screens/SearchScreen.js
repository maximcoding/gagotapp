import React, {useState, useEffect, memo} from 'react';
import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  View,
  Image,
  ActivityIndicator,
  SafeAreaView,
  Dimensions,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useDispatch, useSelector} from 'react-redux';
import {AppIcon, AppStyles, TwoColumnListStyle} from '../AppStyles';
import {Configuration} from '../Configuration';
import {Translate} from '../core/i18n/IMLocalization';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Fonts from '../themes/fonts';
import {
  showCategoryFilter,
  searchListingAction,
} from '../redux/actions/postListing';
import {Images} from '../themes';
import Colors from '../themes/colors';

let {width} = Dimensions.get('window');

function SearchScreen({navigation}) {
  let {posts, searchPosts} = useSelector((state) => state.postReducer);
  let {categories} = useSelector((state) => state.categories);
  let {updator} = useSelector((state) => state.languageReducer);
  const dispatch = useDispatch();
  let [state, setState] = useState({
    renting_data: [],
    selling_data: [],
  });
  let [text, setText] = useState('');
  let [sLoader, setSLoader] = useState(false);

  useEffect(() => {
    if (searchPosts.length > 0) {
      getRentsSales(searchPosts);
    } else {
      getRentsSales(posts);
    }
  }, [searchPosts, sLoader]);

  const getRentsSales = (posts) => {
    let postsListing = posts.slice(0, 20);
    let one = postsListing.filter((p) => p.type == 'rent');
    let two = postsListing.filter((p) => p.type == 'buy');
    setState({
      ...state,
      renting_data: one,
      selling_data: two,
    });
  };

  const renderCategoryItem = ({item}) => (
    <TouchableOpacity
      onPress={() => onPressCategoryItem(item)}
      style={styles.categoryItem}>
      <FastImage style={styles.categoryItemPhoto} source={{uri: item.photo}} />
      <Text style={styles.categoryItemTitle}>{item.name}</Text>
    </TouchableOpacity>
  );

  const renderListing = ({item}) => {
    return (
      <TouchableOpacity onPress={() => onPressListingItem(item)}>
        <View style={TwoColumnListStyle.listingItemContainer}>
          <FastImage
            style={TwoColumnListStyle.listingPhoto}
            source={{uri: item.photo}}
          />
          <Text
            ellipsizeMode="tail"
            numberOfLines={1}
            style={[
              AppStyles.text.listTitle,
              {maxHeight: 40, paddingHorizontal: 5, paddingVertical: 1},
            ]}>
            {item.title}
          </Text>
          <Text
            ellipsizeMode="tail"
            numberOfLines={1}
            style={[
              AppStyles.text.description,
              {paddingHorizontal: 5, paddingVertical: 1},
            ]}>
            {item.place}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const onPressListingItem = (item) => {
    navigation.navigate('Detail', {
      item: item,
      customLeft: true,
      routeName: 'Home',
    });
  };

  const callBack = () => {
    navigation.navigate('Home');
  };

  const onPressCategoryItem = (item) => {
    dispatch(showCategoryFilter(item._id));
    callBack();
  };

  const stopLoader = () => {
    setSLoader(false);
  };

  const searchListings = (text) => {
    setText(text);
    setSLoader(true);
    dispatch(searchListingAction(text, stopLoader));
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <View style={styles.header}>
        <Text style={{color: Colors.white}}>For</Text>
        <Text style={AppStyles.text.sectionTitle}>{Translate('Filter')}</Text>
        <TouchableOpacity onPress={() => navigation.navigate('FiltersScreen')}>
          <Image style={[AppIcon.style]} source={Images.filter} />
        </TouchableOpacity>
      </View>
      <View style={styles.container}>
        <View
          style={{
            height: 35,
            display: 'flex',
            marginBottom: 30,
            backgroundColor: '#EEEE',
            borderRadius: 5,
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 5,
            width: '100%',
          }}>
          <TextInput
            placeholder={Translate('Search')}
            onChangeText={searchListings}
            style={{height: 35, width: width / 1.1}}
            value={text}
          />
          {sLoader ? (
            <View style={{position: 'absolute', right: 5, top: 10}}>
              <ActivityIndicator size="small" color={Colors.green} />
            </View>
          ) : (
            <Ionicons
              name="ios-search"
              style={{
                fontSize: Fonts.size.input,
                color: Colors.grey,
                position: 'absolute',
                right: 5,
                top: 10,
              }}
            />
          )}
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text style={AppStyles.text.sectionTitle}>
            {Translate('Categories')}
          </Text>
          <View style={styles.categories}>
            <FlatList
              horizontal={true}
              initialNumToRender={5}
              data={categories}
              showsHorizontalScrollIndicator={false}
              renderItem={(item) => renderCategoryItem(item)}
              keyExtractor={(item) => `${item.id}`}
            />
          </View>
          {state.selling_data.length > 0 && (
            <>
              <Text style={[AppStyles.text.sectionTitle]}>
                {Translate('Selling')}
              </Text>
              <View>
                <FlatList
                  horizontal
                  data={state.selling_data}
                  renderItem={renderListing}
                  keyExtractor={(item) => `${item.id}`}
                  showsHorizontalScrollIndicator={false}
                />
              </View>
            </>
          )}

          {state.renting_data.length > 0 && (
            <>
              <Text style={[AppStyles.text.sectionTitle]}>
                {Translate('Renting')}
              </Text>
              <View>
                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={state.renting_data}
                  renderItem={renderListing}
                  keyExtractor={(item) => `${item.id}`}
                />
              </View>
            </>
          )}
        </ScrollView>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: Configuration.home.listing_item.offset,
    backgroundColor: Colors.backgroundColor,
  },
  categoryItem: {
    marginHorizontal: 5,
    marginVertical: 10,
    backgroundColor: 'white',
    shadowColor: Colors.primary,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: Colors.border,
  },

  categoryItemPhoto: {
    height: 60,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    width: 110,
  },
  categoryItemTitle: {
    ...AppStyles.text.listTitle,
    margin: 10,
  },
  userPhoto: {
    width: 36,
    height: 36,
    borderRadius: 18,
    marginLeft: 10,
  },
  mapButton: {
    marginRight: 13,
    marginLeft: 7,
  },
  starStyle: {
    tintColor: Colors.foregroundColor,
  },
  starRatingContainer: {
    width: 90,
    marginTop: 10,
  },
  header: {
    height: 50,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    elevation: 2,
    backgroundColor: 'white',
  },
});

export default memo(SearchScreen);
