import React, {memo} from 'react';
import {AppStyles} from '../../AppStyles';
import {Translate} from '../../core/i18n/IMLocalization';
import {IMUserProfileComponent} from '../../core/profile/ui/components/IMUserProfileComponent';
import Images from '../../themes/images';
import {logoutUser} from '../../redux/actions/user';
import {useSelector, useDispatch} from 'react-redux';

export const MyProfileScreenOptions = () => {
  return {
    title: Translate('Profile'),
    headerTitleStyle: AppStyles.text.headerTitleStyle,
  };
};

function MyProfileScreen(props) {
  const dispatch = useDispatch();
  const {user} = useSelector((state) => state.auth);
  const {updator} = useSelector((state) => state.languageReducer);
  const menuItems =
    user?.type === 'admin'
      ? [
          {
            title: 'Users',
            subTitle: 'Account',
            icon: Images.accountDetail,
            tintColor: '#6b7be8',
            onPress: () => props.navigation.navigate('Users'),
          },

          {
            title: 'Messages',
            icon: Images.contactUs,
            tintColor: '#a6a4b1',
            onPress: () => {
              props.navigation.navigate('Complains');
            },
          },
        ]
      : [
          {
            title: Translate('Account Details'),
            subTitle: 'Account',
            icon: Images.accountDetail,
            tintColor: '#6b7be8',
            onPress: () =>
              props.navigation.navigate('AccountDetail', {
                appStyles: AppStyles,
              }),
          },

          {
            title: Translate('Settings'),
            icon: Images.settings,
            tintColor: '#a6a4b1',
            onPress: () => {
              props.navigation.navigate('Settings');
            },
          },

          {
            title: Translate('Messages'),
            icon: Images.communication,
            tintColor: '#968cbf',
            onPress: () => {
              props.navigation.navigate('Messages');
            },
          },
          {
            title: Translate('My Favorites'),
            tintColor: '#df9292',
            icon: Images.wishlistFilled,
            onPress: () =>
              props.navigation.navigate('SaveRecentListing', {type: 'fav'}),
          },

          {
            title: Translate('Bids'),
            icon: Images.bell,
            tintColor: '#a6a4b1',
            onPress: () => {},
          },
          {
            title: Translate('Selling'),
            icon: Images.homefilled,
            tintColor: '#a6a4b1',
            onPress: () => {
              props.navigation.navigate('ListingScreen', {type: 'buy'});
            },
          },
          {
            title: Translate('Renting'),
            icon: Images.homefilled,
            tintColor: '#a6a4b1',
            onPress: () => {
              props.navigation.navigate('ListingScreen', {type: 'rent'});
            },
          },
          {
            title: Translate('Recent Viewed'),
            icon: Images.homefilled,
            tintColor: '#a6a4b1',
            onPress: () => {
              props.navigation.navigate('SaveRecentListing', {type: 'recent'});
            },
          },
          {
            title: 'Visits',
            icon: Images.homefilled,
            tintColor: '#a6a4b1',
            onPress: () => {
              props.navigation.navigate('Visits');
            },
          },
          {
            title: Translate('Payment Option'),
            icon: Images.compose,
            tintColor: '#a6a4b1',
            onPress: () => props.navigation.navigate('Payment'),
          },

          {
            title: Translate('Contact Us'),
            icon: Images.contactUs,
            tintColor: '#9ee19f',
            onPress: () => props.navigation.navigate('Contact'),
          },
        ];

  const onLogout = async () => {
    dispatch(logoutUser());
  };

  return (
    <IMUserProfileComponent
      user={user}
      onLogout={onLogout}
      menuItems={menuItems}
      onPressSettings={() => dispatch(onPressSettings())}
      appStyles={AppStyles}
    />
  );
}

export default memo(MyProfileScreen);
