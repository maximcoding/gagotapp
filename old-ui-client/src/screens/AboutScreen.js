import React, {memo} from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Text,
  Image,
  View,
  ScrollView,
} from 'react-native';
import {AppStyles} from '../AppStyles';
import {Translate} from '../core/i18n/IMLocalization';
import Colors from '../themes/colors';
import Fonts from '../themes/fonts'

class AboutScreen extends React.Component {
  static navigationOptions = ({screenProps}) => {
    let currentTheme = AppStyles.navThemeConstants.light;
    return {
      title: Translate('About'),
      headerTintColor: currentTheme.activeTintColor,
      headerTitleStyle: AppStyles.text.headerTitleStyle,
    };
  };

  render() {
    return (
      <View style={{flex: 1, backgroundColor: Colors.backgroundColor}}>
        <ScrollView>
          <TouchableOpacity
            style={{
              height: 60,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingHorizontal: 10,
              borderBottomWidth: 1,
              borderBottomColor: Colors.border,
            }}>
            <Text
              style={{
                fontSize: Fonts.size.normal,
                fontWeight: '500',
                color: 'rgba(0,0,0,0.7)',
              }}>
              {Translate('Version')}
            </Text>
            <Text
              style={{
                fontSize: Fonts.size.normal,
                fontWeight: '500',
                color: 'rgba(0,0,0,0.3)',
              }}>
              6.3.1
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('Legal');
            }}
            style={{
              height: 60,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingHorizontal: 10,
              borderBottomWidth: 1,
              borderBottomColor: Colors.border,
            }}>
            <Text
              style={{
                fontSize: Fonts.size.normal,
                fontWeight: '500',
                color: 'rgba(0,0,0,0.7)',
              }}>
              {Translate('Legal')}
            </Text>
            <Image
              style={{height: 20, width: 20}}
              source={require('../../assets/icons/right-arrow.png')}
            />
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mapView: {
    width: '100%',
    height: '100%',
    backgroundColor: Colors.backgroundColor,
  },
  filtersButton: {
    marginRight: 10,
  },
  toggleButton: {
    marginRight: 7,
  },
});

export default memo(AboutScreen);
