import React, {useState, useEffect, memo} from 'react';
import {View, Text, FlatList, StyleSheet, Image} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {getFunctionHelper, onRefresh, loadMore} from '../utils/lazyLoading';
import {getSymbol, getPrice} from '../utils/currencyConverter';
import {getVisits} from '../redux/actions/postListing';
import ActivityIndicator from '../components/activityIndicator';
import NumberFormat from 'react-number-format';
import { Fonts } from '../themes';

export default memo(function Visits({route}) {
  let params = 'visits';
  let {user, currencyRates} = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  let {visits} = useSelector((state) => state.postReducer);
  let [isFetching, setFetching] = useState(false);
  let [loader, setLoader] = useState(false);
  let [moreLoader, setMoreLoader] = useState(false);
  let [page, setPage] = useState(0);

  const renderItem = ({item, index}) => {
    return (
      <View style={styles.row}>
        <View style={styles.rowInner}>
          <Image source={{uri: item.listingID.photo}} style={styles.img} />
          <View style={styles.col}>
            <Text>{item.listingID.title}</Text>

            <NumberFormat
              renderText={(text) => <Text>{text}</Text>}
              value={getPrice(
                Number(item.listingID.price),
                user,
                currencyRates,
              )}
              displayType={'text'}
              thousandSeparator={true}
              prefix={getSymbol(user)}
            />

            <Text style={styles.time}>{item.listingID.place}</Text>
          </View>
        </View>
        <Text style={[styles.time, styles.pos]}>
          {item.day} {item.time}
        </Text>
      </View>
    );
  };

  useEffect(() => {
    getFunctionHelper(params, 0, setLoader, dispatch, getVisits);
  }, []);

  if (loader) {
    return <ActivityIndicator />;
  } else
    return (
      <FlatList
        vertical
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{flexGrow: 1, backgroundColor: 'white'}}
        data={visits}
        renderItem={renderItem}
        keyExtractor={(item, index) => item._id}
        onEndReachedThreshold={0.5}
        onRefresh={() =>
          onRefresh(params, setPage, setFetching, dispatch, getVisits)
        }
        refreshing={isFetching}
        onEndReached={() =>
          loadMore(
            params,
            moreLoader,
            page,
            setMoreLoader,
            setPage,
            dispatch,
            getVisits,
          )
        }
        ListEmptyComponent={() => {
          return (
            <View style={styles.blankView}>
              <Text style={styles.backImage}>You have no visits yet!</Text>
            </View>
          );
        }}
      />
    );
});

const styles = StyleSheet.create({
  backImage: {
    color: 'gray',
  },
  img: {
    height: 55,
    width: 55,
    borderRadius: 5,
  },
  blankView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  col: {
    alignItems: 'flex-start',
    paddingLeft: 10,
  },
  row: {
    width: '95%',
    alignSelf: 'center',
    backgroundColor: '#f0f5f5',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10,
    marginTop: 10,
  },
  rowInner: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  time: {
    fontSize: Fonts.size.tiny,
    color: 'gray',
  },
  pos: {
    position: 'absolute',
    top: 5,
    right: 10,
  },
});
