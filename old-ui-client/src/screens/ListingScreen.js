import React, {useEffect, useState, memo} from 'react';
import {
  FlatList,
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native';

import {useSelector, useDispatch} from 'react-redux';

import {
  getListingRS,
  saveUnsaveListing,
  removeProperty,
} from '../redux/actions/postListing';
import {getFunctionHelper, onRefresh, loadMore} from '../utils/lazyLoading';

import IIcon from 'react-native-vector-icons/Ionicons';

import RentingSellingItem from '../components/rentingSellingItem';
import {Colors, Fonts} from '../themes';

export default memo(function ListingScreen(props) {
  let {type} = props.route.params;
  const {buyingPosts, rentingPosts} = useSelector((state) => state.postReducer);
  const [loader, setLoader] = useState(false);

  let [isFetching, setFetching] = useState(false);
  let [moreLoader, setMoreLoader] = useState(false);
  let [page, setPage] = useState(0);
  const dispatch = useDispatch();

  useEffect(() => {
    getFunctionHelper(type, 0, setLoader, dispatch, getListingRS);
  }, [type]);

  const navigateTo = () => {
    props.navigation.goBack();
  };

  const renderTitle = () => {
    switch (type) {
      case 'buy':
        return 'Selling';
      case 'rent':
        return 'Renting';
    }
  };

  const renderData = () => {
    switch (type) {
      case 'buy':
        return buyingPosts;
      case 'rent':
        return rentingPosts;
    }
  };

  const renderItem = ({item, index}) => (
    <RentingSellingItem
      item={item}
      index={index}
      dispatch={dispatch}
      removeUpdateListing={removeProperty}
      saveUnsaveListing={saveUnsaveListing}
      type={type}
    />
  );
  return (
    <SafeAreaView style={styles.main}>
      <View style={styles.header}>
        <IIcon name="arrow-back" color="black" size={24} onPress={navigateTo} />
        <Text style={styles.hTitle}>{renderTitle()}</Text>
        <Text style={{color: Colors.white}}>Test</Text>
      </View>
      {loader ? (
        <View style={styles.container}>
          <ActivityIndicator size="small" color="green" />
        </View>
      ) : (
        <FlatList
          contentContainerStyle={{flexGrow: 1}}
          data={renderData()}
          renderItem={renderItem}
          keyExtractor={(item) => `${item._id}`}
          onEndReachedThreshold={0.5}
          onRefresh={() =>
            onRefresh(type, setPage, setFetching, dispatch, getListingRS)
          }
          refreshing={isFetching}
          onEndReached={() =>
            loadMore(
              type,
              moreLoader,
              page,
              setMoreLoader,
              setPage,
              dispatch,
              getListingRS,
            )
          }
          ListEmptyComponent={() => {
            return (
              <View style={styles.noItem}>
                <Text style={styles.text}>No items yet</Text>
              </View>
            );
          }}
        />
      )}
    </SafeAreaView>
  );
});

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  filtersButton: {
    marginRight: 10,
  },
  toggleButton: {
    marginRight: 7,
  },
  noItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  text: {
    color: 'gray',
  },
  main: {
    flex: 1,
  },
  pri: {position: 'absolute', bottom: 10, right: 10},
  rightSwipeItem: {
    height: 120,
    width: 80,
    justifyContent: 'center',
    marginTop: 20,
    alignItems: 'center',
  },
  rowM: {
    flexDirection: 'row',
    marginTop: 20,
    height: 120,
    width: '100%',
    backgroundColor: 'white',
  },
  header: {
    height: 50,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingHorizontal: 20,
    elevation: 2,
  },
  hTitle: {
    fontSize: Fonts.style.title,
    fontWeight: 'bold',
  },
});
