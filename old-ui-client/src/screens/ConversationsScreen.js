import React, { Component } from "react";
import { View } from "react-native";
import { connect } from "react-redux";
import { AppStyles } from "../AppStyles";
import { Translate } from "../core/i18n/IMLocalization";
import IMConversationListView from "../core/chat/IMConversationListView/IMConversationListView";

export const ConversationsScreenOptions = () => {
  let currentTheme = AppStyles.navThemeConstants.light;
  return {
    headerTitle: Translate("Messages"),
    headerStyle: {
      backgroundColor: currentTheme.backgroundColor,
      borderBottomColor: currentTheme.hairlineColor,
    },
    headerTintColor: currentTheme.fontColor,
  };
};

class ConversationsScreen extends Component {
  componentDidMount() {
    const self = this;
    self.props.navigation.setParams({
      openDrawer: self.openDrawer,
    });
  }

  onEmptyStatePress() {
    this.props.navigation.navigate("Categories");
  }

  render() {
    const emptyStateConfig = {
      title: Translate("No Messages"),
      description: Translate(
        "You can contact vendors by messaging them on the listings page. Your conversations with them will show up here."
      ),
      buttonName: Translate("Browse Listings"),
      onPress: () => {
        this.onEmptyStatePress();
      },
    };

    return (
      <View
        style={{ flex: 1, marginLeft: 15, marginRight: 15, paddingTop: 10 }}
      >
        <IMConversationListView
          route={this.props.route}
          navigation={this.props.navigation}
          appStyles={AppStyles}
          emptyStateConfig={emptyStateConfig}
        />
      </View>
    );
  }
}

const mapStateToProps = ({ auth, languageReducer }) => {
  return {
    user: auth.user,
    updator: languageReducer.updator,
  };
};

export default connect(mapStateToProps)(ConversationsScreen);
