import {INSTANCE} from "./instance";

// add complain
export const addComplainApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: `/users/complain/save`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });

// get complains
export const getComplainsApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: `/admin/users/complains`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });
