import axios from "axios";
import endPoint from "./endPoint";

export const INSTANCE = axios.create({
  baseURL: endPoint,
});

INSTANCE.interceptors.request.use((request) => {
  return request;
});
INSTANCE.interceptors.response.use((response) => {
  return response;
});
