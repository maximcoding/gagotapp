import {INSTANCE} from "./instance";

// get map properties
export const getMapPropertiesApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: `/previews/map`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });
