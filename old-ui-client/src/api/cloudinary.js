import axios from 'axios';
const CryptoJS = require('crypto-js');

export const cloudinaryImageUrl = (source, setProgress) =>
    new Promise((resolve, reject) => {
        let timestamp = ((Date.now() / 1000) | 0).toString();
        let api_key = '491536922662481';
        let api_secret = 'oSj5bYL3BBDmAUYS2jbHNKq7vqQ';
        let cloud = 'pigui';
        let hash_string = 'timestamp=' + timestamp + api_secret;
        let signature = CryptoJS.SHA1(hash_string).toString();
        let upload_url = 'https://api.cloudinary.com/v1_1/' + cloud + '/auto/upload';
        let obj = {...source, name: source.name ? source.name : `realasatte${Math.random() * 45345}`};
        const formData = new FormData();
        formData.append('file', obj);
        formData.append('timestamp', timestamp);
        formData.append('api_key', api_key);
        formData.append('signature', signature);
        axios.post(upload_url, formData, {
            onUploadProgress: (progressEvent) => {
                const percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                setProgress(percentCompleted);
            },
        }).then(resolve).catch(reject);
    });
