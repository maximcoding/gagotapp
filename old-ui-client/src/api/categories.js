// get categories api
import {INSTANCE} from "./instance";

export const getCategoriesApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'GET',
            url: `/categories`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });

// filter categories api
export const filterCategoriesApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: `/categories/filter`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });
