import {INSTANCE} from "./instance";

export const signUpWithEmailApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: '/user/signup',
            data: data,
        })
            .then(resolve)
            .catch(reject);
    });

// login api
export const loginWithEmailApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: '/user/login',
            data: data,
        })
            .then(resolve)
            .catch(reject);
    });
// login api
export const loginWithPhoneApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: '/user/loginWithPhone',
            data: data,
        })
            .then(resolve)
            .catch(reject);
    });

// logout api
export const logoutApi = () =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: '/user/logout',
        })
            .then(resolve)
            .catch(reject);
    });

// send otp
export const sendVerificationCodeApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: '/user/code/verify',
            data,
        })
            .then(resolve)
            .catch(reject);
    });

// send Opt Exists
export const sendVerificationCodeApiExists = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: '/user/code/exist',
            data,
        })
            .then(resolve)
            .catch(reject);
    });
