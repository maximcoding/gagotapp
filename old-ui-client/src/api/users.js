// signup api
import {INSTANCE} from "./instance";
import axios from "axios";
import baseURL from "./endPoint";


// admin user
export const UserApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: `/admin//users/favorites`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });

// get current user
export const getUserApi = () =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'GET',
            url: '/user/authenticate',
        })
            .then(resolve)
            .catch(reject);
    });

// get users
export const getUsersApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: `/admin/users`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });

// update user
export const updateUserApi = (data, uid) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'PUT',
            url: `/user/update/${uid}`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });

// search user
const once = (config = {}) => {
    let call;
    if (call) {
        call.cancel('Only one request allowed at a time.');
    }
    call = axios.CancelToken.source();
    config.cancelToken = call.token;
    return axios(config);
};

export const searchUsersApi = (search) => {
    const config = {
        method: 'POST',
        url: `${baseURL}/admin/users/search`,
        data: search,
        timeout: 60000,
    };
    return once(config);
};
