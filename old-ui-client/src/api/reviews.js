import {INSTANCE} from "./instance";

export const postReviewApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: `/reviews/save`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });

export const getReviewApi = (propertyId) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'GET',
            url: `/reviews/property/fetch/${propertyId}`,
        })
            .then(resolve)
            .catch(reject);
    });

export const getPostedReviewApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'GET',
            url: `/reviews`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });

export const replyReviewApi = (reviewID, data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'PUT',
            url: `/reviews/reply/${reviewID}`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });
