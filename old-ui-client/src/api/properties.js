import {INSTANCE} from "./instance";
import {IPropertyDetails} from "../types/property";
import axios from "axios";
import baseURL from "./endPoint";

// get properties previews for home page

export const getPropertiesPreviewsApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: `/properties/previews`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });


// get property by id
export const getPropertyByIdApi = (id) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'GET',
            url: `/properties/fetch/${id}`,
        })
            .then(resolve)
            .catch(reject);
    });

// update property by id
export const updatePropertyApi = (id, data: IPropertyDetails) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'PUT',
            url: `/properties/update/${id}`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });

// save new property
export const addProperty = (data: IPropertyDetails) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: '/properties/save',
            data,
        })
            .then(resolve)
            .catch(reject);
    });

// delete property by id
export const deleteListingApi = (id) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'DELETE',
            url: `/properties/delete/${id}`,
        })
            .then(resolve)
            .catch(reject);
    });

// change property status

export const changePropertyStatusApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: `/properties/status/update`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });

// filter properties api
export const filterListingApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: `/properties/previews/filter`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });

// save recent properties
export const saveRecentListingApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: `/recent/addRecent`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });

// get recent properties
export const getRecentListingApi = (uid) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'GET',
            url: `/recent/getRecent/${uid}`,
        })
            .then(resolve)
            .catch(reject);
    });

export const getListingRSApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: `/properties/previews/rs`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });

const once2 = (config = {}) => {
    let call2;
    if (call2) {
        call2.cancel('Only one request allowed at a time.');
    }
    call2 = axios.CancelToken.source();

    config.cancelToken = call2.token;
    return axios(config);
};

export const searchListingActionApi = (data) => {
    let config = {
        method: 'POST',
        url: `${baseURL}/properties/previews/search`,
        data: data,
        timeout: 60000,
    };

    return once2(config);
};
