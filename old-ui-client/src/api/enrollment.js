// save user to visit (enroll users)
import {INSTANCE} from "./instance";

// get users - visitors ( enrolled )
export const getVisitsApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: `/properties/visits`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });

// get enrollers
export const getEnrollersApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: `/enroll/getEnrollers`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });

// enroll user to visit
export const enrollUserApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({
            method: 'POST',
            url: `/enroll/enrollUser`,
            data,
        })
            .then(resolve)
            .catch(reject);
    });

// edit enroll time
export const editEnrollTimeApi = (data) =>
    new Promise((resolve, reject) => {
        INSTANCE({method: 'POST', url: `/enroll/editTime`, data})
            .then(resolve)
            .catch(reject);
    });
