import React, { Component } from "react";
import { BackHandler, Linking } from "react-native";
import { Translate } from "../../../i18n/IMLocalization";
import { IMFormComponent } from "./IMFormComponent";

export const IMContactUsScreenOptions = (screenProps, route) => {
  let appStyles = route.params.appStyles;
  let screenTitle = route.params.screenTitle || Translate("Contact Us");
  let currentTheme = appStyles.navThemeConstants.light;
  return {
    headerTitle: screenTitle,
    headerStyle: {
      backgroundColor: currentTheme.backgroundColor,
    },
    headerTintColor: currentTheme.fontColor,
  };
};

class IMContactUsScreen extends Component {
  appStyles: any;
  form: any;
  phone: any;
  initialValuesDict: any;
  didFocusSubscription: any;
  constructor(props) {
    super(props);

    this.appStyles = props.route.params.appStyles || props.appStyles;
    this.form = props.route.params.form || props.form;
    this.phone = props.route.params.phone || props.phone;
    this.initialValuesDict = {};

    this.state = {
      alteredFormDict: {},
    };

    this.didFocusSubscription = props.navigation.addListener(
      "didFocus",
      (payload) =>
        BackHandler.addEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );
  }

  componentDidMount() {
    this.willBlurSubscription = this.props.navigation.addListener(
      "willBlur",
      (payload) =>
        BackHandler.removeEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );
  }

  componentWillUnmount() {
    this.didFocusSubscription && this.didFocusSubscription();
    this.willBlurSubscription && this.willBlurSubscription();
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  onFormButtonPress = (_buttonField) => {
    Linking.openURL(`tel:${this.phone}`);
  };

  render() {
    return (
      <IMFormComponent
        form={this.form}
        initialValuesDict={this.initialValuesDict}
        navigation={this.props.navigation}
        appStyles={this.appStyles}
        onFormButtonPress={this.onFormButtonPress}
      />
    );
  }
}

export default IMContactUsScreen;
