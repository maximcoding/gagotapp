import React, {memo} from 'react';
import {useSelector} from 'react-redux';
import {Translate} from '../../../i18n/IMLocalization';
import {IMFormComponent} from './IMFormComponent';

export const IMUserSettingsOptions = (navigation, route) => {
  let appStyles = route.params.appStyles;
  let screenTitle = route.params.screenTitle || Translate('Settings');
  let currentTheme = appStyles.navThemeConstants.light;

  return {
    headerTitle: screenTitle,
    headerStyle: {
      backgroundColor: currentTheme.backgroundColor,
    },
    headerTintColor: currentTheme.fontColor,
  };
};

function IMUserSettingsScreen(props) {
  let {user} = useSelector((state) => state.auth);
  return (
    <IMFormComponent
      user={user}
      navigation={props.navigation}
      appStyles={props.route.params.appStyles || props.appStyles}
    />
  );
}

export default memo(IMUserSettingsScreen);
