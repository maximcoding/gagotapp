import React, { memo } from "react";
import { useSelector } from "react-redux";
import { IMFormComponent } from "./IMFormComponent";

function IMEditProfileScreen({ route, navigation }) {
  let { user, updator } = useSelector((state) => {
    return { user: state.auth.user, updator: state.languageReducer.updator };
  });
  let { appStyles } = route.params;

  return (
    <IMFormComponent
      user={user}
      appStyles={appStyles}
      navigation={navigation}
    />
  );
}

export default memo(IMEditProfileScreen);
