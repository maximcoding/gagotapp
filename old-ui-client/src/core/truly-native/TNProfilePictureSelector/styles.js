import { Dimensions } from "react-native";
import { DynamicStyleSheet } from "react-native-dark-mode";
import Colors from "../../../themes/colors";
import Fonts from "../../../themes/fonts";
import Images from "../../../themes/images";
const { height } = Dimensions.get("window");
const imageSize = height * 0.14;
const photoIconSize = imageSize * 0.27;

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    image: {
      width: "100%",
      height: "100%",
    },
    imageBlock: {
      flex: 2,
      flexDirection: "row",
      width: "100%",
      justifyContent: "center",
      alignItems: "center",
    },
    imageContainer: {
      height: imageSize - 20,
      width: imageSize - 20,
      borderRadius: imageSize - 20,
      shadowColor: "#006",
      shadowOffset: {
        width: 10,
        height: 10,
      },
      shadowOpacity: 0.1,
      overflow: "hidden",
    },
    addButton: {
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: Colors.backgroundColor,
      zIndex: 2,
      marginTop: imageSize * 0.3,
      marginLeft: -imageSize * 0.2,
      width: photoIconSize,
      height: photoIconSize,
      borderRadius: photoIconSize,
    },
    closeButton: {
      alignSelf: "flex-end",
      alignItems: "center",
      justifyContent: "center",
      marginTop: 40,
      marginRight: 15,
      backgroundColor: Colors.grey6,
      width: 28,
      height: 28,
      borderRadius: 20,
      overflow: "hidden",
    },
    closeIcon: {
      width: 27,
      height: 27,
    },
  });
};

export default dynamicStyles;
