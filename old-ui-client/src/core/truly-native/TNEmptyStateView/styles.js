import { DynamicStyleSheet } from "react-native-dark-mode";
import { Fonts } from "../../../themes";

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    title: {
      fontSize: Fonts.size.large,
      fontWeight: "bold",
      alignSelf: "center",
      color: Colors.title,
      marginBottom: 15,
    },
    description: {
      alignSelf: "center",
      color: Colors.title,
      textAlign: "center",
      width: "85%",
      lineHeight: 20,
    },
    buttonContainer: {
      backgroundColor: Colors.foregroundColor,
      width: "75%",
      height: 45,
      alignSelf: "center",
      borderRadius: 10,
      alignItems: "center",
      justifyContent: "center",
      marginTop: 30,
    },
    buttonName: {
      color: "#ffffff",
      fontSize: Fonts.size.normal,
      fontWeight: "600",
    },
  });
};

export default dynamicStyles;
