import React, {memo} from 'react';
import Button from 'react-native-button';
import {Text, View, Image, ImageBackground, SafeAreaView} from 'react-native';
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import {Translate} from '../../i18n/IMLocalization';
import dynamicStyles from './styles';
import {AppStyles} from '../../../AppStyles';
import {Images} from '../../../themes';

const WelcomeScreen = (props) => {
    const styles = useDynamicStyleSheet(dynamicStyles(AppStyles));
    const appConfig = props.route.params.appConfig;
    const appStyles = AppStyles;

    return (
        <ImageBackground
            style={AppStyles.screenTransparent}
            source={Images.background}>
            <SafeAreaView
                style={[AppStyles.screenTransparent, {alignItems: 'center'}]}>
                <Image
                    style={[AppStyles.logo]}
                    source={Images.logo}
                    resizeMode="contain"
                />
                <View
                    style={{
                        flexDirection: 'column',
                        alignItems: 'center'
                    }}>
                    <Text style={[AppStyles.text.title, styles.welcomeTitle]}>
                        {Translate('welcomeTitle')}
                    </Text>
                    <Text style={[styles.welcomeCaption]}>
                        {Translate('welcomeCaption')}
                    </Text>
                </View>
                <View style={{width: '100%', paddingVertical: 65}}>
                    <Button
                        containerStyle={[styles.loginContainer]}
                        style={[AppStyles.text.buttonText]}
                        onPress={() => {
                            appConfig.isSMSAuthEnabled
                                ? props.navigation.navigate('Sms', {
                                    isSigningUp: false,
                                    appStyles,
                                    appConfig,
                                })
                                : props.navigation.navigate('Login', {appStyles, appConfig});
                        }}>
                        {Translate('Log In')}
                    </Button>
                    <Button
                        containerStyle={styles.SignUpContainer}
                        style={[AppStyles.text.buttonInverseText]}
                        onPress={() => {
                            appConfig.isSMSAuthEnabled
                                ? props.navigation.navigate('Sms', {
                                    isSigningUp: true,
                                    appStyles,
                                    appConfig,
                                })
                                : props.navigation.navigate('SignUp', {appStyles, appConfig});
                        }}>
                        {Translate('Sign Up')}
                    </Button>
                </View>
            </SafeAreaView>
        </ImageBackground>
    );
};

export default memo(WelcomeScreen);
