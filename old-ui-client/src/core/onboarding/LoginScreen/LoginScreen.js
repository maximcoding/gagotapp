import React, {useState, memo} from 'react';
import {
    Text,
    TextInput,
    TouchableOpacity,
    Image,
    ImageBackground,
    SafeAreaView,
} from 'react-native';
import Button from 'react-native-button';
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import TNActivityIndicator from '../../truly-native/TNActivityIndicator';
import {Translate} from '../../i18n/IMLocalization';
import dynamicStyles from './styles';
import {AppStyles} from '../../../AppStyles';
import Images from '../../../themes/images';
import {Colors} from '../../../themes';
import {signinWithEmail} from '../../../redux/actions/user';
import {useDispatch} from 'react-redux';

const LoginScreen = (props) => {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const appStyles = props.route.params.appStyles;
    const styles = useDynamicStyleSheet(dynamicStyles(appStyles));
    const appConfig = props.route.params.appConfig;

    const onPressLogin = () => {
        if (email == '' || password == '') {
            alert('Both Fields required!');
            return;
        }
        setLoading(true);
        dispatch(signinWithEmail({email, password}, () => setLoading(false)));
    };

    return (
        <ImageBackground
            style={AppStyles.screenTransparent}
            source={Images.background}>
            <SafeAreaView style={[AppStyles.screenTransparent]}>
                <TouchableOpacity
                    style={{alignSelf: 'flex-start'}}
                    onPress={() => props.navigation.goBack()}>
                    <Image
                        style={[AppStyles.backArrowStyle, {marginTop: 6}]}
                        source={Images.backArrow}
                    />
                </TouchableOpacity>
                <Image
                    style={[AppStyles.logo, {marginTop: 54, marginBottom: '15%'}]}
                    source={Images.logo}
                    resizeMode="contain"
                />
                <KeyboardAwareScrollView
                    style={{flex: 1, width: '100%'}}
                    keyboardShouldPersistTaps="always">
                    <TextInput
                        style={[AppStyles.inputContainer]}
                        placeholder={Translate('Email')}
                        placeholderTextColor={Colors.placeholder}
                        onChangeText={(text) => setEmail(text)}
                        value={email}
                        underlineColorAndroid="transparent"
                        autoCapitalize="none"
                    />
                    <TextInput
                        style={[AppStyles.inputContainer, {marginTop: 5}]}
                        placeholderTextColor={Colors.placeholder}
                        secureTextEntry
                        placeholder={Translate('Password')}
                        onChangeText={(text) => setPassword(text)}
                        value={password}
                        underlineColorAndroid="transparent"
                        autoCapitalize="none"
                    />
                    <Button
                        containerStyle={[styles.loginContainer, {marginTop: 38}]}
                        style={[AppStyles.text.buttonText]}
                        onPress={() => onPressLogin()}>
                        {Translate('Log In')}
                    </Button>
                    <Text style={[AppStyles.orTextStyle, {paddingRight: 5}]}> {Translate('OR')}</Text>
                    {/*<Button*/}
                    {/*  containerStyle={styles.facebookContainer}*/}
                    {/*  style={styles.facebookText}*/}
                    {/*  onPress={() => onFBButtonPress()}*/}
                    {/*>*/}
                    {/*  {Translate('Login With Facebook')}*/}
                    {/*</Button>*/}
                    {appConfig.isSMSAuthEnabled && (
                        <Button
                            containerStyle={styles.phoneNumberContainer}
                            onPress={() =>
                                props.navigation.navigate('Sms', {
                                    isSigningUp: false,
                                    appStyles,
                                    appConfig,
                                })
                            }>
                            {Translate('Login with phone number')}
                        </Button>
                    )}

                    {loading && <TNActivityIndicator appStyles={appStyles}/>}
                </KeyboardAwareScrollView>
            </SafeAreaView>
        </ImageBackground>
    );
};

export default memo(LoginScreen);
