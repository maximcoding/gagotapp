import React, {useState, memo} from 'react';
import {
    Text,
    View,
    Alert,
    Image,
    TouchableOpacity,
    ImageBackground, StyleSheet,
} from 'react-native';
import Button from 'react-native-button';
import CodeInput from 'react-native-confirmation-code-input';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import dynamicStyles from './styles';
import TNActivityIndicator from '../../truly-native/TNActivityIndicator';
import TNProfilePictureSelector from '../../truly-native/TNProfilePictureSelector/TNProfilePictureSelector';
import {Translate} from '../../i18n/IMLocalization';
import {useDispatch} from 'react-redux';
import TermsOfUseView from '../components/TermsOfUseView';
import ResendButton from '../../../components/resendButton';
import {Colors, Fonts, Images} from '../../../themes';
import {AppStyles} from '../../../AppStyles';
import {AppConfig} from '../../../AppConfig';
import PhoneInputRender from "./phoneInput";
import {
    sendOtp,
    signupWithPhone,
    signinWithPhone,
    uploadImageSignup,
} from '../../../redux/actions/user';
import {TabView} from 'react-native-tab-view';
import AgentInfo from '../SignupScreen/agentInfo';
import BesicInfo from './besicInfo';

const styles = StyleSheet.create({
    background: {width: "100%", height: "100%"},
    container: {
        flex: 1,
        display: "flex",
        flexDirection: "column",
    },
    centerContainer: {
        display: "flex",
        flex: 1,
        flexDirection: "column",
        alignItems: "center",
        paddingTop: "25%",
    },
    languages: {
        paddingTop: 50,
        paddingRight: 50,
        display: "flex",
        alignItems: "flex-end",
    },
    title: {
        minHeight: 40,
        display: "flex",
        textAlign: "center",
        flexDirection: "row",
        justifyContent: "center",
    },
    subTitle: {
        paddingBottom: "5%",
    },
    inputContainer: {
        display: "flex",
        justifyContent: "center",
        paddingBottom: 30,
    },
    input: {
        height: 50,
        width: 240,
        margin: 12,
        paddingRight: 12,
        fontSize: Fonts.size.large,
        borderBottomWidth: 1,
        borderColor: Colors.underlayColor,
    },
});

const SmsAuthenticationScreen = (props) => {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [loading, setLoading] = useState(false);
    const [isPhoneVisible, setIsPhoneVisible] = useState(true);
    const [phoneNumber, setPhoneNumber] = useState('');
    const [profilePictureURL, setProfilePictureURL] = useState(null);
    const [companyID, setCompanyID] = useState('');
    const [address, setAddress] = useState('');
    const [imageLoader, setImageLoader] = useState(false);

    const appStyles = props.route.params.appStyles;
    const styles = useDynamicStyleSheet(dynamicStyles(appStyles));
    const appConfig = props.route.params.appConfig;
    const {isSigningUp} = props.route.params;

    const [index, setIndex] = useState(0);
    const routes = [
        {key: 'basicInfo', title: 'Basic Info'},
        {key: 'agentInfo', title: 'Agent Info'},
    ];

    const dispatch = useDispatch();

    const onChangeText = ({dialCode, unmaskedPhoneNumber}) => {
        let number = `${dialCode}${unmaskedPhoneNumber}`;
        setPhoneNumber(number);
    };

    const signInWithPhoneNumber = (userValidPhoneNumber) => {
        setLoading(true);
        dispatch(
            sendOtp(userValidPhoneNumber, (type) => {
                if (type === 'success') {
                    setIsPhoneVisible(false);
                }

                setLoading(false);
            }),
        );
    };

    const SignUpWithPhoneNumber = (smsCode) => {
        const userDetails = {
            firstName,
            lastName,
            phone: phoneNumber,
            appIdentifier: appConfig.appIdentifier,
            photoURI: profilePictureURL,
        };
        setLoading(true);

        dispatch(signupWithPhone(userDetails, smsCode, () => setLoading(false)));
    };

    const onPressSend = () => {
        if (phoneNumber !== '') {
            setPhoneNumber(phoneNumber);

            signInWithPhoneNumber(phoneNumber);
        } else {
            Alert.alert(
                '',
                Translate('Please enter a valid phone number.'),
                [{text: Translate('OK')}],
                {
                    cancelable: false,
                },
            );
        }
    };

    const renderScene = ({route}) => {
        switch (route.key) {
            case 'basicInfo':
                return (
                    <BesicInfo
                        firstName={firstName}
                        lastName={lastName}
                        setFirstName={setFirstName}
                        setLastName={setLastName}
                        onPressSend={onPressSend}
                        onChangeText={onChangeText}
                    />
                );
            case 'agentInfo':
                return (
                    <AgentInfo
                        setCompanyID={setCompanyID}
                        companyID={companyID}
                        address={address}
                        setAddress={setAddress}
                    />
                );
        }
    };

    const renderTabBar = (props) => {
        const activeIndex = props.navigationState.index;

        return (
            <View style={AppStyles.tabStyles.tabBar}>
                {props.navigationState.routes.map((route, i) => {
                    const active = activeIndex === i;

                    const tabItemStyle = active
                        ? AppStyles.tabStyles.tabItemActive
                        : AppStyles.tabStyles.tabItem;

                    const tabItemTxtStyle = active
                        ? AppStyles.tabStyles.tabItemTxtActive
                        : AppStyles.tabStyles.tabItemTxt;

                    return (
                        <TouchableOpacity
                            key={route.title}
                            style={tabItemStyle}
                            onPress={() => setIndex(i)}>
                            <View style={{alignItems: 'center'}}>
                                <Text style={tabItemTxtStyle}>{route.title}</Text>
                            </View>
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    };
    const onFinishCheckingCode = (newCode) => {
        // console.log('newCode', newCode);
        setLoading(true);
        if (isSigningUp) {
            SignUpWithPhoneNumber(newCode);
        } else {
            // login
            let data = {
                phone: phoneNumber,
            };

            dispatch(signinWithPhone(data, newCode, () => setLoading(false)));
        }
    };

    const codeInputRender = () => {
        return (
            <View style={styles.codeInput}>
                <CodeInput
                    autoFocus
                    codeLength={6}
                    activeColor="#000"
                    inactiveColor="#000"
                    autoFocus={true}
                    inputPosition='center'
                    space={10}
                    size={40}
                    className='border-b'
                    onFulfill={(text) => onFinishCheckingCode(text)}
                    codeInputStyle={{
                        borderBottomColor: Colors.grey,
                        fontSize: 40,
                        paddingBottom: 4,
                        fontFamily: "Arial",
                        color: Colors.primary
                    }}
                    keyboardType="numeric"
                />
                <ResendButton resendSms={() => signInWithPhoneNumber(phoneNumber)}/>
            </View>
        );
    };

    const renderAsSignUpState = () => {
        return (
            <>
                <Text style={[AppStyles.text.headerTitle, {paddingBottom: 45}]}>
                    {Translate('Create new Account')}
                </Text>
                <TNProfilePictureSelector
                    setProfilePictureURL={uploadImageFunc}
                    appStyles={AppStyles}
                    imageLoader={imageLoader}
                />
                <View>
                    {isPhoneVisible ? (
                        <TabView
                            lazy={false}
                            onIndexChange={(i) => setIndex(i)}
                            renderScene={renderScene}
                            renderTabBar={renderTabBar}
                            navigationState={{index, routes}}
                        />
                    ) : (
                        codeInputRender()
                    )}
                </View>

                <Text style={AppStyles.orTextStyle}>{Translate('OR')}</Text>
                <Button
                    containerStyle={styles.signWithEmailContainer}
                    style={[AppStyles.text.buttonInverseText]}
                    onPress={() =>
                        props.navigation.navigate('SignUp', {
                            appStyles: AppStyles,
                            appConfig: AppConfig,
                        })
                    }>
                    {Translate('Sign up with Email')}
                </Button>
            </>
        );
    };

    const renderAsLoginState = () => {
        return (
            <View>
                <Image
                    style={[AppStyles.logo, {marginTop: 54}]}
                    source={Images.logo}
                    resizeMode="contain"
                />
                {isPhoneVisible ? (
                        <View
                            style={{
                                display: "flex",
                                flex: 1,
                                flexDirection: "column",
                                alignItems: "center",
                                width: '100%'
                            }}>
                            <Text style={[AppStyles.text.title]}>
                                {Translate("verify_mobile")}
                            </Text>
                            <Text style={AppStyles.text.subTitle}>
                                {Translate("we_will_send_you_sms")}
                            </Text>
                            <PhoneInputRender
                                onPressSend={onPressSend}
                                onChangeText={onChangeText}
                                noBorder={true}
                            />
                        </View>)
                    : (<View
                        style={{
                            display: "flex",
                            flex: 1,
                            flexDirection: "column",
                            alignItems: "center",
                            width: '100%'
                        }}>
                        <Text style={[AppStyles.text.title]}>
                            {Translate("verify_mobile")}
                        </Text>
                        <Text style={AppStyles.text.subTitle}>
                            {Translate("enter_the_code")}
                        </Text>
                        {codeInputRender()}
                    </View>)
                }
                <Text style={AppStyles.orTextStyle}>{Translate('OR')}</Text>
                <Button
                    containerStyle={[styles.signWithEmailContainer]}
                    onPress={() =>
                        props.navigation.navigate('Login', {
                            appStyles: AppStyles,
                            appConfig: AppConfig,
                        })
                    }>
                    {Translate('Sign in with Email')}
                </Button>
            </View>
        );
    };

    const stopImageLoader = (url) => {
        if (url) {
            setProfilePictureURL(url);
        }
        setImageLoader(false);
    };

    const uploadImageFunc = (file) => {
        setImageLoader(true);
        dispatch(uploadImageSignup(file, stopImageLoader));
    };

    return (
        <ImageBackground
            style={AppStyles.screenTransparent}
            source={Images.background}>
            <KeyboardAwareScrollView
                style={{flex: 1, width: '100%'}}
                keyboardShouldPersistTaps="always">
                <TouchableOpacity onPress={() => props.navigation.goBack()}>
                    <Image style={[AppStyles.backArrowStyle]} source={Images.backArrow}/>
                </TouchableOpacity>
                {isSigningUp ? renderAsSignUpState() : renderAsLoginState()}
                {isSigningUp && (
                    <TermsOfUseView tosLink={appConfig.tosLink} style={AppStyles.tos}/>
                )}
            </KeyboardAwareScrollView>
            {loading && <TNActivityIndicator appStyles={AppStyles}/>}
        </ImageBackground>
    );
};
export default memo(SmsAuthenticationScreen);
