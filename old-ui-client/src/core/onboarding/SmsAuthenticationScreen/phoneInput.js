import React from 'react';
import Button from 'react-native-button';
import {AppStyles} from '../../../AppStyles';
import {Translate} from '../../i18n/IMLocalization';
import IntlPhoneInput from '../../../components/phoneInput';

const phoneInputRender = ({onPressSend, onChangeText, noBorder = false}) => {
    return (
        <>
            <IntlPhoneInput onChangeText={onChangeText}
                            defaultCountry="IL"
                            noBorder={noBorder}/>
            <Button
                containerStyle={[AppStyles.buttonInverseContainer]}
                style={[AppStyles.text.buttonInverseText]}
                onPress={() => onPressSend()}
            >
                {Translate('Send code')}
            </Button>
        </>
    );
};

export default phoneInputRender;
