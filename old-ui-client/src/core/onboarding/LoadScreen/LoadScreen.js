import React, { useEffect } from "react";
import { View, Image, ImageBackground } from "react-native";
import PropTypes from "prop-types";
import { Images } from "../../../themes";

const LoadScreen = (props) => {
  return (
    <View style={{ flex: 1 }}>
      <ImageBackground
        style={{ flex: 1, resizeMode: "cover" }}
        source={Images.splashScreen}
      ></ImageBackground>
    </View>
  );
};

LoadScreen.propTypes = {
  user: PropTypes.object,
  navigation: PropTypes.object,
};

LoadScreen.navigationOptions = {
  header: null,
};

export default LoadScreen;
