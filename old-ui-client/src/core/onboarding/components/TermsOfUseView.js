import React from "react";
import { Text, Linking, View } from "react-native";
import { Translate } from "../../i18n/IMLocalization";
import {Fonts} from '../../../themes';

const TermsOfUseView = (props) => {
  const { tosLink, style } = props;
  return (
    <View style={style}>
      <Text style={{ fontSize: Fonts.style.description }}>
        {Translate("By creating an account you agree with our")}
      </Text>
      <Text
        style={{ color: "blue", fontSize: Fonts.style.description }}
        onPress={() => Linking.openURL(tosLink)}
      >
        {Translate("Terms of Use")}
      </Text>
    </View>
  );
};

export default TermsOfUseView;
