import React from "react";
// import { withTranslation } from "react-i18next";
import "./index";

export const LocalizationContext = React.createContext({
  t: (key, data) => {},
  i18n: {},
});

export const Provider = (props) => {
  const { t, i18n, children } = props;
  return (
    <LocalizationContext.Provider value={{ t, i18n }}>
      {children}
    </LocalizationContext.Provider>
  );
};

export const LocalizationProvider = null; // withTranslation()(Provider);
