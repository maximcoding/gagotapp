import i18n from "i18n-js";
// import {initReactI18next} from 'react-i18next';
import * as RNLocalize from "react-native-localize";

export const languages = {
  en: require("./en.json"),
};

export const DEFAULT_LANGUAGE_CODE = RNLocalize.findBestAvailableLanguage(
  Object.keys(languages)
);

// i18n
//     .use(initReactI18next)
//     // init i18next
//     // for all options read: https://www.i18next.com/overview/configuration-options
//     .init({
//         resources: languages,
//         lng: DEFAULT_LANGUAGE_CODE ? DEFAULT_LANGUAGE_CODE.languageTag : 'en',
//         fallbackLng: DEFAULT_LANGUAGE_CODE ? DEFAULT_LANGUAGE_CODE.languageTag : 'en',
//         debug: true,
//         interpolation: {
//             escapeValue: false,
//         },
//     });

export default i18n;
