import { DynamicStyleSheet } from "react-native-dark-mode";
import { Dimensions } from "react-native";
import { Fonts } from "../../../themes";

const { height } = Dimensions.get("window");

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    container: {
      flex: 1,
      backgroundColor: Colors.backgroundColor,
    },
    userImageContainer: {
      borderWidth: 0,
    },
    chatsChannelContainer: {
      // flex: 1,
      padding: 0,
    },
    chatItemContainer: {
      flexDirection: "row",
      marginBottom: 20,
    },
    chatItemContent: {
      flex: 1,
      alignSelf: "center",
      marginLeft: 10,
    },
    chatFriendName: {
      color: Colors.title,
      fontSize: Fonts.size.normal,
    },
    content: {
      flexDirection: "row",
    },
    message: {
      flex: 2,
      color: Colors.subTitle,
    },
    emptyViewContainer: {
      marginTop: height / 5,
    },
  });
};

export default dynamicStyles;
