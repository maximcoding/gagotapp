export enum RoleEnum {
  ADMIN,
  AGENT,
  USER, // ( default to all after registration)
}

export enum PopularityEnum {
  Low,
  Medium,
  High,
}

export enum SquareUnitsEnum {
  Meters,
  Feets,
}

export enum PropertyCategoryEnum {
  Houses,
  Apartments,
  Condos,
  Buildings,
  TownHouses,
  RecentlySold,
  RecenltyAdded,
  Land,
}

export enum PropertyStatusEnum {
  ForSale,
  ForRent,
}

export enum PlatformEum {
  IOS,
  ANDROID,
  WINDOWS,
  LINUX,
  MACOS,
  BLACKBERRY,
  OTHER,
}
