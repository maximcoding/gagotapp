import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Translate} from '../core/i18n/IMLocalization';
import {Fonts} from '../themes';
import {AppStyles} from "../AppStyles";

export default function ResendButton({resendSms}) {
    let [counter, setCounter] = useState(60);

    useEffect(() => {
        if (counter == 0) {
            return;
        }
        setTimeout(() => {
            setCounter(counter - 1);
        }, 1000);
    }, [counter]);

    const resendCode = () => {
        setCounter(60);
        resendSms();
    };
    return (
        <View style={{flexDirection: "column"}}>
            <View style={styles.row}>
                <TouchableOpacity
                    style={AppStyles.buttonInverseContainer}
                    disabled={counter !== 0}
                    onPress={resendCode}>
                    <Text
                        style={{
                            color: counter !== 0 ? 'gray' : 'green',
                            fontSize: Fonts.size.normal,
                        }}>
                        {Translate('Resend code in {counter}')}
                        {" "} {counter}
                        {/*{Translate('Resend')}*/}
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 36,
        justifyContent: 'space-between',
        alignSelf: 'center',
    },
    text: {
        color: 'black',
        fontSize: Fonts.size.normal
    },
    btn: {
        justifyContent: 'center',
        alignItems: 'center',
    },
});
