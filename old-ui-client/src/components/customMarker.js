import React, {memo} from 'react';
import {View, StyleSheet, Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Colors} from '../themes';
let Img = 'https://pngimg.com/uploads/house/house_PNG57.png';

const Marker = memo(({item}) => {
  return (
    <View
      style={{
        alignItems: 'center',
      }}>
      <Icon color={Colors.green} name="map-marker" size={130} />
      {item.photo ? (
        <Image
          source={{uri: item.photo}}
          style={{
            width: 60,
            height: 60,
            borderRadius: 60 / 2,
            position: 'absolute',
            top: 15,
            left: 8,
            borderWidth: 4,
            borderColor: 'white',
            alignSelf: 'center',
          }}
        />
      ) : (
        <Image
          source={{uri: Img}}
          style={{
            width: 25,
            height: 25,
            top: 40,
            left: 35,
            alignSelf: 'center',
            position: 'absolute',
          }}
        />
      )}
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    width: 60,
    height: 60,
    display: 'flex',
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'blue',
  },
  activeContainer: {
    width: 100,
    height: 100,
    display: 'flex',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'blue',
  },
  img: {
    width: 53,
    height: 53,
    borderRadius: 26.5,
    borderWidth: 3,
    borderColor: 'white',
  },
  activeImg: {
    width: 90,
    height: 90,
    borderRadius: 45,
    borderWidth: 10,
    borderColor: 'white',
  },
});

export default Marker;
