import React, {useState} from 'react';
import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
import Modal from "./BottomModal";
import RadioButton from 'react-native-radio-button';
import {Translate} from '../core/i18n/IMLocalization';
import Colors from '../themes/colors';
import {AppStyles, ModalHeaderStyle} from '../AppStyles';

export default function SortModal({isOpen, closeSort, callBack, isSort}) {
  const [active, setActive] = useState(isSort);
  let data = [
    {
      title: 'All',
      value: 'all',
    },
    {
      title: 'New Users',
      value: 'new',
    },
    {
      title: 'Old Users',
      value: 'old',
    },
    {
      title: 'Users',
      value: 'users',
    },
    {
      title: 'Agents',
      value: 'agents',
    },
    {
      title: 'Favorite',
      value: 'fav',
    },
    {
      title: 'Rank',
      value: 'rank',
    },
  ];

  const changeRadio = (value) => {
    callBack(value);
    setActive(value);
    closeSort();
  };

  return (
    <Modal isOpen={isOpen} closeModal={closeSort}>
      <SafeAreaView style={styles.container}>
        <Text style={ModalHeaderStyle.text.title}>{Translate('Sort')}</Text>
        {data.map((obj) => {
          return (
            <View style={styles.item}>
              <RadioButton
                animation={'bounceIn'}
                isSelected={active == obj.value}
                onPress={() => changeRadio(obj.value)}
                size={14}
              />
              <View
                style={{
                  paddingHorizontal: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={[AppStyles.text.listValue]}>{obj?.title}</Text>
              </View>
            </View>
          );
        })}
      </SafeAreaView>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor,
  },
  item: {
    width: '100%',
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 20,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    minHeight: 10,
  },
});
