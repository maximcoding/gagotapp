import React, {memo} from 'react';
import {StyleSheet} from 'react-native';
import IntlPhoneInput from 'react-native-intl-phone-input';
import {Colors, Fonts} from '../themes';

export default ({
                  defaultCountry,
                  onChangeText,
                  inputProps,
                  noBorder = false,
                }) => {
  return (
    <IntlPhoneInput
      containerStyle={noBorder ? styles.container : styles.container2}
      phoneInputStyle={styles.phoneInputStyle}
      flagStyle={styles.flagStyle}
      dialCodeTextStyle={styles.dialCodeTextStyle}
      onChangeText={onChangeText}
      defaultCountry={defaultCountry}
      inputProps={inputProps}
    />
  );
};

const styles = StyleSheet.create({
  phoneInputStyle: {
    fontSize: 30,
    color: Colors.black,
  },
  flagStyle: {
    fontSize: 30,
    borderColor: Colors.grey0
  },
  dialCodeTextStyle: {
    fontSize: 16,
    color: Colors.grey9,
  },
  container: {
    width: '80%',
    marginTop: 10,
    marginBottom: 22
  },
  container2: {
    borderWidth: 1,
    borderColor: Colors.border,
    borderRadius: 4,
    marginTop: 10,
    marginBottom: 22
  },
  iconView: {
    width: 70,
    alignItems: 'center',
  },
  title: {
    color: Colors.white,
    fontSize: Fonts.style.title,
    fontWeight: '600',
    textTransform: 'uppercase',
  },
  subtitle: {
    color: Colors.white,
    opacity: 0.8,
  },
});
