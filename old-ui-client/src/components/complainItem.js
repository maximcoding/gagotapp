import React, {memo, useState} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Image} from 'react-native';
import Collapsible from 'react-native-collapsible';
import moment from 'moment';
const img = 'https://assets.stickpng.com/images/585e4bf3cb11b227491c339a.png';
import Fonts from '../themes/fonts';

const Details = memo(({item, show}) => {
  return (
    <Collapsible collapsed={!show}>
      <>
        <View style={styles.textC}>
          <Text>{item.text}</Text>
        </View>
      </>
    </Collapsible>
  );
});

export default memo(({item, index}) => {
  let [show, setShow] = useState(false);

  const onClick = () => {
    setShow(!show);
  };
  return (
    <>
      <TouchableOpacity
        key={index}
        onPress={onClick}
        activeOpacity={0.9}
        style={styles.container}>
        <View style={styles.iconView}>
          <Image
            source={{uri: item.userId?.photoURI ? item.userId?.photoURI : img}}
            style={styles.image}
          />
        </View>
        <View style={styles.rightView}>
          <View>
            <Text style={styles.title}>{item.from}</Text>
            <Text style={styles.subtitle}>{item.text.slice(0, 16)}...</Text>
          </View>
          <View>
            <Text style={styles.subtitle}>
              {moment(item.createdAt).fromNow(true)} ago
            </Text>
          </View>
        </View>
      </TouchableOpacity>
      <Details item={item} show={show} />
    </>
  );
});

const styles = StyleSheet.create({
  textC: {
    padding: 20,
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    minHeight: 60,
    backgroundColor: 'rgba(0,0,0,.08)',
    marginBottom: 5,
  },
  iconView: {
    width: 70,
    alignItems: 'center',
  },
  title: {
    color: 'black',
    fontSize: Fonts.size.small,
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
  subtitle: {
    color: 'black',
    opacity: 0.8,
    textTransform: 'uppercase',
    fontSize: Fonts.size.tiny,
  },
  image: {
    height: 45,
    width: 45,
    borderRadius: 45 / 2,
  },
});
