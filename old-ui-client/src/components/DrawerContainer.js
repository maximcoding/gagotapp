import React from "react";
import { StyleSheet, View } from "react-native";
import { connect } from "react-redux";
import MenuButton from "../components/MenuButton";
import { AppIcon } from "../AppStyles";
import authManager from "../core/onboarding/AuthManager";
import { Images } from "../themes";
import { logoutUser } from "../redux/actions/user";

class DrawerContainer extends React.Component {
  handleLogout = async () => {
    this.props.logoutUser();

    // try {
    //   await authManager.logout(this.props.user);
    //   this.props.dispatch({ type: "LOG_OUT" });
    // } catch (e) {}
  };

  render() {
    const { user } = this.props;
    return (
      <View style={styles.content}>
        <View style={styles.container}>
          <MenuButton
            title="LOG OUT"
            source={Images.logout}
            onPress={this.handleLogout}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  container: {
    flex: 1,
    alignItems: "flex-start",
    paddingHorizontal: 20,
  },
});

const mapStateToProps = (state) => ({
  user: state.auth.user,
});

export default connect(mapStateToProps, { logoutUser })(DrawerContainer);
