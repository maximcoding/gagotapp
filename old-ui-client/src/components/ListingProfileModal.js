import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  ActivityIndicator,
  StyleSheet,
  BackHandler,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from "react-native";
import FastImage from "react-native-fast-image";
import { connect } from "react-redux";
import SavedButton from "../components/SavedButton";
import StarRating from "react-native-star-rating";
import ProfileImageCard from "../screens/ProfileScreen/ProfileImageCard";
import { AppStyles, TwoColumnListStyle } from "../AppStyles";
import { Configuration } from "../Configuration";
import { Translate } from "../core/i18n/IMLocalization";
import Colors from "../themes/colors";
import { Fonts } from "../themes";

export const ListingProfileModalOptions = () => {

  return {
    title: Translate("Profile"),
    headerTitleStyle: AppStyles.text.headerTitleStyle,
  };
};

class ListingProfileModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      listings: [],
      data: null,
      reviews: [],
      isListingDetailVisible: false,
      selectedItem: null,
      loading: true,
    };

    this.didFocusSubscription = props.navigation.addListener(
      "didFocus",
      (payload) =>
        BackHandler.addEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );

    this.userID = props.route.params.userID;
    this.listingItemActionSheet = React.createRef();
    this.listingsUnsubscribe = null;
  }

  async componentDidMount() {
    const res = await firebaseUser.getUserData(this.userID);

    if (res.success) {
      this.setState({
        user: res.data,
        loading: false,
      });
    } else {
      this.onGetUserError();
    }
    this.listingsUnsubscribe = firebaseListing.subscribeListings(
      { userId: this.userID },
      this.onListingsCollectionUpdate
    );

    this.willBlurSubscription = this.props.navigation.addListener(
      "willBlur",
      (payload) =>
        BackHandler.removeEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );
  }

  onGetUserError = () => {
    this.setState({ loading: false }, () => {
      alert(
        "0ops! an error occured  while loading profile. This user profile may be incomplete."
      );
      this.props.navigation.goBack();
    });
  };

  componentWillUnmount() {
    if (this.listingsUnsubscribe) {
      this.listingsUnsubscribe();
    }
    if (this.unsubscribe) {
      this.unsubscribe();
    }
    if (this.reviewsUnsubscribe) {
      this.reviewsUnsubscribe();
    }

    this.didFocusSubscription && this.didFocusSubscription();
    this.willBlurSubscription && this.willBlurSubscription();
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();

    return true;
  };

  onListingsCollectionUpdate = (querySnapshot) => {
    const listings = [];
    querySnapshot.forEach((doc) => {
      const listing = doc.data();

      listings.push({ ...listing, id: doc.id });
    });

    this.setState({
      listings,
    });
  };

  onPressListingItem = (item) => {
    this.props.navigation.navigate("ListingProfileModalDetailsScreen", {
      item,
    });


  };

  updateReviews = (reviews) => {
    this.setState({
      reviews: reviews,
    });
  };

  onDocUpdate = (doc) => {
    const listing = doc.data();

    this.setState(
      {
        data: { ...listing, id: doc.id },
        loading: false,
      },
      () => {}
    );


  };






  renderListingItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => this.onPressListingItem(item)}>
        <View style={TwoColumnListStyle.listingItemContainer}>
          <FastImage
            style={TwoColumnListStyle.listingPhoto}
            source={{ uri: item.photo }}
          />
          <SavedButton
            style={TwoColumnListStyle.savedIcon}
            onPress={() => this.onPressSavedIcon(item)}
            item={item}
          />
          <Text numberOfLines={1} style={TwoColumnListStyle.listingName}>
            {item.title}
          </Text>
          <Text style={TwoColumnListStyle.listingPlace}>{item.place}</Text>
          <StarRating
            containerStyle={styles.starRatingContainer}
            maxStars={5}
            starSize={15}
            disabled={true}
            starStyle={styles.starStyle}
            emptyStar={Images.starNoFilled}
            fullStar={Images.starFilled}
            halfStarColor={Colors.foregroundColor}
            rating={item.starCount}
          />
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    const { user } = this.state;

    if (this.state.loading) {
      return <ActivityIndicator size="small" color={Colors.main} />;
    }

    // console.log("this.user", this.state.user);

    if (!this.state.loading && !this.state.user) {
      return null;
    }

    return (
      <View style={styles.container}>
        <ScrollView style={styles.container}>
          <View style={styles.profileCardContainer}>
            <ProfileImageCard disabled={true} user={user} />
          </View>
          <View style={styles.profileItemContainer}>
            <View style={styles.detailContainer}>
              <Text style={styles.profileInfo}>
                {Translate("Profile Info")}
              </Text>
              <View style={styles.profileInfoContainer}>
                <View style={styles.profileInfoTitleContainer}>
                  <Text style={styles.profileInfoTitle}>
                    {Translate("Phone Number :")}
                  </Text>
                </View>
                <View style={styles.profileInfoValueContainer}>
                  <Text style={styles.profileInfoValue}>
                    {user.phoneNumber ? user.phoneNumber : ""}
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.gridContainer}>
              <Text style={styles.myListings}>{Translate("Listings")}</Text>
              <FlatList
                vertical
                showsVerticalScrollIndicator={false}
                numColumns={2}
                data={this.state.listings}
                renderItem={(item) => this.renderListingItem(item)}
                keyExtractor={(item) => `${item.id}`}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

ListingProfileModal.propTypes = {
  user: PropTypes.object,
  onModal: PropTypes.func,
  isProfileModalVisible: PropTypes.bool,
  presentationStyle: PropTypes.string,
};

const itemIconSize = 26;
const itemNavigationIconSize = 23;

const styles = StyleSheet.create({
  cardContainer: {
    flex: 1,
  },
  cardImageContainer: {
    flex: 4,
    justifyContent: "center",
    alignItems: "center",
  },
  cardImage: {
    height: 130,
    width: 130,
    borderRadius: 65,
  },
  gridContainer: {
    padding: Configuration.home.listing_item.offset,
    marginTop: 10,
  },
  cardNameContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  cardName: {
    color: Colors.text,
    fontSize: Fonts.size.large,
  },
  container: {
    flex: 1,
    borderBottomColor: Colors.border,
  },
  profileCardContainer: {
    marginTop: 16,
    justifyContent: "center",
    alignItems: "center",
  },
  profileItemContainer: {
    marginTop: 6,
  },
  itemContainer: {
    flexDirection: "row",
    height: 54,
    width: "85%",
    alignSelf: "center",
    marginBottom: 10,
  },
  itemIconContainer: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
  },
  itemIcon: {
    height: itemIconSize,
    width: itemIconSize,
  },
  itemTitleContainer: {
    flex: 6,
    justifyContent: "center",
    alignItems: "flex-start",
  },
  itemTitle: {
    color: Colors.text,
    fontSize: Fonts.size.normal,
    paddingLeft: 20,
  },
  itemNavigationIconContainer: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
  },
  rightButton: {
    marginRight: 10,
    color: Colors.main,
  },
  itemNavigationIcon: {
    height: itemNavigationIconSize,
    width: itemNavigationIconSize,
    tintColor: Colors.grey,
  },
  detailContainer: {
    backgroundColor: "#efeff4",
    padding: 20,
    marginTop: 25,
  },
  profileInfo: {
    padding: 5,
    color: "#333333",
    fontSize: Fonts.size.regular,
  },
  myListings: {
    paddingTop: 5,
    paddingBottom: 20,
    fontWeight: "500",
    color: "#333333",
    fontSize: Fonts.size.normal,
  },
  profileInfoContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 6,
    width: "100%",
  },
  profileInfoTitleContainer: {
    flex: 1,
    // alignItems: "center",
    // backgroundColor: "green",
    justifyContent: "center",
  },
  profileInfoTitle: {
    color: "#595959",
    fontSize: Fonts.size.small,
    padding: 5,
  },
  profileInfoValueContainer: {
    flex: 2,
    // alignItems: "center",
    // backgroundColor: "yellow",
    justifyContent: "center",
  },
  profileInfoValue: {
    color: Colors.grey6,
    fontSize: Fonts.size.normal,
    padding: 5,
  },
  footerButtonContainer: {
    flex: 2,
    justifyContent: "flex-start",
    marginTop: 8,
  },
  footerContainerStyle: {
    // borderColor: Colors.grey
  },
  blank: {
    flex: 0.5,
  },
});

const mapStateToProps = (state) => ({
  user: state.auth.user,
});

export default connect(mapStateToProps)(ListingProfileModal);
