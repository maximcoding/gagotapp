import React from 'react';
import {TouchableOpacity, View, Platform} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LaunchNavigator from 'react-native-launch-navigator';

export default function Map({coordinates}) {
  if (Platform.OS === 'android')
    LaunchNavigator.setGoogleApiKey('AIzaSyC-V3J1Wz4wqgHqrqffIeZqhVM0gDfTsIg');

  const openMap = () => {
    LaunchNavigator.navigate([coordinates.latitude, coordinates.longitude], {
      start: `${coordinates.latitude},${coordinates.longitude}`,
    })
      .then(() => {
          //console.log('Launched navigator')
      })
      .catch((err) => {
          // console.error('Error launching navigator: ' + err)
      });
  };
  return (
    <View style={{position: 'relative'}}>
      <MapView
        style={{width: '100%', height: 250}}
        region={{
          latitude: coordinates.latitude,
          longitude: coordinates.longitude,
          latitudeDelta: 0.02,
          longitudeDelta: 0.02,
        }}>
        <Marker
          coordinate={{
            latitude: coordinates.latitude,
            longitude: coordinates.longitude,
          }}
        />
      </MapView>
      <TouchableOpacity
        style={{
          position: 'absolute',
          height: 50,
          width: 50,
          borderRadius: 30,
          backgroundColor: 'green',
          elevation: 4,
          justifyContent: 'center',
          alignItems: 'center',
          padding: 10,
          bottom: 20,
          right: 20,
          zIndex: 100,
        }}
        onPress={openMap}>
        <Icon
          name="directions"
          type="FontAwesome5"
          style={{color: 'white', fontSize: 25}}
        />
      </TouchableOpacity>
    </View>
  );
}
