import React from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import {Fonts} from '../../themes';
let {width} = Dimensions.get('window');

export default function Index(props) {
  return (
    <TouchableOpacity
      style={[
        styles.bottomBtn,
        props.setRadius && styles.radius,
        {backgroundColor: props.light ? 'gray' : '#00b33c'},
      ]}
      {...props}
      disabled={props.loader || props.light}>
      {props.loader ? (
        <ActivityIndicator size="large" color={'white'} />
      ) : (
        <Text style={styles.btnColor}>{props.title}</Text>
      )}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  bottomBtn: {
    position: 'absolute',
    bottom: 0,
    width: width,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
  },
  btnColor: {
    color: 'white',
    fontSize: Fonts.size.small,
  },
  radius: {
    borderRadius: 30,
  },
});
