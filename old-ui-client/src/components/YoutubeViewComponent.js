import React from 'react';
import {StyleSheet, View, ActivityIndicator, Dimensions} from 'react-native';
import {WebView} from 'react-native-webview';

// const height = Dimensions.get('window').height - 50;
const height = Dimensions.get('window').height - 100;

const youtubeParser = (url) => {
  const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
  const match = url.match(regExp);
  return match && match[7].length === 11 ? match[7] : false;
};
const vimeoParser = (url) => {
  var regExp = /(videos|video|channels|\.com)\/([\d]+)/;
  const match = url.match(regExp);
  return match && match.length >= 2 ? match[2] : false;
};

const YoutubeViewComponent = ({
  video,
  autoplay,
  rel,
  modest,
  smallScreen,
  allowsFullscreenVideo,
}) => {
  const [loaded, setLoaded] = React.useState(false);
  //   var videoId = youtubeParser(video);

  //   var videoSrc = `https://www.youtube.com/embed/${videoId}?autoplay=${autoplay}&rel=${rel}&modestbranding=${modest}`;
  //   if (!videoId) {
  //     videoId = vimeoParser(video)
  //     videoSrc = `https://player.vimeo.com/video/${
  //       videoId}?autoplay=${
  //       autoplay}&rel=${
  //       rel}&modestbranding=${
  //       modest}`;

  //   }
//   var html = `     <iframe
//   title="Youtube video"
  
//   type="text/html"
//   width= '100%'
//   height='1000px'
//   src=${videoSrc}
//   frameBorder="0"
//   allowfullscreen= ${allowsFullscreenVideo} 
//    mozallowfullscreen=${allowsFullscreenVideo} 
//     msallowfullscreen=${allowsFullscreenVideo} 
//    oallowfullscreen=${allowsFullscreenVideo} 
//    webkitallowfullscreen=${allowsFullscreenVideo} 
// />`;
  return (
    <View style={[styles.container, {height: smallScreen ? 194 : height}]}>
      <WebView
        originWhitelist={['*']}
        containerStyle={[styles.webView, {height: smallScreen ? 194 : height}]}
        source={{uri: video}}
        //////
        allowsFullscreenVideo={allowsFullscreenVideo}
        mediaPlaybackRequiresUserAction={false}
        userAgent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36"
        ///
        onLoad={() => {
          setLoaded(true);
        }}
      />
      {!loaded && <ActivityIndicator size="large" />}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    borderRadius: 8,
  },
  webView: {
    width: '100%',
    borderRadius: 8,
  },
});

YoutubeViewComponent.defaultProps = {
  rel: 0,
  modest: 1,
  autoplay: 0,
};

export default YoutubeViewComponent;
