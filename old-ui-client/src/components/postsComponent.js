import React, {useState, useEffect, memo} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Dimensions,
  Alert,
  Share,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {getFunctionHelper, onRefresh, loadMore} from '../utils/lazyLoading';
import HomePicturesList from '../screens/HomeScreen/HomePicturesList';
import HomeTextList from '../screens/HomeScreen/HomeTextList';
import HomeList from '../screens/HomeScreen/HomeList';
import {saveUnsaveListing} from '../redux/actions/postListing';
import ActivityIndicator from './activityIndicator';
import {Colors} from '../themes';
import {Translate} from '../core/i18n/IMLocalization';

let {width} = Dimensions.get('window');

export default memo(function PostsComponent({
  data,
  Func,
  params,
  grid,
  navigation,
  onLongPressListingItem,
}) {
  const dispatch = useDispatch();
  const {user, currencyRates} = useSelector((state) => state.auth);
  let {savedPosts} = useSelector((state) => state.postReducer);
  let [isFetching, setFetching] = useState(false);
  let [loader, setLoader] = useState(false);
  let [moreLoader, setMoreLoader] = useState(false);
  let [page, setPage] = useState(0);

  const authFunc = () => {
    Alert.alert(
      Translate('Authorization'),
      Translate('nlm'),
      [
        {
          text: Translate('Log In'),
          onPress: () => navigation.navigate('Welcome'),
        },
        {
          text: Translate('CANCEL'),
          onPress: () => {
            // console.log('Yes, discard changes')
          },
          style: 'cancel',
        },
      ],
      {cancelable: false},
    );
  };

  const onPressListingItem = (item) => {
    if (user) {
      navigation.navigate('Detail', {
        item: item,
        customLeft: true,
        routeName: 'Home',
      });
    } else {
      authFunc();
    }
  };

  const onPressSavedIcon = (item) => {
    if (user) {
      dispatch(saveUnsaveListing(item, savedPosts));
    } else {
      authFunc();
    }
  };

  const share = async (item) => {
    try {
      const result = await Share.share({
        title: 'Real State',
        message: `
         Title: ${item.title}
         Details: ${item.description}
         Place: ${item.place.slice(-10)}
         Image: ${item.photo}
         app link is state://app
          `,
        url: item.photo,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  const renderListingItem = ({item, index}) => {
    switch (grid) {
      case 'images':
        return (
          <HomePicturesList
            item={item}
            index={index}
            onPressListingItem={onPressListingItem}
            onLongPressListingItem={onLongPressListingItem}
            onPressSavedIcon={onPressSavedIcon}
            savedPosts={savedPosts}
            currencyRates={currencyRates}
            user={user}
            share={share}
          />
        );

      case 'lists':
        return (
          <HomeList
            item={item}
            index={index}
            onPressListingItem={onPressListingItem}
            onLongPressListingItem={onLongPressListingItem}
            onPressSavedIcon={onPressSavedIcon}
            savedPosts={savedPosts}
            currencyRates={currencyRates}
            user={user}
          />
        );
      case 'text':
        return (
          <HomeTextList
            item={item}
            index={index}
            onPressListingItem={onPressListingItem}
            onLongPressListingItem={onLongPressListingItem}
            onPressSavedIcon={onPressSavedIcon}
            savedPosts={savedPosts}
            currencyRates={currencyRates}
            user={user}
          />
        );
    }
  };

  useEffect(() => {
    getFunctionHelper(params, 0, setLoader, dispatch, Func);
  }, [params]);

  if (loader) {
    return (
      <View style={styles.blankView}>
        <ActivityIndicator />
      </View>
    );
  } else
    return (
      <FlatList
        vertical
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{flexGrow: 1, width}}
        data={data}
        renderItem={renderListingItem}
        keyExtractor={(item, index) => item._id}
        onEndReachedThreshold={0.5}
        onRefresh={() =>
          onRefresh(params, setPage, setFetching, dispatch, Func)
        }
        refreshing={isFetching}
        onEndReached={() =>
          loadMore(
            params,
            moreLoader,
            page,
            setMoreLoader,
            setPage,
            dispatch,
            Func,
          )
        }
        ListEmptyComponent={() => {
          return (
            <View style={styles.blankView}>
              <Text style={styles.backImage}>No result found !</Text>
            </View>
          );
        }}
      />
    );
});

const styles = StyleSheet.create({
  backImage: {
    color: Colors.grey,
  },
  blankView: {
    flex: 1,
    height: '100%',
    width: width,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
