import React, {useRef, useState, memo} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Image} from 'react-native';
import moment from 'moment';
import Swipeable from 'react-native-swipeable';
import ActionSheet from 'react-native-actionsheet';
import AIcon from 'react-native-vector-icons/AntDesign';
import EIcon from 'react-native-vector-icons/SimpleLineIcons';
import {UserAction} from '../redux/actions/admin';
import {useDispatch} from 'react-redux';
import {Translate} from '../core/i18n/IMLocalization';
import {ListStyle} from '../AppStyles';
import {Fonts} from '../themes';
let img = 'https://assets.stickpng.com/images/585e4bf3cb11b227491c339a.png';

export default memo(function UserItem({item, index}) {
  let [user, setUser] = useState({...item});
  const dispatch = useDispatch();
  const refAction = useRef();
  const onLisingItemActionDone = (index) => {
    if (index == 0) {
      setUser({
        ...user,
        status: user.status == 'active' ? 'block' : 'active',
      });
      dispatch(
        UserAction(item, {
          status: user.status == 'active' ? 'block' : 'active',
        }),
      );
    }
    if (index == 2) {
      setUser({
        ...user,
        type: user.type == 'agent' ? 'user' : 'agent',
      });
      dispatch(
        UserAction(item, {
          type: user.type == 'agent' ? 'user' : 'agent',
        }),
      );
    }
  };

  const openEdit = () => {
    refAction.current.show();
  };

  // console.log('item', item);

  const favUser = () => {
    setUser({
      ...user,
      favorite: !user.favorite,
    });
    dispatch(
      UserAction(item, {
        favorite: !user.favorite,
      }),
    );
  };

  return (
    <>
      <Swipeable
        key={index}
        rightButtonWidth={80}
        rightButtons={[
          <TouchableOpacity
            style={[styles.rightSwipeItem, {backgroundColor: '#c2d6d6'}]}
            onPress={favUser}>
            <AIcon
              name="heart"
              color={user.favorite ? 'red' : 'white'}
              size={20}
            />
            <Text style={{color: 'white'}}>Favorite</Text>
          </TouchableOpacity>,
          <TouchableOpacity
            style={[styles.rightSwipeItem, {backgroundColor: 'orange'}]}
            onPress={openEdit}>
            <EIcon name="options-vertical" color="white" size={18} />
            <Text style={{color: 'white'}}>Options</Text>
          </TouchableOpacity>,
        ]}>
        <View
          style={[
            styles.rowM,
            {backgroundColor: user.status == 'block' ? '#F8F8F8' : 'white'},
          ]}>
          <Image
            source={{uri: user.photoURI ? user.photoURI : img}}
            style={styles.avatar}
          />
          <View
            style={{
              alignItems: 'flex-start',
              paddingLeft: 20,
            }}>
            <Text style={ListStyle.title}>
              {user.firstName} {user.lastName}
            </Text>
            <Text style={styles.mText}>{user.email}</Text>
            <Text style={styles.mText}>{user.phone}</Text>
          </View>
          <Text style={styles.time}>
            {moment(user.createdAt).fromNow(true)} ago
          </Text>
          <Text style={styles.type}>{user.type}</Text>
        </View>
      </Swipeable>

      <ActionSheet
        ref={refAction}
        title={`Options`}
        options={[
          `${user.status == 'active' ? 'block' : 'active'}`,
          'Cancel',

          `${user.type == 'user' ? 'Change to Agent' : 'Change to User'}`,
        ]}
        cancelButtonIndex={1}
        destructiveButtonIndex={1}
        onPress={(index) => {
          onLisingItemActionDone(index);
        }}
      />
    </>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  filtersButton: {
    marginRight: 10,
  },
  toggleButton: {
    marginRight: 7,
  },
  noItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  text: {
    color: 'gray',
  },
  main: {
    flex: 1,
  },
  pri: {position: 'absolute', bottom: 10, right: 10},
  rightSwipeItem: {
    height: 70,
    width: 80,
    justifyContent: 'center',
    marginTop: 10,
    alignItems: 'center',
  },
  rowM: {
    flexDirection: 'row',
    marginTop: 10,
    height: 70,
    width: '100%',
    padding: 10,
    alignItems: 'center',
  },
  avatar: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
  },
  header: {
    height: 50,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingHorizontal: 20,
    elevation: 2,
  },
  time: {
    position: 'absolute',
    bottom: 10,
    right: 10,
    fontSize: Fonts.size.small,
    color: 'gray',
  },
  hTitle: {
    fontSize: Fonts.size.large,
  },
  mText: {
    fontSize: Fonts.size.small,
    color: 'gray',
  },
  type: {
    fontSize: Fonts.size.small,
    color: 'gray',
    position: 'absolute',
    top: 10,
    right: 10,
  },
});
