import React, {useState} from 'react';
import {TouchableOpacity, Image, Modal} from 'react-native';
import YoutubeViewComponent from './YoutubeViewComponent';

export default ({visible, helpUrl, setVisible}) => {
  return (
    <Modal
      visible={visible}
      onRequestClose={() => setVisible(false)}
      transparent>
      <TouchableOpacity
        activeOpacity={1}
        style={{
          flex: 1,
          backgroundColor: 'rgba(255,255,255,.5)',
          justifyContent: 'center',
          padding: 10,
        }}>
        <TouchableOpacity
          onPress={() => setVisible(false)}
          style={{alignSelf: 'flex-end', padding: 10}}>
          <Image
            source={require('../../assets/icons/Cancel.png')}
            style={{height: 20, width: 20}}
          />
        </TouchableOpacity>
        <YoutubeViewComponent
          video={helpUrl}
          allowsFullscreenVideo={false}
          autoplay={1}
        />
      </TouchableOpacity>
    </Modal>
  );
};
