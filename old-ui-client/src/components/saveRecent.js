import React, {memo} from 'react';
import {FlatList, StyleSheet, Text, View, SafeAreaView} from 'react-native';

import {useSelector, useDispatch} from 'react-redux';
import {saveUnsaveListing} from '../redux/actions/postListing';

import IIcon from 'react-native-vector-icons/Ionicons';

import RentingSellingItem from '../components/rentingSellingItem';
import {Fonts, Colors} from '../themes';

export default memo(function ListingScreen(props) {
  const dispatch = useDispatch();
  let {type} = props.route.params;
  const {savedPosts, recentPosts} = useSelector((state) => state.postReducer);

  const navigateTo = () => {
    props.navigation.goBack();
  };

  const renderTitle = () => {
    switch (type) {
      case 'fav':
        return 'Favorites';
      case 'recent':
        return 'Recent Viewed';
      default:
        break;
    }
  };

  const renderData = () => {
    switch (type) {
      case 'fav':
        return savedPosts;
      case 'recent':
        return recentPosts;
      default:
        break;
    }
  };

  const renderItem = ({item, index}) => (
    <RentingSellingItem
      item={item}
      index={index}
      dispatch={dispatch}
      removeUpdateListing={() => {}}
      saveUnsaveListing={saveUnsaveListing}
      type={type}
    />
  );
  return (
    <SafeAreaView style={styles.main}>
      <View style={styles.header}>
        <IIcon name="arrow-back" color="black" size={24} onPress={navigateTo} />
        <Text style={styles.hTitle}>{renderTitle()}</Text>
        <Text style={{color: Colors.white}}>Test</Text>
      </View>

      <FlatList
        contentContainerStyle={{flexGrow: 1}}
        data={renderData()}
        renderItem={renderItem}
        keyExtractor={(item) => `${item.id}`}
        initialNumToRender={20}
        ListEmptyComponent={() => {
          return (
            <View style={styles.noItem}>
              <Text style={styles.text}>No items yet</Text>
            </View>
          );
        }}
      />
    </SafeAreaView>
  );
});

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  filtersButton: {
    marginRight: 10,
  },
  toggleButton: {
    marginRight: 7,
  },
  noItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: 'gray',
  },
  main: {
    flex: 1,
  },
  pri: {position: 'absolute', bottom: 10, right: 10},
  rightSwipeItem: {
    height: 120,
    width: 80,
    justifyContent: 'center',
    marginTop: 20,
    alignItems: 'center',
  },
  rowM: {
    flexDirection: 'row',
    marginTop: 20,
    height: 120,
    width: '100%',
    backgroundColor: 'white',
  },
  header: {
    height: 50,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingHorizontal: 20,
  },
  hTitle: {
    fontSize: Fonts.style.title,
    fontWeight: 'bold',
  },
});
