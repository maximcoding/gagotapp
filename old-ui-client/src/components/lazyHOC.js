import React, {PureComponent} from 'react';
import {InteractionManager} from 'react-native';
import Activity from './activityIndicator';

export default class LazyHOC extends PureComponent {
  state = {
    hidden: true,
  };

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.setState({hidden: false});
    });
  }

  render() {
    if (this.state.hidden) {
      return <Activity />;
    }

    return this.props.children;
  }
}
