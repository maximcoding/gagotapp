import React from "react";
import { TouchableOpacity, Image, ActivityIndicator } from "react-native";
import { AppIcon, AppStyles } from "../AppStyles";
import { Colors } from "../themes";

export default class HeaderButton extends React.Component {
  render() {
    return (
      <TouchableOpacity
        style={this.props.customStyle}
        onPress={this.props.onPress}
        disabled={this.props.disabled}
      >
        {this.props.loading ? (
          <ActivityIndicator
            style={{ padding: 6 }}
            size={5}
            color={Colors.main}
          />
        ) : (
          <Image
            style={[AppIcon.style, this.props.iconStyle]}
            source={this.props.icon}
          />
        )}
      </TouchableOpacity>
    );
  }
}
