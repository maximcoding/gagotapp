import React from 'react';
import {View, ActivityIndicator, StyleSheet} from 'react-native';

export default function Index() {
  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'white',
      }}>
      <ActivityIndicator size="small" color={'green'} />
    </View>
  );
}
