import React, {useRef, useState, memo} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import moment from 'moment';
import Swipeable from 'react-native-swipeable';
import ActionSheet from 'react-native-actionsheet';
import AIcon from 'react-native-vector-icons/AntDesign';
import EIcon from 'react-native-vector-icons/EvilIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {Translate} from '../core/i18n/IMLocalization';
import {ListStyle} from '../AppStyles';
import {styles} from '../screens/ListingScreen';
import {useSelector} from 'react-redux';
import AddListingScreen from '../screens/AddListingScreen';
import {useNavigation} from '@react-navigation/native';
import {getSymbol, getPrice} from '../utils/currencyConverter';
import NumberFormat from 'react-number-format';
import Colors from '../themes/colors';
import {updateStatus} from '../redux/actions/postListing';

export default memo(function RentingSellingItem({
  item,
  index,
  dispatch,
  removeUpdateListing,
  saveUnsaveListing,
  type,
}) {
  let {user, currencyRates} = useSelector((state) => state.auth);
  let {savedPosts} = useSelector((state) => state.postReducer);
  let {categories} = useSelector((state) => state.categories);
  let [edit, setEdit] = useState(false);
  let [state, setState] = useState(item);
  let navigation = useNavigation();
  const refAction = useRef();
  const onLisingItemActionDone = (index) => {
    if (index == 0) {
      dispatch(removeUpdateListing({...state, delete: true}, () => {}));
    }
  };

  const saveListing = () => {
    dispatch(saveUnsaveListing(state, savedPosts));
  };
  const onPostCancel = () => {
    setEdit(false);
  };

  const openEdit = () => {
    setEdit(true);
  };

  const navigateTo = () => {
    navigation.navigate('Detail', {
      item: state,
    });
  };

  const changeStatusHandler = () => {
    let statusObj = {
      status: state.status === 'open' ? 'close' : 'open',
      id: state._id,
      type: state.type,
    };

    setState({
      ...state,
      status: state.status === 'open' ? 'close' : 'open',
    });

    dispatch(updateStatus(statusObj, changeStatusFinally));
  };

  const changeStatusFinally = (type, status) => {
    // console.log('final type', type);
  };

  return (
    <>
      <Swipeable
        key={index}
        rightButtonWidth={80}
        rightButtons={
          type !== 'fav' &&
          type !== 'recent' && [
            <TouchableOpacity
              style={[styles.rightSwipeItem, {backgroundColor: '#c2d6d6'}]}
              onPress={saveListing}>
              <AIcon
                name="heart"
                color={
                  savedPosts.some((k) => k._id == state._id) ? 'red' : 'white'
                }
                size={20}
              />
              <Text style={{color: 'white'}}>Favorites</Text>
            </TouchableOpacity>,
            <TouchableOpacity
              style={[styles.rightSwipeItem, {backgroundColor: 'orange'}]}
              onPress={openEdit}>
              <EIcon name="pencil" color="white" size={24} />
              <Text style={{color: 'white'}}>Edit</Text>
            </TouchableOpacity>,
            <TouchableOpacity
              style={[
                styles.rightSwipeItem,
                {
                  backgroundColor:
                    state.status === 'open' ? Colors.fire : Colors.green,
                },
              ]}
              onPress={changeStatusHandler}>
              {state.status === 'open' ? (
                <FontAwesome5 name="door-closed" color="white" size={24} />
              ) : (
                <FontAwesome5 name="door-open" color="white" size={24} />
              )}
              <Text style={{color: 'white'}}>
                {state.status === 'open' ? 'Close' : 'Open'}
              </Text>
            </TouchableOpacity>,
            <TouchableOpacity
              style={[styles.rightSwipeItem, {backgroundColor: '#ff4d4d'}]}
              onPress={() => refAction.current.show()}>
              <EIcon name="trash" color="white" size={24} />
              <Text style={{color: 'white'}}>Delete</Text>
            </TouchableOpacity>,
          ]
        }>
        <TouchableOpacity onPress={navigateTo}>
          <View style={styles.rowM}>
            <Image source={{uri: state.photo}} style={ListStyle.avatarStyle} />
            <View
              style={{
                alignItems: 'flex-start',
                justifyContent: 'space-between',
                paddingHorizontal: 15,
                paddingVertical: 15,
              }}>
              <Text style={ListStyle.title}>{state.title}</Text>
              <Text style={ListStyle.time}>
                {moment(state.createdAt).fromNow(true)} ago
              </Text>
              <Text style={ListStyle.place}>{state.place?.slice(0, 20)}</Text>
            </View>

            <NumberFormat
              renderText={(text) => (
                <Text style={[ListStyle.price, styles.pri]}>{text}</Text>
              )}
              value={getPrice(Number(state.price), user, currencyRates)}
              displayType={'text'}
              thousandSeparator={true}
              prefix={getSymbol(user)}
            />
          </View>
        </TouchableOpacity>
      </Swipeable>

      <ActionSheet
        ref={refAction}
        title={`Confirm to Delete`}
        options={[Translate('Confirm'), Translate('Cancel')]}
        cancelButtonIndex={1}
        destructiveButtonIndex={1}
        onPress={(index) => {
          onLisingItemActionDone(index);
        }}
      />
      {edit && (
        <AddListingScreen
          categories={categories}
          onCancel={onPostCancel}
          selectedItem={state}
        />
      )}
    </>
  );
});
