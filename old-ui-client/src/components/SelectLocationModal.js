import React, {useEffect} from 'react';
import {
  Modal,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  Platform,
} from 'react-native';
import LocationView from 'react-native-location-view';
import Colors from '../themes/colors';
import {Fonts} from '../themes';
import {useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/AntDesign';

export default SelectLocationModal = ({onDone, onCancel}) => {
  let {coordinates} = useSelector((state) => state.auth);

  const onDoneFunc = (location) => {
    onDone({
      latitude: location.latitude,
      longitude: location.longitude,
    });
  };

  const onCancelFunc = () => {
    onCancel();
  };
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <LocationView
        apiKey={'AIzaSyA4JsBWizKjDWSrf-vxPvcn8WZEYVn_LUU'}
        initialLocation={{
          latitude: coordinates.latitude,
          longitude: coordinates.longitude,
        }}
        onLocationSelect={(value) => onDoneFunc(value)}
        markerColor={Colors.green}
        actionButtonStyle={{
          backgroundColor: Colors.green,
        }}
        actionTextStyle={{fontSize: Fonts.size.normal}}
      />
      <TouchableOpacity style={styles.icon} onPress={onCancelFunc}>
        <Icon type="AntDesign" name="close" style={styles.iconS} />
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  body: {
    width: '100%',
    height: '100%',
  },
  rightButton: {
    paddingRight: 10,
  },
  topbar: {
    position: 'absolute',
    backgroundColor: Colors.transparent,
    width: '100%',
  },
  mapView: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: Colors.backgroundColor,
  },
  icon: {
    position: 'absolute',
    top: Platform.OS == 'ios' ? 43 : 20,
    right: 20,
  },
  iconS: {
    fontSize: 28,
    color: Colors.green,
  },
});
