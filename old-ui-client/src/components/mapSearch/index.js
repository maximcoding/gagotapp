import React from 'react';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {useDispatch} from 'react-redux';
import {updateLocation} from '../../redux/actions/user';
import {styles} from './style';

export default function MapInput() {
  const dispatch = useDispatch();
  return (
    <>
      <GooglePlacesAutocomplete
        placeholder={'Search Location'}
        placeholderTextColor="gray"
        selectionColor={'gray'}
        minLength={2}
        returnKeyType={'google'}
        listViewDisplayed="false"
        fetchDetails={true}
        renderDescription={(row) => row.description}
        onPress={(data, details = null) => {
          let location = details.geometry.location;
          dispatch(
            updateLocation({latitude: location.lat, longitude: location.lng}),
          );
        }}
        query={{
          key: 'AIzaSyA4JsBWizKjDWSrf-vxPvcn8WZEYVn_LUU',
          language: 'en',
          radius: 10000,
        }}
        styles={{
          textInputContainer: styles.textContainer,
          description: {
            color: 'black',
          },
          textInput: styles.textInput,
          predefinedPlacesDescription: {
            color: '#1faadb',
          },
          poweredContainer: {
            display: 'none',
          },
          listView: styles.listView,
          row: styles.row,
        }}
        currentLocation={false}
        GoogleReverseGeocodingQuery={{}}
        GooglePlacesSearchQuery={{
          rankby: 'distance',
          types: 'food',
        }}
        filterReverseGeocodingByTypes={[
          'locality',
          'administrative_area_level_3',
        ]}
        debounce={200}
      />
    </>
  );
}
