import {StyleSheet, Dimensions} from 'react-native';
let {width} = Dimensions.get('window');

export const styles = StyleSheet.create({
  textContainer: {
    width: width - 20,
    alignSelf: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
    borderTopWidth: 0,
    borderBottomWidth: 0,
  },
  textInput: {
    height: 50,
    backgroundColor: 'white',
    padding: 15,
    borderRadius: 35,
    zIndex: 21231,
  },
  row: {
    height: 50,
    alignItems: 'center',
    flexDirection: 'row',
  },
  listView: {
    marginTop: 20,
    paddingHorizontal: 10,
    backgroundColor: 'white',
  },
});
