import {
  getComplainsApi,
  getUsersApi,
  UserApi,
  searchUsersApi,
} from '../../api/index';
export const GET_COMPLAINS = 'GET_COMPLAINS';
export const GET_COMPLAINS_MORE = 'GET_COMPLAINS_MORE';
export const GET_USERS_MORE = 'GET_USERS_MORE';
export const GET_USERS = 'GET_USERS';
export const CHANGE_SORT_USER = 'CHANGE_SORT_USER';
export const getComplains = (callBack, page, params, more = false) => async (
  dispatch,
) => {
  try {
    let res = await getComplainsApi({page});
    if (more) {
      dispatch({
        type: GET_COMPLAINS_MORE,
        payload: res.data,
      });
    } else {
      dispatch({
        type: GET_COMPLAINS,
        payload: res.data,
      });
    }
    callBack();
  } catch (error) {
    callBack();
    alert(error.response ? error.response.data : error);
  }
};

export const getUsers = (callBack, page, params, more = false) => async (
  dispatch,
) => {
  try {
    let res = await getUsersApi({page, params});
    if (more) {
      dispatch({
        type: GET_USERS_MORE,
        payload: res.data,
      });
    } else {
      dispatch({
        type: GET_USERS,
        payload: res.data,
      });
    }
    callBack();
  } catch (error) {
    callBack();
    alert(error.response ? error.response.data : error);
  }
};

export const searchUsers = (search, cb) => async (dispatch) => {
  try {
    let res = await searchUsersApi({search});
    dispatch({
      type: GET_USERS,
      payload: res.data,
    });
    cb();
  } catch (error) {
    cb();
  }
};

export const changeSort = (value) => {
  return {
    type: CHANGE_SORT_USER,
    payload: value,
  };
};

export const UserAction = (user, query) => async (dispatch) => {
  try {
    let res = await UserApi({user, query});
  } catch (error) {
    alert(error);
  }
};
