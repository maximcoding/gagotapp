import {
  postReviewApi,
  getReviewApi,
  getPostedReviewApi,
  replyReviewApi,
  updatePropertyApi,
} from '../../api';

export const USER_MESSAGES = 'USER_MESSAGES';
export const USER_MESSAGES_MORE = 'USER_MESSAGES_MORE';

export const subscribeReviews = (listingId, callback) => async (dispatch) => {
  try {
    const res = await getReviewApi(listingId);

    callback(res.data);
  } catch (error) {
    callback();
  }
};

export const postReview = (user, data, starCount, content, callback) => async (
  dispatch,
) => {
  try {
    let reviewObj = {
      authorID: user._id,
      listingID: data._id,
      starCount,
      content: content,
      firstName: user.firstName,
      lastName: user.lastName,
      postUser: data.authorID,
      image: user.profilePictureURL,
      replies: [],
    };

    const res = await postReviewApi(reviewObj);

    let totalStarCount = 0;
    let count = 0;
    res.data.forEach((review) => {
      totalStarCount += review.starCount;
      count++;
    });

    if (count > 0) {
      data.starCount = totalStarCount / count;
    } else {
      data.starCount = 0;
    }

    await updatePropertyApi(data._id, data);


    callback();
  } catch (error) {
    // console.log('');
    callback();
  }
};

export const getPostReviews = (callback, page, params, more = false) => async (
  dispatch,
) => {
  try {
    const res = await getPostedReviewApi({page});
    if (more) {
      dispatch({
        type: USER_MESSAGES_MORE,
        payload: res.data,
      });
    } else {
      dispatch({
        type: USER_MESSAGES,
        payload: res.data,
      });
    }

    callback();
  } catch (error) {
    callback();
    alert(error.response ? error.response.data : error);
  }
};

export const replyToReview = (review, content, cb) => async (dispatch) => {
  try {
    let updateReview = review;
    updateReview.replies.push({
      text: content,
      createdAt: new Date(),
    });

    await replyReviewApi(review._id, {...updateReview});

    cb();
  } catch (error) {
    cb();
    alert(error);
  }
};
