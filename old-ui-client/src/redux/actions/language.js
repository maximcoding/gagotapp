export const UPDATE_LANGUAGE = "UPDATE_LANGUAGE";
export const updateLanguage = () => {
  return {
    type: UPDATE_LANGUAGE,
  };
};
