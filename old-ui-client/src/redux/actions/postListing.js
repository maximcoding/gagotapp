import geohash from 'ngeohash';
import {
  addProperty,
  filterCategoriesApi,
  getPropertiesPreviewsApi,
  getCategoriesApi,
  getPropertyByIdApi,
  updatePropertyApi,
  getListingRSApi,
  deleteListingApi,
  changePropertyStatusApi,
  getMapPropertiesApi,
  enrollUserApi,
  editEnrollTimeApi,
  getEnrollersApi,
  getVisitsApi,
  searchListingActionApi,
} from '../../api';
import {FILTER_RESULT} from './filters';
import {updateCategories} from './category';
import {cloudinaryImageUrl} from '../../api/cloudinary';

export const POST_LISTING = 'POST_LISTING';
export const SAVED_POSTS = 'SAVED_POSTS';
export const RENTING_POSTS = 'RENTING_POSTS';
export const RENTING_POSTS_MORE = 'RENTING_POSTS_MORE';
export const BUYING_POSTS = 'BUYING_POSTS';
export const BUYING_POSTS_MORE = 'BUYING_POSTS_MORE';
export const RECENT_POSTS = 'RECENT_POSTS';
export const RECENT_POSTS_LIST = 'RECENT_POSTS_LIST';
export const SAVE_RECENT_LISTING = 'SAVE_RECENT_LISTING';
export const DELETE_LISTING = 'DELETE_LISTING';
export const ADD_TO_SAVED = 'ADD_TO_SAVED';
export const UNSAVE_POST = 'UNSAVE_POST';
export const CHANGE_STATUS = 'CHANGE_STATUS';
export const POST_LISTING_MORE = 'POST_LISTING_MORE';
export const CATEGORY_FILTER = 'CATEGORY_FILTER';
export const FILTER_RESULT_MORE = 'FILTER_RESULT_MORE';
export const MAP_POSTS = 'MAP_POSTS';
export const DAY_ENROLLERS_MORE = 'DAY_ENROLLERS_MORE';
export const DAY_ENROLLERS = 'DAY_ENROLLERS';
export const VISITS = 'VISITS';
export const VISITS_MORE = 'VISITS_MORE';
export const SEARCH_LISTS = 'SEARCH_LISTS';

export const getGeohashRange = (latitude, longitude, distance) => {
  const lat = 0.0144927536231884;
  const lon = 0.0181818181818182;

  const lowerLat = latitude - lat * distance;
  const lowerLon = longitude - lon * distance;

  const upperLat = latitude + lat * distance;
  const upperLon = longitude + lon * distance;

  const lower = geohash.encode(lowerLat, lowerLon);
  const upper = geohash.encode(upperLat, upperLon);

  return {
    lower,
    upper,
  };
};

export const getPropertyPreviews = (callback, page, sort, more = false) => async (dispatch, getState) => {
  try {
    let {coordinates} = getState().auth;
    let range;
    if (sort == 'near') {
      range = getGeohashRange(coordinates.latitude, coordinates.longitude, 10);
    }
    const res = await getPropertiesPreviewsApi({sort, page, range});
    if (more) {
      dispatch({
        type: POST_LISTING_MORE,
        payload: res.data,
      });
    } else {
      dispatch({
        type: POST_LISTING,
        payload: res.data,
      });
    }
    callback();
  } catch (error) {
    callback();
    alert(error);
  }
};

export const getMapsPosts = (coordinates) => async (dispatch, getState) => {
  try {
    let range = getGeohashRange(
      coordinates.latitude,
      coordinates.longitude,
      40,
    );

    //ALSO CHECK IF FILTER IS ON
    const {filterValues} = getState().filterReducer;
    const filters = filterValues.persistState ? filterValues : null;

    const res = await getMapPropertiesApi({range, filters});
    dispatch({
      type: MAP_POSTS,
      payload: res.data,
    });
  } catch (error) {
    alert(error);
  }
};

export const getDocById = (id, cb) => async (dispatch) => {
  try {
    let result = await getPropertyByIdApi(id);
    cb(result.data);
  } catch (error) {
    alert(error);
  }
};

export const subscribeListingCategories = (callback) => async (dispatch) => {
  try {
    let res = await getCategoriesApi();
    // console.log('cate are here', res.data);
    callback();
    dispatch(updateCategories(res.data));
  } catch (error) {
    // console.log('error is here', error);
  }
};

export const filterCategories = (callback, page, categoryId, more = false) => async (dispatch) => {
  try {
    if (categoryId) {
      let res = await filterCategoriesApi({categoryId, page});
      if (more) {
        dispatch({
          type: FILTER_RESULT_MORE,
          payload: res.data,
        });
      } else {
        dispatch({
          type: FILTER_RESULT,
          payload: res.data,
        });
      }
      callback();
    }
  } catch (error) {
    callback();
    // console.log('error while filter categories', error);
  }
};

export const showCategoryFilter = (categoryId) => {
  return {
    type: CATEGORY_FILTER,
    payload: categoryId,
  };
};

export const searchListingAction = (text, cb) => async (dispatch) => {
  try {
    let res = await searchListingActionApi({text});
    // console.log('my result posts are here', res.data);
    dispatch({
      type: SEARCH_LISTS,
      payload: res.data,
    });
    cb();
  } catch (error) {
    cb();
  }
};

export const saveRecentListing = (item, recentSaved) => async (dispatch) => {
  try {
    if (recentSaved.some((e) => e._id === item._id)) {
      return;
    } else {
      dispatch({
        type: SAVE_RECENT_LISTING,
        payload: item,
      });
    }
  } catch (error) {
    // console.log('error is here', error);
  }
};

export const saveUnsaveListing = (item, savedPosts) => async (dispatch) => {
  try {
    if (savedPosts.some((e) => e._id === item._id)) {
      dispatch({
        type: UNSAVE_POST,
        payload: item,
      });
    } else {
      dispatch({
        type: ADD_TO_SAVED,
        payload: item,
      });
    }
  } catch (error) {
    alert(error);
  }
};

export const removeProperty = (listing, callback) => async (dispatch) => {
  try {
    await deleteListingApi(listing._id);
    dispatch({
      type: DELETE_LISTING,
      payload: listing._id,
    });
    callback(true);
  } catch (error) {
    callback(false);
  }
};

export const postListing = async (selectedItem, uploadObject, callback) => {
  try {
    const updatedUploadObjects = {
      ...uploadObject,
      coordinate: [uploadObject.latitude, uploadObject.longitude],
    };
    if (selectedItem) {
      await updatePropertyApi(selectedItem._id, {...updatedUploadObjects});
      callback(true);
    } else {
      // console.log('updatedUploadObjects ==== >', updatedUploadObjects);

      const res = await addProperty(updatedUploadObjects);
      // console.log('res while add state', res.data);
      callback(true);
    }
  } catch (error) {
    callback(false);
    alert(error.response ? error.response.data : error);
  }
};

export const uploadToCloudiary = (file, setProgress, cb) => async (dispatch) => {
  try {
    let res = await cloudinaryImageUrl(file, setProgress);

    cb(res.data.secure_url);
    setProgress(0);
  } catch (error) {
    // console.log('error is here', error.response.data);
    setProgress(0);
    alert(error);
  }
};

export const uploadImage = async (source) => {
  let res = await cloudinaryImageUrl(source);
  let image = res.data.secure_url;
};

export const removeUpdateListing = (item, cb) => async (dispatch) => {
  try {
    await updatePropertyApi(item._id, {item});
    cb();
  } catch (error) {
    cb();
    alert(error.response ? error.response.data : error);
  }
};

export const getListingRS = (callback, page, type, more = false) => async (dispatch) => {
  try {
    const res = await getListingRSApi({type, page});

    if (type == 'buy') {
      if (more) {
        dispatch({
          type: BUYING_POSTS_MORE,
          payload: res.data,
        });
      } else {
        dispatch({
          type: BUYING_POSTS,
          payload: res.data,
        });
      }
    } else {
      if (more) {
        dispatch({
          type: RENTING_POSTS_MORE,
          payload: res.data,
        });
      } else {
        dispatch({
          type: RENTING_POSTS,
          payload: res.data,
        });
      }
    }
    callback();
  } catch (error) {
    callback();
    alert(error.response ? error.response.data : error);
  }
};

function transform2d(srcArray, modulus) {
  let result = undefined;
  if (modulus > 0) {
    result = [];
    for (let i = 0; i < srcArray.length; i += modulus) {
      result.push(srcArray.slice(i, i + modulus));
    }
  }
  return result;
}

export const getFavouriteListing = (savedPosts, cb) => async (dispatch) => {
  try {
    let arrays = transform2d(savedPosts, 10);
    let promises = [];
    arrays.map((ar) => {
      promises.push(listingsRef.where('id', 'in', ar).get());
    });

    let resultOne = await Promise.all(promises);
    let docsArr = [];
    resultOne.forEach((item) => {
      docsArr.push(item.docs);
    });
    let result = [];

    docsArr.flat(1).forEach(function (doc) {
      result.push({...doc.data(), id: doc.id});
    });

    dispatch({
      type: FAVOURITE_POSTS,
      payload: result,
    });
    cb();
  } catch (error) {
    cb();
    alert(error);
  }
};

export const getRecentListings = (recentSaved, cb) => async (dispatch) => {
  try {
    let arrays = transform2d(recentSaved, 10);
    let promises = [];
    arrays.map((ar) => {
      promises.push(listingsRef.where('id', 'in', ar).get());
    });

    let resultOne = await Promise.all(promises);
    let docsArr = [];
    resultOne.forEach((item) => {
      docsArr.push(item.docs);
    });
    let result = [];

    docsArr.flat(1).forEach(function (doc) {
      result.push({...doc.data(), id: doc.id});
    });

    dispatch({
      type: RECENT_POSTS_LIST,
      payload: result,
    });
    cb();
  } catch (error) {
    cb();
    alert(error);
  }
};

export const updateStatus = (data, cb) => async (dispatch) => {
  try {
    let res = await changePropertyStatusApi(data);
    dispatch({
      type: CHANGE_STATUS,
      payload: data,
    });
    cb('success', data.status);
  } catch (er) {
    // console.log('err while update status', er);
    cb('error');
  }
};

export const enrollUser = (day, time, post, cb) => async (dispatch) => {
  try {
    let res = await enrollUserApi({day, time, post});
    if (res.data == 'success') {
      alert('Successfully enrolled for this day');
    }
    cb();
  } catch (error) {
    cb();
    alert(error.response ? error.response.data : error);
  }
};

export const editEnrollTime = (day, time, post, cb) => async (dispatch) => {
  try {
    let res = await editEnrollTimeApi({day, time, post});
    cb(res.data);
  } catch (error) {
    cb();
    alert(error.response ? error.response.data : error);
  }
};

export const getEnrollers = (callback, page, data, more = false) => async (dispatch) => {
  try {
    let res = await getEnrollersApi({...data, page});
    if (more) {
      dispatch({
        type: DAY_ENROLLERS_MORE,
        payload: res.data,
      });
    } else {
      dispatch({
        type: DAY_ENROLLERS,
        payload: res.data,
      });
    }
    callback();
  } catch (error) {
    callback();
    alert(error.response ? error.response.data : error);
  }
};

export const getVisits = (callback, page, data, more = false) => async (dispatch) => {
  try {
    let res = await getVisitsApi({page});
    if (more) {
      dispatch({
        type: VISITS_MORE,
        payload: res.data,
      });
    } else {
      dispatch({
        type: VISITS,
        payload: res.data,
      });
    }
    callback();
  } catch (error) {
    callback();
    alert(error.response ? error.response.data : error);
  }
};
