import {filterListingApi} from '../../api';
import _ from 'lodash';

export const FILTER_RESULT = 'FILTER_RESULT';
export const FILTER_RESULT_MORE = 'FILTER_RESULT_MORE';
export const RESET_FILTER = 'RESET_FILTER';
export const ONCHANGE_FILTER = 'ONCHANGE_FILTER';
export const CLEAR_FILTER = 'CLEAR_FILTER';
export const SEARCH_FILTER = 'SEARCH_FILTER';

export const filterPosts = (callback, page, data, more = false) => async (
  dispatch,
) => {
  try {
    const res = await filterListingApi(data);
    if (more) {
      dispatch({
        type: FILTER_RESULT,
        payload: res.data,
      });
    } else {
      dispatch({
        type: FILTER_RESULT,
        payload: res.data,
      });
    }

    callback();
  } catch (error) {
    // console.log('error is here', error);
    callback();
  }
};

export const setSearchFilter = () => {
  return {
    type: SEARCH_FILTER,
  };
};

export const onChangeFilter = (payload) => ({
  type: ONCHANGE_FILTER,
  payload,
});

export const onClearFilter = () => ({
  type: CLEAR_FILTER,
});

export const resetFilter = () => {
  return {
    type: RESET_FILTER,
  };
};
