import axios from 'axios';
import {
  signUpWithEmailApi,
  loginWithEmailApi,
  getUserApi,
  sendVerificationCodeApi,
  sendVerificationCodeApiExists,
  loginWithPhoneApi,
  updateUserApi,
  addComplainApi
} from '../../api';
import {cloudinaryImageUrl} from '../../api/cloudinary';
import {dispatchSetUserData, dispatchLogOut, updateUserData} from '../reducers';
import {Alert, Platform} from 'react-native';
import {INSTANCE} from '../../api/instance';

export const SAVE_CURRENCY = 'SAVE_CURRENCY';
export const CHANGE_GRID_VIEW = 'CHANGE_GRID_VIEW';
export const CHANGE_SORT_VIEW = 'CHANGE_SORT_VIEW';
export const SAVE_CODE = 'SAVE_CODE';
export const USER_LOCATION = 'USER_LOCATION';

export const updateUser = (user, cb) => async (dispatch) => {
  try {
    const res = await updateUserApi(user, user._id);
    dispatch(updateUserData(user));
    cb(true);
  } catch (error) {
    // console.log('error is here', error);
    cb();
    Alert.alert(
      '',
      error.response ? error.response.data : error,
      [{text: 'OK'}],
      {
        cancelable: false
      }
    );
  }
};

export const changePhoneNumber = (newPhone, cb) => async () => {
  try {
    // const confirmation = await auth().signInWithPhoneNumber(newPhone);

    const snapshot = await auth()
      .verifyPhoneNumber(newPhone)
      .on('state_changed', (phoneAuthSnapshot) => {
        // console.log('Snapshot state: ', phoneAuthSnapshot.state);
      });
    cb(snapshot);
  } catch (error) {
    cb();
    alert(error);
  }
};

export const verifyPhoneNumber = (code, updatedUser, cb) => async (
  dispatch,
  getState
) => {
  try {
    let oldCode = getState().auth.otp;
    // console.log('otp saved', oldCode, ':new:', code);
    if (+code === +oldCode) {
      const res = await updateUserApi(updatedUser, updatedUser._id);
      dispatch(updateUserData(updatedUser));
    } else {
      Alert.alert(
        'Incorrect OTP',
        'The OTP you typed is not correct',
        [{text: 'OK'}],
        {
          cancelable: false
        }
      );
    }

    cb(true);
  } catch (error) {
    Alert.alert(
      '',
      error.response ? error.response.data : error,
      [{text: 'OK'}],
      {
        cancelable: false
      }
    );
    cb();
  }
};

export const complainUs = (complain, user, cb) => async (dispatch) => {
  try {
    let data = {
      from: `${user.firstName} ${user.lastName}`,
      userId: user._id,
      text: complain
    };

    const res = await addComplainApi(data);
    // console.log('res from complain', res);

    cb(true);
  } catch (error) {
    cb();
    alert(error);
  }
};

export const getCurrencyPrices = () => async (dispatch) => {
  try {
    let res = await axios.get('https://api.exchangeratesapi.io/latest?base=USD');
    dispatch({
      type: SAVE_CURRENCY,
      payload: res.data.rates || {
        "USD": 1.17534,
        "ILS": 1.549855,
        "EUR": 1.478689
      }
    });
  } catch (error) {
  }
};

export const changeView = (value) => {
  return {
    type: CHANGE_GRID_VIEW,
    payload: value
  };
};

export const changeSort = (value) => {
  return {
    type: CHANGE_SORT_VIEW,
    payload: value
  };
};

// signup with email and password

export const signUpWithEmail = (data, cb) => async (dispatch, getState) => {
  try {
    let {notificationId} = getState().auth;
    const user = await signUpWithEmailApi({
      ...data,
      notificationId,
      device: Platform.OS
    });

    if (user) {
      dispatch(
        dispatchSetUserData({
          user: user.data,
          authorization: user.headers.authorization
        })
      );

      INSTANCE.defaults.headers.common[
        'Authorization'
        ] = `Bearer ${user.headers.authorization}`;
    } else {
      throw Error('something went wrong');
    }
  } catch (error) {
    // console.log('err while signup', error.response);

    Alert.alert(
      '',
      error.response ? error.response.data : error,
      [{text: 'OK'}],
      {
        cancelable: false
      }
    );
  } finally {
    cb();
  }
};
// signin with email and password

export const signinWithEmail = (data, cb) => async (dispatch, getState) => {
  try {
    let {notificationId} = getState().auth;
    const user = await loginWithEmailApi({
      ...data,
      notificationId,
      device: Platform.OS
    });
    if (user) {
      dispatch(
        dispatchSetUserData({
          user: user.data,
          authorization: user.headers.authorization
        })
      );
      INSTANCE.defaults.headers.common[
        'Authorization'
        ] = `Bearer ${user.headers.authorization}`;
    } else {
      throw Error('Something went wrong');
    }
  } catch (error) {
    Alert.alert(
      '',
      error.response ? error.response.data : error,
      [{text: 'OK'}],
      {
        cancelable: false
      }
    );
  } finally {
    cb();
  }
};

// Get user action
export const getUser = () => {
  return async (dispatch, getState) => {
    let token = getState().auth.authorization;
    INSTANCE.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  };
};

// Logout user action
export const logoutUser = () => {
  return async (dispatch) => {
    try {
      dispatch(dispatchLogOut());
    } catch (error) {
    }
  };
};

const saveCode = (code) => ({
  type: SAVE_CODE,
  payload: code
});

export const sendOtp = (phone, cb) => async (dispatch) => {
  try {
    let code = new Date().valueOf().toString().slice(7);
    dispatch(saveCode(code));
    let res = await sendVerificationCodeApi({phone, code});
    cb('success');
  } catch (error) {
    cb('fail');
    Alert.alert(
      '',
      error.response ? error.response.data : 'something went wrong',
      [{text: 'OK'}],
      {
        cancelable: false
      }
    );
  }
};

export const sendOptExists = (phone, cb) => async (dispatch) => {
  try {
    let code = new Date().valueOf().toString().slice(7);
    dispatch(saveCode(code));
    let res = await sendVerificationCodeApiExists({phone, code});
    cb(true);
  } catch (error) {
    cb(false);
    Alert.alert(
      '',
      error.response ? error.response.data : 'something went wrong',
      [{text: 'OK'}],
      {
        cancelable: false
      }
    );
  }
};

export const signupWithPhone = (data, newCode, cb) => async (
  dispatch,
  getState
) => {
  try {
    let code = getState().auth.otp;
    let {notificationId} = getState().auth;
    // console.log('otp saved', code, ':new:', newCode);
    if (+code === +newCode) {
      const user = await signUpWithEmailApi({
        ...data,
        notificationId,
        device: Platform.OS
      });

      if (user) {
        dispatch(
          dispatchSetUserData({
            user: user.data,
            authorization: user.headers.authorization
          })
        );

        INSTANCE.defaults.headers.common[
          'Authorization'
          ] = `Bearer ${user.headers.authorization}`;
      } else {
        throw Error('something went wrong');
      }
    } else {
      Alert.alert(
        'Incorrect OTP',
        'The OTP you typed is not correct',
        [{text: 'OK'}],
        {
          cancelable: false
        }
      );
    }
  } catch (error) {
    Alert.alert(
      '',
      error.response ? error.response.data : 'something went wrong',
      [{text: 'OK'}],
      {
        cancelable: false
      }
    );
  } finally {
    cb();
  }
};

// signin with phone

export const signinWithPhone = (data, newCode, cb) => async (
  dispatch,
  getState
) => {
  try {
    let code = getState().auth.otp;
    let {notificationId} = getState().auth;
    // console.log('otp saved', code, ':new:', notificationId);
    if (+code === +newCode) {
      const user = await loginWithPhoneApi({
        ...data,
        notificationId,
        device: Platform.OS
      });
      console.log('loginWithPhoneApi', user);
      if (user) {
        dispatch(
          dispatchSetUserData({
            user: user.data,
            authorization: user.headers.authorization
          })
        );
        INSTANCE.defaults.headers.common[
          'Authorization'
          ] = `Bearer ${user.headers.authorization}`;
      } else {
        throw Error('something went wrong');
      }
    } else {
      Alert.alert(
        'Incorrect OTP',
        'The OTP you typed is not correct',
        [{text: 'OK'}],
        {
          cancelable: false
        }
      );
    }
  } catch (error) {
    console.log('err while signin', error);

    Alert.alert(
      '',
      error.response ? error.response.data : error,
      [{text: 'OK'}],
      {
        cancelable: false
      }
    );
  } finally {
    cb();
  }
};

export const updateLocation = (position) => {
  return {
    type: USER_LOCATION,
    payload: position
  };
};

export const uploadImage = (source) => async (dispatch, getState) => {
  try {
    let {user} = getState().auth;
    if (!source) {
      dispatch(updateUser({...user, photoURI: ''}, () => {
      }));
      return;
    }
    let res = await cloudinaryImageUrl(source, () => {
    });
    let image = res.data.secure_url;
    dispatch(updateUser({...user, photoURI: image}, () => {
    }));
  } catch (error) {
    // console.log('error is here guys', error.response);
    alert(error);
  }
};

export const uploadImageSignup = (source, cb) => async (dispatch) => {
  try {
    let res = await cloudinaryImageUrl(source, () => {
    });
    let image = res.data.secure_url;
    cb(image);
  } catch (error) {
    cb();
    alert(error);
  }
};

export const getNotificationId = (id) => {
  return {
    type: 'NOTIFICATION_ID',
    payload: id
  };
};

export const changeDetect = () => {
  return {
    type: "AUTO_DETECT_LANG"
  };
};
